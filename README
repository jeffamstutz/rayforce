
----------------------------------------------------------------
  Rayforce:ospGraph - support for ospGraph ray tracing modules
----------------------------------------------------------------

This package contains source code for a version of the Rayforce
high-performance ray tracing libraries designed to support the
ospGraph graph-based ray tracing module for OSPRay 1.2.0.  Unless
otherwise stated directly in the source, this code is distributed
under the GNU Lesser General Public License, version 2.1 (see LICENSE
for more information).


----------------------------------------------


Building and installing Rayforce:ospGraph requires CMake v3.5.1 or
better (http://www.cmake.org).  Please ensure this dependency is
installed on your system before proceeeding.


----------------------------------------------


To build and install the Rayforce libraries:


  Step 1.)  Run the CMake GUI on the source directory:

    prompt> cd <path to Rayforce build directory>
    prompt> ccmake <path to Rayforce source directory>


  Step 2.)  Configure and generate the build:

    ccmake> Press 'c' to configure
    ccmake> Press 'c' to configure
    ccmake> Press 'g' to generate and exit


  Step 3.)  Run make to build and install the Rayforce libraries:

    prompt> make
    prompt> sudo make install


Unless otherwise specified during configuration, Rayforce components
are installed in '/usr/local':

  - Headers        --> /usr/local/include/Rayforce
  - Libraries      --> /usr/local/lib
  - CMake elements --> /usr/local/share/Rayforce


----------------------------------------------


Questions should be directed to:

  - Christiaan Gribble: christiaan.gribble@survice.com
