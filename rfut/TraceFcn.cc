
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <rfut/TraceFcn.hpp>


namespace rfut
{

  template<>
  TraceFcn<Target::System>::TraceFcn(Scene<Target::System>& scene,
                                     rfPipeline&            pipeline) :
    m_scene(scene)
  {
    m_tfcn = rfTracePath(m_scene.getRawPtr(), m_scene.m_handle, &pipeline);
    if (!m_tfcn)
      throw rfTracePathFailure();
  }


#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  template<>
  TraceFcn<Target::CUDA>::TraceFcn(      Context&              context,
                                         Device<Target::CUDA>& device,
                                         Scene<Target::CUDA>&  scene,
                                   const string&               filename,
                                         Kernel::Type          type) :
    m_scene(scene)
  {
    m_kernel = rfCudaCreateKernel(context.getRawPtr(), device.id(), type);
    if (!m_kernel)
      throw rfCudaCreateKernelFailure();

    if (!rfCudaLoadKernelFile(m_kernel, filename.c_str(), MaxRegCount))
      throw rfCudaLoadKernelFileFailure();
  }

  template<>
  TraceFcn<Target::CUDA>::~TraceFcn()
  {
    if (m_kernel)
      rfCudaDestroyKernel(m_kernel);
  }

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT

} // namespace rfut
