
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <iostream>

#include <rfut/CullMode.hpp>


namespace CullMode
{

  const string& to_string(const Type& t)
  {
    static string type;

    switch (t)
    {
    case None:
      type = "CullMode::None";
      break;
    case Back:
      type = "CullMode::Back";
      break;
    case Front:
      type = "CullMode::Front";
      break;
    case Unknown:
    default:
      type = "CullMode::Unknown";
      break;
    }

    return type;
  }

  ostream& operator<<(ostream& out, const Type& t)
  {
    switch (t)
    {
    case None:
      out << "CullMode::None";
      break;
    case Back:
      out << "CullMode::Back";
      break;
    case Front:
      out << "CullMode::Front";
      break;
    case Unknown:
    default:
      out << "CullMode::Unknown";
      break;
    }

    return out;
  }

} // namespace Target

