
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <exception>
#include <iosfwd>

#include <RF/rf.h>

#include <rfut/CullMode.hpp>
#include <rfut/rfInterface.hpp>
#include <rfut/Target.hpp>

namespace rfut
{

  using std::exception;
  using std::ostream;

  class Model;

  template<typename TargetT>
  class Scene;

  class Object : public rfInterface<rfObject>
  {
  public:

    class rfCreateObjectFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    template<typename TargetT>
    Object(Scene<TargetT>& scene,
           CullMode::Type cullmode) :
      m_cullmode(cullmode)
    {
      init(scene.getRawPtr());
    }

    template<typename TargetT>
    Object(Scene<TargetT>& scene,
           Model&         model,
           CullMode::Type cullmode) :
      m_cullmode(cullmode)
    {
      init(scene.getRawPtr());
      attach(model);
    }

    virtual ~Object();

    void attach(Model&);

    friend ostream& operator<<(ostream&, const Object&);

  private:

    void init(rfScene*);

    CullMode::Type m_cullmode;
  };

} // namespace rfut
