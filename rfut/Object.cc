
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <iostream>

#include <rfut/Model.hpp>
#include <rfut/Object.hpp>


namespace rfut
{

  const char* Object::rfCreateObjectFailure::what() const throw()
  {
    return "rfut::Object::rfCreateObjectFailure";
  }

  Object::~Object()
  {
    if (m_rawPtr)
      rfDestroyObject(m_rawPtr);
  }

  void Object::attach(Model& model)
  {
    rfAttachModel(m_rawPtr, model.getRawPtr());
    rfEnableObject(m_rawPtr);
  }

  void Object::init(rfScene* sRawPtr)
  {
    m_rawPtr = rfCreateObject(sRawPtr);
    if (!m_rawPtr)
      throw rfCreateObjectFailure();

    rfCullFace(m_rawPtr, m_cullmode);
  }

  ostream& operator<<(ostream& out, const Object& o)
  {
    out << "rfut::Object(" << o.m_cullmode << ")";
    return out;
  }

} // namespace rfut
