
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <exception>
#include <iostream>
#include <string>

#include <rfut/Context.hpp>
#include <rfut/Target.hpp>


namespace rfut
{
  using std::exception;
  using std::ostream;
  using std::string;

  template<typename TargetT>
  class Device : public TargetT::DeviceData
  {
  public:

    static const uint Undefined = static_cast<uint>(-1);

    class UnsupportedTarget : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    Device(Context& context, uint id = Device::Undefined) :
      TargetT::DeviceData(context, id)
    {
    }

    ~Device() = default;

    friend ostream& operator<<(ostream& out, const Device& device)
    {
      const string id = (device.m_id == Device::Undefined ? "" :
                         "(" + to_string(device.m_id) + ")");

      out << "rfut::Device<" << TargetT::Name << ">" << id;

      return out;
    }
  };

  // Inlined definitions //////////////////////////////////////////////////////

  template<typename TargetT>
  const char* Device<TargetT>::UnsupportedTarget::what() const throw()
  {
    static const string msg = "rfut::Device<" + TargetT::Name +
      ">::UnsupportedTarget";

    return msg.c_str();
  }

} // namespace rfut
