
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <iostream>

#include <rfut/ModelType.hpp>


namespace ModelType
{

  const string& to_string(const Type& t)
  {
    static string type;

    switch (t)
    {
    case Triangles:
      type = "ModelType::Triangles";
      break;
    case Unknown:
    default:
      type = "ModelType::Unknown";
      break;
    }

    return type;
  }

  ostream& operator<<(ostream& out, const Type& t)
  {
    switch (t)
    {
    case Triangles:
      out << "ModelType::Triangles";
      break;
    case Unknown:
    default:
      out << "ModelType::Unknown";
      break;
    }

    return out;
  }

} // namespace Target

