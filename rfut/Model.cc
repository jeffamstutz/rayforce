
/******************************************************************************
 * Copyright (c) 2013, 2017, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <iostream>

#include <rfut/Model.hpp>


namespace rfut
{

  const ModelLayout::Mode Model::DefaultLayoutMode  = ModelLayout::Full;
  const uint              Model::DefaultMemoryHint  = 128;
  const uint              Model::DefaultQualityHint = 128;

  const char* Model::rfCreateModelFailure::what() const throw()
  {
    static const string msg = "rfut::Model::rfCreateModelFailure";

    return msg.c_str();
  }

  const char* Model::rfModelCacheFileFailure::what() const throw()
  {
    static const string msg = "rfut::Model::rfModelCacheFileFailure";

    return msg.c_str();
  }

  const char* Model::rfGetModelCacheFailure::what() const throw()
  {
    static const string msg = "rfut::Model::rfGetModelCacheFailure";

    return msg.c_str();
  }

  const char* Model::GetCacheMemorySizeFailure::what() const throw()
  {
    static const string msg = "rfut::Model::GetCacheSizeMemorySizeFailure";

    return msg.c_str();
  }

  const char* Model::OpenCacheFileFailure::what() const throw()
  {
    static const string msg = "rfut::Model::OpenCacheFileFailure";

    return msg.c_str();
  }

  const char* Model::CacheFileTruncateFailure::what() const throw()
  {
    static const string msg = "rfut::Model::CacheFileTruncateFailure";

    return msg.c_str();
  }

  const char* Model::CacheFileMMapFailure::what() const throw()
  {
    static const string msg = "rfut::Model::CacheFileMMapFailure";

    return msg.c_str();
  }

  const char* Model::CacheFileSizeFailure::what() const throw()
  {
    static const string msg = "rfut::Model::CacheFileSizeFailure";

    return msg.c_str();
  }


  Model::~Model()
  {
    if (m_rawPtr)
      rfDestroyModel(m_rawPtr);
  }

  void Model::setData(uint   ntris,
                      float* vertices,
                      uint*  indices,
                      size_t psize,
                      size_t pstride,
                      void*  pdata,
                      size_t msize,
                      void*  mdata)
  {
    rfModelEnv(m_rawPtr, RF_BUILDHINT_LAYOUT,  m_lmode, 0);
    rfModelEnv(m_rawPtr, RF_BUILDHINT_MEMORY,  m_mhint, 0);
    rfModelEnv(m_rawPtr, RF_BUILDHINT_QUALITY, m_qhint, 0);

    rfVertexPointer(m_rawPtr, vertices, RF_FLOAT, 3*sizeof(float));
    rfIndexPointer( m_rawPtr, indices,  RF_UINT);

    if (pdata)
      rfPrimitiveData(m_rawPtr, pdata, psize, pstride);

    if (mdata)
      rfModelData(m_rawPtr, mdata, msize);

    rfReadyModel(m_rawPtr, ntris, 0);
    rfFinishModel(m_rawPtr);
  }

  size_t Model::getCacheMemory(void*& cache, size_t cachesize)
  {
    size_t nbytes;
    if (!rfGetModelCache(m_rawPtr, 0, &nbytes))
      throw rfGetModelCacheFailure();

    if (cachesize == 0)
    {
      cache     = reinterpret_cast<void*>(new char[nbytes]);
      cachesize = nbytes;
    }

    if (nbytes > cachesize)
      throw GetCacheMemorySizeFailure();

    if (!rfGetModelCache(m_rawPtr, cache, &nbytes))
      throw rfGetModelCacheFailure();

    return nbytes;
  }

  void Model::loadCacheFile(const string& filename)
  {
    // XXX(cpg) - biff!  const_cast is ugly...
    char* nonconst = const_cast<char*>(filename.c_str());
    if (!rfModelCacheFile(m_rawPtr, nonconst))
      throw rfModelCacheFileFailure();
  }

  void Model::saveCacheFile(const string& filename,
                            const void*   cacheIn,
                                  size_t  cachesize)
  {
    int fd = open(filename.c_str(), O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if (fd < 0)
      throw OpenCacheFileFailure();

    size_t nbytes = 0;
    if (!rfGetModelCache(m_rawPtr, 0, &nbytes))
      throw rfGetModelCacheFailure();

    if (ftruncate(fd, nbytes) != 0)
      throw CacheFileTruncateFailure();

    void* cache = mmap(0, nbytes, PROT_WRITE, MAP_SHARED, fd, 0);
    if (cache == MAP_FAILED)
    {
      close(fd);
      throw CacheFileMMapFailure();
    }

    if (cachesize > 0)
    {
      if (nbytes > cachesize)
      {
        munmap(cache, nbytes);
        close(fd);
        throw CacheFileSizeFailure();
      }

      memcpy(cache, cacheIn, nbytes);
    }
    else
    {
      if (!rfGetModelCache(m_rawPtr, cache, &nbytes))
      {
        munmap(cache, nbytes);
        close(fd);
        throw rfGetModelCacheFailure();
      }
    }

    // Success!
    munmap(cache, nbytes);
    close(fd);
  }

  ostream& operator<<(ostream& out, const Model& m)
  {
    out << "rfut::Model(" << m.m_mtype << ", " << m.m_lmode << ", "
        << m.m_mhint << ", " << m.m_qhint << ")";

    return out;
  }

} // namespace rfut
