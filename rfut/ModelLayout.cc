
/******************************************************************************
 * Copyright (c) 2017, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <iostream>

#include <rfut/ModelLayout.hpp>

namespace ModelLayout
{

  const string& to_string(const Mode& m)
  {
    static string mode;

    switch (m)
    {
    case None:
      mode = "ModelLayout::None";
      break;
    case Full:
    default:
      mode = "ModelLayout::Full";
      break;
    }

    return mode;
  }

  ostream& operator<<(ostream& out, const Mode& m)
  {
    switch (m)
    {
    case None:
      out << "ModelLayout::None";
      break;
    case Full:
    default:
      out << "ModelLayout::Full";
      break;
    }

    return out;
  }

} // namespace ModelLayout
