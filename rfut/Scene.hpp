
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <exception>
#include <ostream>
#include <string>

#include <RF/rf.h>

#include <rfut/Context.hpp>
#include <rfut/Device.hpp>
#include <rfut/rfInterface.hpp>
#include <rfut/Target.hpp>


namespace rfut
{

  using std::exception;
  using std::ostream;
  using std::string;

  template<typename TargetT>
  class TraceFcn;

  template<typename TargetT>
  class Scene : public rfInterface<rfScene>
  {
  public:

    class rfCreateSceneFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class Incomplete : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    Scene(Context& context, Device<TargetT>& device) :
      m_handle(0),
      m_deviceID(device.id())
    {
      m_rawPtr = rfCreateScene(context.getRawPtr(), TargetT::Enum, m_deviceID,
                               0);
      if (!m_rawPtr)
        throw rfCreateSceneFailure();

      // XXX(cpg) - expose me?
      rfSceneEnv(m_rawPtr, RF_SCENE_DATATYPE, RF_FLOAT, 0);
    }

    virtual ~Scene()
    {
      if (m_rawPtr)
        rfDestroyScene(m_rawPtr);
    }

    void acquire()
    {
      if (m_handle)
        return;

      m_handle = rfAcquireScene(m_rawPtr);
      if (!m_handle)
        throw Incomplete();
    }

    // XXX(cpg) - is there a clever way to avoid overloading 'resolve'?
    rfRoot resolve(float* origin)
    {
      return rfResolveOriginf(m_rawPtr, m_handle, origin);
    }

    rfRoot resolve(double* origin)
    {
      return rfResolveOrigind(m_rawPtr, m_handle, origin);
    }

    // NOTE(cpg) - I don't like this approach, but TraceFcn<TargetT> requires
    //             the Scene<TargetT>'s handle and is (currently) the only
    //             other rfut object that does...
    friend class TraceFcn<TargetT>;

    friend ostream& operator<<(ostream& out, const Scene& /* unused */)
    {
      out << "rfut::Scene<" << TargetT::Name << ">";
      return out;
    }

    ///////////////////////////////////////////////////////////////////////////
    // XXX(cpg) - CPU pipelines that recursively invoke other CPU
    //            pipelines (e.g., AO pipelines in rtVTK::rfRenderer)
    //            currently require access to m_handle to initialize
    //            the corresponding member of a TRACEPARAM data type
    //
    //            The near-term fix is to enable C++ CPU pipelines, in
    //            which case both rfut::TraceFcn<TargetT> and
    //            rfut::Scene<TargetT> objects can be used directly in
    //            pipeline code: clients will simply pass these
    //            objects via corresponding TRACEPARAM members and
    //            manipulate them within the pipeline
    //
    //            A longer-term, potentially more flexible (but also
    //            more complicated) fix involves a new rfut object:
    //
    //              template<typename UserDataT>
    //              class TraceData : public UserDataT
    //              {
    //                ...
    //              };
    //
    //            that is used as 'D' in TraceFcn<TargetT>::operator(...).
    //            in this case, rfut::TraceData maintains members
    //            necessary to deal with recursive CPU pipelines, and
    //            the UserDataT template parameter adds data members
    //            actually required by a user's pipeline.
    //            TraceFcn<TargetT>::operator(...) could then strip out
    //            C-style data for downstream pipelines, or
    //            TraceData<UserDataT> could be passed C++ pipelines
    //            directly (as above)
    //
    //            The near- and longer-term solutions are not mutually
    //            exclusive:  'near-term' provides a relatively quick
    //            fix and extends rfut with true C++ support in CPU
    //            pipelines, while 'longer-term' alleviates some of
    //            the burden of TRACEPARAM management from the user
    //            and allow rfut to repackage C++ data structures for
    //            C-style pipelines.  These are all desirable features
    //            for rfut, so...  Now we just need to implement them.

    rfHandle* m_handle;

  private:
    uint      m_deviceID;
    // rfHandle* m_handle;
  };

  template<typename TargetT>
  const char* Scene<TargetT>::rfCreateSceneFailure::what() const throw()
  {
    static const string msg = "rfut::Scene<" + TargetT::Name +
      ">::rfCreateSceneFailure";

    return msg.c_str();
  }

  template<typename TargetT>
  const char* Scene<TargetT>::Incomplete::what() const throw()
  {
    static const string msg = "rfut::Scene<" + TargetT::Name +
      ">::Incomplete";

    return msg.c_str();
  }

} // namespace rfut
