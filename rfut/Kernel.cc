
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <iostream>

#include <rfut/Kernel.hpp>


namespace Kernel
{

  const string& to_string(const Type& t)
  {
    static string type;

    switch (t)
    {
    case Trace:
      type = "Kernel::Trace";
      break;
    case Quad:
      type = "Kernel::Quad";
      break;
    case Resolve:
      type = "Kernel::Resolve";
      break;
    case Unknown:
    default:
      type = "Kernel::Unknown";
      break;
    }

    return type;
  }

  ostream& operator<<(ostream& out, const Type& t)
  {
    switch (t)
    {
    case Trace:
      out << "Kernel::Trace";
      break;
    case Quad:
      out << "Kernel::Quad";
      break;
    case Resolve:
      out << "Kernel::Resolve";
      break;
    case Unknown:
    default:
      out << "Kernel::Unknown";
      break;
    }

    return out;
  }

} // namespace Kernel
