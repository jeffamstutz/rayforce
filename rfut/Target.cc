
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <stdexcept>

#include <rfut/Device.hpp>
#include <rfut/Target.hpp>


namespace Target
{

  const string System::Name = "Target::System";
  const uint   System::Enum = RF_TARGET_SYSTEM;

  const uint System::DeviceData::m_id = rfut::Device<Target::System>::Undefined;

  const string NUMA::Name = "Target::NUMA";
  const uint   NUMA::Enum = RF_TARGET_NUMA;

#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  const string CUDA::Name = "Target::CUDA";
  const uint   CUDA::Enum = RF_TARGET_CUDA;

        uint CUDA::DeviceData::m_nextID = 0;

  const uint CUDA::TraceFcnData::ThreadsPerBlock = 128;
  const uint CUDA::TraceFcnData::MaxRegCount     = 0;

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT

  const string OpenCL::Name = "Target::OpenCL";
  const uint   OpenCL::Enum = RF_TARGET_OPENCL;

  const string Unknown::Name = "Target::Uknown";
  const uint   Unknown::Enum = static_cast<uint>(-1);

  System::DeviceData::DeviceData(Context&, uint)
  {
    // no-op
  }

  uint System::DeviceData::id() const
  {
    return m_id;
  }

  System::TraceFcnData::TraceFcnData() :
    m_tfcn(0)
  {
    // no-op
  }

#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  const char* CUDA::DeviceData::Properties::rfCudaQueryDeviceFailure::what() const throw()
  {
    static const string msg = "rfut::CUDA::Device::Properties::rfCudaQueryDeviceFailure";

    return msg.c_str();
  }

  const char* CUDA::DeviceData::Properties::PropertyNotFound::what() const throw()
  {
    static const string msg = "rfut::CUDA::Device::Properties::PropertyNotFound";

    return msg.c_str();
  }

  CUDA::DeviceData::Properties::Properties(Context& context, uint id)
  {
    rfCudaDeviceProp* rfProps = rfCudaQueryDevice(context.getRawPtr(), id);
    if (!rfProps)
      throw rfCudaQueryDeviceFailure();

    m_props["Name"]                = to_string(rfProps->name);
    m_props["ComputeMajor"]        = to_string(rfProps->computemajor);
    m_props["ComputeMinor"]        = to_string(rfProps->computeminor);
    m_props["Memory"]              = to_string(rfProps->memory);
    m_props["WarpSize"]            = to_string(rfProps->warpsize);
    m_props["MaxThreadPerBlock"]   = to_string(rfProps->maxblockthreads);
    m_props["MaxBlockSize"]        =
      to_string(rfProps->maxblockdim[0]) + " x " +
      to_string(rfProps->maxblockdim[1]) + " x " +
      to_string(rfProps->maxblockdim[2]);
    m_props["MaxGridSize"]         =
      to_string(rfProps->maxgriddim[0]) + " x " +
      to_string(rfProps->maxgriddim[1]) + " x " +
      to_string(rfProps->maxgriddim[2]);
    m_props["MaxRegsPerBlock"]     = to_string(rfProps->maxblockregs);
    m_props["ClockRate"]           = (rfProps->clockrate != -1 ?
                                      to_string(rfProps->clockrate) :
                                      "(unknown)");
    m_props["MultiprocessorCount"] = (rfProps->multiprocessorcount != -1 ?
                                      to_string(rfProps->multiprocessorcount) :
                                      "(unknown)");
    m_props["TotalCoreCount"]      = (rfProps->corestotal != -1 ?
                                      to_string(rfProps->corestotal) :
                                      "(unknown)");
    m_props["CacheSize"]           = (rfProps->cachesize != -1 ?
                                      to_string(rfProps->cachesize) :
                                      "(unknown)");
    m_props["ConcurrentKernels"]   = (rfProps->concurrentkernels == 1 ? "Yes" :
                                      "No");
    m_props["RuntimeLimit"]        = (rfProps->runtimelimit == 1 ? "Yes" :
                                      "No");
    m_props["Tex1DLinearWidth"]    = (rfProps->tex1dlinearwidth != -1 ?
                                      to_string(rfProps->tex1dlinearwidth) :
                                      "(unknown)");
  }

  CUDA::DeviceData::Properties::iterator CUDA::DeviceData::Properties::begin()
  {
    return m_props.begin();
  }

  CUDA::DeviceData::Properties::const_iterator CUDA::DeviceData::Properties::begin() const
  {
    return m_props.begin();
  }

  CUDA::DeviceData::Properties::iterator CUDA::DeviceData::Properties::end()
  {
    return m_props.end();
  }

  CUDA::DeviceData::Properties::const_iterator CUDA::DeviceData::Properties::end() const
  {
    return m_props.end();
  }

  const string& CUDA::DeviceData::Properties::operator[](const string& key) const
  {
    try
    {
      return m_props.at(key);
    }
    catch (std::out_of_range& /* unused */)
    {
      throw PropertyNotFound();
    }
  }

  const char* CUDA::DeviceData::InvalidDeviceID::what() const throw()
  {
    static const string msg = "rfut::CUDA::Device::InvalidDeviceID";

    return msg.c_str();
  }

  uint CUDA::DeviceData::count(Context& context)
  {
    return rfCudaDeviceCount(context.getRawPtr());
  }

  CUDA::DeviceData::DeviceData(Context& context, uint id) :
    m_context(context.getRawPtr()),
    m_id(id == rfut::Device<Target::CUDA>::Undefined ? m_nextID++ : id),
    m_props(context, m_id)
  {
    if (m_id > count(context))
      throw InvalidDeviceID();
  }

  uint CUDA::DeviceData::id() const
  {
    return m_id;
  }

  const CUDA::DeviceData::Properties& CUDA::DeviceData::query() const
  {
    return m_props;
  }

  const string& CUDA::DeviceData::query(const string& key) const
  {
    return m_props[key];
  }

  void CUDA::DeviceData::synchronize() const
  {
    rfCudaSynchronizeDevice(m_context, m_id);
  }

  CUDA::TraceFcnData::TraceFcnData() :
    m_kernel(0)
  {
    // no-op
  }

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT

} // namespace Target
