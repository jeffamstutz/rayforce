
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <rfut/Context.hpp>


namespace rfut
{

  const char* Context::rfCreateContextFailure::what() const throw()
  {
    return "rfut::Context::rfCreateContextFailure";
  }

  Context::Context()
  {
    m_rawPtr = rfCreateContext();
    if (!m_rawPtr)
      throw rfCreateContextFailure();
  }

  Context::~Context()
  {
    if (m_rawPtr)
      rfDestroyContext(m_rawPtr);
  }

} // namespace rfut
