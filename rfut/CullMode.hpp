
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <iosfwd>
#include <string>

#include <RF/rf.h>


namespace CullMode
{
  using std::ostream;
  using std::string;

  enum Type
    {
      None  = RF_CULLMODE_NONE,
      Back  = RF_CULLMODE_BACK,
      Front = RF_CULLMODE_FRONT,
      Unknown
    };

  const string& to_string(const Type&);

  ostream& operator<<(ostream&, const Type&);

} // namespace CullMode
