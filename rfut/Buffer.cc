
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#include <rfut/Buffer.hpp>


namespace rfut
{

  /////////////////////////////////////////////////////////////////////////////
  // Full specialization definitions - Buffer<Target::System>

  template<>
  void Buffer<Target::System>::allocate(size_t nbytes)
  {
    m_rawPtr = malloc(nbytes);
    if (!m_rawPtr)
      throw BadAlloc();

    m_nbytes = nbytes;
  }

  template<>
  void Buffer<Target::System>::release()
  {
    free(m_rawPtr);

    m_rawPtr = 0;
    m_nbytes = 0;
  }

  template<>
  void Buffer<Target::System>::clear()
  {
    memset(m_rawPtr, 0, m_nbytes);
  }

  // System --> System transfer
  template<>
    template<>
  void Buffer<Target::System>::copy(Buffer<Target::System>& src, size_t nbytes)
  {
    nbytes = (nbytes == 0 ? m_nbytes : (nbytes > m_nbytes ? m_nbytes : nbytes));
    memcpy(m_rawPtr, src.getRawPtr(), nbytes);
  }

  // System --> System transfer
  template<>
  void Buffer<Target::System>::copy(void* dst, size_t nbytes)
  {
    nbytes = (nbytes == 0 ? m_nbytes : (nbytes > m_nbytes ? m_nbytes : nbytes));
    memcpy(dst, m_rawPtr, nbytes);
  }

#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  // CUDA --> System transfer
  template<>
    template<>
  void Buffer<Target::System>::copy(Buffer<Target::CUDA>& src, size_t nbytes)
  {
    nbytes = (nbytes == 0 ? m_nbytes : (nbytes > m_nbytes ? m_nbytes : nbytes));
    rfCudaCopyDtoH(src.m_context, src.m_deviceID, 0, m_rawPtr, src.getRawPtr(),
                   nbytes);
  }


  /////////////////////////////////////////////////////////////////////////////
  // Full specialization definitions - Buffer<Target::CUDA>

  template<>
  void Buffer<Target::CUDA>::allocate(size_t nbytes)
  {
    if (nbytes <= 0)
      throw InvalidSize();

    if (!rfCudaAlloc(m_context, m_deviceID, &m_rawPtr, nbytes))
      throw BadAlloc();

    m_nbytes = nbytes;
  }

  template<>
  void Buffer<Target::CUDA>::release()
  {
    if (m_rawPtr)
      rfCudaFree(m_context, m_deviceID, m_rawPtr, m_nbytes);

    m_rawPtr = 0;
    m_nbytes = 0;
  }

  template<>
  void Buffer<Target::CUDA>::clear()
  {
    char* zeros = new char[m_nbytes];

    memset(zeros, 0, m_nbytes);
    setData(zeros, m_nbytes);

    delete [] zeros;
  }

  // System --> CUDA transfer
  template<>
    template<>
  void Buffer<Target::CUDA>::copy(Buffer<Target::System>& src, size_t nbytes)
  {
    nbytes = (nbytes == 0 ? m_nbytes : (nbytes > m_nbytes ? m_nbytes : nbytes));
    setData(src.getRawPtr(), nbytes);
  }

  // CUDA --> System transfer
  template<>
  void Buffer<Target::CUDA>::copy(void* dst, size_t nbytes)
  {
    nbytes = (nbytes == 0 ? m_nbytes : (nbytes > m_nbytes ? m_nbytes : nbytes));
    rfCudaCopyDtoH(m_context, m_deviceID, 0, dst, m_rawPtr, nbytes);
  }

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT

} // namespace rfut
