
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */


#pragma once

#include <exception>

#include <RF/rf.h>

#include <rfut/rfInterface.hpp>

namespace rfut
{
  using std::exception;

  class Context : public rfInterface<rfContext>
  {
  public:

    class rfCreateContextFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    Context();
    virtual ~Context();
  };

} // namespace rfut
