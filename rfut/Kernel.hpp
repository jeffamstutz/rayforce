
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <iosfwd>
#include <string>

#include <RF/rf.h>


namespace Kernel
{
  using std::ostream;
  using std::string;

  enum Type
    {
      Trace   = RF_CUDA_KERNEL_TRACE,
      Quad    = RF_CUDA_KERNEL_QUADTRACE,
      Resolve = RF_CUDA_KERNEL_RESOLVE,
      Unknown
    };

  const string& to_string(const Type&);

  ostream& operator<<(ostream&, const Type&);

} // namespace Kernel
