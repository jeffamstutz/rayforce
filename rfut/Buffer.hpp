
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <cstdlib>
#include <cstring>
#include <exception>
#include <iostream>

#include <RF/rf.h>

#include <rfut/Context.hpp>
#include <rfut/Device.hpp>
#include <rfut/rfInterface.hpp>
#include <rfut/Target.hpp>
#include <rfut/Types.hpp>


namespace rfut
{
  using std::exception;
  using std::ostream;
  using std::string;

  template<typename TargetT>
  class Buffer : public rfInterface<void>
  {
  public:

    class BadAlloc : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class InvalidSize : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class UnsupportedTarget : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    template<typename OtherTargetT>
    Buffer(Context&              context,
           Device<OtherTargetT>& device,
           size_t                nbytes = 0) :
      m_context(context.getRawPtr()),
      m_deviceID(device.id()),
      m_nbytes(nbytes)
    {
      resize(m_nbytes);
    }

    Buffer(Context&         context,
           Device<TargetT>& device,
           size_t           nbytes = 0) :
      m_context(context.getRawPtr()),
      m_deviceID(device.id()),
      m_nbytes(nbytes)
    {
      resize(m_nbytes);
    }

    virtual ~Buffer()
    {
      release();
    }

    // Memory management //

    void allocate(size_t nbytes)
    {
      throw UnsupportedTarget();
    }

    void release()
    {
      throw UnsupportedTarget();
    }

    void resize(size_t nbytes)
    {
      release();

      if (nbytes > 0)
        allocate(nbytes);
    }

    size_t size()
    {
      return m_nbytes;
    }

    // Data manipulation //

    template<typename T>
    T* getData()
    {
      throw UnsupportedTarget();
    }

    template<typename T>
    void setData(T*, size_t)
    {
      throw UnsupportedTarget();
    }

    void clear()
    {
      throw UnsupportedTarget();
    }

    void copy(Buffer&, size_t = 0)
    {
      throw UnsupportedTarget();
    }

    template<typename OtherTargetT>
    void copy(Buffer<OtherTargetT>&, size_t = 0)
    {
      throw UnsupportedTarget();
    }

    void copy(void*, size_t = 0)
    {
      throw UnsupportedTarget();
    }

    template<typename OtherTargetT>
    friend class Buffer;

    friend ostream& operator<<(ostream& out, const Buffer& buffer)
    {
      out << "rfut::Buffer<" << TargetT::Name << ">(" << buffer.m_nbytes << ")";
      return out;
    }

  private:
    rfContext* m_context;
    uint       m_deviceID;
    size_t     m_nbytes;

    // Disallow copy construction and assigment
    Buffer(const Buffer&);
    Buffer& operator=(const Buffer&);
  };


  /////////////////////////////////////////////////////////////////////////////
  // Full specialization declarations - Buffer<Target::System>

  template<>
  void Buffer<Target::System>::allocate(size_t);

  template<>
  void Buffer<Target::System>::release();

  template<>
  void Buffer<Target::System>::clear();

  // System --> System transfer
  template<>
    template<>
  void Buffer<Target::System>::copy(Buffer<Target::System>&, size_t);

  // System --> System transfer
  template<>
  void Buffer<Target::System>::copy(void*, size_t);

#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  // CUDA --> System transfer
  template<>
    template<>
  void Buffer<Target::System>::copy(Buffer<Target::CUDA>&, size_t);


#endif // RAYFORCE_CONFIG_CUDA_SUPPORT


  /////////////////////////////////////////////////////////////////////////////
  // Partial specialization definitions - Buffer<Target::System>

  template<>
    template<typename T>
  T* Buffer<Target::System>::getData()
  {
    return reinterpret_cast<T*>(m_rawPtr);
  }

  template<>
    template<typename T>
  void Buffer<Target::System>::setData(T* src, size_t nbytes)
  {
    if (nbytes <= 0 || nbytes > m_nbytes)
      throw InvalidSize();

    memcpy(m_rawPtr, reinterpret_cast<void*>(src), nbytes);
  }


#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  /////////////////////////////////////////////////////////////////////////////
  // Full specialization declarations - Buffer<Target::CUDA>

  template<>
  void Buffer<Target::CUDA>::allocate(size_t);
  template<>
  void Buffer<Target::CUDA>::release();

  template<>
  void Buffer<Target::CUDA>::clear();

  // System --> CUDA transfer
  template<>
    template<>
  void Buffer<Target::CUDA>::copy(Buffer<Target::System>&, size_t);


  /////////////////////////////////////////////////////////////////////////////
  // Partial specialization definitions - Buffer<Target::CUDA>

  template<>
    template<typename T>
  void Buffer<Target::CUDA>::setData(T* src, size_t nbytes)
  {
    if (nbytes <= 0 || nbytes > m_nbytes)
      throw InvalidSize();

    void* mem = reinterpret_cast<void*>(src);
    rfCudaCopyHtoD(m_context, m_deviceID, 0, m_rawPtr, mem, nbytes);
  }

#ifndef __CUDACC__
#  define __host__
#  define __device__
#endif

  template<>
    template<typename T>
  __host__ __device__ T* Buffer<Target::CUDA>::getData()
  {
    return reinterpret_cast<T*>(m_rawPtr);
  }

#ifndef __CUDACC__
#  undef __host__
#  undef __device__
#endif

  // CUDA --> System transfer
  template<>
  void Buffer<Target::CUDA>::copy(void* dst, size_t nbytes);

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT


  /////////////////////////////////////////////////////////////////////////////
  // Exception class member function definitions

  template<typename Target>
  const char* Buffer<Target>::BadAlloc::what() const throw()
  {
    static const string msg = "rfut::Buffer<" + Target::Name + ">::BadAlloc";

    return msg.c_str();
  }

  template<typename Target>
  const char* Buffer<Target>::InvalidSize::what() const throw()
  {
    static const string msg = "rfut::Buffer<" + Target::Name +
      ">::InvalidSize";

    return msg.c_str();
  }

  template<typename Target>
  const char* Buffer<Target>::UnsupportedTarget::what() const throw()
  {
    static const string msg = "rfut::Buffer<" + Target::Name +
      ">::UnsupportedTarget";

    return msg.c_str();
  }

} // namespace rfut
