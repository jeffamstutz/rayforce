
/******************************************************************************
 * Copyright (c) 2013, 2017, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <exception>
#include <iosfwd>
#include <string>

#include <RF/rf.h>

#include <rfut/ModelLayout.hpp>
#include <rfut/ModelType.hpp>
#include <rfut/rfInterface.hpp>
#include <rfut/Target.hpp>
#include <rfut/Types.hpp>


namespace rfut
{
  using std::exception;
  using std::ostream;
  using std::string;

  template<typename TargetT>
  class Scene;

  class Model : public rfInterface<rfModel>
  {
  public:

    static const ModelLayout::Mode DefaultLayoutMode;
    static const uint              DefaultMemoryHint;
    static const uint              DefaultQualityHint;

    class rfCreateModelFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class rfModelCacheFileFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class rfGetModelCacheFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class GetCacheMemorySizeFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class OpenCacheFileFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class CacheFileTruncateFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class CacheFileMMapFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class CacheFileSizeFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    template<typename TargetT>
    Model(Scene<TargetT>&   scene,
          ModelType::Type   mtype,
          ModelLayout::Mode lmode = DefaultLayoutMode,
          uint              mhint = DefaultMemoryHint,
          uint              qhint = DefaultQualityHint) :
      m_mtype(mtype),
      m_lmode(lmode),
      m_mhint(mhint),
      m_qhint(qhint)
    {
      m_rawPtr = rfCreateModel(scene.getRawPtr(), m_mtype);
      if (!m_rawPtr)
        throw rfCreateModelFailure();
    }

    virtual ~Model();

    void setData(uint   ntris,
                 float* vertices,
                 uint*  indices,
                 size_t psize   = 0,
                 size_t pstride = 0,
                 void*  pdata   = 0,
                 size_t msize   = 0,
                 void*  mdata   = 0);

    size_t getCacheMemory(void*& cache,
                          size_t cachesize = 0);

    void loadCacheFile(const string& filename);
    void saveCacheFile(const string& filename,
                       const void*   cacheIn   = 0,
                             size_t  cachesize = 0);

    friend ostream& operator<<(ostream&, const Model&);

  private:
    ModelType::Type   m_mtype;
    ModelLayout::Mode m_lmode;

    uint m_mhint;
    uint m_qhint;
  };

} // namespace rfut
