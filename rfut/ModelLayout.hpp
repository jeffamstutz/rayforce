
/******************************************************************************
 * Copyright (c) 2017, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <iosfwd>
#include <string>

#include <RF/rf.h>


namespace ModelLayout
{

  using std::ostream;
  using std::string;

  enum Mode
    {
      None = RF_BUILDHINT_LAYOUTMODE_NONE,
      Full = RF_BUILDHINT_LAYOUTMODE_FULL
    };

  const string& to_string(const Mode&);

  ostream& operator<<(ostream&, const Mode&);

} // namespace ModelLayout
