
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

namespace rfut
{

  template<typename T>
  class rfInterface
  {
  public:
    rfInterface() :
      m_rawPtr(0)
    {
      // no-op
    }

    virtual T* getRawPtr() { return m_rawPtr; }

  protected:
    T* m_rawPtr;

  private:
    // none
  };

} // namespace rfut
