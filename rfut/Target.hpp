
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <exception>
#include <iostream>
#include <map>
#include <string>

#include <RF/rf.h>

#include <rfut/Context.hpp>
#include <rfut/Types.hpp>


namespace Target
{

  using std::exception;
  using std::map;
  using std::ostream;
  using std::string;

  using rfut::Context;

  class System
  {
  public:
    static const string Name;
    static const uint   Enum;

    struct DeviceData
    {
      static const uint m_id;

      DeviceData(Context&, uint);

      uint id() const;
    };

    struct TraceFcnData
    {
      TraceFcnData();

      void* m_tfcn;
    };
  };

  class NUMA
  {
  public:
    static const string Name;
    static const uint   Enum;

    struct DeviceData
    {
      // none
    };

    struct TraceFcnData
    {
      // none
    };
  };

#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  class CUDA
  {
  public:
    static const string Name;
    static const uint   Enum;

    struct DeviceData
    {

      static uint count(Context&);

      class InvalidDeviceID : public exception
      {
      public:
        virtual const char* what() const throw();
      };

      class Properties
      {
      public:
        class rfCudaQueryDeviceFailure : public exception
        {
        public:
          virtual const char* what() const throw();
        };

        class PropertyNotFound : public exception
        {
        public:
          virtual const char* what() const throw();
        };

        typedef map<string, string>::iterator       iterator;
        typedef map<string, string>::const_iterator const_iterator;

        Properties(Context&, uint);

              iterator begin();
        const_iterator begin() const;

              iterator end();
        const_iterator end() const;

        const string& operator[](const string&) const;

      private:
        map<string, string> m_props;
      };

      DeviceData(Context&, uint);

      uint id() const;

      const Properties& query()              const;
      const string&     query(const string&) const;

      void synchronize() const;

      rfContext* m_context;
      uint       m_id;
      Properties m_props;

    private:
      static uint m_nextID;
    };

    // NOTE(cpg) - enables clients to specify "Target::CUDA::Device" rather
    //             than "Target::CUDA::DeviceData" when naming members
    //             (e.g., DeviceData::Properties)
    typedef DeviceData Device;

    struct TraceFcnData
    {
      static const uint ThreadsPerBlock;
      static const uint MaxRegCount;

      TraceFcnData();

      rfCudaKernel* m_kernel;
    };
  };

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT

  class OpenCL
  {
  public:
    static const string Name;
    static const uint   Enum;

    struct DeviceData
    {
      // none
    };

    struct TraceFcnData
    {
      // none
    };
  };

  class Unknown
  {
  public:
    static const string Name;
    static const uint   Enum;

    struct DeviceData
    {
      // none
    };

    struct TraceFcnData
    {
      // none
    };
  };

  // NOTE(cpg) - make this function template inaccessible outside of the Target
  //             namespace by defining it inside a nested unnnamed namespace

  namespace /* unnamed */
  {

    template<typename TargetT>
    ostream& operator<<(ostream& out, const TargetT& /* unused */)
    {
      out << TargetT::Name;
      return out;
    }

  } // namespace /* unnamed */

} // namespace Target
