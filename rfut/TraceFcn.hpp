
/******************************************************************************
 * Copyright (c) 2013, SURVICE Engineering Company
 * All rights reserved.
 *
 * The U.S. Government has unrestricted/unlimited rights to this
 * software and associated documentation per DFARS 252.227-7014 and
 * DFARS 252.227-7013.
 *
 */

#pragma once

#include <RF/rf.h>

#include <rfut/Device.hpp>
#include <rfut/Kernel.hpp>
#include <rfut/Scene.hpp>
#include <rfut/Target.hpp>


namespace rfut
{

  using std::exception;
  using std::ostream;
  using std::string;

  template<typename TargetT>
  class TraceFcn : public TargetT::TraceFcnData, rfInterface<void>
  {
  public:

    class rfTracePathFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

    class rfCudaCreateKernelFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class rfCudaLoadKernelFileFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    class rfCudaLaunchKernelFailure : public exception
    {
    public:
      virtual const char* what() const throw();
    };

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT

    class UnsupportedTarget : public exception
    {
    public:
      virtual const char* what() const throw();
    };

    TraceFcn(Scene<TargetT>&, rfPipeline&) :
      m_scene(0)
    {
      throw UnsupportedTarget();
    }

    TraceFcn(      Context&,
                   Device<TargetT>&,
                   Scene<TargetT>&,
             const string&,
                   Kernel::Type) :
      m_scene(0)
    {
      throw UnsupportedTarget();
    }

    virtual ~TraceFcn() = default;

    template<typename R, typename D>
    void operator()(R&, D&)
    {
      throw UnsupportedTarget();
    }

    template<typename D>
    void operator()(uint, uint, D&)
    {
      throw UnsupportedTarget();
    }

    friend ostream& operator<<(ostream& out, const TraceFcn& /* unused */)
    {
      out << "rfut::TraceFcn<" << TargetT::Name << ">";
      return out;
    }

  private:
    Scene<TargetT>& m_scene;

    // NOTE(cpg) - members specific to trace function target are inherited
    //             via TargetT::TraceFcnData
  };


  /////////////////////////////////////////////////////////////////////////////
  // Full specialization declarations - TraceFcn<Target::System>

  template<>
  TraceFcn<Target::System>::TraceFcn(Scene<Target::System>&, rfPipeline&);


  /////////////////////////////////////////////////////////////////////////////
  // Partial specialization definitions - TraceFcn<Target::System>

  template<>
    template<typename R, typename D>
  void TraceFcn<Target::System>::operator()(R& ray, D& data)
  {
    typedef void (*TFcn)(void*, D*, R*);
    (reinterpret_cast<TFcn>(m_tfcn))(m_scene.m_handle, &data, &ray);
  }


#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  /////////////////////////////////////////////////////////////////////////////
  // Full specialization declarations - TraceFcn<Target::CUDA>

  template<>
  TraceFcn<Target::CUDA>::TraceFcn(      Context&,
                                         Device<Target::CUDA>&,
                                         Scene<Target::CUDA>&,
                                   const string&,
                                         Kernel::Type);

  template<>
  TraceFcn<Target::CUDA>::~TraceFcn();


  /////////////////////////////////////////////////////////////////////////////
  // Partial specialization definitions - TraceFcn<Target::CUDA>

  template<>
    template<typename D>
  void TraceFcn<Target::CUDA>::operator()(uint nx, uint ny, D& data)
  {
    if (!rfCudaLaunchKernel(m_scene.getRawPtr(), m_scene.m_handle, m_kernel,
                            0 /* rfCudaStream* */, nx, ny, ThreadsPerBlock,
                            &data))
      throw rfCudaLaunchKernelFailure();
  }

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT

  /////////////////////////////////////////////////////////////////////////////
  // Exception class member function definitions

  template<typename TargetT>
  const char* TraceFcn<TargetT>::rfTracePathFailure::what() const throw()
  {
    static const string msg = "rfut::TraceFcn<" + TargetT::Name +
      ">::rfTracePathFailure";

    return msg.c_str();
  }

#ifdef RAYFORCE_CONFIG_CUDA_SUPPORT

  template<typename TargetT>
  const char* TraceFcn<TargetT>::rfCudaCreateKernelFailure::what() const throw()
  {
    static const string msg = "rfut::TraceFcn<" + TargetT::Name +
      ">::rfCudaCreateKernelFailure";

    return msg.c_str();
  }

  template<typename TargetT>
  const char* TraceFcn<TargetT>::rfCudaLoadKernelFileFailure::what() const throw()
  {
    static const string msg = "rfut::TraceFcn<" + TargetT::Name +
      ">::rfCudaLoadKernelFileFailure";

    return msg.c_str();
  }

  template<typename TargetT>
  const char* TraceFcn<TargetT>::rfCudaLaunchKernelFailure::what() const throw()
  {
    static const string msg = "rfut::TraceFcn<" + TargetT::Name +
      ">::rfCudaLaunchKernelFailure";

    return msg.c_str();
  }

#endif // RAYFORCE_CONFIG_CUDA_SUPPORT

  template<typename TargetT>
  const char* TraceFcn<TargetT>::UnsupportedTarget::what() const throw()
  {
    static const string msg = "rfut::TraceFcn<" + TargetT::Name +
      ">::UnsupportedTarget";

    return msg.c_str();
  }

} // namespace rfut
