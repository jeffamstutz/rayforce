
/******************************************************************************
 * Copyright (c) 2012, 2013, Christiaan Gribble <cgribble[]rtvtk org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 *  * Neither the name of the author nor the names of its contributors
 *    may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


/* NOTE(cpg) - returns maximum compute major on success, 0 on failure */

#include <stdio.h>
#include <cuda_runtime.h>

int main()
{
  int cnt;
  int dev;
  int max = 0;
  cudaError_t rc;
  struct cudaDeviceProp prop;

  rc = cudaGetDeviceCount(&cnt);
  if (rc != cudaSuccess)
  {
    if (rc == cudaErrorNoDevice)
      printf("Unable to find any CUDA-capable devices!\n");
    else if (rc == cudaErrorInsufficientDriver)
      printf("Unable to load CUDA driver!\n");
    else
      printf("Unknown error:  %x\n", rc);

    return 0;
  }

  for (dev = 0; dev < cnt; ++dev)
  {
    rc = cudaGetDeviceProperties(&prop, dev);
    if (rc == cudaSuccess)
    {
      printf("sm_%d%d;", prop.major, prop.minor);
      max = (prop.major > max ? prop.major : max);
    }
  }

  if (max == 0)
  {
    printf("Unable to find any CUDA-capable devices!\n");
    return 0;
  }

  return max;
}
