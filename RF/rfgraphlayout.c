/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "cpuconfig.h"

#include "cc.h"
#include "ccspacecurve.h"
#include "mm.h"
#include "mmdirectory.h"

#include "rfmath.h"

#include "rf.h"
#include "rfdefs.h"
#include "rfdata.h"
#include "rfinternal.h"
#include "rfgraph.h"
#include "rfgraphbuild.h"


////


#define LAYOUT_SPACECURVE_BITS (10)

/* Using PREP_TRIREF_REORDERING in rfgraphprep.c is recommended instead,
 * it provides slightly better results.
 */
#define LAYOUT_TRIREF_REORDERING (0)


#define LAYOUT_EXPERIMENTAL_PACKING (0)

#define LAYOUT_EXPERIMENTAL_PACKING_RADIXBITS (10)


////


#ifdef CPUCONF_CAP_SSE
 #ifndef __SSE__
  #warning SSE does not appear to be naturally enabled, forcing it.
  #define __SSE__ 1
 #endif
 #include <xmmintrin.h>
 #define LAYOUT_SSE_AVAILABLE (1)
#endif


#define LAYOUT_SPACECURVE_TOTALBITS (RF_AXIS_COUNT*LAYOUT_SPACECURVE_BITS)

#if LAYOUT_SPACECURVE_TOTALBITS < 24
 #define LAYOUT_SPACECURVE_RADIXBITS ((LAYOUT_SPACECURVE_TOTALBITS+1)/2)
#elif LAYOUT_SPACECURVE_TOTALBITS < 36
 #define LAYOUT_SPACECURVE_RADIXBITS ((LAYOUT_SPACECURVE_TOTALBITS+2)/3)
#elif LAYOUT_SPACECURVE_TOTALBITS < 48
 #define LAYOUT_SPACECURVE_RADIXBITS ((LAYOUT_SPACECURVE_TOTALBITS+3)/4)
#elif LAYOUT_SPACECURVE_TOTALBITS < 60
 #define LAYOUT_SPACECURVE_RADIXBITS ((LAYOUT_SPACECURVE_TOTALBITS+4)/5)
#else
 #define LAYOUT_SPACECURVE_RADIXBITS ((LAYOUT_SPACECURVE_TOTALBITS+5)/6)
#endif

#if (3*LAYOUT_SPACECURVE_BITS) > CPUCONF_INT_BITS
 #define LAYOUT_SPACECURVE_STORAGE64 (1)
#else
 #define LAYOUT_SPACECURVE_STORAGE64 (0)
#endif

#if LAYOUT_SPACECURVE_STORAGE64
typedef uint64_t orderuint;
typedef int64_t orderint;
#else
typedef unsigned int orderuint;
typedef int orderint;
#endif


typedef struct
{
  rfindex index;
  union
  {
    rff edgesizesum;
    orderuint storageindex;
    rfindex offset;
  } u;
/*
  rfindex next;
*/
} layoutSector;


////


static inline int layoutSectorSizeSortCmp( layoutSector *ls0, layoutSector *ls1 )
{
  if( ls1->u.edgesizesum > ls0->u.edgesizesum )
    return 1;
  return 0;
}

static inline void layoutSectorSizeSortCopy( layoutSector *dst, layoutSector *src )
{
  dst->index = src->index;
  dst->u.edgesizesum = src->u.edgesizesum;
  return;
}

#define MSORT_MAIN layoutSectorSizeSort
#define MSORT_CMP layoutSectorSizeSortCmp
#define MSORT_COPY(d,s) layoutSectorSizeSortCopy(d,s)
#define MSORT_TYPE layoutSector
#include "ccmergesort.h"
#undef MSORT_MAIN
#undef MSORT_CMP
#undef MSORT_COPY
#undef MSORT_TYPE



static inline int layoutSectorStorageRadix( layoutSector *ls, int bitindex )
{
  return ( ls->u.storageindex >> bitindex ) & ((1<<LAYOUT_SPACECURVE_RADIXBITS)-1);
}

#define RSORT_MAIN layoutSectorStorageSort
#define RSORT_RADIX layoutSectorStorageRadix
#define RSORT_TYPE layoutSector
#define RSORT_RADIXBITS (LAYOUT_SPACECURVE_RADIXBITS)
#define RSORT_BIGGEST_FIRST (0)
#include "ccradixsort.h"
#undef RSORT_MAIN
#undef RSORT_RADIX
#undef RSORT_TYPE
#undef RSORT_RADIXBITS
#undef RSORT_BIGGEST_FIRST


////


typedef struct
{
  rfindex *nodeassign;
  rfindex *triangleassign;

  rfindex storageoffset;
  rfindex sectorsize;
  rfindex trirefsize;
  rfindex nodeoffsetcount;
  rfindex triangleoffsetcount;

  rfindex *sectoroffset;
  rfindex *nodeoffset;
  rfindex *triangleoffset;

  rfTri *trilist;
} rfGraphLayout;


static rff layoutSectorSizeSum( rfPrepSector *prepsector )
{
  rff sizex, sizey, sizez, area[3];

  sizex = prepsector->edge[RF_EDGE_MAXX] - prepsector->edge[RF_EDGE_MINX];
  sizey = prepsector->edge[RF_EDGE_MAXY] - prepsector->edge[RF_EDGE_MINY];
  sizez = prepsector->edge[RF_EDGE_MAXZ] - prepsector->edge[RF_EDGE_MINZ];
  area[0] = sizey * sizez;
  area[1] = sizex * sizez;
  area[2] = sizex * sizey;

  return area[0] + area[1] + area[2];
}


static void layoutSectorAssignNode( rfGraphLayout *graphlayout, rfindex sectorindex, rfPrepNode *prepnode )
{
  int childindex;
  rfPrepNode *childnode;

  for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
  {
    if( !( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) ) )
    {
      childnode = prepnode->child[childindex].p;
      if( graphlayout->nodeassign[ childnode->index ] == -1 )
      {
        graphlayout->nodeassign[ childnode->index ] = sectorindex;
        layoutSectorAssignNode( graphlayout, sectorindex, childnode );
      }
    }
  }

  return;
}

static void layoutSectorAssignAll( rfGraphLayout *graphlayout, rfPrepSector *prepsector )
{
  int index, edgeindex;
  rfindex triindex;
  rfindex sectorindex;
  rfPrepLink *preptrilist;
  rfPrepNode *prepnode;

  sectorindex = prepsector->index;
  preptrilist = PREP_SECTOR_TRILIST( prepsector );
  for( index = 0 ; index < prepsector->tricount ; index++ )
  {
    triindex = preptrilist[index].i;
    if( graphlayout->triangleassign[ triindex ] == -1 )
      graphlayout->triangleassign[ triindex ] = sectorindex;
  }
  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    if( !( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) ) )
    {
      prepnode = prepsector->link[edgeindex].p;
      if( graphlayout->nodeassign[ prepnode->index ] == -1 )
      {
        graphlayout->nodeassign[ prepnode->index ] = sectorindex;
        layoutSectorAssignNode( graphlayout, sectorindex, prepnode );
      }
    }
  }

  return;
}


////


#if LAYOUT_TRIREF_REORDERING

/* Compute the area of a triangle given two scaled edge planes. */
static rff layoutTriangleArea( rfTri *tri )
{
  rff area, scale, scu, scv, dotpr;

  scu = ( tri->edpu[0] * tri->edpu[0] ) + ( tri->edpu[1] * tri->edpu[1] ) + ( tri->edpu[2] * tri->edpu[2] );
  scv = ( tri->edpv[0] * tri->edpv[0] ) + ( tri->edpv[1] * tri->edpv[1] ) + ( tri->edpv[2] * tri->edpv[2] );
#if LAYOUT_SSE_AVAILABLE
  scale = _mm_cvtss_f32( _mm_mul_ss( _mm_rsqrt_ss( _mm_set_ss( scu ) ), _mm_rsqrt_ss( _mm_set_ss( scv ) ) ) );
#else
  scale = 1.0 / ( sqrt( scu ) * sqrt( scv ) );
#endif

  dotpr = ( ( tri->edpu[0] * tri->edpv[0] ) + ( tri->edpu[1] * tri->edpv[1] ) + ( tri->edpu[2] * tri->edpv[2] ) ) * scale;
  if( rffabs( dotpr ) > 0.999 )
    dotpr = 0.999;
#if LAYOUT_SSE_AVAILABLE
  area = ( 0.5 * scale ) * _mm_cvtss_f32( _mm_rsqrt_ss( _mm_set_ss( 1.0 - ( dotpr * dotpr ) ) ) );
#else
  area = ( 0.5 * scale ) / sqrt( 1.0 - ( dotpr * dotpr ) );
#endif

  return area;
}


#define LAYOUT_TRIREF_SORT_MAX (128)

typedef struct
{
  rfindex triindex;
  rff area;
} layoutTriRef;


static inline int layoutTriRefSortCmp( layoutTriRef *ref0, layoutTriRef *ref1 )
{
  if( ref1->area > ref0->area )
    return 1;
  return 0;
}

#define MSORT_MAIN layoutTriRefSort
#define MSORT_CMP layoutTriRefSortCmp
#define MSORT_TYPE layoutTriRef
#include "ccmergesort.h"
#undef MSORT_MAIN
#undef MSORT_CMP
#undef MSORT_TYPE


static void layoutSectorTriangleReorder( rfGraphLayout *graphlayout, rfPrepSector *prepsector )
{
  int index, refcount;
  rfindex triindex;
  rfPrepLink *preptrilist;
  layoutTriRef reftable[LAYOUT_TRIREF_SORT_MAX];
  layoutTriRef reftmp[LAYOUT_TRIREF_SORT_MAX];
  layoutTriRef *ref, *refsort;

  refcount = prepsector->tricount;
  if( refcount > LAYOUT_TRIREF_SORT_MAX )
    refcount = LAYOUT_TRIREF_SORT_MAX;

  ref = reftable;
  preptrilist = PREP_SECTOR_TRILIST( prepsector );
  for( index = 0 ; index < refcount ; index++, ref++ )
  {
    triindex = preptrilist[index].i;
    ref->triindex = triindex;
    ref->area = layoutTriangleArea( &graphlayout->trilist[ triindex ] );
  }

  refsort = layoutTriRefSort( reftable, reftmp, refcount );

  ref = refsort;
  for( index = 0 ; index < refcount ; index++, ref++ )
    preptrilist[index].i = ref->triindex;

  return;
}

#endif


////


static void layoutSectorStoreNode( rfGraphLayout *graphlayout, rfindex sectorindex, rfPrepNode *prepnode, rff *parentedge )
{
  int childindex, childmask, childmodindex, nodeaxis, edgeplane;
  rfPrepNode *childnode;
  rff localedge[RF_EDGE_COUNT], size, offset;

  childmask = 0x0;
  nodeaxis = PREP_NODE_GETAXIS( prepnode );
  if( !( prepnode->nodeflags & ( (3*RF_LINK_SECTOR) << RF_NODE_LINKFLAGS_SHIFT ) ) )
  {
    /* We have 2 child nodes, prioritize based on coverage */
    size = parentedge[ RF_AXIS_TO_EDGE(nodeaxis,1) ] - parentedge[ RF_AXIS_TO_EDGE(nodeaxis,0) ];
    offset = prepnode->plane - parentedge[ RF_AXIS_TO_EDGE(nodeaxis,0) ];
    if( offset < 0.5*size )
      childmask = 0x1;
  }
  localedge[RF_EDGE_MINX] = parentedge[RF_EDGE_MINX];
  localedge[RF_EDGE_MAXX] = parentedge[RF_EDGE_MAXX];
  localedge[RF_EDGE_MINY] = parentedge[RF_EDGE_MINY];
  localedge[RF_EDGE_MAXY] = parentedge[RF_EDGE_MAXY];
  localedge[RF_EDGE_MINZ] = parentedge[RF_EDGE_MINZ];
  localedge[RF_EDGE_MAXZ] = parentedge[RF_EDGE_MAXZ];

  for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
  {
    childmodindex = childindex ^ childmask;
    if( !( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childmodindex + RF_NODE_LINKFLAGS_SHIFT ) ) ) )
    {
      childnode = prepnode->child[ childmodindex ].p;
      if( graphlayout->nodeassign[ childnode->index ] == sectorindex )
      {
        graphlayout->nodeassign[ childnode->index ] = -1;
        graphlayout->nodeoffset[ childnode->index ] = graphlayout->storageoffset;
        graphlayout->storageoffset += graphlayout->nodeoffsetcount;
        edgeplane = RF_AXIS_TO_EDGE(nodeaxis,childmodindex^0x1);
        localedge[ edgeplane ] = prepnode->plane;
        layoutSectorStoreNode( graphlayout, sectorindex, childnode, localedge );
        localedge[ edgeplane ] = parentedge[ edgeplane ];
      }
    }
  }

  return;
}

static void layoutSectorStoreAll( rfGraphLayout *graphlayout, rfPrepSector *prepsector )
{
  int index, edgeindex, edgemodindex, axisorder[3], axistmp;
  rff size[RF_AXIS_COUNT], area[RF_AXIS_COUNT];
  rfindex triindex;
  rfindex sectorindex;
  rfPrepLink *preptrilist;
  rfPrepNode *prepnode;

#if LAYOUT_TRIREF_REORDERING
  if( prepsector->tricount > 2 )
    layoutSectorTriangleReorder( graphlayout, prepsector );
#endif

  sectorindex = prepsector->index;
  graphlayout->sectoroffset[ sectorindex ] = graphlayout->storageoffset;
  graphlayout->storageoffset += RF_GRAPH_OFFSET_COUNT( graphlayout->sectorsize + ( prepsector->tricount * graphlayout->trirefsize ) );

/*
printf( "Sector %d, Offset %d\n", (int)sectorindex, (int)graphlayout->sectoroffset[ sectorindex ] << RF_GRAPH_OFFSET_SHIFT );
*/

  preptrilist = PREP_SECTOR_TRILIST( prepsector );
  for( index = 0 ; index < prepsector->tricount ; index++ )
  {
    triindex = preptrilist[index].i;
    if( graphlayout->triangleassign[ triindex ] == sectorindex )
    {
      graphlayout->triangleoffset[ triindex ] = graphlayout->storageoffset;
      graphlayout->storageoffset += graphlayout->triangleoffsetcount;
    }
  }

  /* Find the edge area of the 3 axis, pick the proper order from biggest edge to smallest */
  size[RF_AXIS_X] = prepsector->edge[RF_EDGE_MAXX] - prepsector->edge[RF_EDGE_MINX];
  size[RF_AXIS_Y] = prepsector->edge[RF_EDGE_MAXY] - prepsector->edge[RF_EDGE_MINY];
  size[RF_AXIS_Z] = prepsector->edge[RF_EDGE_MAXZ] - prepsector->edge[RF_EDGE_MINZ];
  area[RF_AXIS_X] = size[RF_AXIS_Y] * size[RF_AXIS_Z];
  area[RF_AXIS_Y] = size[RF_AXIS_X] * size[RF_AXIS_Z];
  area[RF_AXIS_Z] = size[RF_AXIS_X] * size[RF_AXIS_Y];
  axisorder[0] = RF_AXIS_X;
  axisorder[1] = RF_AXIS_Y;
  axisorder[2] = RF_AXIS_Z;
  if( area[ axisorder[1] ] > area[ axisorder[0] ] )
  {
    axistmp = axisorder[0];
    axisorder[0] = axisorder[1];
    axisorder[1] = axistmp;
  }
  if( area[ axisorder[2] ] > area[ axisorder[0] ] )
  {
    axistmp = axisorder[0];
    axisorder[0] = axisorder[2];
    axisorder[2] = axistmp;
  }
  if( area[ axisorder[2] ] > area[ axisorder[1] ] )
  {
    axistmp = axisorder[1];
    axisorder[1] = axisorder[2];
    axisorder[2] = axistmp;
  }

  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    edgemodindex = RF_AXIS_TO_EDGE( axisorder[ RF_EDGE_TO_AXIS(edgeindex) ], RF_EDGE_IS_MAX( edgeindex ) );
    if( !( prepsector->flags & ( RF_LINK_SECTOR << ( edgemodindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) ) )
    {
      prepnode = prepsector->link[edgemodindex].p;
      if( graphlayout->nodeassign[ prepnode->index ] == sectorindex )
      {
        graphlayout->nodeassign[ prepnode->index ] = -1;
        graphlayout->nodeoffset[ prepnode->index ] = graphlayout->storageoffset;
        graphlayout->storageoffset += graphlayout->nodeoffsetcount;
        layoutSectorStoreNode( graphlayout, sectorindex, prepnode, prepsector->edge );
      }
    }
  }

  return;
}


////


#if LAYOUT_EXPERIMENTAL_PACKING

static rfsize layoutSectorStorageSizeNode( rfGraphLayout *graphlayout, rfindex sectorindex, rfPrepNode *prepnode )
{
  int childindex, nodeaxis, edgeplane;
  rfsize storagesize;
  rfPrepNode *childnode;

  storagesize = graphlayout->nodeoffsetcount;
  for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
  {
    if( !( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) ) )
    {
      childnode = prepnode->child[ childindex ].p;
      if( graphlayout->nodeassign[ childnode->index ] == sectorindex )
        storagesize += layoutSectorStorageSizeNode( graphlayout, sectorindex, childnode );
    }
  }

  return storagesize;
}

static rfsize layoutSectorStorageSize( rfGraphLayout *graphlayout, rfPrepSector *prepsector )
{
  int index, edgeindex;
  rfsize storagesize;
  rfindex triindex;
  rfindex sectorindex;
  rfPrepLink *preptrilist;
  rfPrepNode *prepnode;

  sectorindex = prepsector->index;
  storagesize = RF_GRAPH_OFFSET_COUNT( graphlayout->sectorsize + ( prepsector->tricount * graphlayout->trirefsize ) );

  preptrilist = PREP_SECTOR_TRILIST( prepsector );
  for( index = 0 ; index < prepsector->tricount ; index++ )
  {
    triindex = preptrilist[index].i;
    if( graphlayout->triangleassign[ triindex ] == sectorindex )
      storagesize += graphlayout->triangleoffsetcount;
  }

  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    if( !( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) ) )
    {
      prepnode = prepsector->link[edgeindex].p;
      /* We will count more than once any repeated node reference, no big deal though */
      if( graphlayout->nodeassign[ prepnode->index ] == sectorindex )
        storagesize += layoutSectorStorageSizeNode( graphlayout, sectorindex, prepnode );
    }
  }

  return storagesize;
}

/*
#define LAYOUT_PACK_TRIREF_RANGE (1<<(15+RF_GRAPH_LINK32_SHIFT-RF_GRAPH_OFFSET_SHIFT))
#define LAYOUT_PACK_SECTOR_RANGE (1<<(15+RF_GRAPH_LINK32_SHIFT-RF_GRAPH_OFFSET_SHIFT))
*/
#define LAYOUT_PACK_TRIREF_RANGE (1<<(15+RF_GRAPH_LINK32_SHIFT-RF_GRAPH_OFFSET_SHIFT))
#define LAYOUT_PACK_SECTOR_RANGE (1<<(15+RF_GRAPH_LINK32_SHIFT-RF_GRAPH_OFFSET_SHIFT))


static rfsindex layoutSectorDisplacement( rfGraphLayout *graphlayout, rfPrepSector *prepsector )
{
  int index, edgeindex, dispcount;
  rfindex triindex;
  rfindex sectoroffset, targetoffset;
  rfindex sectorindex, targetindex;
  rfsindex displacement, offsetdiff;
  rfPrepLink *preptrilist;
  rfPrepNode *prepnode;
  rfPrepSector *linksector;

  displacement = 0;
  dispcount = 0;
  sectorindex = prepsector->index;
  sectoroffset = graphlayout->sectoroffset[sectorindex];

/*
  preptrilist = PREP_SECTOR_TRILIST( prepsector );
  for( index = 0 ; index < prepsector->tricount ; index++ )
  {
    triindex = preptrilist[index].i;
    targetindex = graphlayout->triangleassign[triindex];
    targetoffset = graphlayout->sectoroffset[targetindex];
    offsetdiff = (rfsindex)targetoffset - (rfsindex)sectoroffset;
    if( ( offsetdiff > LAYOUT_PACK_TRIREF_RANGE ) || ( offsetdiff < -LAYOUT_PACK_TRIREF_RANGE ) )
    {
      displacement += offsetdiff;
      dispcount++;
    }
  }
*/

  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    if( !( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) ) )
    {
/*
      prepnode = prepsector->link[edgeindex].p;
      if( graphlayout->nodeassign[ prepnode->index ] == sectorindex )
        storagesize += layoutSectorStorageSizeNode( graphlayout, sectorindex, prepnode );
*/
    }
    else
    {
      linksector = prepsector->link[edgeindex].p;
      if( prepsector->link[edgeindex].p )
      {
        targetoffset = graphlayout->sectoroffset[ linksector->index ];
        offsetdiff = (rfsindex)targetoffset - (rfsindex)sectoroffset;
        if( ( offsetdiff > LAYOUT_PACK_SECTOR_RANGE ) || ( offsetdiff < -LAYOUT_PACK_SECTOR_RANGE ) )
        {
          displacement += offsetdiff;
          dispcount++;
        }
      }
    }
  }

  return ( dispcount ? displacement / dispcount : 0 );
}

static inline int layoutSectorOffsetRadix( layoutSector *ls, int bitindex )
{
  return ( ls->u.offset >> bitindex ) & ((1<<LAYOUT_EXPERIMENTAL_PACKING_RADIXBITS)-1);
}

#define RSORT_MAIN layoutSectorOffsetSort
#define RSORT_RADIX layoutSectorOffsetRadix
#define RSORT_TYPE layoutSector
#define RSORT_RADIXBITS (LAYOUT_EXPERIMENTAL_PACKING_RADIXBITS)
#define RSORT_BIGGEST_FIRST (0)
#include "ccradixsort.h"
#undef RSORT_MAIN
#undef RSORT_RADIX
#undef RSORT_TYPE
#undef RSORT_RADIXBITS
#undef RSORT_BIGGEST_FIRST

#endif


////


void rfInitGraphLayout( rfGraphCache *cache )
{
  cache->layoutmode = RF_GRAPHCACHE_LAYOUTMODE_NONE;
  cache->sectoroffset = 0;
  cache->nodeoffset = 0;
  cache->triangleoffset = 0;
  return;
}


/* Decide graph memory layout to produce good locality */
void rfBuildGraphLayout( rfGraphCache *cache, rfGeometry *geometry, rfint buildlayout )
{
  rfindex index;
  rff graphmin[RF_AXIS_COUNT], graphfactor[RF_AXIS_COUNT], center[RF_AXIS_COUNT];
  rfPrepSector *prepsector;
  layoutSector *lstable, *lstmp, *ls, *lsresult;
  rfGraphLayout graphlayout;
  orderuint point[RF_AXIS_COUNT];

  /* Allocate and build list of sectors */
  lstable = malloc( cache->sectorcount * sizeof(layoutSector) );
  lstmp = malloc( cache->sectorcount * sizeof(layoutSector) );
  ls = lstable;
  for( index = 0 ; index < cache->sectorcount ; index++, ls++ )
  {
    prepsector = mmDirectoryGetFast( &cache->sectordir, index );
    ls->index = index;
    ls->u.edgesizesum = layoutSectorSizeSum( prepsector );
/*
    ls->next = -1;
*/
  }

  /* Sort all sectors from biggest to smallest */
  lsresult = layoutSectorSizeSort( lstable, lstmp, cache->sectorcount );
  if( lsresult == lstmp )
  {
    lstmp = lstable;
    lstable = lsresult;
  }

  /* Initialize assignment tables */
  graphlayout.nodeassign = malloc( cache->nodecount * sizeof(rfindex) );
  graphlayout.triangleassign = malloc( cache->trianglecount * sizeof(rfindex) );
  for( index = 0 ; index < cache->nodecount ; index++ )
    graphlayout.nodeassign[index] = -1;
  for( index = 0 ; index < cache->trianglecount ; index++ )
    graphlayout.triangleassign[index] = -1;

  /* To avoid multiplying centers by 0.5 below, it is implied here */
  graphmin[RF_AXIS_X] = 2.0 * cache->edge[RF_EDGE_MINX];
  graphmin[RF_AXIS_Y] = 2.0 * cache->edge[RF_EDGE_MINY];
  graphmin[RF_AXIS_Z] = 2.0 * cache->edge[RF_EDGE_MINZ];
  graphfactor[RF_AXIS_X] = 0.5 * ( (rff)(1<<LAYOUT_SPACECURVE_BITS) / ( cache->edge[RF_EDGE_MAXX] - cache->edge[RF_EDGE_MINX] ) );
  graphfactor[RF_AXIS_Y] = 0.5 * ( (rff)(1<<LAYOUT_SPACECURVE_BITS) / ( cache->edge[RF_EDGE_MAXY] - cache->edge[RF_EDGE_MINY] ) );
  graphfactor[RF_AXIS_Z] = 0.5 * ( (rff)(1<<LAYOUT_SPACECURVE_BITS) / ( cache->edge[RF_EDGE_MAXZ] - cache->edge[RF_EDGE_MINZ] ) );

  /* Assign triangle/node storage to first sorted sector encountered, assign space-filling curve index */
  ls = lstable;
  for( index = cache->sectorcount ; index ; index--, ls++ )
  {
    prepsector = mmDirectoryGetFast( &cache->sectordir, ls->index );
    layoutSectorAssignAll( &graphlayout, prepsector );

    center[RF_AXIS_X] = prepsector->edge[RF_EDGE_MINX] + prepsector->edge[RF_EDGE_MAXX];
    center[RF_AXIS_Y] = prepsector->edge[RF_EDGE_MINY] + prepsector->edge[RF_EDGE_MAXY];
    center[RF_AXIS_Z] = prepsector->edge[RF_EDGE_MINZ] + prepsector->edge[RF_EDGE_MAXZ];
    point[RF_AXIS_X] = (orderuint)( ( center[RF_AXIS_X] - graphmin[RF_AXIS_X] ) * graphfactor[RF_AXIS_X] );
    point[RF_AXIS_Y] = (orderuint)( ( center[RF_AXIS_Y] - graphmin[RF_AXIS_Y] ) * graphfactor[RF_AXIS_Y] );
    point[RF_AXIS_Z] = (orderuint)( ( center[RF_AXIS_Z] - graphmin[RF_AXIS_Z] ) * graphfactor[RF_AXIS_Z] );

#if LAYOUT_SPACECURVE_STORAGE64
 #if LAYOUT_SPACECURVE_BITS == 21
    ls->u.storageindex = ccSpaceCurveMap3D21Bits64( point );
 #else
    ls->u.storageindex = ccSpaceCurveMap3D64( LAYOUT_SPACECURVE_BITS, point );
 #endif
#else
 #if LAYOUT_SPACECURVE_BITS == 10
    ls->u.storageindex = ccSpaceCurveMap3D10Bits( point );
 #else
    ls->u.storageindex = ccSpaceCurveMap3D( LAYOUT_SPACECURVE_BITS, point );
 #endif
#endif

/*
printf( "%d : %f %f %f : 0x%llx / 0x%llx : %f\n", (int)index, 0.5 * center[0], 0.5 * center[1], 0.5 * center[2], (long long)ls->u.storageindex, (long long)1 << LAYOUT_SPACECURVE_TOTALBITS, (double)ls->u.storageindex / (double)( (unsigned long long)1 << LAYOUT_SPACECURVE_TOTALBITS ) );
*/
  }

  /* Radix sort all sectors based on space curve position */
  lsresult = layoutSectorStorageSort( lstable, lstmp, cache->sectorcount, LAYOUT_SPACECURVE_TOTALBITS );
  if( lsresult == lstmp )
  {
    lstmp = lstable;
    lstable = lsresult;
  }

  /* Prepare storage computation */
  graphlayout.storageoffset = 0;
  graphlayout.sectorsize = sizeof(rfSector32);
  graphlayout.trirefsize = sizeof(int32_t);
  graphlayout.nodeoffsetcount = RF_GRAPH_OFFSET_COUNT( RF_NODE32_SIZE );
  graphlayout.triangleoffsetcount = RF_GRAPH_OFFSET_COUNT( sizeof(rfTri) + geometry->datasize );
  graphlayout.sectoroffset = malloc( cache->sectorcount * sizeof(rfindex) );
  graphlayout.nodeoffset = malloc( cache->nodecount * sizeof(rfindex) );
  graphlayout.triangleoffset = malloc( cache->trianglecount * sizeof(rfindex) );
  graphlayout.trilist = cache->trilist;



  /* TODO: Loop continuously to correct any reference outside addressing range */



#if LAYOUT_EXPERIMENTAL_PACKING
  int offsetbitcount;
  rfsize *sectorstoragesize;
  rfsize storageoffset, storageoffsetmax;
  rfsindex displacement, dispeffective, dispabs;
  rfsindex displacementsum, displacementcount;
  ccQuickRandState32 randstate;

  ccQuickRand32Seed( &randstate, 0x0 );

  sectorstoragesize = malloc( cache->sectorcount * sizeof(rfsize) );

  /* Compute sector storage size, sector offsets */
  storageoffset = 0;
  ls = lstable;
  for( index = cache->sectorcount ; index ; index--, ls++ )
  {
    prepsector = mmDirectoryGetFast( &cache->sectordir, ls->index );
    sectorstoragesize[ls->index] = layoutSectorStorageSize( &graphlayout, prepsector );
    graphlayout.sectoroffset[ls->index] = storageoffset;
    ls->u.offset = storageoffset;
    storageoffset += sectorstoragesize[ls->index];
  }

  offsetbitcount = ccLog2Int64( storageoffset ) + 1;
  storageoffsetmax = ( ( (rfindex)1 ) << offsetbitcount ) -1;

  /* Loop until all references lie within addressing range */
  for( ; ; )
  {


displacementsum = 0;
displacementcount = 0;


    /* Find how much to displace that sector */
    ls = lstable;
    for( index = cache->sectorcount ; index ; index--, ls++ )
    {
      prepsector = mmDirectoryGetFast( &cache->sectordir, ls->index );
      displacement = layoutSectorDisplacement( &graphlayout, prepsector );

displacement >>= 4;

      if( !( displacement ) )
        continue;



      dispabs = ( displacement > 0 ? displacement : -displacement );

      /* TODO: The C standard doesn't guarantee divisions round towards zero, this could get ugly if not */
      dispeffective = ccQuickRand32( &randstate ) % dispabs;
      if( displacement < 0 )
        dispeffective = -dispeffective;

#if 0
      rfsindex dispmin, dispmax;
/*
      dispmin = -16384;
      dispmax = 16384;
*/
/*
      dispmin = -256;
      dispmax = 256;
*/
      dispmin = -16384*256;
      dispmax = 16384*256;
      if( dispmin < -(rfsindex)ls->u.offset )
        dispmin = -ls->u.offset;
      if( dispeffective < dispmin )
        dispeffective = dispmin;
      if( dispeffective > dispmax )
        dispeffective = dispmax;
#else
      if( dispeffective < -(rfsindex)ls->u.offset )
        dispeffective = -ls->u.offset;
#endif


#if 0
printf( "Adjust %lld from %lld to %lld ( displacement %lld, effective %lld )\n", (long long)ls->index, (long long)ls->u.offset, (long long)( ls->u.offset + dispeffective ), (long long)displacement, (long long)dispeffective );
#endif
      ls->u.offset += dispeffective;
      if( ls->u.offset > storageoffsetmax )
        ls->u.offset = storageoffsetmax;


displacementsum += ( displacement > 0 ? displacement : -displacement );
displacementcount++;



    }

    /* Radix sort all sectors based on displaced offsets */
    lsresult = layoutSectorOffsetSort( lstable, lstmp, cache->sectorcount, offsetbitcount );
    if( lsresult == lstmp )
    {
      lstmp = lstable;
      lstable = lsresult;
    }

    /* Compute new offsets */
    storageoffset = 0;
    ls = lstable;
    for( index = cache->sectorcount ; index ; index--, ls++ )
    {
#if 0
      printf( "Sort Offset of %d : %d\n", (int)ls->index, (int)ls->u.offset );
      if( graphlayout.sectoroffset[ls->index] != storageoffset )
        printf( "  Move Sector %d from %lld to %lld\n", (int)ls->index, (long long)graphlayout.sectoroffset[ls->index], (long long)storageoffset );
#endif


      graphlayout.sectoroffset[ls->index] = storageoffset;
      ls->u.offset = storageoffset;
      storageoffset += sectorstoragesize[ls->index];
    }



    printf( "Displacement Count : %d\n", (int)displacementcount );
    printf( "Displacement Sum : %lld ( %lldM )\n", (long long)displacementsum, (long long)displacementsum / 1000000 );
    printf( "\n" );
    fflush( stdout );
    usleep( 50000 );



  }

  free( sectorstoragesize );

#endif





  /* Set storage offsets for all graph content */
  ls = lstable;
  for( index = cache->sectorcount ; index ; index--, ls++ )
  {
    prepsector = mmDirectoryGetFast( &cache->sectordir, ls->index );
    layoutSectorStoreAll( &graphlayout, prepsector );
  }

  switch (buildlayout)
  {
  case RF_BUILDHINT_LAYOUTMODE_NONE:
    cache->layoutmode = RF_GRAPHCACHE_LAYOUTMODE_NONE;
    fprintf(stderr, "  RF_GRAPHCACHE_LAYOUTMODE_NONE\n");
    break;
  case RF_BUILDHINT_LAYOUTMODE_FULL:
  default:
    cache->layoutmode = RF_GRAPHCACHE_LAYOUTMODE_FULL;
    fprintf(stderr, "  RF_GRAPHCACHE_LAYOUTMODE_FULL\n");
    break;
  }

  cache->layoutaddressmode = RF_GRAPH_ADDRESSMODE_32BITS;
  cache->sectoroffset = graphlayout.sectoroffset;
  cache->nodeoffset = graphlayout.nodeoffset;
  cache->triangleoffset = graphlayout.triangleoffset;
  cache->layoutmemory = ( (size_t)graphlayout.storageoffset ) << RF_GRAPH_OFFSET_SHIFT;

  free( graphlayout.nodeassign );
  free( graphlayout.triangleassign );

  free( lstable );
  free( lstmp );

  return;
}


void rfFreeGraphLayout( rfGraphCache *cache )
{
  if( cache->sectoroffset )
    free( cache->sectoroffset );
  if( cache->nodeoffset )
    free( cache->nodeoffset );
  if( cache->triangleoffset )
    free( cache->triangleoffset );
  return;
}


