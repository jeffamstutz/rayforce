/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "cpuconfig.h"

#include "cc.h"
#include "mm.h"
#include "mmdirectory.h"


#define MM_DIRECTORY_ENTRY(dir,index) ( (dir)->table[ index >> (dir)->pageshift ][ index & (dir)->pagemask ] )
#define MM_DIRECTORY_ATOMIC_BUSY ((void *)~((uintptr_t)0))


int mmDirectoryInit( mmDirectory *dir, size_t pageshift, size_t pagecount )
{
  if( !( dir->table = mmAlloc( 0, pagecount * sizeof(mmAtomicP) ) ) )
    return 0;
  memset( dir->table, 0, pagecount * sizeof(mmAtomicP) );
  dir->pagecount = pagecount;
  dir->pageshift = pageshift;
  dir->pagesize = 1 << pageshift;
  dir->pagemask = dir->pagesize - 1;
  return 1;
}

void mmDirectoryFree( mmDirectory *dir )
{
  ssize_t pageindex;
  mmAtomicP *page;
  if( !( dir->table ) )
    return;
  for( pageindex = dir->pagecount-1 ; pageindex >= 0 ; pageindex-- )
  {
    page = mmAtomicReadP( &dir->table[ pageindex ] );
    if( page )
      mmFree( 0, page, 0 );
  }
  dir->pagecount = 0;
  mmFree( 0, dir->table, 0 );
  dir->table = 0;
  return;
}


static inline void mmDirectoryResizeTable( mmDirectory *dir, size_t newcount )
{
  ssize_t pageindex;
  mmAtomicP *page;
  for( pageindex = newcount ; pageindex < dir->pagecount ; pageindex++ )
  {
    page = mmAtomicReadP( &dir->table[ pageindex ] );
    if( page )
      mmFree( 0, page, 0 );
  }
  if( !( dir->table = mmRealloc( 0, dir->table, newcount * sizeof(mmAtomicP) ) ) )
    return;
  if( newcount > dir->pagecount )
    memset( ADDRESS( dir->table, dir->pagecount * sizeof(mmAtomicP) ), 0, ( newcount - dir->pagecount ) * sizeof(mmAtomicP) );
  dir->pagecount = newcount;
  return;
}


/* Not multithreading safe, caller must ensure exclusive access */
void mmDirectorySetSize( mmDirectory *dir, size_t size )
{
  ssize_t pageindex, pagecount;
  mmAtomicP *page;

  pagecount = ( size >> dir->pageshift ) + 1;
  if( pagecount != dir->pagecount )
    mmDirectoryResizeTable( dir, pagecount );
  for( pageindex = 0 ; pageindex < pagecount ; pageindex++ )
  {
    page = mmAtomicReadP( &dir->table[ pageindex ] );
    if( page )
      continue;
    if( !( page = mmAlloc( 0, dir->pagesize * sizeof(mmAtomicP) ) ) )
      abort();
    mmAtomicWriteP( &dir->table[ pageindex ], page );
  }

  return;
}


void mmDirectorySet( mmDirectory *dir, size_t index, void *entry )
{
  ssize_t pageindex;
  mmAtomicP *page;

  pageindex = index >> dir->pageshift;
  if( pageindex >= dir->pagecount )
    abort();

  for( ; ; )
  {
    page = mmAtomicReadP( &dir->table[ pageindex ] );
    if( ( page ) && ( page != MM_DIRECTORY_ATOMIC_BUSY ) )
      break;
    if( ( page == MM_DIRECTORY_ATOMIC_BUSY ) || !( mmAtomicCmpReplaceP( &dir->table[ pageindex ], 0, MM_DIRECTORY_ATOMIC_BUSY ) ) )
    {
      mmAtomicPause();
      continue;
    }
    if( !( page = mmAlloc( 0, dir->pagesize * sizeof(mmAtomicP) ) ) )
      abort();
    mmAtomicWriteP( &dir->table[ pageindex ], page );
    break;
  }
  mmAtomicWriteP( &page[ index & dir->pagemask ], entry );

  return;
}


void *mmDirectoryGet( mmDirectory *dir, size_t index )
{
  ssize_t pageindex;
  void *entry;
  mmAtomicP *page;

  pageindex = index >> dir->pageshift;
  if( pageindex >= dir->pagecount )
    return 0;
  for( ; ; )
  {
    page = mmAtomicReadP( &dir->table[ pageindex ] );
    if( page == MM_DIRECTORY_ATOMIC_BUSY )
    {
      mmAtomicPause();
      continue;
    }
    if( !( page ) )
      return 0;
    break;
  }
  entry = mmAtomicReadP( &page[ index & dir->pagemask ] );

  return entry;
}


/* If absolutely sure entry was allocated before */
void mmDirectorySetFast( mmDirectory *dir, size_t index, void *entry )
{
  mmAtomicP *page;
  page = mmAtomicReadP( &dir->table[ index >> dir->pageshift ] );
  mmAtomicWriteP( &page[ index & dir->pagemask ], entry );
  return;
}

/* If absolutely sure entry was written before */
void *mmDirectoryGetFast( mmDirectory *dir, size_t index )
{
  void *entry;
  mmAtomicP *page;
  page = mmAtomicReadP( &dir->table[ index >> dir->pageshift ] );
  entry = mmAtomicReadP( &page[ index & dir->pagemask ] );
  return entry;
}

