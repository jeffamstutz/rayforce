/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "cpuconfig.h"

#include "cc.h"
#include "mm.h"
#include "mmqueue.h"


typedef struct
{
  int readoffset, writeoffset;
  float min, max;
  mmListNode list;
} mmQueueBundle;

#define MM_QUEUE_BUNDLE_ITEM(b,i,s) (void *)ADDRESS( (b), sizeof(mmQueueBundle) + ( (i) * (s) ) )


////


void mmQueueInit( mmQueue *queue, size_t itemsize, int bundlesize, int blocksize, float bundlerangefactor )
{
  queue->bundlelist = 0;
  queue->itemsize = itemsize;
  queue->bundlesize = bundlesize;
  if( bundlerangefactor < 1.0 )
    bundlerangefactor = 1.0;
  queue->rangefactor = bundlerangefactor;
  queue->rangefactorinv = 1.0 / bundlerangefactor;
  mmBlockInit( &queue->bundleblock, sizeof(mmQueueBundle) + ( bundlesize * itemsize ), blocksize, blocksize, 0x8 );
#if MM_QUEUE_MONITOR
  queue->bundlecount = 0;
  queue->bundlecountmax = 0;
#endif
  return;
}


void mmQueueFree( mmQueue *queue )
{
  mmBlockFreeAll( &queue->bundleblock );

#if MM_QUEUE_MONITOR
  printf( "mmQueue bundle max : %d\n", queue->bundlecountmax );
#endif

  return;
}


/* Bundles are sorted big to small */
void mmQueueAdd( mmQueue *queue, void *item, float value )
{
  float bundlemin, bundlemax;
  mmQueueBundle *bundle, *bsearch;
  void *bundleitem;
  void **listhead;

  bundlemin = value * queue->rangefactorinv;
  bundlemax = value * queue->rangefactor;

  for( bundle = queue->bundlelist ; bundle ; bundle = bundle->list.next )
  {
    if( bundle->max < bundlemin )
      break;

    if( bundle->max > bundlemax )
      continue;
    if( bundle->min < bundlemin )
      continue;

    if( bundle->writeoffset >= queue->bundlesize )
      continue;
    bundleitem = MM_QUEUE_BUNDLE_ITEM( bundle, bundle->writeoffset++, queue->itemsize );
    memcpy( bundleitem, item, queue->itemsize );
    bundle->min = fminf( bundle->min, value );
    bundle->max = fmaxf( bundle->max, value );
    return;
  }

  bundle = mmBlockAlloc( &queue->bundleblock );
  bundle->readoffset = 0;
  bundle->writeoffset = 0;
  bundle->min = value;
  bundle->max = value;
  bundleitem = MM_QUEUE_BUNDLE_ITEM( bundle, bundle->writeoffset++, queue->itemsize );
  memcpy( bundleitem, item, queue->itemsize );
#if MM_QUEUE_MONITOR
  queue->bundlecount++;
  if( queue->bundlecount > queue->bundlecountmax )
    queue->bundlecountmax = queue->bundlecount;
#endif

  listhead = &queue->bundlelist;
  for( ; ; )
  {
    bsearch = *listhead;
    if( !( bsearch ) || ( bsearch->max < bundle->max ) )
    {
      mmListAdd( listhead, bundle, offsetof(mmQueueBundle,list) );
      break;
    }
    listhead = &bsearch->list.next;
  }

  return;
}


int mmQueueGet( mmQueue *queue, void *item )
{
  mmQueueBundle *bundle;
  void *bundleitem;

  bundle = queue->bundlelist;
  if( !( bundle ) )
    return 0;
  bundleitem = MM_QUEUE_BUNDLE_ITEM( bundle, bundle->readoffset++, queue->itemsize );
  memcpy( item, bundleitem, queue->itemsize );
  if( bundle->readoffset == bundle->writeoffset )
  {
    mmListRemove( bundle, offsetof(mmQueueBundle,list) );

#if MM_QUEUE_MONITOR
    queue->bundlecount--;
#endif

    mmBlockRelease( &queue->bundleblock, bundle );
  }

  return 1;
}


