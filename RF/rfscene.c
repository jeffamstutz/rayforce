/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "cpuconfig.h"

#include "cc.h"
#include "mm.h"
#include "mmdirectory.h"

#include "rfmath.h"

#include "rf.h"
#include "rfdefs.h"
#include "rfdata.h"
#include "rfinternal.h"
#include "rfgraph.h"
#include "rfgraphbuild.h"

#include "rfnuma.h"

int rfBuildObjectData( rfContext *context, rfObject *object, void *newobjectdata, size_t newobjectdatasize )
{
  rfScene *scene;
  void *cudamem;
  size_t datasize;
  rfObjectHeader staticheader;

  scene = object->scene;

  datasize = object->objectdatasize;
  if( newobjectdatasize )
    datasize = RF_OBJECTHEADER_SIZE + newobjectdatasize;
  else if( !( object->objectdatasize ) )
    datasize = RF_OBJECTHEADER_SIZE;

  switch( scene->target )
  {
    case RF_TARGET_SYSTEM:
      if( datasize != object->objectdatasize )
      {
        if( object->objectdatasize )
          free( object->objectdata );
        object->objectdata = 0;
        object->objectdatasize = 0;
        if( !( object->objectdata = malloc( datasize ) ) )
        {
          rfRegisterError( object->context, RF_ERROR_MEMORY_DENIED, "rfObjectData()" );
          return 0;
        }
        object->objectdatasize = datasize;
      }
      ((rfObjectHeader *)object->objectdata)->cullmode = object->cullmode;
      if( newobjectdatasize )
        memcpy( RF_OBJECT_DATA( object->objectdata ), newobjectdata, newobjectdatasize );
      break;
    case RF_TARGET_NUMA:
      if( datasize != object->objectdatasize )
      {
        if( object->objectdatasize )
          free( object->objectdata );
        object->objectdata = 0;
        object->objectdatasize = 0;
        if( !( object->objectdata = mmNodeAlloc( scene->targetindex, datasize ) ) )
        {
          rfRegisterError( object->context, RF_ERROR_NUMA_MEMORY_DENIED, "rfObjectData()" );
          return 0;
        }
        object->objectdatasize = datasize;
      }
      ((rfObjectHeader *)object->objectdata)->cullmode = object->cullmode;
      if( newobjectdatasize )
        memcpy( RF_OBJECT_DATA( object->objectdata ), newobjectdata, newobjectdatasize );
      break;
#if RF_CONFIG_CUDA_SUPPORT
    case RF_TARGET_CUDA:
      if( datasize != object->objectdatasize )
      {
        if( object->objectdatasize )
          rfCudaFree( context, scene->targetindex, object->objectdata, object->objectdatasize );
        object->objectdata = 0;
        object->objectdatasize = 0;
        if( !( rfCudaAlloc( context, scene->targetindex, &cudamem, datasize ) ) )
          return 0;
        object->objectdata = cudamem;
        object->objectdatasize = datasize;
      }
      staticheader.cullmode = object->cullmode;
      rfCudaCopyHtoD( context, scene->targetindex, 0, object->objectdata, &staticheader, sizeof(rfObjectHeader) );
      if( newobjectdatasize )
        rfCudaCopyHtoD( context, scene->targetindex, 0, RF_OBJECT_DATA( object->objectdata ), newobjectdata, newobjectdatasize );
      break;
#endif
    case RF_TARGET_OPENCL:
    default:
      fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
      abort();
  }

  return 1;
}


/**/


typedef struct
{
  int objectcount;
  void *child[2];
} prepRegion;

#define PREP_REGION_OBJECTLIST(x) ((int32_t *)ADDRESS(x,sizeof(prepRegion)))

typedef struct
{
  int objectcount;
  rfSceneObjectEntry *objentrytable;
  prepRegion *root;
  size_t computedsize;
  mmVolumeHead mmvolume;
} prepScene;


void rfBuildScenePrep( rfContext *context, rfScene *scene, prepScene *prepscene )
{
  int objectindex, cornerindex;
  rff corner[RF_AXIS_COUNT], point[RF_AXIS_COUNT];
  rff *matrix;
  rfObject *object;
  rfModel *model;
  rfGraph *graph;
  rfSceneObjectEntry *objentry;
  prepRegion *prepregion;
  int32_t *objectlist;

  mmVolumeInit( &prepscene->mmvolume, 1048576, RF_REGIONLEAF_SIZE, 0, 0x10 );

  prepscene->objectcount = scene->activeobjectcount;
  prepscene->objentrytable = mmVolumeAlloc( &prepscene->mmvolume, scene->activeobjectcount * sizeof(rfSceneObjectEntry) );
  prepscene->root = 0;
  prepscene->computedsize = 0;

  prepregion = mmVolumeAlloc( &prepscene->mmvolume, sizeof(prepRegion) + ( scene->activeobjectcount * sizeof(int32_t) ) );
  prepregion->objectcount = scene->activeobjectcount;
  prepregion->child[0] = 0;
  prepregion->child[1] = 0;
  prepscene->root = prepregion;

  objectlist = PREP_REGION_OBJECTLIST( prepregion );
  objentry = prepscene->objentrytable;
  objectindex = 0;
  for( object = scene->objectlist ; object ; object = object->scenenode.next )
  {
    object->sceneobjectindex = -1;
    if( !( object->flags & RF_OBJECT_FLAGS_ENABLED ) )
      continue;

    model = object->model;
    graph = &model->graph;

    objentry->edge[RF_EDGE_MINX] =  RFF_MAX;
    objentry->edge[RF_EDGE_MAXX] = -RFF_MAX;
    objentry->edge[RF_EDGE_MINY] =  RFF_MAX;
    objentry->edge[RF_EDGE_MAXY] = -RFF_MAX;
    objentry->edge[RF_EDGE_MINZ] =  RFF_MAX;
    objentry->edge[RF_EDGE_MAXZ] = -RFF_MAX;
    memcpy( objentry->matrix, object->matrix, 16*sizeof(rff) );
    memcpy( objentry->matrixinv, object->matrixinv, 16*sizeof(rff) );
    switch( object->cullmode )
    {
      case RF_CULLMODE_FRONT:
        objentry->hitsideskip = 0;
        break;
      case RF_CULLMODE_BACK:
        objentry->hitsideskip = 1;
        break;
      case RF_CULLMODE_NONE:
      default:
        objentry->hitsideskip = -1;
        break;
    }
    objentry->objectgraph = graph->memory;
    objentry->objectdata = object->objectdata;

    /* TODO: This is not the best way to compute the bounding box */
    matrix = object->matrix;
    for( cornerindex = 0 ; cornerindex < 8 ; cornerindex++ )
    {
      corner[RF_AXIS_X] = ( cornerindex & 0x1 ? graph->edge[RF_EDGE_MAXX] : graph->edge[RF_EDGE_MINX] );
      corner[RF_AXIS_Y] = ( cornerindex & 0x2 ? graph->edge[RF_EDGE_MAXY] : graph->edge[RF_EDGE_MINY] );
      corner[RF_AXIS_Z] = ( cornerindex & 0x4 ? graph->edge[RF_EDGE_MAXZ] : graph->edge[RF_EDGE_MINZ] );
      point[RF_AXIS_X] = ( corner[RF_AXIS_X] * matrix[0*4+0] ) + ( corner[RF_AXIS_Y] * matrix[1*4+0] ) + ( corner[RF_AXIS_Z] * matrix[2*4+0] ) + matrix[3*4+0];
      point[RF_AXIS_Y] = ( corner[RF_AXIS_X] * matrix[0*4+1] ) + ( corner[RF_AXIS_Y] * matrix[1*4+1] ) + ( corner[RF_AXIS_Z] * matrix[2*4+1] ) + matrix[3*4+1];
      point[RF_AXIS_Z] = ( corner[RF_AXIS_X] * matrix[0*4+2] ) + ( corner[RF_AXIS_Y] * matrix[1*4+2] ) + ( corner[RF_AXIS_Z] * matrix[2*4+2] ) + matrix[3*4+2];
      edgeExtendToPoint( objentry->edge, point );
    }

    objectlist[ objectindex ] = objectindex;
    object->sceneobjectindex = objectindex;
    objentry++;
    objectindex++;
  }



  /* TODO: Build a real scene graph, don't put all objects in root region! */
  prepscene->computedsize = RF_SIZE_ROUND64( RF_REGIONLEAF_SIZE + ( prepregion->objectcount * sizeof(int32_t) ) );
  /* TODO: Build a real scene graph, don't put all objects in root region! */



  return;
}

void rfFreeScenePrep( rfContext *context, prepScene *prepscene )
{
  mmVolumeFreeAll( &prepscene->mmvolume );
  return;
}


/**/


int rfBuildScene( rfContext *context, rfScene *scene )
{
  int objectindex;
  rff sizex, sizey, sizez;
  size_t headersize, scenegraphmemsize;
  rfObject *object;
  rfModel *model;
  rfGraph *graph;
  rfSceneHeader *sceneheader;
  rfSceneObjectEntry *objentry;
  rfRegion *region;
  prepRegion *prepregion;
  prepScene prepscene;
  void *cudamem;

  /* Free any existing scene graph stuff */
  if( scene->scenegraphmemsize )
  {
    switch( scene->target )
    {
      case RF_TARGET_SYSTEM:
        free( scene->scenehandle.scenegraph );
        break;
      case RF_TARGET_NUMA:
        mmNodeFree( scene->targetindex, scene->scenehandle.scenegraph, scene->scenegraphmemsize );
        break;
#if RF_CONFIG_CUDA_SUPPORT
      case RF_TARGET_CUDA:
        rfCudaFree( context, scene->targetindex, scene->scenehandle.scenegraph, scene->scenegraphmemsize );
        break;
#endif
      default:
        fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
        abort();
    }
    scene->scenehandle.scenegraph = 0;
    scene->scenegraphmemsize = 0;
  }

  if( !( scene->activeobjectcount ) )
    return 0;

  if( scene->activeobjectcount == 1 )
  {
    for( object = scene->objectlist ; !( object->flags & RF_OBJECT_FLAGS_ENABLED ) ; object = object->scenenode.next );
    if( object->flags & RF_OBJECT_FLAGS_IDENTITYMATRIX )
    {
      model = object->model;
      graph = &model->graph;

      scene->singleobject = object;
      scene->edge[RF_EDGE_MINX] = graph->edge[RF_EDGE_MINX];
      scene->edge[RF_EDGE_MAXX] = graph->edge[RF_EDGE_MAXX];
      scene->edge[RF_EDGE_MINY] = graph->edge[RF_EDGE_MINY];
      scene->edge[RF_EDGE_MAXY] = graph->edge[RF_EDGE_MAXY];
      scene->edge[RF_EDGE_MINZ] = graph->edge[RF_EDGE_MINZ];
      scene->edge[RF_EDGE_MAXZ] = graph->edge[RF_EDGE_MAXZ];

      scene->scenehandle.handletype = RF_HANDLETYPE_OBJECT;
      scene->scenehandle.objectgraph = graph->memory;
      scene->scenehandle.objectdata = object->objectdata;
      scene->scenehandle.scenegraph = 0;
      scene->scenehandle.rfscene = scene;
      scene->scenehandle.addressmode = graph->addressmode;
      scene->scenehandle.cullmode = object->cullmode;

      scene->flags &= ~RF_SCENE_FLAGS_MODIFIED;
      scene->flags |= RF_SCENE_FLAGS_READY;
      return 1;
    }
  }


  /* Build scene-wide acceleration structure for multiple objects */
  rfBuildScenePrep( context, scene, &prepscene );

  headersize = RF_SIZE_ROUND64( RF_SCENEHEADER_SIZE + ( prepscene.objectcount * sizeof(rfSceneObjectEntry) ) );
  scenegraphmemsize = headersize + prepscene.computedsize;
  if( scene->target == RF_TARGET_NUMA )
    sceneheader = mmNodeAlloc( scene->targetindex, scenegraphmemsize );
  else
    sceneheader = malloc( scenegraphmemsize );
  if( !( sceneheader ) )
    goto error;
  sceneheader->objectcount = prepscene.objectcount;
  sceneheader->rootoffset = headersize;
  memcpy( RF_SCENEHEADER_OBJECTTABLE(sceneheader), prepscene.objentrytable, prepscene.objectcount * sizeof(rfSceneObjectEntry) );



  scene->singleobject = 0;
  scene->edge[RF_EDGE_MINX] =  RFF_MAX;
  scene->edge[RF_EDGE_MAXX] = -RFF_MAX;
  scene->edge[RF_EDGE_MINY] =  RFF_MAX;
  scene->edge[RF_EDGE_MAXY] = -RFF_MAX;
  scene->edge[RF_EDGE_MINZ] =  RFF_MAX;
  scene->edge[RF_EDGE_MAXZ] = -RFF_MAX;
  objentry = prepscene.objentrytable;
  for( objectindex = prepscene.objectcount ; objectindex ; objectindex-- )
  {
    edgeExtendToEdge( scene->edge, objentry->edge );
    objentry++;
  }

  sizex = scene->edge[RF_EDGE_MAXX] - scene->edge[RF_EDGE_MINX];
  sizey = scene->edge[RF_EDGE_MAXY] - scene->edge[RF_EDGE_MINY];
  sizez = scene->edge[RF_EDGE_MAXZ] - scene->edge[RF_EDGE_MINZ];
  sceneheader->diagonalsize = rfsqrt( ( sizex * sizex ) + ( sizey * sizey ) + ( sizez * sizez ) );



  /* TODO: Fix rfBuildScenePrep() and build the real thing! */
  prepregion = prepscene.root;
  region = ADDRESS( sceneheader, sceneheader->rootoffset );
  region->parent = 0;
  region->objectcount = prepregion->objectcount;
  memcpy( RF_REGIONLEAF_OBJECTLIST(region), PREP_REGION_OBJECTLIST(prepregion), region->objectcount * sizeof(int32_t) );
  /* TODO: Fix rfBuildScenePrep() and build the real thing! */



  if( scene->target == RF_TARGET_CUDA )
  {
#if RF_CONFIG_CUDA_SUPPORT
    if( !( rfCudaAlloc( context, scene->targetindex, &cudamem, scenegraphmemsize ) ) )
    {
      free( sceneheader );
      goto error;
    }
    rfCudaCopyHtoD( context, scene->targetindex, 0, cudamem, sceneheader, scenegraphmemsize );
    free( sceneheader );
    sceneheader = cudamem;
#else
    goto error;
#endif
  }

  scene->scenehandle.handletype = RF_HANDLETYPE_SCENE;
  scene->scenehandle.objectgraph = 0;
  scene->scenehandle.objectdata = 0;
  scene->scenehandle.scenegraph = sceneheader;
  scene->scenehandle.rfscene = scene;
  /* TODO TODO TODO */
  scene->scenehandle.addressmode = RF_GRAPH_ADDRESSMODE_32BITS;
  /* TODO: Get the proper cullmode for all objects in scene */
  scene->scenehandle.cullmode = RF_CULLMODE_NONE;

  scene->scenegraphmemsize = scenegraphmemsize;
  rfFreeScenePrep( context, &prepscene );

  scene->flags &= ~RF_SCENE_FLAGS_MODIFIED;
  scene->flags |= RF_SCENE_FLAGS_READY | RF_SCENE_FLAGS_MULTIOBJECTS;
  return 1;


  error:
  rfFreeScenePrep( context, &prepscene );
  return 0;
}





