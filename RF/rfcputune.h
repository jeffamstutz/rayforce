/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#ifndef RFCPUTUNE_H
#define RFCPUTUNE_H


#if defined(__tune_bdver3__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_STEAMROLLER (1)
#elif defined(__tune_bdver2__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_PILEDRIVER (1)
#elif defined(__tune_bdver1__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_BULLDOZER (1)
#elif defined(__tune_btver2__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_JAGUAR (1)
#elif defined(__tune_btver1__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_BOBCAT (1)
#elif defined(__tune_amdfam10__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_BARCELONA (1)
#elif defined(__tune_k8__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_HAMMER (1)
#elif defined(__tune_athlon__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_ATHLON (1)
#elif defined(__tune_k6__)
 #define RF_CPUVENDOR_AMD (1)
 #define RF_CPUTUNE_K6 (1)
#elif defined(__tune_corei7__)
 #define RF_CPUVENDOR_INTEL (1)
 #define RF_CPUTUNE_COREI7 (1)
#elif defined(__tune_slm__)
 #define RF_CPUVENDOR_INTEL (1)
 #define RF_CPUTUNE_SLM (1)
 #define RF_CPUSLOW_BLENDPS (1)
 #define RF_CPUSLOW_BLENDVPS (1)
#elif defined(__tune_core2__)
 #define RF_CPUVENDOR_INTEL (1)
 #define RF_CPUTUNE_CORE2 (1)
 #define RF_CPUSLOW_BLENDPS (1)
 #define RF_CPUSLOW_BLENDVPS (1)
#elif defined(__tune_atom__)
 #define RF_CPUVENDOR_INTEL (1)
 #define RF_CPUTUNE_ATOM (1)
#elif defined(__tune_nocona__)
 #define RF_CPUVENDOR_INTEL (1)
 #define RF_CPUTUNE_NOCONA (1)
 #define RF_CPUSLOW_SAL (1)
 #define RF_CPUSLOW_SHL (1)
 #define RF_CPUSLOW_SAR (1)
 #define RF_CPUSLOW_SHR (1)
#elif defined(__tune_pentium4__)
 #define RF_CPUVENDOR_INTEL (1)
 #define RF_CPUTUNE_PENTIUM4 (1)
 #define RF_CPUSLOW_SAL (1)
 #define RF_CPUSLOW_SHL (1)
 #define RF_CPUSLOW_SAR (1)
 #define RF_CPUSLOW_SHR (1)
#elif defined(__tune_pentium3__)
 #define RF_CPUVENDOR_INTEL (1)
 #define RF_CPUTUNE_PENTIUM3 (1)
#elif defined(__tune_pentium2__)
 #define RF_CPUVENDOR_INTEL (1)
 #define RF_CPUTUNE_PENTIUM2 (1)
#elif defined(__tune_i686__)
#elif defined(__tune_i586__)
#else
#endif


#endif /* RFCPUTUNE_H */

