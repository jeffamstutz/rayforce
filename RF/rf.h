/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#ifndef RF_H
#define RF_H


#ifdef __cplusplus
extern "C" {
#endif


#define RF_RAYFORCE_VERSION (0x200)


#include "rfdefs.h"


/* Context management */
rfContext *rfCreateContext();
void rfDestroyContext( rfContext *context );
int rfQueryCapability( rfContext *context, rfint capability );
rfint rfGetError( rfContext *context );
void rfErrorHandler( rfContext *context, void (*handler)( void *userpointer, rfint code, char *string ), void *userpointer );
rfPipeline *rfSelectPipeline( rfContext *context, rfPipeline **pipelinelist, rfint pipelinecount );

/* Scene management */
rfScene *rfCreateScene( rfContext *context, rfint target, rfint targetindex, void *targetpointer );
void rfDestroyScene( rfScene *scene );
void rfSceneEnv( rfScene *scene, rfint param, rfint value, void *data );
int rfReadyScene( rfScene *scene );
rfHandle *rfAcquireScene( rfScene *scene );
void *rfTracePath( rfScene *scene, rfHandle *handle, rfPipeline *pipeline );
void rfReleaseScene( rfScene *scene, rfHandle *handle );
void rfGetBoundingBoxf( rfScene *scene, float *box );
void rfGetBoundingBoxd( rfScene *scene, double *box );
rfRoot rfResolveOriginf( rfScene *scene, rfHandle *handle, float *origin );
rfRoot rfResolveOrigind( rfScene *scene, rfHandle *handle, double *origin );

/* Model management */
rfModel *rfCreateModel( rfScene *scene, rfint modeltype );
int rfCopyModel( rfModel *model, rfModel *modelref );
void rfDestroyModel( rfModel *model );
void rfModelEnv( rfModel *model, rfint param, rfint value, void *data );
void rfPrimitiveData( rfModel *model, void *pointer, size_t size, size_t stride );
void rfVertexPointer( rfModel *model, void *pointer, rfint type, size_t stride );
void rfIndexPointer( rfModel *model, void *pointer, rfint type );
void rfModelData( rfModel *model, void *data, size_t datasize );
int rfReadyModel( rfModel *model, size_t primitivecount, rfint flags );
int rfFinishModel( rfModel *model );
void rfClearModel( rfModel *model );
int rfGetModelCache( rfModel *model, void *cache, size_t *cachesize );
int rfModelCache( rfModel *model, void *cache, size_t cachesize );
int rfModelCacheFile( rfModel *model, char *filename );
size_t rfQueryModel( rfModel *model, int property );

/* Object management */
rfObject *rfCreateObject( rfScene *scene );
void rfDestroyObject( rfObject *object );
void rfAttachModel( rfObject *object, rfModel *model );
int rfObjectData( rfObject *object, void *data, size_t datasize );
void rfCullFace( rfObject *object, rfint mode );
void rfLoadMatrixf( rfObject *object, float *m );
void rfLoadMatrixd( rfObject *object, double *m );
int rfEnableObject( rfObject *object );
void rfDisableObject( rfObject *object );



/* NUMA Support Begin */

rfint rfNumaCpuCount( rfContext *context );
rfint rfNumaNodeCount( rfContext *context );
size_t rfNumaSystemMemory( rfContext *context );
int rfNumaQueryNode( rfContext *context, rfint numanode, rfint *nodecpucount, size_t *memory );
int rfNumaGetNodeForCpu( rfContext *context, rfint cpuindex, rfint *numanode );
int rfNumaBindNodeThread( rfContext *context, rfint numanode, rfint index );
int rfNumaBindCpuThread( rfContext *context, rfint cpuindex );
void *rfNumaAlloc( rfContext *context, rfint numanode, size_t size );
void rfNumaFree( rfContext *context, rfint numanode, void *mptr, size_t size );

/* NUMA Support End */



/* CUDA Support Begin */

typedef struct _rfCudaKernel rfCudaKernel;
typedef struct _rfCudaStream rfCudaStream;
typedef struct _rfCudaEvent rfCudaEvent;
typedef struct _rfCudaArray rfCudaArray;

#define RF_CUDA_DEVICE_NAME_MAX (256)

typedef struct
{
  char name[RF_CUDA_DEVICE_NAME_MAX];
  int computemajor;
  int computeminor;
  size_t memory;

  int warpsize;
  int maxblockthreads;
  int maxblockdim[3];
  int maxgriddim[3]; 
  int maxblockregs;
  int clockrate;
  int multiprocessorcount;
  int corespermultiprocessor;
  int corestotal;
  int cachesize;
  int concurrentkernels;
  int runtimelimit;
  int tex1dlinearwidth;

} rfCudaDeviceProp;

rfint rfCudaDeviceCount( rfContext *context );
rfCudaDeviceProp *rfCudaQueryDevice( rfContext *context, rfint device );
int rfCudaQueryMemory( rfContext *context, rfint deviceindex, size_t *contextusage, size_t *totalusage, size_t *freememory, size_t *totalmemory );
int rfCudaGetContext( rfContext *context, rfint device, void *cudacontext ); /* CUcontext *cudacontext */
int rfCudaGetDevice( rfContext *context, rfint device, void *cudadevice ); /* CUdevice *cudadevice */
int rfCudaBindDevice( rfContext *context, rfint deviceindex );
void rfCudaSynchronizeDevice( rfContext *context, rfint device );
rfCudaStream *rfCudaCreateStream( rfContext *context, rfint device );
void rfCudaDestroyStream( rfCudaStream *stream );
void rfCudaSynchronizeStream( rfCudaStream *stream );
int rfCudaQueryStream( rfCudaStream *stream );
void rfCudaStreamWait( rfCudaStream *stream, rfCudaEvent *event );
rfCudaEvent *rfCudaCreateEvent( rfContext *context, rfint device );
void rfCudaDestroyEvent( rfCudaEvent *event );
void rfCudaRecordEvent( rfCudaEvent *event, rfCudaStream *stream );
void rfCudaSynchronizeEvent( rfCudaEvent *event );
int rfCudaQueryEvent( rfCudaEvent *event );
rfCudaKernel *rfCudaCreateKernel( rfContext *context, rfint device, rfint kerneltype );
void rfCudaDestroyKernel( rfCudaKernel *kernel );
void rfCudaKernelConfig( rfCudaKernel *kernel, int blocksizex, int blocksizey, int growxflag, int growyflag, int sharedmemory, int shmemperthread );
int rfCudaLoadKernel( rfCudaKernel *kernel, void *data, int maxregcount );
int rfCudaLoadKernelFile( rfCudaKernel *kernel, const char *filename, int maxregcount );
int rfCudaLaunchKernel( rfScene *scene, rfHandle *handle, rfCudaKernel *kernel, rfCudaStream *stream, int elemx, int elemy, int threadsperblock, void *param );
int rfCudaSetTextureAddress( rfCudaKernel *kernel, char *texname, int format, int components, int clampmodeflag, int linearflag, int normcoordsflag, int readintegerflag, void *address, size_t datasize );
int rfCudaSetTextureArray( rfCudaKernel *kernel, char *texname, int format, int components, int clampmodeflag, int linearflag, int normcoordsflag, int readintegerflag, rfCudaArray *array );
int rfCudaAlloc( rfContext *context, rfint device, void **cudamem, size_t size );
void rfCudaFree( rfContext *context, rfint device, void *cudamem, size_t size );
void rfCudaCopyDtoH( rfContext *context, rfint deviceindex, rfCudaStream *stream, void *sysmem, void *cudamem, size_t size );
void rfCudaCopyHtoD( rfContext *context, rfint deviceindex, rfCudaStream *stream, void *cudamem, void *sysmem, size_t size );
void rfCudaCopyDtoD( rfContext *context, rfint deviceindex, rfCudaStream *stream, void *dstcudamem, void *srccudamem, size_t size );

/* CUDA Support End */



/**/


typedef struct RF_ALIGN16
{
  rfRoot root;
  float RF_ALIGN16 origin[4];
  float RF_ALIGN16 vector[4];
  float clipdist;
  float skipdist;
} rfRaySingle;

typedef struct
{
  rfRoot RF_ALIGN16 root[4];
  float RF_ALIGN16 origin[4*3];
  float RF_ALIGN16 vector[4*3];
  float RF_ALIGN16 clipdist[4];
  float RF_ALIGN16 skipdist[4];
} rfRayQuad RF_ALIGN16;

typedef struct
{
  uint32_t raymask;
  rfRoot RF_ALIGN16 root[4];
  float RF_ALIGN16 origin[4*3];
  float RF_ALIGN16 vector[4*3];
  float RF_ALIGN16 clipdist[4];
  float RF_ALIGN16 skipdist[4];
} rfRayPacket4 RF_ALIGN16;

typedef struct
{
  uint32_t raymask;
  rfRoot RF_ALIGN32 root[8];
  float RF_ALIGN32 origin[8*3];
  float RF_ALIGN32 vector[8*3];
  float RF_ALIGN32 clipdist[8];
  float RF_ALIGN32 skipdist[8];
} rfRayPacket8 RF_ALIGN32;

typedef struct
{
  uint32_t raymask;
  rfRoot RF_ALIGN64 root[16];
  float RF_ALIGN64 origin[16*3];
  float RF_ALIGN64 vector[16*3];
  float RF_ALIGN64 clipdist[16];
  float RF_ALIGN64 skipdist[16];
} rfRayPacket16 RF_ALIGN64;

typedef struct
{
  uint32_t raymask;
  rfRoot RF_ALIGN128 root[32];
  float RF_ALIGN128 origin[32*3];
  float RF_ALIGN128 vector[32*3];
  float RF_ALIGN128 clipdist[32];
  float RF_ALIGN128 skipdist[32];
} rfRayPacket32 RF_ALIGN128;

static inline void rfRaySingleInit( rfRaySingle *raysingle )
{
  raysingle->origin[3] = 1.0;
  raysingle->vector[3] = 0.0;
  return;
}


/**/


enum
{
  RF_FLOAT = 0x1000,
  RF_DOUBLE,
  RF_SHORT,
  RF_USHORT,
  RF_INT,
  RF_UINT,
  RF_INT8,
  RF_UINT8,
  RF_INT16,
  RF_UINT16,
  RF_INT32,
  RF_UINT32,
  RF_INT64,
  RF_UINT64,

  RF_HALF
};


#define RF_TARGET_SYSTEM (0x6000)
#define RF_TARGET_NUMA (0x6001)
#define RF_TARGET_CUDA (0x6002)
#define RF_TARGET_OPENCL (0x6003)


#define RF_MODEL_TYPE_TRIANGLES (0x4000)


#define RF_CAPABILITY_NUMA (0xa000)
#define RF_CAPABILITY_CUDA (0xa001)
#define RF_CAPABILITY_OPENCL (0xa002)

#define RF_CAPABILITY_CPU_CMOV (0xa110)
#define RF_CAPABILITY_CPU_CLFLUSH (0xa111)
#define RF_CAPABILITY_CPU_TSC (0xa112)
#define RF_CAPABILITY_CPU_MMX (0xa113)
#define RF_CAPABILITY_CPU_MMXEXT (0xa114)
#define RF_CAPABILITY_CPU_3DNOW (0xa115)
#define RF_CAPABILITY_CPU_3DNOWEXT (0xa116)
#define RF_CAPABILITY_CPU_SSE (0xa117)
#define RF_CAPABILITY_CPU_SSE2 (0xa118)
#define RF_CAPABILITY_CPU_SSE3 (0xa119)
#define RF_CAPABILITY_CPU_SSSE3 (0xa11a)
#define RF_CAPABILITY_CPU_SSE4P1 (0xa11b)
#define RF_CAPABILITY_CPU_SSE4P2 (0xa11c)
#define RF_CAPABILITY_CPU_SSE4A (0xa11d)
#define RF_CAPABILITY_CPU_AVX (0xa11e)
#define RF_CAPABILITY_CPU_AVX2 (0xa11f)
#define RF_CAPABILITY_CPU_XOP (0xa120)
#define RF_CAPABILITY_CPU_FMA3 (0xa121)
#define RF_CAPABILITY_CPU_FMA4 (0xa122)
#define RF_CAPABILITY_CPU_MISALIGNSSE (0xa123)
#define RF_CAPABILITY_CPU_AES (0xa124)
#define RF_CAPABILITY_CPU_PCLMUL (0xa125)
#define RF_CAPABILITY_CPU_RDRND (0xa126)
#define RF_CAPABILITY_CPU_CMPXCHG16B (0xa127)
#define RF_CAPABILITY_CPU_POPCNT (0xa128)
#define RF_CAPABILITY_CPU_LZCNT (0xa129)
#define RF_CAPABILITY_CPU_MOVBE (0xa12a)
#define RF_CAPABILITY_CPU_RDTSCP (0xa12b)
#define RF_CAPABILITY_CPU_CONSTANTTSC (0xa12c)
#define RF_CAPABILITY_CPU_F16C (0xa12d)
#define RF_CAPABILITY_CPU_BMI (0xa12e)
#define RF_CAPABILITY_CPU_BMI2 (0xa12f)
#define RF_CAPABILITY_CPU_TBM (0xa130)
#define RF_CAPABILITY_CPU_ADX (0xa131)
#define RF_CAPABILITY_CPU_AVX512F (0xa132)
#define RF_CAPABILITY_CPU_AVX512PF (0xa133)
#define RF_CAPABILITY_CPU_AVX512ER (0xa134)
#define RF_CAPABILITY_CPU_AVX512CD (0xa135)
#define RF_CAPABILITY_CPU_SHA (0xa136)

#define RF_CAPABILITY_CPU_CACHELINE_L1CODE (0xa200)
#define RF_CAPABILITY_CPU_CACHESIZE_L1CODE (0xa201)
#define RF_CAPABILITY_CPU_CACHEASSOCIATIVITY_L1CODE (0xa202)
#define RF_CAPABILITY_CPU_CACHESHARED_L1CODE (0xa203)
#define RF_CAPABILITY_CPU_CACHELINE_L1DATA (0xa208)
#define RF_CAPABILITY_CPU_CACHESIZE_L1DATA (0xa209)
#define RF_CAPABILITY_CPU_CACHEASSOCIATIVITY_L1DATA (0xa20a)
#define RF_CAPABILITY_CPU_CACHESHARED_L1DATA (0xa20b)
#define RF_CAPABILITY_CPU_CACHEUNIFIED_L1 (0xa210)
#define RF_CAPABILITY_CPU_CACHEFLUSHSIZE (0xa211)
#define RF_CAPABILITY_CPU_CACHELINE_L2 (0xa220)
#define RF_CAPABILITY_CPU_CACHESIZE_L2 (0xa221)
#define RF_CAPABILITY_CPU_CACHEASSOCIATIVITY_L2 (0xa222)
#define RF_CAPABILITY_CPU_CACHESHARED_L2 (0xa223)
#define RF_CAPABILITY_CPU_CACHELINE_L3 (0xa228)
#define RF_CAPABILITY_CPU_CACHESIZE_L3 (0xa229)
#define RF_CAPABILITY_CPU_CACHEASSOCIATIVITY_L3 (0xa22a)
#define RF_CAPABILITY_CPU_CACHESHARED_L3 (0xa22b)


enum
{
  RF_PROPERTY_GRAPH_MEMORY = 0x5000,
  RF_PROPERTY_GRAPH_COST,
  RF_PROPERTY_SECTOR_COUNT,
  RF_PROPERTY_NODE_COUNT,
  RF_PROPERTY_TRIANGLE_COUNT,
  RF_PROPERTY_TRIREF_COUNT,
  RF_PROPERTY_TRIREF_MAXIMUM
};


enum
{
  RF_CULLMODE_NONE = 0x7000,
  RF_CULLMODE_BACK,
  RF_CULLMODE_FRONT
};


enum
{
  RF_ERROR_NONE = 0x0,
  RF_ERROR_INVALID_VALUE = 0xb000,
  RF_ERROR_INVALID_TARGET,
  RF_ERROR_INVALID_OPERATION,
  RF_ERROR_NOT_SUPPORTED,
  RF_ERROR_MEMORY_DENIED,

  RF_ERROR_NUMA_BAD_OPERATION = 0xb200,
  RF_ERROR_NUMA_MEMORY_DENIED,
  RF_ERROR_NUMA_UNKNOWN,

  RF_ERROR_CUDA_BAD_OPERATION = 0xb400,
  RF_ERROR_CUDA_MEMORY_DENIED,
  RF_ERROR_CUDA_UNKNOWN,
};


#define RF_FLAG_ASYNC (0x1)


#define RF_SCENE_DATATYPE (0xc000)

#define RF_BUILDHINT_QUALITY (0xc200)
#define RF_BUILDHINT_MEMORY  (0xc201)
#define RF_BUILDHINT_LAYOUT  (0xc202)
#define RF_GRAPH_VALUE_FLAG  (0xc204)

#define RF_BUILDHINT_VALUE_MIN (0x0)
#define RF_BUILDHINT_VALUE_MAX (0xff)
#define RF_BUILDHINT_VALUE_DEFAULT (0x180)

#define RF_BUILDHINT_LAYOUTMODE_FULL (0x1)
#define RF_BUILDHINT_LAYOUTMODE_NONE (0x2)
#define RF_BUILDHINT_LAYOUTMODE_DEFAULT RF_BUILDHINT_LAYOUTMODE_FULL

enum
{
  RF_CUDA_KERNEL_TRACE = 0xd000,
  RF_CUDA_KERNEL_QUADTRACE,
  RF_CUDA_KERNEL_RESOLVE
};



#ifdef __cplusplus
}
#endif


#endif /* RF_H */

