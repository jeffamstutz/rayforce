/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#define MM_QUEUE_MONITOR (0)

typedef struct
{
  void *bundlelist;
  size_t itemsize;
  size_t bundlesize;
  float rangefactor;
  float rangefactorinv;
  mmBlockHead bundleblock;
#if MM_QUEUE_MONITOR
  int bundlecount, bundlecountmax;
#endif
} mmQueue;

void mmQueueInit( mmQueue *queue, size_t itemsize, int bundlesize, int blocksize, float bundlerangefactor );
void mmQueueFree( mmQueue *queue );
void mmQueueAdd( mmQueue *queue, void *item, float value );
int mmQueueGet( mmQueue *queue, void *item );


