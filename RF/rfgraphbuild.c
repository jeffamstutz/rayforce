/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "cpuconfig.h"

#include "cc.h"
#include "mm.h"
#include "mmdirectory.h"

#include "rfmath.h"

#include "rf.h"
#include "rfdefs.h"
#include "rfdata.h"
#include "rfinternal.h"
#include "rfgraph.h"
#include "rfgraphbuild.h"

#ifndef RF_GRAPH_WRITE_LOG
 #define RF_GRAPH_WRITE_LOG (0)
#endif


#define RF_GRAPH_PACKED16_DISTMAX (((((long long)1)<<15)-1)<<RF_GRAPH_LINK16_SHIFT)
#define RF_GRAPH_PACKED32_DISTMAX (((((long long)1)<<31)-1)<<RF_GRAPH_LINK32_SHIFT)


////


/* Build graph from cache */
static int buildGraphMemoryNoLayout( rfGraph *graph, rfGraphCache *cache, rfGeometry *geometry )
{
  int addressmode, childindex, edgeindex, triindex;
  ssize_t index;
  size_t tridatasize, trifullsize, sectorsize, trirefsize;
  size_t headermem, originmem, trianglemem, nodemem, sectormem, totalmem;
  size_t headeroffset, originoffset, triangleoffset, nodeoffset, sectoroffset, modeldataoffset;
  ssize_t offset, targetoffset;
  rfTri *tri, *triend, *cachetri;
  size_t *sectortable;
  rfPrepNode *prepnode;
  rfNode16 *node16;
  rfNode32 *node32;
  rfNode64 *node64;
  rfPrepSector *prepsector;
  rfSector16 *sector16;
  rfSector32 *sector32;
  rfSector64 *sector64;
  rfPrepLink *preptrilist;
  int16_t *trilist16;
  int32_t *trilist32;
  int64_t *trilist64;
  rforigin *originlist;
  rfGraphHeader *header;

  /* Size of user-defined per-primitive data */
  tridatasize = geometry->datasize;
  trifullsize = RF_GRAPH_SIZE_ALIGN( sizeof(rfTri) + tridatasize );

  /* In the absence of graph layout information, compute required memory and pick addressing mode */
  trianglemem = RF_SIZE_ROUND64( cache->trianglecount * trifullsize );
  nodemem = RF_SIZE_ROUND64( cache->nodecount * RF_NODE16_SIZE );
  sectormem = RF_SIZE_ROUND64( cache->sector16size );
  totalmem = trianglemem + nodemem + sectormem;
  if( totalmem < RF_GRAPH_PACKED16_DISTMAX )
  {
    addressmode = RF_GRAPH_ADDRESSMODE_16BITS;
    sectorsize = sizeof(rfSector16);
    trirefsize = sizeof(int16_t);
  }
  else
  {
    nodemem = RF_SIZE_ROUND64( cache->nodecount * RF_NODE32_SIZE );
    sectormem = RF_SIZE_ROUND64( cache->sector32size );
    totalmem = RF_SIZE_ROUND64( trianglemem + nodemem + sectormem );
    if( totalmem < RF_GRAPH_PACKED32_DISTMAX )
    {
      addressmode = RF_GRAPH_ADDRESSMODE_32BITS;
      sectorsize = sizeof(rfSector32);
      trirefsize = sizeof(int32_t);
    }
    else
    {
      nodemem = RF_SIZE_ROUND64( cache->nodecount * RF_NODE64_SIZE );
      sectormem = RF_SIZE_ROUND64( cache->sector64size );
      totalmem = RF_SIZE_ROUND64( trianglemem + nodemem + sectormem );
      addressmode = RF_GRAPH_ADDRESSMODE_64BITS;
      sectorsize = sizeof(rfSector64);
      trirefsize = sizeof(int64_t);
    }
  }
  headermem = RF_GRAPHHEADER_SIZE;
  originmem = RF_SIZE_ROUND64( sizeof(rfOriginTable) + ( cache->origintable.totalcount * sizeof(rforigin) ) );
  totalmem += headermem + originmem + geometry->modeldatasize;

  /* Compute offsets */
  headeroffset = 0;
  originoffset = headermem;
  triangleoffset = headermem + originmem;
  nodeoffset = headermem + originmem + trianglemem;
  sectoroffset = headermem + originmem + trianglemem + nodemem;
  modeldataoffset = headermem + originmem + trianglemem + nodemem + sectormem;

  /*
  FILE* file = fopen("nolayout.txt", "w");
  fprintf(file, "\n");
  fprintf(file, "headermem     = %lld\n", headermem);
  fprintf(file, "originmem     = %lld\n", originmem);
  fprintf(file, "trianglemem   = %lld\n", trianglemem);
  fprintf(file, "nodemem       = %lld\n", nodemem);
  fprintf(file, "sectormem     = %lld\n", sectormem);
  fprintf(file, "modeldatasize = %lld\n", geometry->modeldatasize);
  fprintf(file, "totalmem      = %lld\n", totalmem);
  fprintf(file, "\n");
  fprintf(file, "headeroffset    = %lld\n", headeroffset);
  fprintf(file, "originoffset    = %lld\n", originoffset);
  fprintf(file, "triangleoffset  = %lld\n", triangleoffset);
  fprintf(file, "nodeoffset      = %lld\n", nodeoffset);
  fprintf(file, "sectoroffset    = %lld\n", sectoroffset);
  fprintf(file, "modeldataoffset = %lld\n", modeldataoffset);
  fprintf(file, "\n");
  */

  /* Allocate graph memory */
  graph->memsize = RF_GRAPH_ROUND_SIZE(totalmem);
  graph->memory = 0;
  if( graph->numanode >= 0 )
    graph->memory = mmNodeAlloc( graph->numanode, graph->memsize );
  else
    graph->memory = malloc( graph->memsize );
  if( !( graph->memory ) )
    return 0;

  /* Build sector address table */
  sectortable = malloc( cache->sectorcount * sizeof(size_t) );
  offset = sectoroffset;
  for( index = 0 ; index < cache->sectorcount ; index++ )
  {
    prepsector = mmDirectoryGetFast( &cache->sectordir, index );
    sectortable[index] = offset;
    offset += RF_GRAPH_SIZE_ALIGN( sectorsize + ( prepsector->tricount * trirefsize ) );
  }

  /* Store triangles */
#if 0
  void *tridata;
  cachetri = cache->trilist;
  tridata = geometry->datapointer;
  tri = ADDRESS( graph->memory, triangleoffset );
  triend = ADDRESS( tri, cache->trianglecount * trifullsize );
  for( ; tri < triend ; cachetri++, tri = ADDRESS( tri, trifullsize ) )
  {
    *tri = *cachetri;
    if( tridatasize )
      memcpy( ADDRESS( tri, sizeof(rfTri) ), tridata, tridatasize );
    tridata = ADDRESS( tridata, geometry->datastride );
  }
#else
  cachetri = cache->trilist;
  tri = ADDRESS( graph->memory, triangleoffset );
  triend = ADDRESS( tri, cache->trianglecount * trifullsize );
  index = 0;
  for( ; tri < triend ; cachetri++, tri = ADDRESS( tri, trifullsize ), index++ )
  {
    *tri = *cachetri;
    if( tridatasize )
      memcpy( ADDRESS( tri, sizeof(rfTri) ), ADDRESS( geometry->datapointer, cache->tridataoffset[index] ), tridatasize );
    /*
    fprintf(file,
            "tri[%lld]\n  %f %f %f %f\n  %f %f %f %f\n  %f %f %f %f\n  %lld : (%lld)\n",
            index,
            tri->plane[0], tri->plane[1], tri->plane[2], tri->plane[3],
            tri->edpu [0], tri->edpu [1], tri->edpu [2], tri->edpu [3],
            tri->edpv [0], tri->edpv [1], tri->edpv [2], tri->edpv [3],
            *((uint32_t*)(tri+1)), trifullsize);
    */
  }
#endif

  /* Store nodes */
  offset = nodeoffset;
  if( addressmode == RF_GRAPH_ADDRESSMODE_16BITS )
  {
    fprintf(stderr, "WARNING %s:%d\n", __FILE__, __LINE__ );
    fprintf(stderr, "  NONE : not using rfNode32\n");

    for( index = 0 ; index < cache->nodecount ; index++, offset += RF_NODE16_SIZE )
    {
      prepnode = mmDirectoryGetFast( &cache->nodedir, index );
      node16 = ADDRESS( graph->memory, offset );
      node16->flags = prepnode->nodeflags;
      node16->plane = prepnode->plane;
      for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
      {
        if( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) )
          targetoffset = sectortable[ ((rfPrepSector *)prepnode->child[childindex].p)->index ];
        else
          targetoffset = nodeoffset + ( ((rfPrepNode *)prepnode->child[childindex].p)->index * RF_NODE16_SIZE );
        node16->link[childindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK16_SHIFT;
      }
    }
  }
  else if( addressmode == RF_GRAPH_ADDRESSMODE_32BITS )
  {
    for( index = 0 ; index < cache->nodecount ; index++, offset += RF_NODE32_SIZE )
    {
      prepnode = mmDirectoryGetFast( &cache->nodedir, index );
      node32 = ADDRESS( graph->memory, offset );
      node32->flags = prepnode->nodeflags;
      node32->plane = prepnode->plane;
      for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
      {
        if( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) )
          targetoffset = sectortable[ ((rfPrepSector *)prepnode->child[childindex].p)->index ];
        else
          targetoffset = nodeoffset + ( ((rfPrepNode *)prepnode->child[childindex].p)->index * RF_NODE32_SIZE );
        node32->link[childindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK32_SHIFT;
      }
      /*
      fprintf(file, "node[%lld] --> %f : etc.\n", index, node32->plane);
      */
    }
  }
  else
  {
    fprintf(stderr, "WARNING %s:%d\n", __FILE__, __LINE__ );
    fprintf(stderr, "  NONE : not using rfNode32\n");

    for( index = 0 ; index < cache->nodecount ; index++, offset += RF_NODE64_SIZE )
    {
      prepnode = mmDirectoryGetFast( &cache->nodedir, index );
      node64 = ADDRESS( graph->memory, offset );
      node64->flags = prepnode->nodeflags;
      node64->plane = prepnode->plane;
      for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
      {
        if( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) )
          targetoffset = sectortable[ ((rfPrepSector *)prepnode->child[childindex].p)->index ];
        else
          targetoffset = nodeoffset + ( ((rfPrepNode *)prepnode->child[childindex].p)->index * RF_NODE64_SIZE );
        node64->link[childindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK64_SHIFT;
      }
    }
  }

  /* Store sectors */
  if( addressmode == RF_GRAPH_ADDRESSMODE_16BITS )
  {
    fprintf(stderr, "WARNING %s:%d\n", __FILE__, __LINE__ );
    fprintf(stderr, "  NONE : not using rfSector32\n");

    for( index = 0 ; index < cache->sectorcount ; index++ )
    {
      offset = sectortable[index];
      prepsector = mmDirectoryGetFast( &cache->sectordir, index );
      sector16 = ADDRESS( graph->memory, offset );
      memcpy( sector16->edge, prepsector->edge, RF_EDGE_COUNT * sizeof(rff) );
      RF_SECTOR_SET_FLAGSPRIMCOUNT( sector16, prepsector->flags, prepsector->tricount );
      for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
      {
        if( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) )
        {
          targetoffset = offset;
          if( prepsector->link[edgeindex].p )
            targetoffset = sectortable[ ((rfPrepSector *)prepsector->link[edgeindex].p)->index ];
        }
        else
          targetoffset = nodeoffset + ( ((rfPrepNode *)prepsector->link[edgeindex].p)->index * RF_NODE16_SIZE );
        sector16->link[edgeindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK16_SHIFT;
      }
      preptrilist = PREP_SECTOR_TRILIST( prepsector );
      trilist16 = RF_SECTOR16_TRILIST( sector16 );
      for( triindex = 0 ; triindex < prepsector->tricount ; triindex++ )
      {
        targetoffset = triangleoffset + ( preptrilist[triindex].i * trifullsize );
        trilist16[triindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK16_SHIFT;
      }
    }
  }
  else if( addressmode == RF_GRAPH_ADDRESSMODE_32BITS )
  {
    for( index = 0 ; index < cache->sectorcount ; index++ )
    {
      offset = sectortable[index];
      prepsector = mmDirectoryGetFast( &cache->sectordir, index );
      sector32 = ADDRESS( graph->memory, offset );
      memcpy( sector32->edge, prepsector->edge, RF_EDGE_COUNT * sizeof(rff) );
      RF_SECTOR_SET_FLAGSPRIMCOUNT( sector32, prepsector->flags, prepsector->tricount );
      for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
      {
        if( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) )
        {
          targetoffset = offset;
          if( prepsector->link[edgeindex].p )
            targetoffset = sectortable[ ((rfPrepSector *)prepsector->link[edgeindex].p)->index ];
        }
        else
          targetoffset = nodeoffset + ( ((rfPrepNode *)prepsector->link[edgeindex].p)->index * RF_NODE32_SIZE );
        sector32->link[edgeindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK32_SHIFT;
      }
      preptrilist = PREP_SECTOR_TRILIST( prepsector );
      trilist32 = RF_SECTOR32_TRILIST( sector32 );
      for( triindex = 0 ; triindex < prepsector->tricount ; triindex++ )
      {
        targetoffset = triangleoffset + ( preptrilist[triindex].i * trifullsize );
        trilist32[triindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK32_SHIFT;
      }
      /*
      fprintf(file, "sector[%lld] --> %lld : %lld (%lld) : etc.\n", index,
              sector32->primcount, trilist32[0], trilist32[0] << RF_GRAPH_LINK32_SHIFT);
      */
    }
  }
  else
  {
    fprintf(stderr, "WARNING %s:%d\n", __FILE__, __LINE__ );
    fprintf(stderr, "  NONE : not using rfSector32\n");

    for( index = 0 ; index < cache->sectorcount ; index++ )
    {
      offset = sectortable[index];
      prepsector = mmDirectoryGetFast( &cache->sectordir, index );
      sector64 = ADDRESS( graph->memory, offset );
      memcpy( sector64->edge, prepsector->edge, RF_EDGE_COUNT * sizeof(rff) );
      RF_SECTOR_SET_FLAGSPRIMCOUNT( sector64, prepsector->flags, prepsector->tricount );
      for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
      {
        if( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) )
        {
          targetoffset = offset;
          if( prepsector->link[edgeindex].p )
            targetoffset = sectortable[ ((rfPrepSector *)prepsector->link[edgeindex].p)->index ];
        }
        else
          targetoffset = nodeoffset + ( ((rfPrepNode *)prepsector->link[edgeindex].p)->index * RF_NODE64_SIZE );
        sector64->link[edgeindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK64_SHIFT;
      }
      preptrilist = PREP_SECTOR_TRILIST( prepsector );
      trilist64 = RF_SECTOR64_TRILIST( sector64 );
      for( triindex = 0 ; triindex < prepsector->tricount ; triindex++ )
      {
        targetoffset = triangleoffset + ( preptrilist[triindex].i * trifullsize );
        trilist64[triindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK64_SHIFT;
      }
    }
  }

  /* Record the real data size */
  graph->datasize = graph->memsize;

  /* Store origins */
  graph->origintable = ADDRESS( graph->memory, originoffset );
  *(rfOriginTable *)graph->origintable = cache->origintable;
  originlist = RF_ORIGINTABLE_LIST( graph->origintable );
  for( index = 0 ; index < cache->origintable.totalcount ; index++ )
    originlist[index] = sectortable[ cache->origin[index] ];

  /* Store user model data */
  if( geometry->modeldatasize )
    memcpy( ADDRESS( graph->memory, modeldataoffset ), geometry->modeldata, geometry->modeldatasize );

  /* Store graph header */
  header = ADDRESS( graph->memory, headeroffset );
  memset( header->identifier, 0, RF_GRAPH_HEADER_IDENTIFIER_LENGTH );
  strncpy( header->identifier, RF_CACHE_IDENTIFIER, RF_GRAPH_HEADER_IDENTIFIER_LENGTH );
  header->graphtype = RF_GRAPHEADER_TYPE_TRIANGLES;
  header->cacheversion = RF_CACHE_VERSION;
  header->rfversion = RF_RAYFORCE_VERSION;
  header->headersize = headermem;
  switch( addressmode )
  {
    case RF_GRAPH_ADDRESSMODE_16BITS:
      header->sectoraddrbits = 16;
      header->nodeaddrbits = 16;
      header->trirefaddrbits = 16;
      header->addrshift = RF_GRAPH_LINK16_SHIFT;
      break;
    case RF_GRAPH_ADDRESSMODE_32BITS:
      header->sectoraddrbits = 32;
      header->nodeaddrbits = 32;
      header->trirefaddrbits = 32;
      header->addrshift = RF_GRAPH_LINK32_SHIFT;
      break;
    case RF_GRAPH_ADDRESSMODE_64BITS:
      header->sectoraddrbits = 64;
      header->nodeaddrbits = 64;
      header->trirefaddrbits = 64;
      header->addrshift = RF_GRAPH_LINK64_SHIFT;
      break;
    default:
      fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
      abort();
  }
  header->alignment = RF_GRAPH_ALIGNMENT;
  header->primdatasize = tridatasize;
  header->rffwidth = sizeof(rff);
  header->graphflags = RF_GRAPH_FLAGS_PACKED;
  header->sectorprimitivemax = cache->sectortrimax;
  header->edge[0] = cache->edge[0];
  header->edge[1] = cache->edge[1];
  header->edge[2] = cache->edge[2];
  header->edge[3] = cache->edge[3];
  header->edge[4] = cache->edge[4];
  header->edge[5] = cache->edge[5];
  header->mediansized = (1.0/3.0) * ( ( cache->edge[RF_EDGE_MAXX] - cache->edge[RF_EDGE_MINX] ) + ( cache->edge[RF_EDGE_MAXY] - cache->edge[RF_EDGE_MINY] ) + ( cache->edge[RF_EDGE_MAXZ] - cache->edge[RF_EDGE_MINZ] ) );
  header->mediansizef = header->mediansized;

  header->sectorcount = cache->sectorcount;
  header->nodecount = cache->nodecount;
  header->primitivecount = cache->trianglecount;
  header->graphdatasize = graph->datasize;
  header->modeldataoffset = modeldataoffset;
  header->modeldatasize = geometry->modeldatasize;

  /* Finish graph */
  free( sectortable );
  graph->flags = RF_GRAPH_FLAGS_PACKED;
  graph->addressmode = addressmode;
  memcpy( graph->edge, cache->edge, RF_EDGE_COUNT*sizeof(rff) );

  /*
  fclose(file);
  */

  return 1;
}


////


/* Build graph from cache */
static int buildGraphMemoryLayout( rfGraph *graph, rfGraphCache *cache, rfGeometry *geometry )
{
  int childindex, edgeindex, triindex;
  ssize_t index;
  size_t tridatasize, trifullsize;
  size_t headermem, originmem, totalmem;
  size_t headeroffset, originoffset, graphoffset, modeldataoffset;
  ssize_t offset, targetoffset;
  rfTri *tri, *cachetri;
  void *graphbase;
  void *link;
  rfPrepNode *prepnode;
  rfNode16 *node16;
  rfNode32 *node32;
  rfNode64 *node64;
  rfPrepSector *prepsector;
  rfSector16 *sector16;
  rfSector32 *sector32;
  rfSector64 *sector64;
  rfPrepLink *preptrilist;
  int16_t *trilist16;
  int32_t *trilist32;
  int64_t *trilist64;
  rforigin *originlist;
  rfGraphHeader *header;

  /* Size of user-defined per-primitive data */
  tridatasize = geometry->datasize;
  trifullsize = RF_GRAPH_SIZE_ALIGN( sizeof(rfTri) + tridatasize );

  headermem = RF_GRAPHHEADER_SIZE;
  originmem = RF_SIZE_ROUND64( sizeof(rfOriginTable) + ( cache->origintable.totalcount * sizeof(rforigin) ) );
  totalmem = headermem + originmem + geometry->modeldatasize + cache->layoutmemory;

  /* Compute offsets */
  headeroffset = 0;
  originoffset = headermem;
  graphoffset = headermem + originmem;
  modeldataoffset = headermem + originmem + cache->layoutmemory;

  /* Allocate graph memory */
  graph->memsize = RF_GRAPH_ROUND_SIZE(totalmem);
  graph->memory = 0;
  if( graph->numanode >= 0 )
    graph->memory = mmNodeAlloc( graph->numanode, graph->memsize );
  else
    graph->memory = malloc( graph->memsize );
  if( !( graph->memory ) )
    return 0;
  graphbase = ADDRESS( graph->memory, graphoffset );

  /* Store triangles */
#if 0
  void *tridata;
  cachetri = cache->trilist;
  tridata = geometry->datapointer;
  for( index = 0 ; index < cache->trianglecount ; index++, cachetri++ )
  {
    tri = ADDRESS( graphbase, ( cache->triangleoffset[index] << RF_GRAPH_OFFSET_SHIFT ) );
    *tri = *cachetri;
    if( tridatasize )
      memcpy( ADDRESS( tri, sizeof(rfTri) ), tridata, tridatasize );
    tridata = ADDRESS( tridata, geometry->datastride );
  }
#else
  cachetri = cache->trilist;
  for( index = 0 ; index < cache->trianglecount ; index++, cachetri++ )
  {
    tri = ADDRESS( graphbase, ( cache->triangleoffset[index] << RF_GRAPH_OFFSET_SHIFT ) );
    *tri = *cachetri;
    if( tridatasize )
      memcpy( ADDRESS( tri, sizeof(rfTri) ), ADDRESS( geometry->datapointer, cache->tridataoffset[index] ), tridatasize );
  }
#endif

  /* Store nodes */
  if( cache->layoutaddressmode == RF_GRAPH_ADDRESSMODE_16BITS )
  {
    for( index = 0 ; index < cache->nodecount ; index++ )
    {
      offset = (rfsize)cache->nodeoffset[index] << RF_GRAPH_OFFSET_SHIFT;
      prepnode = mmDirectoryGetFast( &cache->nodedir, index );
      node16 = ADDRESS( graphbase, offset );
      node16->flags = prepnode->nodeflags;
      node16->plane = prepnode->plane;
      for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
      {
        link = prepnode->child[childindex].p;
        if( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) )
          targetoffset = (rfsize)cache->sectoroffset[ ((rfPrepSector *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        else
          targetoffset = (rfsize)cache->nodeoffset[ ((rfPrepNode *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        node16->link[childindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK16_SHIFT;
      }
    }
  }
  else if( cache->layoutaddressmode == RF_GRAPH_ADDRESSMODE_32BITS )
  {
    for( index = 0 ; index < cache->nodecount ; index++ )
    {
      offset = (rfsize)cache->nodeoffset[index] << RF_GRAPH_OFFSET_SHIFT;
      prepnode = mmDirectoryGetFast( &cache->nodedir, index );
      node32 = ADDRESS( graphbase, offset );
      node32->flags = prepnode->nodeflags;
      node32->plane = prepnode->plane;
      for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
      {
        link = prepnode->child[childindex].p;
        if( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) )
          targetoffset = (rfsize)cache->sectoroffset[ ((rfPrepSector *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        else
          targetoffset = (rfsize)cache->nodeoffset[ ((rfPrepNode *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        node32->link[childindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK32_SHIFT;
      }
    }
  }
  else
  {
    for( index = 0 ; index < cache->nodecount ; index++ )
    {
      offset = (rfsize)cache->nodeoffset[index] << RF_GRAPH_OFFSET_SHIFT;
      prepnode = mmDirectoryGetFast( &cache->nodedir, index );
      node64 = ADDRESS( graphbase, offset );
      node64->flags = prepnode->nodeflags;
      node64->plane = prepnode->plane;
      for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
      {
        link = prepnode->child[childindex].p;
        if( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) )
          targetoffset = (rfsize)cache->sectoroffset[ ((rfPrepSector *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        else
          targetoffset = (rfsize)cache->nodeoffset[ ((rfPrepNode *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        node64->link[childindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK64_SHIFT;
      }
    }
  }

  /* Store sectors */
  if( cache->layoutaddressmode == RF_GRAPH_ADDRESSMODE_16BITS )
  {
    fprintf(stderr, "WARNING %s:%d\n", __FILE__, __LINE__ );
    fprintf(stderr, "  FULL : not using rfSector32\n");

    for( index = 0 ; index < cache->sectorcount ; index++ )
    {
      offset = (rfsize)cache->sectoroffset[index] << RF_GRAPH_OFFSET_SHIFT;
      prepsector = mmDirectoryGetFast( &cache->sectordir, index );
      sector16 = ADDRESS( graphbase, offset );
      memcpy( sector16->edge, prepsector->edge, RF_EDGE_COUNT * sizeof(rff) );
      RF_SECTOR_SET_FLAGSPRIMCOUNT( sector16, prepsector->flags, prepsector->tricount );
      for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
      {
        link = prepsector->link[edgeindex].p;
        if( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) )
        {
          targetoffset = offset;
          if( link )
            targetoffset = (rfsize)cache->sectoroffset[ ((rfPrepSector *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        }
        else
          targetoffset = (rfsize)cache->nodeoffset[ ((rfPrepNode *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        sector16->link[edgeindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK16_SHIFT;
      }
      preptrilist = PREP_SECTOR_TRILIST( prepsector );
      trilist16 = RF_SECTOR16_TRILIST( sector16 );
      for( triindex = 0 ; triindex < prepsector->tricount ; triindex++ )
      {
        targetoffset = (rfsize)cache->triangleoffset[ preptrilist[triindex].i ] << RF_GRAPH_OFFSET_SHIFT;
        trilist16[triindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK16_SHIFT;
      }
    }
  }
  else if( cache->layoutaddressmode == RF_GRAPH_ADDRESSMODE_32BITS )
  {
    for( index = 0 ; index < cache->sectorcount ; index++ )
    {
      offset = (rfsize)cache->sectoroffset[index] << RF_GRAPH_OFFSET_SHIFT;
      prepsector = mmDirectoryGetFast( &cache->sectordir, index );
      sector32 = ADDRESS( graphbase, offset );
      memcpy( sector32->edge, prepsector->edge, RF_EDGE_COUNT * sizeof(rff) );
      RF_SECTOR_SET_FLAGSPRIMCOUNT( sector32, prepsector->flags, prepsector->tricount );
      for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
      {
        link = prepsector->link[edgeindex].p;
        if( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) )
        {
          targetoffset = offset;
          if( link )
            targetoffset = (rfsize)cache->sectoroffset[ ((rfPrepSector *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        }
        else
          targetoffset = (rfsize)cache->nodeoffset[ ((rfPrepNode *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        sector32->link[edgeindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK32_SHIFT;
      }
      preptrilist = PREP_SECTOR_TRILIST( prepsector );
      trilist32 = RF_SECTOR32_TRILIST( sector32 );
      for( triindex = 0 ; triindex < prepsector->tricount ; triindex++ )
      {
        targetoffset = (rfsize)cache->triangleoffset[ preptrilist[triindex].i ] << RF_GRAPH_OFFSET_SHIFT;
        trilist32[triindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK32_SHIFT;
      }
    }
  }
  else
  {
    fprintf(stderr, "WARNING %s:%d\n", __FILE__, __LINE__ );
    fprintf(stderr, "  FULL : not using rfSector32\n");

    for( index = 0 ; index < cache->sectorcount ; index++ )
    {
      offset = (rfsize)cache->sectoroffset[index] << RF_GRAPH_OFFSET_SHIFT;
      prepsector = mmDirectoryGetFast( &cache->sectordir, index );
      sector64 = ADDRESS( graphbase, offset );
      memcpy( sector64->edge, prepsector->edge, RF_EDGE_COUNT * sizeof(rff) );
      RF_SECTOR_SET_FLAGSPRIMCOUNT( sector64, prepsector->flags, prepsector->tricount );
      for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
      {
        link = prepsector->link[edgeindex].p;
        if( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) )
        {
          targetoffset = offset;
          if( link )
            targetoffset = (rfsize)cache->sectoroffset[ ((rfPrepSector *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        }
        else
          targetoffset = (rfsize)cache->nodeoffset[ ((rfPrepNode *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        sector64->link[edgeindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK64_SHIFT;
      }
      preptrilist = PREP_SECTOR_TRILIST( prepsector );
      trilist64 = RF_SECTOR64_TRILIST( sector64 );
      for( triindex = 0 ; triindex < prepsector->tricount ; triindex++ )
      {
        targetoffset = (rfsize)cache->triangleoffset[ preptrilist[triindex].i ] << RF_GRAPH_OFFSET_SHIFT;
        trilist64[triindex] = ( targetoffset - offset ) >> RF_GRAPH_LINK64_SHIFT;
      }
    }
  }

  /* Record the real data size */
  graph->datasize = graph->memsize;

  /* Store origins */
  graph->origintable = ADDRESS( graph->memory, originoffset );
  *(rfOriginTable *)graph->origintable = cache->origintable;
  originlist = RF_ORIGINTABLE_LIST( graph->origintable );
  for( index = 0 ; index < cache->origintable.totalcount ; index++ )
    originlist[index] = graphoffset + ( cache->sectoroffset[ cache->origin[index] ] << RF_GRAPH_OFFSET_SHIFT );

  /* Store user model data */
  if( geometry->modeldatasize )
    memcpy( ADDRESS( graph->memory, modeldataoffset ), geometry->modeldata, geometry->modeldatasize );

  /* Store graph header */
  header = ADDRESS( graph->memory, headeroffset );
  memset( header->identifier, 0, RF_GRAPH_HEADER_IDENTIFIER_LENGTH );
  strncpy( header->identifier, RF_CACHE_IDENTIFIER, RF_GRAPH_HEADER_IDENTIFIER_LENGTH );
  header->graphtype = RF_GRAPHEADER_TYPE_TRIANGLES;
  header->cacheversion = RF_CACHE_VERSION;
  header->rfversion = RF_RAYFORCE_VERSION;
  header->headersize = headermem;
  switch( cache->layoutaddressmode )
  {
    case RF_GRAPH_ADDRESSMODE_16BITS:
      header->sectoraddrbits = 16;
      header->nodeaddrbits = 16;
      header->trirefaddrbits = 16;
      header->addrshift = RF_GRAPH_LINK16_SHIFT;
      break;
    case RF_GRAPH_ADDRESSMODE_32BITS:
      header->sectoraddrbits = 32;
      header->nodeaddrbits = 32;
      header->trirefaddrbits = 32;
      header->addrshift = RF_GRAPH_LINK32_SHIFT;
      break;
    case RF_GRAPH_ADDRESSMODE_64BITS:
      header->sectoraddrbits = 64;
      header->nodeaddrbits = 64;
      header->trirefaddrbits = 64;
      header->addrshift = RF_GRAPH_LINK64_SHIFT;
      break;
    default:
      fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
      abort();
  }
  header->alignment = RF_GRAPH_ALIGNMENT;
  header->primdatasize = tridatasize;
  header->rffwidth = sizeof(rff);
  header->graphflags = RF_GRAPH_FLAGS_PACKED;
  header->sectorprimitivemax = cache->sectortrimax;
  header->edge[0] = cache->edge[0];
  header->edge[1] = cache->edge[1];
  header->edge[2] = cache->edge[2];
  header->edge[3] = cache->edge[3];
  header->edge[4] = cache->edge[4];
  header->edge[5] = cache->edge[5];
  header->mediansized = (1.0/3.0) * ( ( cache->edge[RF_EDGE_MAXX] - cache->edge[RF_EDGE_MINX] ) + ( cache->edge[RF_EDGE_MAXY] - cache->edge[RF_EDGE_MINY] ) + ( cache->edge[RF_EDGE_MAXZ] - cache->edge[RF_EDGE_MINZ] ) );
  header->mediansizef = header->mediansized;

  header->sectorcount = cache->sectorcount;
  header->nodecount = cache->nodecount;
  header->primitivecount = cache->trianglecount;
  header->graphdatasize = graph->datasize;
  header->modeldataoffset = modeldataoffset;
  header->modeldatasize = geometry->modeldatasize;

  /* Finish graph */
  graph->flags = RF_GRAPH_FLAGS_INTERLEAVED;
  graph->addressmode = cache->layoutaddressmode;
  memcpy( graph->edge, cache->edge, RF_EDGE_COUNT*sizeof(rff) );

  return 1;
}


////


int rfBuildGraphMemory( rfContext *context, rfGraph *graph, rfGraphCache *cache, rfGeometry *geometry, rfint target, rfint targetindex, void *targetpointer )
{
  int retval;
  void *cudamem;

  /* Set graph storage */
  graph->target = target;
  graph->numanode = -1;
  graph->deviceindex = -1;
  if( target == RF_TARGET_NUMA )
  {
    if( (unsigned)targetindex >= mmcontext.nodecount )
      return 0;
    graph->numanode = targetindex;
  }

  /* We aren't building graph layouts yet, so create a packed graph */
  switch( cache->layoutmode )
  {
    case RF_GRAPHCACHE_LAYOUTMODE_NONE:
      retval = buildGraphMemoryNoLayout( graph, cache, geometry );
      break;
    case RF_GRAPHCACHE_LAYOUTMODE_FULL:
      retval = buildGraphMemoryLayout( graph, cache, geometry );
      break;
    default:
      fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
      abort();
  }

#if RF_GRAPH_WRITE_LOG
  rfGraphWriteLog(cache, "unused");
#endif // RF_GRAPH_WRITE_LOG


  /* Copy over to CUDA, we can't build graphs directly on CUDA yet */
  if( target == RF_TARGET_CUDA )
  {
#if RF_CONFIG_CUDA_SUPPORT
    cudamem = rfCudaAllocGraph( context, targetindex, graph->memory, graph->memsize, 0 );
    free( graph->memory );
    graph->memory = cudamem;
    if( !( cudamem ) )
      return 0;
    graph->deviceindex = targetindex;
#else
    return 0;
#endif
  }

  return retval;
}


void rfFreeGraphMemory( rfContext *context, rfGraph *graph )
{
  switch( graph->target )
  {
    case RF_TARGET_SYSTEM:
      free( graph->memory );
      break;
    case RF_TARGET_NUMA:
      mmNodeFree( graph->numanode, graph->memory, graph->memsize );
      break;
    case RF_TARGET_CUDA:
#if RF_CONFIG_CUDA_SUPPORT
      rfCudaFreeGraph( context, graph->deviceindex, graph->memory, graph->memsize );
#endif
      break;
    default:
      fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
      abort();
  }
  graph->memory = 0;
  return;
}


////


#if RF_GRAPH_WRITE_LOG

static inline void getTrianglePoints( rfTri *tri, rff *pt )
{
#if RF_USE_FLOAT
  double f, p[12], ptd[3];
  copy4rff2d( &p[0], tri->plane );
  copy4rff2d( &p[4], tri->edpu );
  copy4rff2d( &p[8], tri->edpv );
  rfMathPlaneIntersect3d( ptd, &p[0], &p[4], &p[8] );
  copy3d2rff( &pt[2*RF_AXIS_COUNT], ptd );
  f = p[4+3];
  p[4+3] -= 1.0;
  rfMathPlaneIntersect3d( ptd, &p[0], &p[4], &p[8] );
  copy3d2rff( &pt[0*RF_AXIS_COUNT], ptd );
  p[4+3] = f;
  p[8+3] -= 1.0;
  rfMathPlaneIntersect3d( ptd, &p[0], &p[4], &p[8] );
  copy3d2rff( &pt[1*RF_AXIS_COUNT], ptd );
#else
  rff p[4];
  rfMathPlaneIntersect3d( &pt[2*RF_AXIS_COUNT], tri->plane, tri->edpu, tri->edpv );
  p[0] = tri->edpu[0];
  p[1] = tri->edpu[1];
  p[2] = tri->edpu[2];
  p[3] = tri->edpu[3] - 1.0;
  rfMathPlaneIntersect3d( &pt[0*RF_AXIS_COUNT], tri->plane, p, tri->edpv );
  p[0] = tri->edpv[0];
  p[1] = tri->edpv[1];
  p[2] = tri->edpv[2];
  p[3] = tri->edpv[3] - 1.0;
  rfMathPlaneIntersect3d( &pt[1*RF_AXIS_COUNT], tri->plane, tri->edpu, p );
#endif
  return;
}

#define RF_GRAPH_WRITE_ORIGINTABLE (1)
#define RF_GRAPH_WRITE_TRIANGLES   (1)
#define RF_GRAPH_WRITE_SECTORS     (1)
#define RF_GRAPH_WRITE_NODES       (1)

static int rfGraphWriteLog( rfGraphCache *cache, char *filename )
{
  (void)filename;

  int edgeindex, triindex, childindex;
  rfindex index;
  FILE *file;
  rff pt[3*RF_AXIS_COUNT];
  rfTri *tri;
  rfPrepSector *prepsector;
  rfPrepNode *prepnode;
  rfPrepLink *preptrilist;
  ssize_t offset, targetoffset;
  void* link;

  size_t headermem, originmem;
  size_t headeroffset, originoffset, graphoffset;

  /* Size of user-defined per-primitive data */
  headermem = RF_GRAPHHEADER_SIZE;
  originmem = RF_SIZE_ROUND64( sizeof(rfOriginTable) + ( cache->origintable.totalcount * sizeof(rforigin) ) );

  /* Compute offsets */
  headeroffset = 0;
  originoffset = headermem;
  graphoffset  = headermem + originmem;

#if RF_GRAPH_WRITE_ORIGINTABLE
  file = fopen( "otable.txt", "w" );

  fprintf( file, "graphoffset = %lld\n", graphoffset );
  for( index = 0 ; index < cache->origintable.totalcount ; index++ )
  {
    offset = graphoffset + (cache->sectoroffset[cache->origin[index]] << RF_GRAPH_OFFSET_SHIFT);
    fprintf( file, "otable[%lld] = %lld (%lld)\n",
             index,
             offset,
             ((offset - graphoffset) >> RF_GRAPH_OFFSET_SHIFT));
  }

  fclose( file );

  fprintf( stderr, "Wrote origin table to \"otable.txt\"\n" );
#endif // RF_GRAPH_WRITE_ORIGIN_TABLE

#if RF_GRAPH_WRITE_TRIANGLES
  file = fopen( "triangles.txt", "w" );

  fprintf( file, "ntris = %lld\n", (long long)cache->trianglecount );

  tri = cache->trilist;
  for( index = 0 ; index < cache->trianglecount ; index++, tri++ )
  {
    offset = cache->triangleoffset[index] << RF_GRAPH_OFFSET_SHIFT;
    getTrianglePoints( tri, pt );
    fprintf( file, "Triangle %lld : %lld (%lld)\n",
             (long long)index,
             offset, (offset >> RF_GRAPH_OFFSET_SHIFT) );
    fprintf( file, "  vertices : %f %f %f ; %f %f %f ; %f %f %f\n",
             pt[0], pt[1], pt[2], pt[3], pt[4], pt[5], pt[6], pt[7], pt[8] );
  }

  fclose( file );
  fprintf( stderr, "Wrote triangles to \"triangles.txt\"\n" );
#endif // RF_GRAPH_WRITE_TRIANGLES

#if RF_GRAPH_WRITE_SECTORS
  file = fopen("sectors.txt", "w" );

  fprintf( file, "nsectors = %lld\n", (long long)cache->sectorcount );

  for( index = 0 ; index < cache->sectorcount ; index++ )
  {
    offset = (rfsize)cache->sectoroffset[index] << RF_GRAPH_OFFSET_SHIFT;
    prepsector = mmDirectoryGetFast( &cache->sectordir, index );
    fprintf( file, "Sector %lld : %lld (%lld)\n",
             (long long)index, offset, (offset >> RF_GRAPH_OFFSET_SHIFT));
    fprintf( file, "  edges : %f %f %f %f %f %f\n",
             prepsector->edge[RF_EDGE_MINX],
             prepsector->edge[RF_EDGE_MAXX],
             prepsector->edge[RF_EDGE_MINY],
             prepsector->edge[RF_EDGE_MAXY],
             prepsector->edge[RF_EDGE_MINZ],
             prepsector->edge[RF_EDGE_MAXZ] );
    fprintf( file, "  flags : %d\n",
             ( prepsector->flags >> RF_SECTOR_LINKFLAGS_SHIFT ) & 0x3f );
    fprintf( file, "  links :\n" );
    for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
    {
      link = prepsector->link[edgeindex].p;
      if( prepsector->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) )
      {
        if( prepsector->link[edgeindex].p )
        {
          targetoffset = (rfsize)cache->sectoroffset[ ((rfPrepSector *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
          fprintf( file, "    s %lld : %lld (%lld)\n",
                   (long long)(((rfPrepSector *)prepsector->link[edgeindex].p)->index),
                   targetoffset, (targetoffset >> RF_GRAPH_OFFSET_SHIFT) );
        }
        else
          fprintf( file, " -1\n" );
      }
      else
      {
        targetoffset = (rfsize)cache->nodeoffset[ ((rfPrepNode *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        fprintf( file, "    n %lld : %lld (%lld)\n",
                 (long long)(((rfPrepNode *)prepsector->link[edgeindex].p)->index),
                   targetoffset, (targetoffset >> RF_GRAPH_OFFSET_SHIFT) );
      }
    }
    fprintf( file, "  ntris : %d\n", (int)prepsector->tricount );
    preptrilist = PREP_SECTOR_TRILIST( prepsector );
    for( triindex = 0 ; triindex < prepsector->tricount ; triindex++ )
    {
      long long tidx = (long long)preptrilist[triindex].i;
      offset = cache->triangleoffset[tidx] << RF_GRAPH_OFFSET_SHIFT;
      fprintf( file, " t %lld : %lld (%lld)\n",
               tidx,
               offset, offset >> RF_GRAPH_OFFSET_SHIFT);
    }
    fprintf( file, "\n" );
  }

  fclose( file );
  fprintf( stderr, "Wrote sectors to \"sectors.txt\"\n" );
#endif // RF_GRAPH_WRITE_SECTORS

#if RF_GRAPH_WRITE_NODES
  file = fopen("nodes.txt", "w" );

  fprintf( file, "nnodes = %lld\n", (long long)cache->nodecount );

  for( index = 0 ; index < cache->nodecount ; index++ )
  {
    offset = cache->nodeoffset[index] << RF_GRAPH_OFFSET_SHIFT;
    prepnode = mmDirectoryGetFast( &cache->nodedir, index );
    fprintf( file, "Node %lld : %lld (%lld)\n",
             (long long) index, offset, (offset >> RF_GRAPH_OFFSET_SHIFT) );
    fprintf( file, "  flags : %d\nlflags : %d\n  plane : %f\n",
             (int)RF_NODE_GET_AXIS(prepnode->nodeflags),
             ( prepnode->nodeflags >> RF_NODE_LINKFLAGS_SHIFT ) & 0x3,
             prepnode->plane );
    fprintf( file, "  links :\n");
    for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
    {
      link = prepnode->child[childindex].p;
      if( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) )
      {
        targetoffset = (rfsize)cache->sectoroffset[ ((rfPrepSector *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        fprintf( file, "    s %lld : %lld (%lld)\n",
                 (long long)(((rfPrepSector *)prepnode->child[childindex].p)->index),
                 targetoffset, (targetoffset >> RF_GRAPH_OFFSET_SHIFT) );
      }
      else
      {
        targetoffset = (rfsize)cache->nodeoffset[ ((rfPrepNode *)link)->index ] << RF_GRAPH_OFFSET_SHIFT;
        fprintf( file, "    n %lld : %lld (%lld)\n",
                 (long long)(((rfPrepNode *)prepnode->child[childindex].p)->index),
                 targetoffset, (targetoffset >> RF_GRAPH_OFFSET_SHIFT) );
      }
    }
    fprintf( file, "\n" );
  }

  fclose( file );
  fprintf( stderr, "Wrote nodes to \"nodes.txt\"\n" );
#endif // RF_GRAPH_WRITE_NODES

  return 1;
}

#endif // RF_GRAPH_WRITE_LOG


////


int rfBuildGraph( rfContext *context, rfGraph *graph, rfGeometry *geometry, rff *box, rfindex originwidth, rfint target, rfint targetindex, void *targetpointer, rfint buildquality, rfint buildmemory, rfint buildlayout )
{
  int retval;
  rfGraphCache cache;

  rfBuildGraphCache( &cache, geometry, box, originwidth, buildquality, buildmemory );
  rfInitGraphLayout( &cache );
  rfBuildGraphLayout( &cache, geometry, buildlayout );
  retval = rfBuildGraphMemory( context, graph, &cache, geometry, target, targetindex, targetpointer );

  graph->graphcost = 0.0;
  if( geometry->estimatecostflag )
    graph->graphcost = (size_t)ceil( 1000.0 * rfEvaluateGraphCost( &cache ) );
  graph->sectorcount = cache.sectorcount;
  graph->nodecount = cache.nodecount;
  graph->trianglecount = cache.trianglecount;
  graph->trirefcount = cache.trirefcount;
  graph->trirefmaximum = cache.sectortrimax;

  rfFreeGraphLayout( &cache );
  rfFreeGraphCache( &cache );

  return retval;
}


int rfSetGraph( rfContext *context, rfGraph *graph, void *graphmem, size_t graphdatasize, rfint target, rfint targetindex, void *targetpointer )
{
  rfGraphHeader *header;

  /* Verify format of graphmem */
  if( graphdatasize < sizeof(rfGraphHeader) )
    return 0;
  header = graphmem;
  if( strncmp( header->identifier, RF_CACHE_IDENTIFIER, RF_GRAPH_HEADER_IDENTIFIER_LENGTH ) )
    return 0;
  if( header->graphtype != RF_GRAPHEADER_TYPE_TRIANGLES )
    return 0;
  if( header->cacheversion != RF_CACHE_VERSION )
    return 0;
  if( header->headersize != RF_GRAPHHEADER_SIZE )
    return 0;
  if( ( header->sectoraddrbits != header->nodeaddrbits ) || ( header->sectoraddrbits != header->trirefaddrbits ) )
    return 0;
  switch( header->sectoraddrbits )
  {
    case 16:
      graph->addressmode = RF_GRAPH_ADDRESSMODE_16BITS;
      if( header->addrshift != RF_GRAPH_LINK16_SHIFT )
        return 0;
      break;
    case 32:
      graph->addressmode = RF_GRAPH_ADDRESSMODE_32BITS;
      if( header->addrshift != RF_GRAPH_LINK32_SHIFT )
        return 0;
      break;
    case 64:
      graph->addressmode = RF_GRAPH_ADDRESSMODE_64BITS;
      if( header->addrshift != RF_GRAPH_LINK64_SHIFT )
        return 0;
      break;
    default:
      return 0;
  }
  if( header->alignment != RF_GRAPH_ALIGNMENT )
    return 0;
  if( header->rffwidth != sizeof(rff) )
    return 0;
  if( header->graphdatasize != graphdatasize )
    return 0;

  switch( target )
  {
    case RF_TARGET_SYSTEM:
      graph->numanode = -1;
      graph->deviceindex = -1;
      graph->memory = malloc( graphdatasize );
      memcpy( graph->memory, graphmem, graphdatasize );
      break;
    case RF_TARGET_NUMA:
      if( (unsigned)targetindex >= mmcontext.nodecount )
        return 0;
      graph->numanode = targetindex;
      graph->deviceindex = -1;
      graph->memory = mmNodeAlloc( graph->numanode, graphdatasize );
      memcpy( graph->memory, graphmem, graphdatasize );
      break;
    case RF_TARGET_CUDA:
#if RF_CONFIG_CUDA_SUPPORT
      graph->numanode = -1;
      graph->deviceindex = targetindex;
      if( !( graph->memory = rfCudaAllocGraph( context, targetindex, graphmem, graphdatasize, 0 ) ) )
        return 0;
      break;
#else
      return 0;
#endif
    default:
      fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
      abort();
  }

  graph->flags = header->graphflags;
  graph->edge[0] = header->edge[0];
  graph->edge[1] = header->edge[1];
  graph->edge[2] = header->edge[2];
  graph->edge[3] = header->edge[3];
  graph->edge[4] = header->edge[4];
  graph->edge[5] = header->edge[5];
  graph->target = target;
  graph->memsize = graphdatasize;
  graph->datasize = graphdatasize;
  graph->origintable = ADDRESS( graph->memory, RF_GRAPHHEADER_SIZE );

  return 1;
}


////


int rfCopyGraph( rfContext *context, rfGraph *graph, rfGraph *graphref, rfint target, rfint targetindex, void *targetpointer )
{
  graph->target = target;
  graph->numanode = -1;
  graph->deviceindex = -1;
  switch( target )
  {
    case RF_TARGET_SYSTEM:
      graph->numanode = -1;
      graph->memory = malloc( graphref->memsize );
      if( graphref->target == RF_TARGET_CUDA )
      {
#if RF_CONFIG_CUDA_SUPPORT
        rfCudaGetGraph( context, graphref->deviceindex, graphref, graph->memory );
#endif
      }
      else
        memcpy( graph->memory, graphref->memory, graphref->memsize );
      break;
    case RF_TARGET_NUMA:
      if( (unsigned)targetindex >= mmcontext.nodecount )
        return 0;
      graph->numanode = targetindex;
      graph->memory = mmNodeAlloc( graph->numanode, graphref->memsize );
      if( graphref->target == RF_TARGET_CUDA )
      {
#if RF_CONFIG_CUDA_SUPPORT
        rfCudaGetGraph( context, graphref->deviceindex, graphref, graph->memory );
#endif
      }
      else
        memcpy( graph->memory, graphref->memory, graphref->memsize );
      break;
    case RF_TARGET_CUDA:
#if RF_CONFIG_CUDA_SUPPORT
      if( !( graph->memory = rfCudaAllocGraph( context, targetindex, graphref->memory, graphref->memsize, ( graphref->target == RF_TARGET_CUDA ) ) ) )
        return 0;
      graph->deviceindex = targetindex;
      break;
#else
      return 0;
#endif
    default:
      fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
      abort();
  }

  graph->flags = graphref->flags;
  graph->addressmode = graphref->addressmode;
  memcpy( graph->edge, graphref->edge, RF_EDGE_COUNT*sizeof(rff) );
  graph->datasize = graphref->datasize;
  graph->memsize = graphref->memsize;
  graph->origintable = ADDRESS( graph->memory, ADDRESSDIFF( graphref->origintable, graphref->memory ) );

  return 1;
}


