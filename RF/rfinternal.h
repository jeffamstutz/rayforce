/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

static inline void copy1rff2f( float *dst, rff *src )
{
  dst[0] = src[0];
  return;
}

static inline void copy3rff2f( float *dst, rff *src )
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  return;
}

static inline void copy4rff2f( float *dst, rff *src )
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  dst[3] = src[3];
  return;
}


static inline void copy1rff2d( double *dst, rff *src )
{
  dst[0] = src[0];
  return;
}

static inline void copy3rff2d( double *dst, rff *src )
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  return;
}

static inline void copy4rff2d( double *dst, rff *src )
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  dst[3] = src[3];
  return;
}


static inline void copy1f2rff( rff *dst, float *src )
{
  dst[0] = src[0];
  return;
}

static inline void copy3f2rff( rff *dst, float *src )
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  return;
}

static inline void copy4f2rff( rff *dst, float *src )
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  dst[3] = src[3];
  return;
}


static inline void copy1d2rff( rff *dst, double *src )
{
  dst[0] = src[0];
  return;
}

static inline void copy3d2rff( rff *dst, double *src )
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  return;
}

static inline void copy4d2rff( rff *dst, double *src )
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  dst[3] = src[3];
  return;
}


/**/


static inline void edgeExtendToEdge( rff * const restrict edge, const rff * const restrict edgeref )
{
  if( edgeref[RF_EDGE_MINX] < edge[RF_EDGE_MINX] )
    edge[RF_EDGE_MINX] = edgeref[RF_EDGE_MINX];
  if( edgeref[RF_EDGE_MAXX] > edge[RF_EDGE_MAXX] )
    edge[RF_EDGE_MAXX] = edgeref[RF_EDGE_MAXX];
  if( edgeref[RF_EDGE_MINY] < edge[RF_EDGE_MINY] )
    edge[RF_EDGE_MINY] = edgeref[RF_EDGE_MINY];
  if( edgeref[RF_EDGE_MAXY] > edge[RF_EDGE_MAXY] )
    edge[RF_EDGE_MAXY] = edgeref[RF_EDGE_MAXY];
  if( edgeref[RF_EDGE_MINZ] < edge[RF_EDGE_MINZ] )
    edge[RF_EDGE_MINZ] = edgeref[RF_EDGE_MINZ];
  if( edgeref[RF_EDGE_MAXZ] > edge[RF_EDGE_MAXZ] )
    edge[RF_EDGE_MAXZ] = edgeref[RF_EDGE_MAXZ];
  return;
}

static inline void edgeExtendToPoint( rff * const restrict edge, const rff * const restrict point )
{
  if( point[0] < edge[RF_EDGE_MINX] )
    edge[RF_EDGE_MINX] = point[0];
  if( point[0] > edge[RF_EDGE_MAXX] )
    edge[RF_EDGE_MAXX] = point[0];
  if( point[1] < edge[RF_EDGE_MINY] )
    edge[RF_EDGE_MINY] = point[1];
  if( point[1] > edge[RF_EDGE_MAXY] )
    edge[RF_EDGE_MAXY] = point[1];
  if( point[2] < edge[RF_EDGE_MINZ] )
    edge[RF_EDGE_MINZ] = point[2];
  if( point[2] > edge[RF_EDGE_MAXZ] )
    edge[RF_EDGE_MAXZ] = point[2];
  return;
}

static inline void edgeExtendSize( rff * const restrict edge, rff factor )
{
  rff sizex, sizey, sizez;
  rff midx, midy, midz;
  factor *= 0.5;
  sizex = factor * ( edge[RF_EDGE_MAXX] - edge[RF_EDGE_MINX] );
  sizey = factor * ( edge[RF_EDGE_MAXY] - edge[RF_EDGE_MINY] );
  sizez = factor * ( edge[RF_EDGE_MAXZ] - edge[RF_EDGE_MINZ] );
  midx = 0.5 * ( edge[RF_EDGE_MAXX] + edge[RF_EDGE_MINX] );
  midy = 0.5 * ( edge[RF_EDGE_MAXY] + edge[RF_EDGE_MINY] );
  midz = 0.5 * ( edge[RF_EDGE_MAXZ] + edge[RF_EDGE_MINZ] );
  edge[RF_EDGE_MINX] = midx - sizex;
  edge[RF_EDGE_MAXX] = midx + sizex;
  edge[RF_EDGE_MINY] = midy - sizey;
  edge[RF_EDGE_MAXY] = midy + sizey;
  edge[RF_EDGE_MINZ] = midz - sizez;
  edge[RF_EDGE_MAXZ] = midz + sizez;
  return;
}

static inline int rfEdgeIntersection( rff *edge0, rff *edge1 )
{
  if( edge0[RF_EDGE_MAXX] < edge1[RF_EDGE_MINX] )
    return 0;
  if( edge0[RF_EDGE_MINX] > edge1[RF_EDGE_MAXX] )
    return 0;
  if( edge0[RF_EDGE_MAXY] < edge1[RF_EDGE_MINY] )
    return 0;
  if( edge0[RF_EDGE_MINY] > edge1[RF_EDGE_MAXY] )
    return 0;
  if( edge0[RF_EDGE_MAXZ] < edge1[RF_EDGE_MINZ] )
    return 0;
  if( edge0[RF_EDGE_MINZ] > edge1[RF_EDGE_MAXZ] )
    return 0;
  return 1;
}
