/* *****************************************************************************
 *
 * Copyright (c) 2007-2014 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */


typedef struct
{
  size_t entrycount;
  size_t mapsize;
#ifdef MM_ATOMIC_SUPPORT
  mmAtomicL *map;
#else
  long *map;
  mtMutex mutex;
#endif
} mmBitMap;

int mmBitMapInit( mmBitMap *bitmap, size_t entrycount, int initvalue );
void mmBitMapReset( mmBitMap *bitmap, int resetvalue );
void mmBitMapFree( mmBitMap *bitmap );

int mmBitMapDirectGet( mmBitMap *bitmap, size_t entryindex );
void mmBitMapDirectSet( mmBitMap *bitmap, size_t entryindex );
void mmBitMapDirectClear( mmBitMap *bitmap, size_t entryindex );

int mmBitMapDirectMaskGet( mmBitMap *bitmap, size_t entryindex, long mask );
void mmBitMapDirectMaskSet( mmBitMap *bitmap, size_t entryindex, long value, long mask );

int mmBitMapGet( mmBitMap *bitmap, size_t entryindex );
void mmBitMapSet( mmBitMap *bitmap, size_t entryindex );
void mmBitMapClear( mmBitMap *bitmap, size_t entryindex );

int mmBitMapMaskGet( mmBitMap *bitmap, size_t entryindex, long mask );
void mmBitMapMaskSet( mmBitMap *bitmap, size_t entryindex, long value, long mask );

int mmBitMapFindSet( mmBitMap *bitmap, size_t entryindex, size_t entryindexlast, size_t *retentryindex );
int mmBitMapFindClear( mmBitMap *bitmap, size_t entryindex, size_t entryindexlast, size_t *retentryindex );



