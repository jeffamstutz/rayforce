/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#ifdef __cplusplus
extern "C" {
#endif


#include "rfcputune.h"

#if __MMX__ || TRACE__MMX__
 #include <mmintrin.h>
 #define RFTRACE__MMX__ (1)
#endif
#if __SSE__ || TRACE__SSE__
 #include <xmmintrin.h>
 #define RFTRACE__SSE__ (1)
#endif
#if __SSE2__ || TRACE__SSE2__
 #include <emmintrin.h>
 #define RFTRACE__SSE2__ (1)
#endif
#if __SSE3__ || TRACE__SSE3__
 #include <pmmintrin.h>
 #define RFTRACE__SSE3__ (1)
#endif
#if __SSSE3__ || TRACE__SSSE3__
 #include <tmmintrin.h>
 #define RFTRACE__SSSE3__ (1)
#endif
#if __SSE4_1__ || TRACE__SSE4_1__
 #include <smmintrin.h>
 #define RFTRACE__SSE4_1__ (1)
#endif
#if __SSE4_2__ || TRACE__SSE4_2__
 #include <nmmintrin.h>
 #define RFTRACE__SSE4_2__ (1)
#endif
#if __SSE4A__ || TRACE__SSE4A__
 #include <ammintrin.h>
 #define RFTRACE__SSE4A__ (1)
#endif
#if __AVX__ || TRACE__AVX__
 #include <immintrin.h>
 #define RFTRACE__AVX__ (1)
#endif
#if __AVX2__ || TRACE__AVX2__
 #include <immintrin.h>
 #define RFTRACE__AVX2__ (1)
#endif
#if __XOP__ || TRACE__XOP__
 #include <immintrin.h>
 #define RFTRACE__XOP__ (1)
#endif
#if __FMA3__ || TRACE__FMA3__
 #include <immintrin.h>
 #define RFTRACE__FMA3__ (1)
#endif
#if __FMA4__ || TRACE__FMA4__
 #include <immintrin.h>
 #define RFTRACE__FMA4__ (1)
#endif
#if __RDRND__ || TRACE__RDRND__
 #include <immintrin.h>
 #define RFTRACE__RDRND__ (1)
#endif
#if __POPCNT__ || TRACE__POPCNT__
 #include <popcntintrin.h>
 #define RFTRACE__POPCNT__ (1)
#endif
#if __LZCNT__ || TRACE__LZCNT__
 #include <lzcntintrin.h>
 #define RFTRACE__LZCNT__ (1)
#endif
#if __F16C__ || TRACE__F16C__
 #include <f16cintrin.h>
 #define RFTRACE__F16C__ (1)
#endif
#if __BMI__ || TRACE__BMI__
 #include <bmiintrin.h>
 #define RFTRACE__BMI__ (1)
#endif
#if __BMI2__ || TRACE__BMI2__
 #include <bmi2intrin.h>
 #define RFTRACE__BMI2__ (1)
#endif
#if __TBM__ || TRACE__TBM__
 #include <tbmintrin.h>
 #define RFTRACE__TBM__ (1)
#endif


#include "rfdefs.h"
#include "rfgraph.h"
#include "rfmath.h"


#ifdef TRACEINIT
 #warning TRACEINIT should not be used when defining a CPU pipeline.
#endif
#ifndef TRACEPIPELINE
 #error TRACEPIPELINE should be used to define a CPU pipeline.
#endif

#if TRACEQUADROOT && ( TRACEDISPLACEROOT || TRACERESOLVEROOT )
 #error You must enable only one of TRACEQUADROOT, TRACEDISPLACEROOT and TRACERESOLVEROOT
#endif


#define RF_DECL_ATTRIB
#define RFNAME2(n,p) _rf##p##n
#define RFNAME(n,p) RFNAME2(n,p)


#include "trace/common.h"


#define RF_ADDRBITS (32)
#define RF_RESOLVENAME RFNAME(Resolve32,TRACEPIPELINE)
#define RF_RESOLVE4NAME RFNAME(Resolve4r32,TRACEPIPELINE)
#define RF_DISPLACENAME RFNAME(Displace32,TRACEPIPELINE)
#define RF_DISPLACE4NAME RFNAME(Displace4r32,TRACEPIPELINE)
#include "trace/resolve.h"


/* Begin Single tracing  */
#if RFTRACE__SSE__
 #define RF_PATHFILE "trace/path4sse.h"
#else
 #define RF_PATHFILE "trace/path4.h"
#endif

#define RF_CULLMODE (0)
#define RF_ADDRBITS (32)
#define RF_TRACEPACKET rfRayPacket4
#define RF_TRACERESULT rfResult4
#define RF_PACKETWIDTH (4)
#define RF_TRACE1NAME RFNAME(Trace1f32,TRACEPIPELINE)
#define RF_TRACE4NAME RFNAME(Trace4f32,TRACEPIPELINE)
#define RF_RESOLVENAME RFNAME(Resolve32,TRACEPIPELINE)
#define RF_RESOLVE4NAME RFNAME(Resolve4r32,TRACEPIPELINE)
#define RF_DISPLACENAME RFNAME(Displace32,TRACEPIPELINE)
#define RF_DISPLACE4NAME RFNAME(Displace4r32,TRACEPIPELINE)
#include RF_PATHFILE

#define RF_CULLMODE (1)
#define RF_ADDRBITS (32)
#define RF_TRACEPACKET rfRayPacket4
#define RF_TRACERESULT rfResult4
#define RF_PACKETWIDTH (4)
#define RF_TRACE1NAME RFNAME(Trace1b32,TRACEPIPELINE)
#define RF_TRACE4NAME RFNAME(Trace4b32,TRACEPIPELINE)
#define RF_RESOLVENAME RFNAME(Resolve32,TRACEPIPELINE)
#define RF_RESOLVE4NAME RFNAME(Resolve4r32,TRACEPIPELINE)
#define RF_DISPLACENAME RFNAME(Displace32,TRACEPIPELINE)
#define RF_DISPLACE4NAME RFNAME(Displace4r32,TRACEPIPELINE)
#include RF_PATHFILE

#define RF_CULLMODE (2)
#define RF_ADDRBITS (32)
#define RF_TRACEPACKET rfRayPacket4
#define RF_TRACERESULT rfResult4
#define RF_PACKETWIDTH (4)
#define RF_TRACE1NAME RFNAME(Trace1d32,TRACEPIPELINE)
#define RF_TRACE4NAME RFNAME(Trace4d32,TRACEPIPELINE)
#define RF_RESOLVENAME RFNAME(Resolve32,TRACEPIPELINE)
#define RF_RESOLVE4NAME RFNAME(Resolve4r32,TRACEPIPELINE)
#define RF_DISPLACENAME RFNAME(Displace32,TRACEPIPELINE)
#define RF_DISPLACE4NAME RFNAME(Displace4r32,TRACEPIPELINE)
#include RF_PATHFILE

#undef RF_PATHFILE
/* End Single tracing  */


rfPipeline TRACEPIPELINE =
{
  .reqflags = 0
#if RFTRACE__SSE__
  | RF_PIPELINE_REQFLAG_SSE
#endif
#if RFTRACE__SSE2__
  | RF_PIPELINE_REQFLAG_SSE2
#endif
#if RFTRACE__SSE3__
  | RF_PIPELINE_REQFLAG_SSE3
#endif
#if RFTRACE__SSSE3__
  | RF_PIPELINE_REQFLAG_SSSE3
#endif
#if RFTRACE__SSE4_1__
  | RF_PIPELINE_REQFLAG_SSE4P1
#endif
#if RFTRACE__SSE4_2__
  | RF_PIPELINE_REQFLAG_SSE4P2
#endif
#if RFTRACE__SSE4A__
  | RF_PIPELINE_REQFLAG_SSE4A
#endif
#if RFTRACE__AVX__
  | RF_PIPELINE_REQFLAG_AVX
#endif
#if RFTRACE__AVX2__
  | RF_PIPELINE_REQFLAG_AVX2
#endif
#if RFTRACE__XOP__
  | RF_PIPELINE_REQFLAG_XOP
#endif
#if RFTRACE__FMA3__
  | RF_PIPELINE_REQFLAG_FMA3
#endif
#if RFTRACE__FMA4__
  | RF_PIPELINE_REQFLAG_FMA4
#endif
#if RFTRACE__RDRND__
  | RF_PIPELINE_REQFLAG_RDRND
#endif
#if RFTRACE__POPCNT__
  | RF_PIPELINE_REQFLAG_POPCNT
#endif
#if RFTRACE__LZCNT__
  | RF_PIPELINE_REQFLAG_LZCNT
#endif
#if RFTRACE__F16C__
  | RF_PIPELINE_REQFLAG_F16C
#endif
#if RFTRACE__BMI__
  | RF_PIPELINE_REQFLAG_BMI
#endif
#if RFTRACE__BMI2__
  | RF_PIPELINE_REQFLAG_BMI2
#endif
#if RFTRACE__TBM__
  | RF_PIPELINE_REQFLAG_TBM
#endif
  ,
  .object = { (void *)RFNAME(Trace4f32,TRACEPIPELINE), (void *)RFNAME(Trace4b32,TRACEPIPELINE), (void *)RFNAME(Trace4d32,TRACEPIPELINE) },
  .scene = { (void *)0, (void *)0, (void *)0 }
};


#undef RF_DECL_ATTRIB
#undef RFNAME
#undef RFNAME2

#undef TRACEPIPELINE
#undef TRACEINIT
#undef TRACEHIT
#undef TRACEVOID
#undef TRACEEND
#undef TRACEPARAM
#undef TRACEPARAMPASSBYREFERENCE
#undef TRACEDISTCLIP
#undef TRACERESOLVEROOT
#undef TRACEDISPLACEROOT
#undef TRACEQUADROOT
#undef TRACESINGLEROOT
#undef TRACEHITMODELDATA
#undef TRACEHITOBJECTDATA
#undef TRACEHITVECTOR
#undef TRACEHITPOINT
#undef TRACEHITUV
#undef TRACEHITDATA
#undef TRACEHITDIST
#undef TRACEHITPLANE
#undef TRACEHITSIDE
#undef TRACEHITROOT
#undef TRACEHITMATRIX

#undef RFTRACE__MMX__
#undef RFTRACE__SSE__
#undef RFTRACE__SSE2__
#undef RFTRACE__SSE3__
#undef RFTRACE__SSSE3__
#undef RFTRACE__SSE4P1__
#undef RFTRACE__SSE4P2__
#undef RFTRACE__SSE4A__
#undef RFTRACE__AVX__
#undef RFTRACE__AVX2__
#undef RFTRACE__XOP__
#undef RFTRACE__FMA3__
#undef RFTRACE__FMA4__


#ifdef __cplusplus
}
#endif


