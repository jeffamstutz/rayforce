/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "cpuconfig.h"

#include "cc.h"
#include "mm.h"
#include "mmhash.h"
#include "mmdirectory.h"
#include "mmqueue.h"

#include "rfmath.h"

#include "rf.h"
#include "rfdefs.h"
#include "rfdata.h"
#include "rfinternal.h"
#include "rfgraph.h"
#include "rfgraphbuild.h"



#if RF_USE_FLOAT
 #define PREP_TRIANGLE_EXTEND_BIAS (2.0/(8388608.0))
#elif RF_USE_DOUBLE
 #define PREP_TRIANGLE_EXTEND_BIAS (2.0/(4503599627370496.0))
#else
 #error !RF_USE_FLOAT && !RF_USE_DOUBLE
#endif


#define PREP_LINK_TRACKING (0)


#define PREP_STEPGAIN_FACTOR (0.95)
#define PREP_STEPGAIN_TRICAP_FACTOR (0.99)
#define PREP_BIN_MAXIMUM (384)

#define PREP_SECTOR_TRICAP (128)


#define PREP_BINCOUNT_BASE (0.5)
#define PREP_BINCOUNT_RANGE (32.0)

#define PREP_BINMAX_BASE (32)
#define PREP_BINMAX_RANGE (PREP_BIN_MAXIMUM-PREP_BINMAX_BASE)

#define PREP_STEPCAP_BASE (0.1)
#define PREP_STEPCAP_RANGE (1.5)

#define PREP_GRAPH_EDGE_SIZE_MIN (0.0001)

#define PREP_EDGE_EXTENSION_FACTOR (1.0001)

#define PREP_QUEUE_BUNDLE_RANGE (3.0)

#define PREP_STEP_MINIMUM_EXTENSION (1.0/4096.0)


#define PREP_DEBUG (1)
#define PREP_DEBUG_PRINT (0)


/* This is preferable to enabling LAYOUT_TRIREF_REORDERING */
#define PREP_TRIREF_REORDERING (1)

#define PREP_TRIREF_SORT_MAX (128)


////


#define EVAL_SIDEFACTOR (1.00) /* Cost of traversal relatively to one triangle test */
#define EVAL_NODECOST (0.50) /* Cost of traversal of a node relatively to a sector */
#define EVAL_TRITESTBASE (0.5) /* Base cost of a non-zero count of triangle tests, in units of triangle tests */


#define EVAL_ESTIMATE_LOGBASE (3.2)
#define EVAL_MODEL_STATIC_COST (2.2)


////


/* Estimate of the cost of traversal of a unbuilt tree of nodes, given count of nodes, to be multiplied by area */
/* SectorCost + ( NodeCost * EstimateDepthOfTree ) */
static inline double NODE_LINKCOUNT_FORMULA( double linkcount )
{
  if( linkcount < 2.0 )
    return 0.2;
  return 1.0 + ( EVAL_NODECOST * ( rflog( linkcount ) * ( 1.0 / rflog( 4.0 ) ) ) );
}

#define NODE_LINKCOUNT_MAX (64)

static const rff nodeCostTable[NODE_LINKCOUNT_MAX] = { 0.200000000000,
0.200000000000, 1.250000000000, 1.396240631245, 1.500000000000,
1.580482032973, 1.646240631245, 1.701838752507, 1.750000000000,
1.792481262490, 1.830482032973, 1.864857925133, 1.896240631245,
1.925109896914, 1.951838752507, 1.976722664218, 2.000000000000,
2.021865702188, 2.042481262490, 2.061981859270, 2.080482032973,
2.098079383752, 2.114857925133, 2.130890491767, 2.146240631245,
2.160964065945, 2.175109896914, 2.188721893735, 2.201838752507,
2.214495213723, 2.226722664218, 2.238549051172, 2.250000000000,
2.261098556378, 2.271865702188, 2.282320785479, 2.292481262490,
2.302363299590, 2.311981859270, 2.321350528159, 2.330482032973,
2.339387982540, 2.348079383752, 2.356566211601, 2.364857925133,
2.372963295463, 2.380890491767, 2.388647253265, 2.396240631245,
2.403677505013, 2.410964065945, 2.418106333433, 2.425109896914,
2.431980087849, 2.438721893735, 2.445339958105, 2.451838666515,
2.458222490515, 2.464495213723, 2.470660791739, 2.476722664218,
2.482684270811, 2.488549051172, 2.494319929006, };

#define NODE_LINKCOUNT_COST(x) ((x<NODE_LINKCOUNT_MAX)?(nodeCostTable[x]):NODE_LINKCOUNT_FORMULA(x))


////


/* Estimate of the cost of traversal of a unbuilt tree of nodes, given count of nodes, to be multiplied by area */
/* SectorCost + ( NodeCost * EstimateDepthOfTree ) */
static inline double STEP_LINKCOUNT_COST( double linkcount )
{
  if( linkcount < 2.0 )
    return 1.0;
  return 1.0 + ( EVAL_NODECOST * ( rflog( linkcount ) * ( 1.0 / rflog( 3.0 ) ) ) );
}


////


/* Estimate of the cost of traversal of a unbuilt sector, given count of triangles, to be multiplied by area sum */
static inline double RF_ALWAYS_INLINE STEP_ESTIMATE_FORMULA( double tricount )
{
  if( tricount > EVAL_ESTIMATE_LOGBASE )
    return EVAL_TRITESTBASE + ( tricount / rfpow( 0.5*EVAL_ESTIMATE_LOGBASE, ( rflog( tricount ) / rflog(EVAL_ESTIMATE_LOGBASE) ) - 1.0 ) );
  else if( tricount )
    return EVAL_TRITESTBASE + tricount;
  return 0.0;
}

#define STEP_ESTIMATE_MAX (64)

static const rff stepEstimateTable[STEP_ESTIMATE_MAX] = { 0.000000000000,
1.500000000000, 2.500000000000, 3.500000000000, 4.155112734713,
4.674953092417, 5.154118496468, 5.601906577304, 6.024477281427,
6.426169853326, 6.810183203958, 7.178959516285, 7.534412145473,
7.878080049384, 8.211216211868, 8.534866058236, 8.849905665372,
9.157082985259, 9.457038587621, 9.750334067865, 10.037452309089,
10.318825620959, 10.594834107756, 10.865818466586, 11.132079564980,
11.393895434911, 11.651511680915, 11.905152582384, 12.155026610243,
12.401320502112, 12.644203850327, 12.883837812380, 13.120366882589,
13.353927717615, 13.584645822816, 13.812637773411, 14.038010762093,
14.260870399886, 14.481307676653, 14.699413665383, 14.915269730136,
15.128957202806, 15.340547204040, 15.550112670311, 15.757719167067,
15.963424233730, 16.167291908637, 16.369376893689, 16.569731346867,
16.768406641901, 16.965448657237, 17.160907186981, 17.354822083496,
17.547233892183, 17.738184199020, 17.927712004796, 18.115855280620,
18.302641928543, 18.488111132341, 18.672289747727, 18.855213723838,
19.036909637721, 19.217406845133, 19.396729579925, };

#define STEP_ESTIMATE_COST(x) ( (x) < STEP_ESTIMATE_MAX ? stepEstimateTable[(x)] : STEP_ESTIMATE_FORMULA(x) )


////


#if RF_USE_FLOAT

int rfBuildTriangle( rfTri * const restrict tri, rff * restrict v0f, rff * restrict v1f, rff * restrict v2f, const rff extendbiasf )
{
  double v0[3], v1[3], v2[3], extendbias, f;
  double edgeu[3], edgev[3], edgew[3];
  double plane[4], planeu[4], planev[4];
  double vm0[3], vm1[3], vm2[3];

  copy3rff2d( v0, v0f );
  copy3rff2d( v1, v1f );
  copy3rff2d( v2, v2f );
  extendbias = (double)extendbiasf;

  edgeu[0] = v2[0] - v0[0];
  edgeu[1] = v2[1] - v0[1];
  edgeu[2] = v2[2] - v0[2];
  edgev[0] = v0[0] - v1[0];
  edgev[1] = v0[1] - v1[1];
  edgev[2] = v0[2] - v1[2];
  edgew[0] = v1[0] - v2[0];
  edgew[1] = v1[1] - v2[1];
  edgew[2] = v1[2] - v2[2];
  if( !( rfMathVectorNormalizeCheckd( edgeu ) ) )
    return 0;
  if( !( rfMathVectorNormalizeCheckd( edgev ) ) )
    return 0;
  if( !( rfMathVectorNormalizeCheckd( edgew ) ) )
    return 0;

  vm0[0] = v0[0] + extendbias * ( edgev[0] - edgeu[0] );
  vm0[1] = v0[1] + extendbias * ( edgev[1] - edgeu[1] );
  vm0[2] = v0[2] + extendbias * ( edgev[2] - edgeu[2] );
  vm1[0] = v1[0] + extendbias * ( edgew[0] - edgev[0] );
  vm1[1] = v1[1] + extendbias * ( edgew[1] - edgev[1] );
  vm1[2] = v1[2] + extendbias * ( edgew[2] - edgev[2] );
  vm2[0] = v2[0] + extendbias * ( edgeu[0] - edgew[0] );
  vm2[1] = v2[1] + extendbias * ( edgeu[1] - edgew[1] );
  vm2[2] = v2[2] + extendbias * ( edgeu[2] - edgew[2] );

  edgeu[0] = vm2[0] - vm0[0];
  edgeu[1] = vm2[1] - vm0[1];
  edgeu[2] = vm2[2] - vm0[2];
  edgev[0] = vm0[0] - vm1[0];
  edgev[1] = vm0[1] - vm1[1];
  edgev[2] = vm0[2] - vm1[2];

  rfMathVectorCrossProduct( plane, edgeu, edgev );
  if( !( rfMathVectorNormalizeCheckd( plane ) ) )
    return 0;
  plane[3] = -rfMathVectorDotProduct( plane, vm0 );
  rfMathVectorCrossProduct( planeu, plane, edgeu );
  if( !( rfMathVectorNormalizeCheckd( planeu ) ) )
    return 0;
  planeu[3] = -rfMathVectorDotProduct( planeu, vm0 );
  rfMathVectorCrossProduct( planev, plane, edgev );
  if( !( rfMathVectorNormalizeCheckd( planev ) ) )
    return 0;
  planev[3] = -rfMathVectorDotProduct( planev, vm0 );

  copy4d2rff( tri->plane, plane );
  f = 1.0 / rfMathPlanePoint( planeu, vm1 );
  tri->edpu[0] = (rff)( planeu[0] * f );
  tri->edpu[1] = (rff)( planeu[1] * f );
  tri->edpu[2] = (rff)( planeu[2] * f );
  tri->edpu[3] = (rff)( planeu[3] * f );
  f = 1.0 / rfMathPlanePoint( planev, vm2 );
  tri->edpv[0] = (rff)( planev[0] * f );
  tri->edpv[1] = (rff)( planev[1] * f );
  tri->edpv[2] = (rff)( planev[2] * f );
  tri->edpv[3] = (rff)( planev[3] * f );

  return 1;
}

#else

int rfBuildTriangle( rfTri * const restrict tri, rff * restrict v0, rff * restrict v1, rff * restrict v2, const rff extendbias )
{
  rff f;
  rff edgeu[3], edgev[3], edgew[3];
  rff planeu[4], planev[4];
  rff vm0[3], vm1[3], vm2[3];

  edgeu[0] = v2[0] - v0[0];
  edgeu[1] = v2[1] - v0[1];
  edgeu[2] = v2[2] - v0[2];
  edgev[0] = v0[0] - v1[0];
  edgev[1] = v0[1] - v1[1];
  edgev[2] = v0[2] - v1[2];
  edgew[0] = v1[0] - v2[0];
  edgew[1] = v1[1] - v2[1];
  edgew[2] = v1[2] - v2[2];
  if( !( rfMathVectorNormalizeCheckd( edgeu ) ) )
    return 0;
  if( !( rfMathVectorNormalizeCheckd( edgev ) ) )
    return 0;
  if( !( rfMathVectorNormalizeCheckd( edgew ) ) )
    return 0;

  vm0[0] = v0[0] + extendbias * ( edgev[0] - edgeu[0] );
  vm0[1] = v0[1] + extendbias * ( edgev[1] - edgeu[1] );
  vm0[2] = v0[2] + extendbias * ( edgev[2] - edgeu[2] );
  vm1[0] = v1[0] + extendbias * ( edgew[0] - edgev[0] );
  vm1[1] = v1[1] + extendbias * ( edgew[1] - edgev[1] );
  vm1[2] = v1[2] + extendbias * ( edgew[2] - edgev[2] );
  vm2[0] = v2[0] + extendbias * ( edgeu[0] - edgew[0] );
  vm2[1] = v2[1] + extendbias * ( edgeu[1] - edgew[1] );
  vm2[2] = v2[2] + extendbias * ( edgeu[2] - edgew[2] );

  edgeu[0] = vm2[0] - vm0[0];
  edgeu[1] = vm2[1] - vm0[1];
  edgeu[2] = vm2[2] - vm0[2];
  edgev[0] = vm0[0] - vm1[0];
  edgev[1] = vm0[1] - vm1[1];
  edgev[2] = vm0[2] - vm1[2];

  rfMathVectorCrossProduct( tri->plane, edgeu, edgev );
  if( !( rfMathVectorNormalizeCheckd( tri->plane ) ) )
    return 0;
  tri->plane[3] = -rfMathVectorDotProduct( tri->plane, vm0 );
  rfMathVectorCrossProduct( planeu, tri->plane, edgeu );
  if( !( rfMathVectorNormalizeCheckd( planeu ) ) )
    return 0;
  planeu[3] = -rfMathVectorDotProduct( planeu, vm0 );
  rfMathVectorCrossProduct( planev, tri->plane, edgev );
  if( !( rfMathVectorNormalizeCheckd( planev ) ) )
    return 0;
  planev[3] = -rfMathVectorDotProduct( planev, vm0 );

  f = 1.0 / rfMathPlanePoint( planeu, vm1 );
  tri->edpu[0] = planeu[0] * f;
  tri->edpu[1] = planeu[1] * f;
  tri->edpu[2] = planeu[2] * f;
  tri->edpu[3] = planeu[3] * f;
  f = 1.0 / rfMathPlanePoint( planev, vm2 );
  tri->edpv[0] = planev[0] * f;
  tri->edpv[1] = planev[1] * f;
  tri->edpv[2] = planev[2] * f;
  tri->edpv[3] = planev[3] * f;

  return 1;
}

#endif


static int rfAddTriangle( rfTri * const restrict tri, rff *v0, rff *v1, rff *v2 )
{
  rff extendbias;
  extendbias = rffmax( rffabs( v0[0] ), rffmax( rffabs( v0[1] ), rffabs( v0[2] ) ) );
  extendbias = rffmax( rffmax( extendbias, rffabs( v1[0] ) ), rffmax( rffabs( v1[1] ), rffabs( v1[2] ) ) );
  extendbias = rffmax( rffmax( extendbias, rffabs( v2[0] ) ), rffmax( rffabs( v2[1] ), rffabs( v2[2] ) ) );
  extendbias *= PREP_TRIANGLE_EXTEND_BIAS;
  return rfBuildTriangle( tri, v0, v1, v2, extendbias );
}


////////////////////////////////////////////////////////////////////////////////



enum
{
  SAMPLE_TRI_LEFT,
  SAMPLE_TRI_RIGHT,

#if PREP_LINK_TRACKING
  SAMPLE_LINK0_LEFT,
  SAMPLE_LINK0_RIGHT,

  SAMPLE_LINK1_LEFT,
  SAMPLE_LINK1_RIGHT,

  SAMPLE_LINK2_LEFT,
  SAMPLE_LINK2_RIGHT,

  SAMPLE_LINK3_LEFT,
  SAMPLE_LINK3_RIGHT,
#endif

  SAMPLE_STATS_COUNT
};

#define HALO_FACTORX (1)
#define HALO_FACTORY (3) 
#define HALO_FACTORZ (9)
#define HALO_INDEX(x,y,z) (((x)*HALO_FACTORX)+((y)*HALO_FACTORY)+((z)*HALO_FACTORZ))

typedef struct
{
  int32_t count[SAMPLE_STATS_COUNT];
} sampleStats;

typedef struct
{
  rff plane;
  rff max;
  sampleStats stats;
} stepSample;

typedef struct
{
  stepSample basesample;
} sampleBin;

typedef struct
{
  sampleBin *bin;
  int binmax;
  rff sortbase;
  rff sortinvsize;
} binSort;


typedef struct
{
  void **list;
  void **alloclist;
  size_t size;
  size_t count;
  size_t max;
} prepPointerBundle;

#if PREP_TRIREF_REORDERING
typedef struct
{
  rff area;
  rff limitsuminv;
} prepTriInfo;
#endif

typedef struct
{
  rfGraphCache *cache;

  mmGrow mgrow;
  prepPointerBundle extsectorbundle[RF_EDGE_COUNT];
  rfPrepSector *halo[3*3*3];

  rff valuestepfactor;
  rff valuetricapfactor;
  rff bincountfactor;
  int bincountmax;
  int sectortricap;
  rfindex stepcap;
  int treestopflag;

  rfTri *trilist;
#if PREP_TRIREF_REORDERING
  prepTriInfo *triinfo;
#endif
  void *nodehashtable;
  rfindex nodehashlimit;

  mmQueue buildtreequeue;

  char _cache0[64];
  mmAtomicL sectorindex;
  char _cache1[64];
  mmAtomicL nodeindex;
  char _cache2[64];
  mmAtomicL trirefcount;
  mmAtomicL sector16size;
  mmAtomicL sector32size;
  mmAtomicL sector64size;
  mmAtomic32 sectortrimax;
  char _cache4[64];
  mmAtomicL stepcount;
  char _cache5[64];

} buildContext;


typedef struct
{
  rfindex index;
  rff limit[RF_EDGE_COUNT];
  uint32_t limflags;
} triLocal;

#define TRIL_LIMFLAGS_MINX (1<<0)
#define TRIL_LIMFLAGS_MAXX (1<<4)
#define TRIL_LIMFLAGS_MINY (1<<8)
#define TRIL_LIMFLAGS_MAXY (1<<12)
#define TRIL_LIMFLAGS_MINZ (1<<16)
#define TRIL_LIMFLAGS_MAXZ (1<<20)

#define TRIL_LIMFLAGS_OUTX (1<<24)
#define TRIL_LIMFLAGS_OUTY (1<<25)
#define TRIL_LIMFLAGS_OUTZ (1<<26)

#define TRI_UPDATE_LIMITS (1)
#define TRI_UPDATE_LIMITS_DOUBLE (1)

#define TRI_MINIMUM_LIMITS_RATIO (1.0/512.0)

#define TRI_MINIMUM_PERPCLIP_RATIO (1.0/256.0)


typedef struct
{
  rfPrepNode *prepnode;
  int axis;
  rff plane;
  rfindex tricountleft;
  rfindex tricountright;
} stepOp;


static const int sideIndexTable[3*4] =
{
  RF_EDGE_MINY, RF_EDGE_MAXY, RF_EDGE_MINZ, RF_EDGE_MAXZ,
  RF_EDGE_MINX, RF_EDGE_MAXX, RF_EDGE_MINZ, RF_EDGE_MAXZ,
  RF_EDGE_MINX, RF_EDGE_MAXX, RF_EDGE_MINY, RF_EDGE_MAXY
};

static const int sideAxisTable[RF_AXIS_COUNT*2] = { 1,2, 0,2, 0,1 };


////


static void triLocalLimits( triLocal *tl, rff *v0, rff *v1, rff *v2 );
static void triUpdateLimitsX( triLocal *tl, rfTri *tri, rff *llims, rff *rlims, rff cutplane );
static void triUpdateLimitsY( triLocal *tl, rfTri *tri, rff *llims, rff *rlims, rff cutplane );
static void triUpdateLimitsZ( triLocal *tl, rfTri *tri, rff *llims, rff *rlims, rff cutplane );
static int triClipLimits( triLocal *tl, rfTri *tri, rff *box );


static void triLocalLimits( triLocal *tl, rff *v0, rff *v1, rff *v2 )
{
  rff range[3], maxrange, diff;

  tl->limflags = ( TRIL_LIMFLAGS_MINX << 0 ) | ( TRIL_LIMFLAGS_MAXX << 0 ) | ( TRIL_LIMFLAGS_MINY << 0 ) | ( TRIL_LIMFLAGS_MAXY << 0 ) | ( TRIL_LIMFLAGS_MINZ << 0 ) | ( TRIL_LIMFLAGS_MAXZ << 0 );

  tl->limit[RF_EDGE_MINX] = v0[0];
  tl->limit[RF_EDGE_MAXX] = v0[0];
  if( v1[0] < tl->limit[RF_EDGE_MINX] )
  {
    tl->limit[RF_EDGE_MINX] = v1[0];
    tl->limflags &= ~( TRIL_LIMFLAGS_MINX * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MINX << 1;
  }
  if( v1[0] > tl->limit[RF_EDGE_MAXX] )
  {
    tl->limit[RF_EDGE_MAXX] = v1[0];
    tl->limflags &= ~( TRIL_LIMFLAGS_MAXX * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MAXX << 1;
  }
  if( v2[0] < tl->limit[RF_EDGE_MINX] )
  {
    tl->limit[RF_EDGE_MINX] = v2[0];
    tl->limflags &= ~( TRIL_LIMFLAGS_MINX * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MINX << 2;
  }
  if( v2[0] > tl->limit[RF_EDGE_MAXX] )
  {
    tl->limit[RF_EDGE_MAXX] = v2[0];
    tl->limflags &= ~( TRIL_LIMFLAGS_MAXX * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MAXX << 2;
  }

  tl->limit[RF_EDGE_MINY] = v0[1];
  tl->limit[RF_EDGE_MAXY] = v0[1];
  if( v1[1] < tl->limit[RF_EDGE_MINY] )
  {
    tl->limit[RF_EDGE_MINY] = v1[1];
    tl->limflags &= ~( TRIL_LIMFLAGS_MINY * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MINY << 1;
  }
  if( v1[1] > tl->limit[RF_EDGE_MAXY] )
  {
    tl->limit[RF_EDGE_MAXY] = v1[1];
    tl->limflags &= ~( TRIL_LIMFLAGS_MAXY * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MAXY << 1;
  }
  if( v2[1] < tl->limit[RF_EDGE_MINY] )
  {
    tl->limit[RF_EDGE_MINY] = v2[1];
    tl->limflags &= ~( TRIL_LIMFLAGS_MINY * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MINY << 2;
  }
  if( v2[1] > tl->limit[RF_EDGE_MAXY] )
  {
    tl->limit[RF_EDGE_MAXY] = v2[1];
    tl->limflags &= ~( TRIL_LIMFLAGS_MAXY * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MAXY << 2;
  }

  tl->limit[RF_EDGE_MINZ] = v0[2];
  tl->limit[RF_EDGE_MAXZ] = v0[2];
  if( v1[2] < tl->limit[RF_EDGE_MINZ] )
  {
    tl->limit[RF_EDGE_MINZ] = v1[2];
    tl->limflags &= ~( TRIL_LIMFLAGS_MINZ * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MINZ << 1;
  }
  if( v1[2] > tl->limit[RF_EDGE_MAXZ] )
  {
    tl->limit[RF_EDGE_MAXZ] = v1[2];
    tl->limflags &= ~( TRIL_LIMFLAGS_MAXZ * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MAXZ << 1;
  }
  if( v2[2] < tl->limit[RF_EDGE_MINZ] )
  {
    tl->limit[RF_EDGE_MINZ] = v2[2];
    tl->limflags &= ~( TRIL_LIMFLAGS_MINZ * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MINZ << 2;
  }
  if( v2[2] > tl->limit[RF_EDGE_MAXZ] )
  {
    tl->limit[RF_EDGE_MAXZ] = v2[2];
    tl->limflags &= ~( TRIL_LIMFLAGS_MAXZ * 7 );
    tl->limflags |= TRIL_LIMFLAGS_MAXZ << 2;
  }

  range[0] = tl->limit[RF_EDGE_MAXX] - tl->limit[RF_EDGE_MINX];
  range[1] = tl->limit[RF_EDGE_MAXY] - tl->limit[RF_EDGE_MINY];
  range[2] = tl->limit[RF_EDGE_MAXZ] - tl->limit[RF_EDGE_MINZ];
  maxrange = range[0];
  if( range[1] > maxrange )
    maxrange = range[1];
  if( range[2] > maxrange )
    maxrange = range[2];

  maxrange *= TRI_MINIMUM_LIMITS_RATIO;
  if( range[0] < maxrange )
  {
    diff = (1.0/2.0) * ( maxrange - range[0] );
    tl->limit[RF_EDGE_MINX] -= diff;
    tl->limit[RF_EDGE_MAXX] += diff;
    tl->limflags |= ( TRIL_LIMFLAGS_MINX * 7 ) | ( TRIL_LIMFLAGS_MAXX * 7 ) | TRIL_LIMFLAGS_OUTX;
    if( tl->limit[RF_EDGE_MINX] == tl->limit[RF_EDGE_MAXX] )
    {
      tl->limit[RF_EDGE_MAXX] = rfnextafter( tl->limit[RF_EDGE_MINX], RFF_MAX );
      if( tl->limit[RF_EDGE_MINX] == tl->limit[RF_EDGE_MAXX] )
        tl->limit[RF_EDGE_MAXX] = tl->limit[RF_EDGE_MINX] + RFF_MIN;
    }
  }
  if( range[1] < maxrange )
  {
    diff = (1.0/2.0) * ( maxrange - range[1] );
    tl->limit[RF_EDGE_MINY] -= diff;
    tl->limit[RF_EDGE_MAXY] += diff;
    tl->limflags |= ( TRIL_LIMFLAGS_MINY * 7 ) | ( TRIL_LIMFLAGS_MAXY * 7 ) | TRIL_LIMFLAGS_OUTY;
    if( tl->limit[RF_EDGE_MINY] == tl->limit[RF_EDGE_MAXY] )
    {
      tl->limit[RF_EDGE_MAXY] = rfnextafter( tl->limit[RF_EDGE_MINY], RFF_MAX );
      if( tl->limit[RF_EDGE_MINY] == tl->limit[RF_EDGE_MAXY] )
        tl->limit[RF_EDGE_MAXY] = tl->limit[RF_EDGE_MINY] + RFF_MIN;
    }
  }
  if( range[2] < maxrange )
  {
    diff = (1.0/2.0) * ( maxrange - range[2] );
    tl->limit[RF_EDGE_MINZ] -= diff;
    tl->limit[RF_EDGE_MAXZ] += diff;
    tl->limflags |= ( TRIL_LIMFLAGS_MINZ * 7 ) | ( TRIL_LIMFLAGS_MAXZ * 7 ) | TRIL_LIMFLAGS_OUTZ;
    if( tl->limit[RF_EDGE_MINZ] == tl->limit[RF_EDGE_MAXZ] )
    {
      tl->limit[RF_EDGE_MAXZ] = rfnextafter( tl->limit[RF_EDGE_MINZ], RFF_MAX );
      if( tl->limit[RF_EDGE_MINZ] == tl->limit[RF_EDGE_MAXZ] )
        tl->limit[RF_EDGE_MAXZ] = tl->limit[RF_EDGE_MINZ] + RFF_MIN;
    }
  }

  return;
}



static int triClipLimits( triLocal *tl, rfTri *tri, rff *box )
{
  rff outlimits[RF_EDGE_COUNT];
  if( !( rfEdgeIntersection( tl->limit, box ) ) )
    return 0;
  if( tl->limit[RF_EDGE_MINX] < box[RF_EDGE_MINX] )
    triUpdateLimitsX( tl, tri, outlimits, tl->limit, box[RF_EDGE_MINX] );
  if( tl->limit[RF_EDGE_MAXX] > box[RF_EDGE_MAXX] )
    triUpdateLimitsX( tl, tri, tl->limit, outlimits, box[RF_EDGE_MAXX] );
  if( tl->limit[RF_EDGE_MINY] < box[RF_EDGE_MINY] )
    triUpdateLimitsY( tl, tri, outlimits, tl->limit, box[RF_EDGE_MINY] );
  if( tl->limit[RF_EDGE_MAXY] > box[RF_EDGE_MAXY] )
    triUpdateLimitsY( tl, tri, tl->limit, outlimits, box[RF_EDGE_MAXY] );
  if( tl->limit[RF_EDGE_MINZ] < box[RF_EDGE_MINZ] )
    triUpdateLimitsZ( tl, tri, outlimits, tl->limit, box[RF_EDGE_MINZ] );
  if( tl->limit[RF_EDGE_MAXZ] > box[RF_EDGE_MAXZ] )
    triUpdateLimitsZ( tl, tri, tl->limit, outlimits, box[RF_EDGE_MAXZ] );
  if( !( rfEdgeIntersection( tl->limit, box ) ) )
    return 0;
  return 1;
}


#ifndef SWAP
 #define SWAP(a,b) ({rff _a=a;a=b;b=_a;})
#endif


/**
 * When I wrote this, only the compiler and I understood what I was doing.
 * Now, only the compiler knows.
 */
static void triUpdateLimitsX( triLocal *tl, rfTri *tri, rff *llims, rff *rlims, rff cutplane )
{
  int a, v0, v1, clipcount;
#if TRI_UPDATE_LIMITS_DOUBLE
  double pty, ptz;
  double crpe[3], crpsy, crpsz, cresy, cresz, d;
  double limits[4];
  rff edpw[4], *triplane[5], *edplane, *plane;
#else
  rff pty, ptz, edpw[4];
  rff crpe[3], crpsy, crpsz, cresy, cresz, d;
  rff *triplane[5], *edplane, limits[4], *plane;
#endif
  static const int edvertices[3*2] = { 2, 0, 0, 1, 1, 2 };

  if( tl->limflags & TRIL_LIMFLAGS_OUTX )
  {
    memcpy( rlims, tl->limit, 6*sizeof(rff) );
    return;
  }

  edpw[0] = -( tri->edpu[0] + tri->edpv[0] );
  edpw[1] = -( tri->edpu[1] + tri->edpv[1] );
  edpw[2] = -( tri->edpu[2] + tri->edpv[2] );
  edpw[3] = -( tri->edpu[3] + tri->edpv[3] ) + 1.0;

  triplane[0] = tri->edpu;
  triplane[1] = tri->edpv;
  triplane[2] = edpw;
  triplane[3] = tri->edpu;
  triplane[4] = tri->edpv;

  limits[0] = tl->limit[RF_EDGE_MINY];
  limits[1] = tl->limit[RF_EDGE_MAXY];
  limits[2] = tl->limit[RF_EDGE_MINZ];
  limits[3] = tl->limit[RF_EDGE_MAXZ];

  llims[RF_EDGE_MINY] = rlims[RF_EDGE_MINY] = limits[1];
  llims[RF_EDGE_MAXY] = rlims[RF_EDGE_MAXY] = limits[0];
  llims[RF_EDGE_MINZ] = rlims[RF_EDGE_MINZ] = limits[3];
  llims[RF_EDGE_MAXZ] = rlims[RF_EDGE_MAXZ] = limits[2];

  llims[RF_EDGE_MINX] = tl->limit[RF_EDGE_MINX];
  rlims[RF_EDGE_MAXX] = tl->limit[RF_EDGE_MAXX];
  llims[RF_EDGE_MAXX] = cutplane;
  rlims[RF_EDGE_MINX] = cutplane;

  clipcount = 0;
  for( a = 0 ; a < 3 ; a++ )
  {
    edplane = triplane[a];

    rfMathVectorCrossProduct( crpe, tri->plane, edplane );

    crpsy = tri->plane[2];
    crpsz = -tri->plane[1];
    cresy = -edplane[2];
    cresz = edplane[1];
    d = ( tri->plane[1] * cresy ) + ( tri->plane[2] * cresz );
    if( rffabs( d ) < TRI_MINIMUM_PERPCLIP_RATIO )
      continue;
    d = -1.0 / d;

    /* Magic. Do not touch. */
    pty = d * ( cresy*tri->plane[3] + crpsy*edplane[3] + crpe[1]*cutplane );
    ptz = d * ( cresz*tri->plane[3] + crpsz*edplane[3] + crpe[2]*cutplane );
    plane = triplane[a+1];
    if( ( cutplane*plane[0] + pty*plane[1] + ptz*plane[2] + plane[3] ) < 0.0 )
      continue;
    plane = triplane[a+2];
    if( ( cutplane*plane[0] + pty*plane[1] + ptz*plane[2] + plane[3] ) < 0.0 )
      continue;
    clipcount++;

    if( pty < limits[0] )
      pty = limits[0];
    else if( pty > limits[1] )
      pty = limits[1];
    if( ptz < limits[2] )
      ptz = limits[2];
    else if( ptz > limits[3] )
      ptz = limits[3];

    if( pty < llims[RF_EDGE_MINY] )
      llims[RF_EDGE_MINY] = pty;
    if( pty > llims[RF_EDGE_MAXY] )
      llims[RF_EDGE_MAXY] = pty;
    if( pty < rlims[RF_EDGE_MINY] )
      rlims[RF_EDGE_MINY] = pty;
    if( pty > rlims[RF_EDGE_MAXY] )
      rlims[RF_EDGE_MAXY] = pty;

    if( ptz < llims[RF_EDGE_MINZ] )
      llims[RF_EDGE_MINZ] = ptz;
    if( ptz > llims[RF_EDGE_MAXZ] )
      llims[RF_EDGE_MAXZ] = ptz;
    if( ptz < rlims[RF_EDGE_MINZ] )
      rlims[RF_EDGE_MINZ] = ptz;
    if( ptz > rlims[RF_EDGE_MAXZ] )
      rlims[RF_EDGE_MAXZ] = ptz;

    v0 = edvertices[ ( a << 1 ) + 0 ];
    v1 = edvertices[ ( a << 1 ) + 1 ];
    if( ( tl->limflags & ( TRIL_LIMFLAGS_MINX << v1 ) ) || ( tl->limflags & ( TRIL_LIMFLAGS_MAXX << v0 ) ) )
    {
      v1 = edvertices[ ( a << 1 ) + 0 ];
      v0 = edvertices[ ( a << 1 ) + 1 ];
    }

    if( tl->limflags & ( TRIL_LIMFLAGS_MINY << v0 ) )
      llims[RF_EDGE_MINY] = limits[0];
    if( tl->limflags & ( TRIL_LIMFLAGS_MINY << v1 ) )
      rlims[RF_EDGE_MINY] = limits[0];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXY << v0 ) )
      llims[RF_EDGE_MAXY] = limits[1];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXY << v1 ) )
      rlims[RF_EDGE_MAXY] = limits[1];

    if( tl->limflags & ( TRIL_LIMFLAGS_MINZ << v0 ) )
      llims[RF_EDGE_MINZ] = limits[2];
    if( tl->limflags & ( TRIL_LIMFLAGS_MINZ << v1 ) )
      rlims[RF_EDGE_MINZ] = limits[2];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXZ << v0 ) )
      llims[RF_EDGE_MAXZ] = limits[3];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXZ << v1 ) )
      rlims[RF_EDGE_MAXZ] = limits[3];
  }

  if( clipcount <= 1 )
  {
    llims[RF_EDGE_MINY] = rlims[RF_EDGE_MINY] = limits[0];
    llims[RF_EDGE_MAXY] = rlims[RF_EDGE_MAXY] = limits[1];
    llims[RF_EDGE_MINZ] = rlims[RF_EDGE_MINZ] = limits[2];
    llims[RF_EDGE_MAXZ] = rlims[RF_EDGE_MAXZ] = limits[3];
  }

  if( llims[2] > llims[3] )
    SWAP( llims[2], llims[3] );
  if( llims[4] > llims[5] )
    SWAP( llims[4], llims[5] );
  if( rlims[2] > rlims[3] )
    SWAP( rlims[2], rlims[3] );
  if( rlims[4] > rlims[5] )
    SWAP( rlims[4], rlims[5] );

  if( tl->limflags & TRIL_LIMFLAGS_OUTY )
  {
    llims[RF_EDGE_MINY] = rlims[RF_EDGE_MINY] = limits[0];
    llims[RF_EDGE_MAXY] = rlims[RF_EDGE_MAXY] = limits[1];
  }
  if( tl->limflags & TRIL_LIMFLAGS_OUTZ )
  {
    llims[RF_EDGE_MINZ] = rlims[RF_EDGE_MINZ] = limits[2];
    llims[RF_EDGE_MAXZ] = rlims[RF_EDGE_MAXZ] = limits[3];
  }

  return;
}


/**
 * Heed my warning, mortal!
 * 
 * Before you even lay eyes on this ancient scroll, burn the incense and chant the incantation :
 * "My name is Ozymandias, king of kings: Look on my works, ye Mighty, and despair!"
 */
static void triUpdateLimitsY( triLocal *tl, rfTri *tri, rff *llims, rff *rlims, rff cutplane )
{
  int a, v0, v1, clipcount;
#if TRI_UPDATE_LIMITS_DOUBLE
  double ptx, ptz;
  double crpe[3], crpsx, crpsz, cresx, cresz, d;
  double limits[4];
  rff edpw[4], *triplane[5], *edplane, *plane;
#else
  rff ptx, ptz, edpw[4];
  rff crpe[3], crpsx, crpsz, cresx, cresz, d;
  rff *triplane[5], *edplane, limits[4], *plane;
#endif
  static const int edvertices[3*2] = { 2, 0, 0, 1, 1, 2 };

  if( tl->limflags & TRIL_LIMFLAGS_OUTY )
  {
    memcpy( rlims, tl->limit, 6*sizeof(rff) );
    return;
  }

  edpw[0] = -( tri->edpu[0] + tri->edpv[0] );
  edpw[1] = -( tri->edpu[1] + tri->edpv[1] );
  edpw[2] = -( tri->edpu[2] + tri->edpv[2] );
  edpw[3] = -( tri->edpu[3] + tri->edpv[3] ) + 1.0;

  triplane[0] = tri->edpu;
  triplane[1] = tri->edpv;
  triplane[2] = edpw;
  triplane[3] = tri->edpu;
  triplane[4] = tri->edpv;

  limits[0] = tl->limit[RF_EDGE_MINX];
  limits[1] = tl->limit[RF_EDGE_MAXX];
  limits[2] = tl->limit[RF_EDGE_MINZ];
  limits[3] = tl->limit[RF_EDGE_MAXZ];

  llims[RF_EDGE_MINX] = rlims[RF_EDGE_MINX] = limits[1];
  llims[RF_EDGE_MAXX] = rlims[RF_EDGE_MAXX] = limits[0];
  llims[RF_EDGE_MINZ] = rlims[RF_EDGE_MINZ] = limits[3];
  llims[RF_EDGE_MAXZ] = rlims[RF_EDGE_MAXZ] = limits[2];

  llims[RF_EDGE_MINY] = tl->limit[RF_EDGE_MINY];
  rlims[RF_EDGE_MAXY] = tl->limit[RF_EDGE_MAXY];
  llims[RF_EDGE_MAXY] = cutplane;
  rlims[RF_EDGE_MINY] = cutplane;

  clipcount = 0;
  for( a = 0 ; a < 3 ; a++ )
  {
    edplane = triplane[a];

    rfMathVectorCrossProduct( crpe, tri->plane, edplane );

    crpsx = -tri->plane[2];
    crpsz = tri->plane[0];
    cresx = edplane[2];
    cresz = -edplane[0];
    d = ( tri->plane[0] * cresx ) + ( tri->plane[2] * cresz );
    if( rffabs( d ) < TRI_MINIMUM_PERPCLIP_RATIO )
      continue;
    d = -1.0 / d;

    ptx = d * ( cresx*tri->plane[3] + crpsx*edplane[3] + crpe[0]*cutplane );
    ptz = d * ( cresz*tri->plane[3] + crpsz*edplane[3] + crpe[2]*cutplane );
    plane = triplane[a+1];
    if( ( ptx*plane[0] + cutplane*plane[1] + ptz*plane[2] + plane[3] ) < 0.0 )
      continue;
    plane = triplane[a+2];
    if( ( ptx*plane[0] + cutplane*plane[1] + ptz*plane[2] + plane[3] ) < 0.0 )
      continue;
    clipcount++;

    if( ptx < limits[0] )
      ptx = limits[0];
    else if( ptx > limits[1] )
      ptx = limits[1];
    if( ptz < limits[2] )
      ptz = limits[2];
    else if( ptz > limits[3] )
      ptz = limits[3];

    if( ptx < llims[RF_EDGE_MINX] )
      llims[RF_EDGE_MINX] = ptx;
    if( ptx > llims[RF_EDGE_MAXX] )
      llims[RF_EDGE_MAXX] = ptx;
    if( ptx < rlims[RF_EDGE_MINX] )
      rlims[RF_EDGE_MINX] = ptx;
    if( ptx > rlims[RF_EDGE_MAXX] )
      rlims[RF_EDGE_MAXX] = ptx;

    if( ptz < llims[RF_EDGE_MINZ] )
      llims[RF_EDGE_MINZ] = ptz;
    if( ptz > llims[RF_EDGE_MAXZ] )
      llims[RF_EDGE_MAXZ] = ptz;
    if( ptz < rlims[RF_EDGE_MINZ] )
      rlims[RF_EDGE_MINZ] = ptz;
    if( ptz > rlims[RF_EDGE_MAXZ] )
      rlims[RF_EDGE_MAXZ] = ptz;

    v0 = edvertices[ ( a << 1 ) + 0 ];
    v1 = edvertices[ ( a << 1 ) + 1 ];
    if( ( tl->limflags & ( TRIL_LIMFLAGS_MINY << v1 ) ) || ( tl->limflags & ( TRIL_LIMFLAGS_MAXY << v0 ) ) )
    {
      v1 = edvertices[ ( a << 1 ) + 0 ];
      v0 = edvertices[ ( a << 1 ) + 1 ];
    }

    if( tl->limflags & ( TRIL_LIMFLAGS_MINX << v0 ) )
      llims[RF_EDGE_MINX] = limits[0];
    if( tl->limflags & ( TRIL_LIMFLAGS_MINX << v1 ) )
      rlims[RF_EDGE_MINX] = limits[0];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXX << v0 ) )
      llims[RF_EDGE_MAXX] = limits[1];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXX << v1 ) )
      rlims[RF_EDGE_MAXX] = limits[1];

    if( tl->limflags & ( TRIL_LIMFLAGS_MINZ << v0 ) )
      llims[RF_EDGE_MINZ] = limits[2];
    if( tl->limflags & ( TRIL_LIMFLAGS_MINZ << v1 ) )
      rlims[RF_EDGE_MINZ] = limits[2];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXZ << v0 ) )
      llims[RF_EDGE_MAXZ] = limits[3];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXZ << v1 ) )
      rlims[RF_EDGE_MAXZ] = limits[3];
  }

  if( clipcount <= 1 )
  {
    llims[RF_EDGE_MINX] = rlims[RF_EDGE_MINX] = limits[0];
    llims[RF_EDGE_MAXX] = rlims[RF_EDGE_MAXX] = limits[1];
    llims[RF_EDGE_MINZ] = rlims[RF_EDGE_MINZ] = limits[2];
    llims[RF_EDGE_MAXZ] = rlims[RF_EDGE_MAXZ] = limits[3];
  }

  if( llims[0] > llims[1] )
    SWAP( llims[0], llims[1] );
  if( llims[4] > llims[5] )
    SWAP( llims[4], llims[5] );
  if( rlims[0] > rlims[1] )
    SWAP( rlims[0], rlims[1] );
  if( rlims[4] > rlims[5] )
    SWAP( rlims[4], rlims[5] );

  if( tl->limflags & TRIL_LIMFLAGS_OUTX )
  {
    llims[RF_EDGE_MINX] = rlims[RF_EDGE_MINX] = limits[0];
    llims[RF_EDGE_MAXX] = rlims[RF_EDGE_MAXX] = limits[1];
  }
  if( tl->limflags & TRIL_LIMFLAGS_OUTZ )
  {
    llims[RF_EDGE_MINZ] = rlims[RF_EDGE_MINZ] = limits[2];
    llims[RF_EDGE_MAXZ] = rlims[RF_EDGE_MAXZ] = limits[3];
  }

  return;
}


/**
 * For the brave souls who get this far: You are the chosen ones,
 * the valiant knights of programming who toil away, without rest,
 * fixing our most awful code. To you, true saviors, kings of men,
 * I say this: never gonna give you up, never gonna let you down,
 * never gonna run around and desert you. Never gonna make you cry,
 * never gonna say goodbye. Never gonna tell a lie and hurt you.
 */
static void triUpdateLimitsZ( triLocal *tl, rfTri *tri, rff *llims, rff *rlims, rff cutplane )
{
  int a, v0, v1, clipcount;
#if TRI_UPDATE_LIMITS_DOUBLE
  double ptx, pty;
  double crpe[3], crpsx, crpsy, cresx, cresy, d;
  double limits[4];
  rff edpw[4], *triplane[5], *edplane, *plane;
#else
  rff ptx, pty, edpw[4];
  rff crpe[3], crpsx, crpsy, cresx, cresy, d;
  rff *triplane[5], *edplane, limits[4], *plane;
#endif
  static const int edvertices[3*2] = { 2, 0, 0, 1, 1, 2 };

  if( tl->limflags & TRIL_LIMFLAGS_OUTZ )
  {
    memcpy( rlims, tl->limit, 6*sizeof(rff) );
    return;
  }

  edpw[0] = -( tri->edpu[0] + tri->edpv[0] );
  edpw[1] = -( tri->edpu[1] + tri->edpv[1] );
  edpw[2] = -( tri->edpu[2] + tri->edpv[2] );
  edpw[3] = -( tri->edpu[3] + tri->edpv[3] ) + 1.0;

  triplane[0] = tri->edpu;
  triplane[1] = tri->edpv;
  triplane[2] = edpw;
  triplane[3] = tri->edpu;
  triplane[4] = tri->edpv;

  limits[0] = tl->limit[RF_EDGE_MINX];
  limits[1] = tl->limit[RF_EDGE_MAXX];
  limits[2] = tl->limit[RF_EDGE_MINY];
  limits[3] = tl->limit[RF_EDGE_MAXY];

  llims[RF_EDGE_MINX] = rlims[RF_EDGE_MINX] = limits[1];
  llims[RF_EDGE_MAXX] = rlims[RF_EDGE_MAXX] = limits[0];
  llims[RF_EDGE_MINY] = rlims[RF_EDGE_MINY] = limits[3];
  llims[RF_EDGE_MAXY] = rlims[RF_EDGE_MAXY] = limits[2];

  llims[RF_EDGE_MINZ] = tl->limit[RF_EDGE_MINZ];
  rlims[RF_EDGE_MAXZ] = tl->limit[RF_EDGE_MAXZ];
  llims[RF_EDGE_MAXZ] = cutplane;
  rlims[RF_EDGE_MINZ] = cutplane;

  clipcount = 0;
  for( a = 0 ; a < 3 ; a++ )
  {
    edplane = triplane[a];

    rfMathVectorCrossProduct( crpe, tri->plane, edplane );

    crpsx = tri->plane[1];
    crpsy = -tri->plane[0];
    cresx = -edplane[1];
    cresy = edplane[0];
    d = ( tri->plane[0] * cresx ) + ( tri->plane[1] * cresy );
    if( rffabs( d ) < TRI_MINIMUM_PERPCLIP_RATIO )
      continue;
    d = -1.0 / d;

    ptx = d * ( cresx*tri->plane[3] + crpsx*edplane[3] + crpe[0]*cutplane );
    pty = d * ( cresy*tri->plane[3] + crpsy*edplane[3] + crpe[1]*cutplane );
    plane = triplane[a+1];
    if( ( ptx*plane[0] + pty*plane[1] + cutplane*plane[2] + plane[3] ) < 0.0 )
      continue;
    plane = triplane[a+2];
    if( ( ptx*plane[0] + pty*plane[1] + cutplane*plane[2] + plane[3] ) < 0.0 )
      continue;
    clipcount++;

    if( ptx < limits[0] )
      ptx = limits[0];
    else if( ptx > limits[1] )
      ptx = limits[1];
    if( pty < limits[2] )
      pty = limits[2];
    else if( pty > limits[3] )
      pty = limits[3];

    if( ptx < llims[RF_EDGE_MINX] )
      llims[RF_EDGE_MINX] = ptx;
    if( ptx > llims[RF_EDGE_MAXX] )
      llims[RF_EDGE_MAXX] = ptx;
    if( ptx < rlims[RF_EDGE_MINX] )
      rlims[RF_EDGE_MINX] = ptx;
    if( ptx > rlims[RF_EDGE_MAXX] )
      rlims[RF_EDGE_MAXX] = ptx;

    if( pty < llims[RF_EDGE_MINY] )
      llims[RF_EDGE_MINY] = pty;
    if( pty > llims[RF_EDGE_MAXY] )
      llims[RF_EDGE_MAXY] = pty;
    if( pty < rlims[RF_EDGE_MINY] )
      rlims[RF_EDGE_MINY] = pty;
    if( pty > rlims[RF_EDGE_MAXY] )
      rlims[RF_EDGE_MAXY] = pty;

    v0 = edvertices[ ( a << 1 ) + 0 ];
    v1 = edvertices[ ( a << 1 ) + 1 ];
    if( ( tl->limflags & ( TRIL_LIMFLAGS_MINZ << v1 ) ) || ( tl->limflags & ( TRIL_LIMFLAGS_MAXZ << v0 ) ) )
    {
      v1 = edvertices[ ( a << 1 ) + 0 ];
      v0 = edvertices[ ( a << 1 ) + 1 ];
    }

    if( tl->limflags & ( TRIL_LIMFLAGS_MINX << v0 ) )
      llims[RF_EDGE_MINX] = limits[0];
    if( tl->limflags & ( TRIL_LIMFLAGS_MINX << v1 ) )
      rlims[RF_EDGE_MINX] = limits[0];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXX << v0 ) )
      llims[RF_EDGE_MAXX] = limits[1];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXX << v1 ) )
      rlims[RF_EDGE_MAXX] = limits[1];

    if( tl->limflags & ( TRIL_LIMFLAGS_MINY << v0 ) )
      llims[RF_EDGE_MINY] = limits[2];
    if( tl->limflags & ( TRIL_LIMFLAGS_MINY << v1 ) )
      rlims[RF_EDGE_MINY] = limits[2];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXY << v0 ) )
      llims[RF_EDGE_MAXY] = limits[3];
    if( tl->limflags & ( TRIL_LIMFLAGS_MAXY << v1 ) )
      rlims[RF_EDGE_MAXY] = limits[3];
  }

  if( clipcount <= 1 )
  {
    llims[RF_EDGE_MINX] = rlims[RF_EDGE_MINX] = limits[0];
    llims[RF_EDGE_MAXX] = rlims[RF_EDGE_MAXX] = limits[1];
    llims[RF_EDGE_MINY] = rlims[RF_EDGE_MINY] = limits[2];
    llims[RF_EDGE_MAXY] = rlims[RF_EDGE_MAXY] = limits[3];
  }

  if( llims[0] > llims[1] )
    SWAP( llims[0], llims[1] );
  if( llims[2] > llims[3] )
    SWAP( llims[2], llims[3] );
  if( rlims[0] > rlims[1] )
    SWAP( rlims[0], rlims[1] );
  if( rlims[2] > rlims[3] )
    SWAP( rlims[2], rlims[3] );

  if( tl->limflags & TRIL_LIMFLAGS_OUTX )
  {
    llims[RF_EDGE_MINX] = rlims[RF_EDGE_MINX] = limits[0];
    llims[RF_EDGE_MAXX] = rlims[RF_EDGE_MAXX] = limits[1];
  }
  if( tl->limflags & TRIL_LIMFLAGS_OUTY )
  {
    llims[RF_EDGE_MINY] = rlims[RF_EDGE_MINY] = limits[2];
    llims[RF_EDGE_MAXY] = rlims[RF_EDGE_MAXY] = limits[3];
  }

  return;
}


////


static void prepPointerBundleInit( rfGraphCache *cache, prepPointerBundle *bundle, void **staticbuffer, size_t staticsize, size_t max )
{
  bundle->list = staticbuffer;
  bundle->alloclist = 0;
  bundle->size = staticsize;
  bundle->count = 0;
  bundle->max = max;
  if( !( bundle->list ) && ( bundle->size ) )
  {
    bundle->list = mmVolumeAlloc( &cache->memvolume, bundle->size * sizeof(void *) );
    bundle->alloclist = bundle->list;
  }
  return;
}

static void prepPointerBundleStore( rfGraphCache *cache, prepPointerBundle *bundle, void *pointer )
{
  size_t staticsize;
  void *staticbuffer;
  if( bundle->count >= bundle->size )
  {
    if( bundle->count >= bundle->max )
      return;
    if( bundle->alloclist )
    {
      bundle->size <<= 1;
      bundle->list = mmVolumeRealloc( &cache->memvolume, bundle->alloclist, bundle->size * sizeof(void *) );
    }
    else
    {
      staticsize = bundle->size;
      staticbuffer = bundle->list;
      bundle->size <<= 1;
      if( bundle->size < 256 )
        bundle->size = 256;
      bundle->list = mmVolumeAlloc( &cache->memvolume, bundle->size * sizeof(void *) );
      memcpy( bundle->list, staticbuffer, staticsize * sizeof(void *) );
    }
    bundle->alloclist = bundle->list;
  }
  bundle->list[ bundle->count++ ] = pointer;
  return;
}

static void prepPointerBundleFree( rfGraphCache *cache, prepPointerBundle *bundle )
{
  if( bundle->alloclist )
    mmVolumeRelease( &cache->memvolume, bundle->alloclist );
  return;
}


/* Build the list of all sectors linked to side */
static void prepBuildLinkBundle( rfGraphCache *cache, rfPrepNode *linknode, int edgeindex, rff *quadedge, prepPointerBundle *bundle )
{
  int nodeaxis, sideaxis, quadoffset;

  if( bundle->count >= bundle->max )
    return;

  if( linknode->status == PREPNODE_STATUS_SECTOR )
    prepPointerBundleStore( cache, bundle, linknode->child[0].p );
  else if( linknode->status == PREPNODE_STATUS_NODE )
  {
    nodeaxis = PREP_NODE_GETAXIS( linknode );
    sideaxis = RF_EDGE_TO_AXIS( edgeindex );
    if( nodeaxis == sideaxis )
      prepBuildLinkBundle( cache, linknode->child[ ( ~edgeindex ) & 0x1 ].p, edgeindex, quadedge, bundle );
    else
    {
      quadoffset = 0;
      if( sideAxisTable[ sideaxis << 1 ] != nodeaxis )
        quadoffset = 2;
      if( linknode->plane > quadedge[quadoffset+RF_EDGE_MIN] )
        prepBuildLinkBundle( cache, linknode->child[0].p, edgeindex, quadedge, bundle );
      if( linknode->plane < quadedge[quadoffset+RF_EDGE_MAX] )
        prepBuildLinkBundle( cache, linknode->child[1].p, edgeindex, quadedge, bundle );
    }
  }
/*
  else
  {
    fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
    abort();
  }
*/

  return;
}


////


static inline sampleBin *binResolve( binSort *bsort, rff coord )
{
  int index;
  index = (int)floor( ( coord - bsort->sortbase ) * bsort->sortinvsize );
  if( (unsigned int)index > bsort->binmax )
  {
    index = 0;
    if( coord >= bsort->sortbase )
      index = bsort->binmax;
  }
  return &bsort->bin[ index ];
}


static inline stepSample *binSortCoord( binSort *bsort, rff coord )
{
  sampleBin *bin;
  bin = binResolve( bsort, coord );
  if( coord < bin->basesample.plane )
    bin->basesample.plane = coord;
  if( coord > bin->basesample.max )
    bin->basesample.max = coord;
  return &bin->basesample;
}


#if PREP_LINK_TRACKING
static void stepDistributionSample( rfPrepSector *step, int axis, binSort *bsort, prepPointerBundle *bundletable )
#else
static void stepDistributionSample( rfPrepSector *step, int axis, binSort *bsort )
#endif
{
  int i, tric;
  int edgemin, edgemax;
  sampleBin *bin;
  stepSample *sample;
  triLocal *tlocal;
  rfPrepLink *steptri;

  bin = bsort->bin;
  for( i = bsort->binmax ; i >= 0 ; i--, bin++ )
  {
    bin->basesample.plane = RFF_MAX;
    bin->basesample.max = -RFF_MAX;
    /* Stats cleared to zero here */
    memset( &bin->basesample.stats, 0, sizeof(sampleStats) );
  }

  edgemin = RF_AXIS_TO_EDGE( axis, 0 );
  edgemax = RF_AXIS_TO_EDGE( axis, 1 );

  /* Sort triangles into bins */
  steptri = PREP_SECTOR_TRILIST( step );
  for( tric = step->tricount ; tric ; tric--, steptri++ )
  {
    tlocal = steptri->p;
    sample = binSortCoord( bsort, tlocal->limit[edgemin] );
    sample->stats.count[ SAMPLE_TRI_LEFT ]++;
    sample = binSortCoord( bsort, tlocal->limit[edgemax] );
    sample->stats.count[ SAMPLE_TRI_RIGHT ]++;
  }

#if PREP_LINK_TRACKING

  int linkindex, edgeindex, linkoffset;
  const int *sideindex;
  prepPointerBundle *bundle;
  void **steplist, **steplistend;
  rfPrepSector *steplink;

  sideindex = &sideIndexTable[ axis << 2 ];
  for( linkindex = 0, linkoffset = 0 ; linkindex < 4 ; linkindex++, linkoffset += SAMPLE_LINK1_LEFT - SAMPLE_LINK0_LEFT )
  {
    edgeindex = sideindex[linkindex];
    bundle = &bundletable[edgeindex];
    steplistend = &bundle->list[bundle->count];
    for( steplist = bundle->list ; steplist < steplistend ; steplist++ )
    {
      steplink = *steplist;
      sample = binSortCoord( bsort, steplink->edge[edgemin] );
      sample->stats.count[ SAMPLE_LINK0_LEFT + linkoffset ]++;
      sample = binSortCoord( bsort, steplink->edge[edgemax] );
      sample->stats.count[ SAMPLE_LINK0_RIGHT + linkoffset ]++;
    }
  }

#endif

  return;
}


////


typedef struct
{
  rfPrepSector *step;
  rff lockedge[6];
  int32_t typecount[SAMPLE_STATS_COUNT];
  rff areacap;
#if PREP_LINK_TRACKING
  rff axiscostl, axiscostr;
#endif
  rff size0, size1;
  rff min, max;
  int axis;
  rff maxrealvalue;

  rff bestvalue;
  int bestaxis;
  rff bestplane;
  int32_t bestcount[SAMPLE_STATS_COUNT];
} stepValue;


#if PREP_LINK_TRACKING
static void stepComputeValue( rfPrepSector *step, int *linkcount )
#else
static void stepComputeValue( rfPrepSector *step )
#endif
{
  rff sizex, sizey, sizez, hitcost, area[3];
  sizex = step->edge[RF_EDGE_MAXX] - step->edge[RF_EDGE_MINX];
  sizey = step->edge[RF_EDGE_MAXY] - step->edge[RF_EDGE_MINY];
  sizez = step->edge[RF_EDGE_MAXZ] - step->edge[RF_EDGE_MINZ];

  area[0] = sizey * sizez;
  area[1] = sizex * sizez;
  area[2] = sizex * sizey;

  hitcost = 0.0;
  if( step->tricount )
    hitcost = (rff)step->tricount;
#if PREP_LINK_TRACKING
  step->value  = area[0] * ( STEP_LINKCOUNT_COST( linkcount[ RF_EDGE_MINX ] ) + STEP_LINKCOUNT_COST( linkcount[ RF_EDGE_MAXX ] ) + hitcost );
  step->value += area[1] * ( STEP_LINKCOUNT_COST( linkcount[ RF_EDGE_MINY ] ) + STEP_LINKCOUNT_COST( linkcount[ RF_EDGE_MAXY ] ) + hitcost );
  step->value += area[2] * ( STEP_LINKCOUNT_COST( linkcount[ RF_EDGE_MINZ ] ) + STEP_LINKCOUNT_COST( linkcount[ RF_EDGE_MAXZ ] ) + hitcost );
#else
  step->value  = ( area[0] + area[1] + area[2] ) * ( EVAL_MODEL_STATIC_COST + hitcost );
#endif

/*
printf( "COUNT : %d ; %d %d %d %d %d %d\n", (int)step->tricount, linkcount[0], linkcount[1], linkcount[2], linkcount[3], linkcount[4], linkcount[5] );
printf( "VALUE : %f\n", step->value );
*/

  return;
}


static rff stepVerifyValueSum( stepValue * const restrict stepvalue, const rff plane, int32_t *typecount, rff valuecap, rff realvaluecap )
{
  rff value, realvalue, area0, area1, hitcostleft, hitcostright, sizeleft, sizeright;
  rff areasumleft, areasumright;

  sizeleft = plane - stepvalue->min;
  sizeright = stepvalue->max - plane;

  area0 = sizeleft * stepvalue->size0;
  area1 = sizeleft * stepvalue->size1;
  hitcostleft = STEP_ESTIMATE_COST( typecount[SAMPLE_TRI_LEFT] );
  areasumleft = stepvalue->areacap + area0 + area1;
#if PREP_LINK_TRACKING
  value  = stepvalue->areacap * ( stepvalue->axiscostl + hitcostleft );
  value += area0 * ( STEP_LINKCOUNT_COST( typecount[SAMPLE_LINK0_LEFT] ) + STEP_LINKCOUNT_COST( typecount[SAMPLE_LINK1_LEFT] ) + hitcostleft );
  value += area1 * ( STEP_LINKCOUNT_COST( typecount[SAMPLE_LINK2_LEFT] ) + STEP_LINKCOUNT_COST( typecount[SAMPLE_LINK3_LEFT] ) + hitcostleft );
#else
  value  = areasumleft * ( EVAL_MODEL_STATIC_COST + hitcostleft );
#endif

  if( value >= valuecap )
    return RFF_MAX;

  area0 = sizeright * stepvalue->size0;
  area1 = sizeright * stepvalue->size1;
  hitcostright = STEP_ESTIMATE_COST( typecount[SAMPLE_TRI_RIGHT] );
  areasumright = stepvalue->areacap + area0 + area1;
#if PREP_LINK_TRACKING
  value += stepvalue->areacap * ( stepvalue->axiscostr + hitcostright );
  value += area0 * ( STEP_LINKCOUNT_COST( typecount[SAMPLE_LINK0_RIGHT] ) + STEP_LINKCOUNT_COST( typecount[SAMPLE_LINK1_RIGHT] ) + hitcostright );
  value += area1 * ( STEP_LINKCOUNT_COST( typecount[SAMPLE_LINK2_RIGHT] ) + STEP_LINKCOUNT_COST( typecount[SAMPLE_LINK3_RIGHT] ) + hitcostright );
#else
  value += areasumright * ( EVAL_MODEL_STATIC_COST + hitcostright );
#endif

  if( value >= valuecap )
    return RFF_MAX;

  realvalue = value;
  if( typecount[SAMPLE_TRI_LEFT] )
    realvalue += areasumleft * ( (rff)typecount[SAMPLE_TRI_LEFT] - hitcostleft );
  if( typecount[SAMPLE_TRI_RIGHT] )
    realvalue += areasumright * ( (rff)typecount[SAMPLE_TRI_RIGHT] - hitcostright );
  if( realvalue >= realvaluecap )
    return RFF_MAX;

  return value;
}


#if PREP_LINK_TRACKING
static void stepDistributionValue( const rfPrepSector * const restrict step, const binSort * const restrict bsort, stepValue * const restrict stepvalue, int *linkcount )
#else
static void stepDistributionValue( const rfPrepSector * const restrict step, const binSort * const restrict bsort, stepValue * const restrict stepvalue )
#endif
{
  int i, fixflag;
  rff plane, valuesum, prevplane;
  const int *sideindex;
  sampleBin *bin;
  stepSample *sample;
  rff planemin, planemax;

  sideindex = &sideIndexTable[ stepvalue->axis << 2 ];
  stepvalue->min = step->edge[ RF_AXIS_TO_EDGE( stepvalue->axis, 0 ) ];
  stepvalue->max = step->edge[ RF_AXIS_TO_EDGE( stepvalue->axis, 1 ) ];

#if PREP_LINK_TRACKING
  stepvalue->axiscostl = STEP_LINKCOUNT_COST( linkcount[ RF_AXIS_TO_EDGE( stepvalue->axis, 0 ) ] ) + STEP_LINKCOUNT_COST( 1 );
  stepvalue->axiscostr = STEP_LINKCOUNT_COST( linkcount[ RF_AXIS_TO_EDGE( stepvalue->axis, 1 ) ] ) + STEP_LINKCOUNT_COST( 1 );
#endif

  stepvalue->size0 = step->edge[ sideindex[3] ] - step->edge[ sideindex[2] ];
  stepvalue->size1 = step->edge[ sideindex[1] ] - step->edge[ sideindex[0] ];
  stepvalue->areacap = stepvalue->size0 * stepvalue->size1;

  stepvalue->typecount[SAMPLE_TRI_LEFT] = 0;
  stepvalue->typecount[SAMPLE_TRI_RIGHT] = step->tricount;

#if PREP_LINK_TRACKING
  stepvalue->typecount[SAMPLE_LINK0_LEFT] = 0;
  stepvalue->typecount[SAMPLE_LINK0_RIGHT] = linkcount[ sideindex[0] ];
  stepvalue->typecount[SAMPLE_LINK1_LEFT] = 0;
  stepvalue->typecount[SAMPLE_LINK1_RIGHT] = linkcount[ sideindex[1] ];
  stepvalue->typecount[SAMPLE_LINK2_LEFT] = 0;
  stepvalue->typecount[SAMPLE_LINK2_RIGHT] = linkcount[ sideindex[2] ];
  stepvalue->typecount[SAMPLE_LINK3_LEFT] = 0;
  stepvalue->typecount[SAMPLE_LINK3_RIGHT] = linkcount[ sideindex[3] ];
#endif

  planemin = stepvalue->min + ( PREP_STEP_MINIMUM_EXTENSION * ( stepvalue->max - stepvalue->min ) );
  planemax = stepvalue->max - ( PREP_STEP_MINIMUM_EXTENSION * ( stepvalue->max - stepvalue->min ) );

  fixflag = 0;
  bin = bsort->bin;
  for( i = bsort->binmax ; i >= 0 ; i--, bin++ )
  {
    sample = &bin->basesample;
    plane = sample->plane;

    /* Sample plane will be RFF_MAX for empty bins, which we want to skip
     * We also need to skip bin 0 if it expends prior to stepmin
     */

    if( plane != RFF_MAX )
    {
      fixflag = 1;
      prevplane = sample->max;
    }
    else if( ( plane == RFF_MAX ) && ( fixflag ) )
    {
      fixflag = 0;
      plane = rfnextafter( prevplane, RFF_MAX );

      /* Catch denormals! */
      if( plane <= prevplane )
      {
        if( plane == 0.0 )
        {
          plane = RFF_MIN;
          if( plane <= prevplane )
            plane = RFF_MAX;
        }
        else
          plane = RFF_MAX;
      }
    }


/* FOOBAR */
#if PREP_LINK_TRACKING
    stepvalue->typecount[SAMPLE_LINK0_RIGHT] -= sample->stats.count[SAMPLE_LINK0_RIGHT];
    stepvalue->typecount[SAMPLE_LINK1_RIGHT] -= sample->stats.count[SAMPLE_LINK1_RIGHT];
    stepvalue->typecount[SAMPLE_LINK2_RIGHT] -= sample->stats.count[SAMPLE_LINK2_RIGHT];
    stepvalue->typecount[SAMPLE_LINK3_RIGHT] -= sample->stats.count[SAMPLE_LINK3_RIGHT];
#endif
/* FOOBAR */

    if( ( plane > planemin ) && ( plane < planemax ) )
    {
      valuesum = stepVerifyValueSum( stepvalue, plane, stepvalue->typecount, stepvalue->bestvalue, stepvalue->maxrealvalue );
      if( valuesum < stepvalue->bestvalue )
      {
        stepvalue->bestvalue = valuesum;
        stepvalue->bestplane = plane;
        stepvalue->bestaxis = stepvalue->axis;
        memcpy( stepvalue->bestcount, stepvalue->typecount, SAMPLE_STATS_COUNT*sizeof(int32_t) );
      }
    }

    stepvalue->typecount[SAMPLE_TRI_LEFT]  += sample->stats.count[SAMPLE_TRI_LEFT];
    stepvalue->typecount[SAMPLE_TRI_RIGHT] -= sample->stats.count[SAMPLE_TRI_RIGHT];

#if PREP_LINK_TRACKING
    stepvalue->typecount[SAMPLE_LINK0_LEFT]  += sample->stats.count[SAMPLE_LINK0_LEFT];
    stepvalue->typecount[SAMPLE_LINK1_LEFT]  += sample->stats.count[SAMPLE_LINK1_LEFT];
    stepvalue->typecount[SAMPLE_LINK2_LEFT]  += sample->stats.count[SAMPLE_LINK2_LEFT];
    stepvalue->typecount[SAMPLE_LINK3_LEFT]  += sample->stats.count[SAMPLE_LINK3_LEFT];
#endif


/* FOOBAR */
/*
#if PREP_LINK_TRACKING
    stepvalue->typecount[SAMPLE_LINK0_RIGHT] -= sample->stats.count[SAMPLE_LINK0_RIGHT];
    stepvalue->typecount[SAMPLE_LINK1_RIGHT] -= sample->stats.count[SAMPLE_LINK1_RIGHT];
    stepvalue->typecount[SAMPLE_LINK2_RIGHT] -= sample->stats.count[SAMPLE_LINK2_RIGHT];
    stepvalue->typecount[SAMPLE_LINK3_RIGHT] -= sample->stats.count[SAMPLE_LINK3_RIGHT];
#endif
*/
/* FOOBAR */


  }

  return;
}


static inline int stepCollectEdgeTest( rfPrepSector *step, triLocal *tlocal )
{
  if( tlocal->limit[RF_EDGE_MAXX] <= step->edge[RF_EDGE_MINX] )
    return 0;
  if( tlocal->limit[RF_EDGE_MINX] >= step->edge[RF_EDGE_MAXX] )
    return 0;
  if( tlocal->limit[RF_EDGE_MINX] == tlocal->limit[RF_EDGE_MAXX] )
    return 0;
  if( tlocal->limit[RF_EDGE_MAXY] <= step->edge[RF_EDGE_MINY] )
    return 0;
  if( tlocal->limit[RF_EDGE_MINY] >= step->edge[RF_EDGE_MAXY] )
    return 0;
  if( tlocal->limit[RF_EDGE_MINY] == tlocal->limit[RF_EDGE_MAXY] )
    return 0;
  if( tlocal->limit[RF_EDGE_MAXZ] <= step->edge[RF_EDGE_MINZ] )
    return 0;
  if( tlocal->limit[RF_EDGE_MINZ] >= step->edge[RF_EDGE_MAXZ] )
    return 0;
  if( tlocal->limit[RF_EDGE_MINZ] == tlocal->limit[RF_EDGE_MAXZ] )
    return 0;
  return 1;
}


static void stepCollect( buildContext *build, rfPrepSector *step, rfPrepSector *stepleft, rfPrepSector *stepright, rff plane, int axis )
{
  int32_t tric, duptricount;
  int edgemin, edgemax;
  triLocal *tlocal, *tnew;
  rfPrepLink *steptri, *tleft, *tright;
  static void (*updateLimits[3])( triLocal *tl, rfTri *tri, rff *llims, rff *rlims, rff cutplane ) =
  {
    triUpdateLimitsX, triUpdateLimitsY, triUpdateLimitsZ
  };

  steptri = PREP_SECTOR_TRILIST( step );
  tleft = PREP_SECTOR_TRILIST( stepleft );
  tright = PREP_SECTOR_TRILIST( stepright );

  edgemin = RF_AXIS_TO_EDGE( axis, 0 );
  edgemax = RF_AXIS_TO_EDGE( axis, 1 );

  duptricount = ( stepleft->tricount + stepright->tricount ) - step->tricount;
  if( duptricount )
    tnew = mmGrowAlloc( &build->mgrow, duptricount*sizeof(triLocal) );

  for( tric = step->tricount ; tric ; tric--, steptri++ )
  {
    tlocal = steptri->p;
    if( tlocal->limit[edgemin] < plane )
    {
      if( tlocal->limit[edgemax] > plane )
      {
        tnew->limflags = tlocal->limflags;
        tnew->index = tlocal->index;
#if TRI_UPDATE_LIMITS
        updateLimits[axis]( tlocal, &build->trilist[ tnew->index ], tlocal->limit, tnew->limit, plane );
#else
        memcpy( tnew->limit, tlocal->limit, RF_EDGE_COUNT*sizeof(rff) );
#endif
        if( stepCollectEdgeTest( stepleft, tlocal ) )
        {
          tleft->p = tlocal;
          tleft++;
        }
        if( stepCollectEdgeTest( stepright, tnew ) )
        {
          tright->p = tnew;
          tright++;
          tnew++;
        }
      }
      else
      {
        tleft->p = tlocal;
        tleft++;
      }
    }
    else
    {
      tright->p = tlocal;
      tright++;
    }
  }

#if PREP_DEBUG
  if( (int)( tleft - (rfPrepLink *)PREP_SECTOR_TRILIST( stepleft ) ) > stepleft->tricount )
  {
    printf( "ERROR: Tricounts %d %d, Expected %d %d\n", (int)( tleft - (rfPrepLink *)PREP_SECTOR_TRILIST( stepleft ) ), (int)( tright - (rfPrepLink *)PREP_SECTOR_TRILIST( stepright ) ), (int)stepleft->tricount, (int)stepright->tricount );
    abort();
  }
  if( (int)( tright - (rfPrepLink *)PREP_SECTOR_TRILIST( stepright ) ) > stepright->tricount )
  {
    printf( "ERROR: Tricounts %d %d, Expected %d %d\n", (int)( tleft - (rfPrepLink *)PREP_SECTOR_TRILIST( stepleft ) ), (int)( tright - (rfPrepLink *)PREP_SECTOR_TRILIST( stepright ) ), (int)stepleft->tricount, (int)stepright->tricount );
    abort();
  }
#endif

  stepleft->tricount = (int)( tleft - (rfPrepLink *)PREP_SECTOR_TRILIST( stepleft ) );
  stepright->tricount = (int)( tright - (rfPrepLink *)PREP_SECTOR_TRILIST( stepright ) );

  return;
}


#if PREP_LINK_TRACKING
 #define LINK_TRACKING_BUFFER (64)
#endif


static void prepTreeBuildSector( buildContext *build, rfPrepNode *prepnode )
{
  rfPrepSector *step;
  rfGraphCache *cache;

  cache = build->cache;
  step = prepnode->child[0].p;

  /* Set sector index, set directory */
  step->index = mmAtomicReadAddL( &build->sectorindex, 1 );
  mmDirectorySet( &cache->sectordir, step->index, step );
  prepnode->status = PREPNODE_STATUS_SECTOR;

  return;
}

static void prepTreeEvaluate( buildContext *build, rfPrepNode *prepnode )
{
  int axis, bincount;
  const int *sideindex;
  rff targetvalue, valuegain;
  rfPrepSector *step;
  binSort bsort;
  stepValue stepvalue;
  sampleBin bin[PREP_BIN_MAXIMUM];
  rfGraphCache *cache;
  stepOp op;

#if PREP_LINK_TRACKING
  rff quadedge[4];
  int linkcount[RF_EDGE_COUNT];
  void *linksectorbuffer[RF_EDGE_COUNT][LINK_TRACKING_BUFFER];
  prepPointerBundle bundletable[RF_EDGE_COUNT];
#endif

  cache = build->cache;
  step = prepnode->child[0].p;
  targetvalue = build->valuetricapfactor;
  if( step->tricount < build->sectortricap )
  {
    if( build->treestopflag )
      goto buildsector;
    targetvalue = build->valuestepfactor;
  }
  targetvalue *= step->value;
  stepvalue.bestvalue = targetvalue;
  stepvalue.maxrealvalue = targetvalue;
  if( stepvalue.bestvalue < 0.0 )
    goto buildsector;
  stepvalue.step = step;
  stepvalue.bestaxis = -1;

  bincount = 1;
  if( step->tricount )
  {
    /* TODO: Consider count of side links to decide bincount? */
    bincount = (int)( build->bincountfactor * rflog2( step->tricount ) );
    if( bincount > build->bincountmax )
      bincount = build->bincountmax;
  }

  if( bincount > 1 )
  {
    bsort.bin = bin;
    bsort.binmax = bincount-1;

#if PREP_LINK_TRACKING
    /* Build link bundles for all edges */
    for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
    {
      linkcount[edgeindex] = 0;
      prepPointerBundleInit( cache, &bundletable[edgeindex], linksectorbuffer[edgeindex], LINK_TRACKING_BUFFER, LINK_TRACKING_BUFFER );
      if( !( step->link[edgeindex].p ) )
        continue;
      sideindex = &sideIndexTable[ edgeindex & ~0x1 ];
      quadedge[0] = step->edge[ sideindex[0] ];
      quadedge[1] = step->edge[ sideindex[1] ];
      quadedge[2] = step->edge[ sideindex[2] ];
      quadedge[3] = step->edge[ sideindex[3] ];
      /* Gather all linked sectors on edge side */
      prepBuildLinkBundle( 0, step->link[edgeindex].p, edgeindex, quadedge, &bundletable[edgeindex] );
      linkcount[edgeindex] = bundletable[edgeindex].count;
    }
#endif

    for( axis = 0 ; axis < 3 ; axis++ )
    {
      bsort.sortbase = step->edge[ RF_AXIS_TO_EDGE( axis, 0 ) ];
      bsort.sortinvsize = (rff)bincount / ( step->edge[ RF_AXIS_TO_EDGE( axis, 1 ) ] - bsort.sortbase );

#if PREP_LINK_TRACKING
      stepDistributionSample( step, axis, &bsort, bundletable );
#else
      stepDistributionSample( step, axis, &bsort );
#endif

      sideindex = &sideIndexTable[ axis << 2 ];
      stepvalue.axis = axis;

#if PREP_LINK_TRACKING
      stepDistributionValue( step, &bsort, &stepvalue, linkcount );
#else
      stepDistributionValue( step, &bsort, &stepvalue );
#endif
    }
  }

/*
  if( stepvalue.bestaxis < 0 )
  {
    if( step->tricount >= build->sectortricap )
      printf( "Step %p : %d tris ; Best is %f at %d\n", step, step->tricount, stepvalue.bestplane, stepvalue.bestaxis );
  }
*/

  if( stepvalue.bestaxis < 0 )
  {
    buildsector:
    prepTreeBuildSector( build, prepnode );
  }
  else
  {
    op.prepnode = prepnode;
    op.axis = stepvalue.bestaxis;
    op.plane = stepvalue.bestplane;
    op.tricountleft = stepvalue.bestcount[SAMPLE_TRI_LEFT];
    op.tricountright = stepvalue.bestcount[SAMPLE_TRI_RIGHT];

    valuegain = step->value - stepvalue.bestvalue;
    mmQueueAdd( &build->buildtreequeue, &op, valuegain );
  }

  return;
}


static void prepTreeBuildStep( buildContext *build, stepOp *op )
{
  int axis, edgemin, edgemax, edgeindex;
  const int *sideindex;
  rff plane;
  rfPrepNode *prepnode;
  rfPrepSector *step;
  rfPrepSector *stepleft, *stepright;
  rfPrepNode *nodeleft, *noderight;
  rfGraphCache *cache;

  /* Get step */
  prepnode = op->prepnode;
  cache = build->cache;
  step = prepnode->child[0].p;
  if( step->tricount < build->sectortricap )
  {
    if( build->treestopflag )
    {
      prepTreeBuildSector( build, op->prepnode );
      return;
    }
    if( mmAtomicReadAddL( &build->stepcount, 1 ) >= build->stepcap )
      build->treestopflag = 1;
  }

  axis = op->axis;
  plane = op->plane;

  stepleft = mmVolumeAlloc( &cache->memvolume, sizeof(rfPrepSector) + ( op->tricountleft * sizeof(rfPrepLink) ) );
  stepleft->tricount = op->tricountleft;
  nodeleft = mmBlockAlloc( &cache->nodeblock );
  nodeleft->child[0].p = stepleft;
  nodeleft->status = PREPNODE_STATUS_NOTREADY;

  stepright = mmVolumeAlloc( &cache->memvolume, sizeof(rfPrepSector) + ( op->tricountright * sizeof(rfPrepLink) ) );
  stepright->tricount = op->tricountright;
  noderight = mmBlockAlloc( &cache->nodeblock );
  noderight->child[0].p = stepright;
  noderight->status = PREPNODE_STATUS_NOTREADY;

  edgemin = RF_AXIS_TO_EDGE( axis, 0 );
  edgemax = RF_AXIS_TO_EDGE( axis, 1 );
  sideindex = &sideIndexTable[ axis << 2 ];

  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    stepleft->edge[edgeindex] = step->edge[edgeindex];
    stepright->edge[edgeindex] = step->edge[edgeindex];
  }
  stepleft->edge[ edgemax ] = plane;
  stepright->edge[ edgemin ] = plane;

  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    stepleft->link[edgeindex] = step->link[edgeindex];
    stepright->link[edgeindex] = step->link[edgeindex];
  }
  stepleft->link[ edgemax ].p = noderight;
  stepright->link[ edgemin ].p = nodeleft;

  stepleft->flags = 0;
  stepright->flags = 0;

  stepCollect( build, step, stepleft, stepright, plane, axis );

  /* Free the sector to create two nodes instead */
  mmVolumeRelease( &cache->memvolume, step );

#if PREP_LINK_TRACKING
  linkcount[ edgemax ] = 1;
  stepComputeValue( stepleft, linkcount );
  linkcount[ edgemax ] = bundletable[ edgemax ].count;
  linkcount[ edgemin ] = 1;
  stepComputeValue( stepright, linkcount );
#else
  stepComputeValue( stepleft );
  stepComputeValue( stepright );
#endif

  /* Build node */
  prepnode->child[0].p = nodeleft;
  prepnode->child[1].p = noderight;
  prepnode->plane = plane;
  prepnode->nodeflags = axis;
  mmWriteBarrier();
  prepnode->status = PREPNODE_STATUS_NODE;

  prepTreeEvaluate( build, nodeleft );
  prepTreeEvaluate( build, noderight );

  return;
}


////


static rfindex prepResolveOrigin( buildContext *build, rfPrepNode *prepnode, rff *origin )
{
  int x, y, z, index, axis;
  rfPrepSector *step;
  rfGraphCache *cache;

  cache = build->cache;

  x = 1;
  if( origin[RF_AXIS_X] < cache->edge[RF_EDGE_MINX] )
    x = 0;
  else if( origin[RF_AXIS_X] > cache->edge[RF_EDGE_MAXX] )
    x = 2;
  y = 1;
  if( origin[RF_AXIS_Y] < cache->edge[RF_EDGE_MINY] )
    y = 0;
  else if( origin[RF_AXIS_Y] > cache->edge[RF_EDGE_MAXY] )
    y = 2;
  z = 1;
  if( origin[RF_AXIS_Z] < cache->edge[RF_EDGE_MINZ] )
    z = 0;
  else if( origin[RF_AXIS_Z] > cache->edge[RF_EDGE_MAXZ] )
    z = 2;
  index = HALO_INDEX(x,y,z);

  if( index != HALO_INDEX(1,1,1) )
    step = build->halo[ index ];
  else
  {
    for( ; ; )
    {
      if( prepnode->status == PREPNODE_STATUS_SECTOR )
        break;
      else if( prepnode->status == PREPNODE_STATUS_NODE )
      {
        axis = PREP_NODE_GETAXIS( prepnode );
        if( origin[axis] < prepnode->plane )
          prepnode = prepnode->child[RF_NODE_LESS].p;
        else
          prepnode = prepnode->child[RF_NODE_MORE].p;
      }
      else
      {
        fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
        abort();
      }
    }
    step = prepnode->child[0].p;
  }


/*
#if PREP_DEBUG_PRINT
  if( ( origin[RF_AXIS_X] < step->edge[RF_EDGE_MINX] ) || ( origin[RF_AXIS_X] > step->edge[RF_EDGE_MAXX] ) || ( origin[RF_AXIS_Y] < step->edge[RF_EDGE_MINY] ) || ( origin[RF_AXIS_Y] > step->edge[RF_EDGE_MAXY] ) || ( origin[RF_AXIS_Z] < step->edge[RF_EDGE_MINZ] ) || ( origin[RF_AXIS_Z] > step->edge[RF_EDGE_MAXZ] ) )
  {
    fprintf( stderr, "Origin : %f %f %f\n", origin[0], origin[1], origin[2] );
    fprintf( stderr, "Haloindex : %d,%d,%d\n", x, y, z );
    fprintf( stderr, "Sector %p %d ; Edge %f,%f ; %f,%f ; %f,%f\n", step, (int)step->index, step->edge[0], step->edge[1], step->edge[2], step->edge[3], step->edge[4], step->edge[5] );
    fprintf( stderr, "Cache Edge %f,%f ; %f,%f ; %f,%f\n", cache->edge[0], cache->edge[1], cache->edge[2], cache->edge[3], cache->edge[4], cache->edge[5] );
  }
#endif
*/

/*
printf( "Origin %f,%f,%f : %d,%d,%d : index %d\n", origin[0], origin[1], origin[2], (int)x, (int)y, (int)z, (int)step->index );
*/

  return step->index;
}

static void prepBuildOriginTable( buildContext *build, rfPrepNode *prepnode )
{
  rfsize x, y, z, index;
  rfsize originwidth;
  rff origin[RF_AXIS_COUNT];
  rfOriginTable *origintable;
  rfGraphCache *cache;

  cache = build->cache;
  origintable = &cache->origintable;
  originwidth = origintable->width;
  cache->origin = malloc( origintable->totalcount * sizeof(rforigin) );

  index = 0;
  origin[RF_AXIS_Z] = origintable->edge[RF_EDGE_MINZ];
  for( z = 0 ; z < originwidth ; z++, origin[RF_AXIS_Z] += origintable->spacing[RF_AXIS_Z] )
  {
    origin[RF_AXIS_Y] = origintable->edge[RF_EDGE_MINY];
    for( y = 0 ; y < originwidth ; y++, origin[RF_AXIS_Y] += origintable->spacing[RF_AXIS_Y] )
    {
      origin[RF_AXIS_X] = origintable->edge[RF_EDGE_MINX];
      for( x = 0 ; x < originwidth ; x++, origin[RF_AXIS_X] += origintable->spacing[RF_AXIS_X], index++ )
        cache->origin[index] = prepResolveOrigin( build, prepnode, origin );
    }
  }

  return;
}


////


typedef struct
{
  rfPrepNode *prepnode;
} prepNodeHashEntry;

static void prepNodeHashClearEntry( void *entry )
{
  prepNodeHashEntry *nodehash;
  nodehash = entry;
  nodehash->prepnode = 0;
  return;
}

static int prepNodeHashEntryValid( void *entry )
{
  prepNodeHashEntry *nodehash;
  nodehash = entry;
  return ( nodehash->prepnode ? 1 : 0 );
}

static uint32_t prepNodeHashEntryKey( void *entry )
{
  uint32_t hashkey;
  prepNodeHashEntry *nodehash;
  rfPrepNode *prepnode;

  nodehash = entry;
  prepnode = nodehash->prepnode;

  hashkey  = prepnode->nodeflags;
  hashkey += hashkey << 10;
  hashkey ^= hashkey >> 6;
  hashkey += (uint32_t)( ( (uintptr_t)prepnode->child[RF_NODE_LESS].p ) >> 3 );
  hashkey += hashkey << 10;
  hashkey ^= hashkey >> 6;
  hashkey += (uint32_t)( ( (uintptr_t)prepnode->child[RF_NODE_MORE].p ) >> 3 );
  hashkey += hashkey << 10;
  hashkey ^= hashkey >> 6;

  hashkey += hashkey << 3;
  hashkey ^= hashkey >> 11;
  hashkey += hashkey << 15;

  return hashkey;
}

static int prepNodeHashEntryCmp( void *entry, void *entryref )
{
  prepNodeHashEntry *nodehash, *nodehashref;
  rfPrepNode *prepnode, *prepnoderef;
  nodehash = entry;
  nodehashref = entryref;
  if( !( nodehash->prepnode ) )
    return MM_HASH_ENTRYCMP_INVALID;
  prepnode = nodehash->prepnode;
  prepnoderef = nodehashref->prepnode;
  if( ( prepnode->child[RF_NODE_LESS].p == prepnoderef->child[RF_NODE_LESS].p ) && ( prepnode->child[RF_NODE_MORE].p == prepnoderef->child[RF_NODE_MORE].p ) && ( prepnode->nodeflags == prepnoderef->nodeflags ) )
    return MM_HASH_ENTRYCMP_FOUND;
  return MM_HASH_ENTRYCMP_SKIP;
}

static mmHashAccess prepNodeHash =
{
  .clearentry = prepNodeHashClearEntry,
  .entryvalid = prepNodeHashEntryValid,
  .entrykey = prepNodeHashEntryKey,
  .entrycmp = prepNodeHashEntryCmp
};


static void prepAllocateNodeHashTable( buildContext *build )
{
  uint32_t hashbits, lockpageshift;
  rfsize nodecount, hashmemsize;

  nodecount = ( 8 * mmAtomicReadL( &build->sectorindex ) ) + 256;
#if CPUCONF_WORD_SIZE == 64
  hashbits = ccLog2Int64( nodecount ) + 1;
#else
  hashbits = ccLog2Int32( nodecount ) + 1;
#endif
  lockpageshift = 8;

  hashmemsize = mmHashRequiredSize( sizeof(prepNodeHashEntry), hashbits, lockpageshift );
  build->nodehashtable = malloc( hashmemsize );
  mmHashInit( build->nodehashtable, &prepNodeHash, sizeof(prepNodeHashEntry), hashbits, lockpageshift, MM_HASH_FLAGS_NO_COUNT );
  build->nodehashlimit = ( 3 * ( (rfsize)1 << hashbits ) ) >> 2;

  return;
}


////


typedef struct
{
  /* WWW TODO: merge type flag with some pointer */
  rff plane;
  int type;
  rfPrepSector *prepsector;
  void *next;
} sideLink;

typedef struct
{
  sideLink *list;
  int allocflag;
  size_t size;
  size_t count;
  void *next;
  void *last;
} sideLinkBundle;

static sideLink *sideLinkBundleNext( rfGraphCache *cache, sideLinkBundle *linkbundle )
{
  size_t bundlesize;
  void **initlast;
  sideLinkBundle *linkbundlenext;

  initlast = &linkbundle->last;
  linkbundlenext = linkbundle->last;
  if( linkbundlenext->count >= linkbundlenext->size )
  {
    bundlesize = linkbundlenext->size << 1;
    linkbundlenext = mmVolumeAlloc( &cache->memvolume, CC_SIZEOF_ALIGN64(sideLinkBundle) + ( bundlesize * sizeof(sideLink) ) );
    linkbundlenext->list = ADDRESS( linkbundlenext, CC_SIZEOF_ALIGN64(sideLinkBundle) );
    linkbundlenext->allocflag = 1;
    linkbundlenext->size = bundlesize;
    linkbundlenext->count = 0;
    linkbundlenext->next = 0;
    linkbundle->next = linkbundlenext;
    *initlast = linkbundlenext;
  }
  return &linkbundlenext->list[ linkbundlenext->count++ ];
}

/* The count of links is usually very low, so this lazy sorting isn't too bad */
static void sideLinkAdd( sideLink *newlink, sideLink **sideroot )
{
  sideLink *sidelink;
  sideLink **prev;
  prev = sideroot;
  for( sidelink = *sideroot ; ; sidelink = sidelink->next )
  {
    if( !( sidelink ) || ( newlink->plane <= sidelink->plane ) )
    {
      newlink->next = *prev;
      *prev = newlink;
      break;
    }
    prev = (sideLink **)&sidelink->next;
  }
  return;
}

/* Order of insertion into linked list is important, min then max, gets inversed during sorting */
static void sideLinkListSort( rfGraphCache *cache, sideLink **sideroot, prepPointerBundle *bundle, const int *sideaxis, sideLinkBundle *linkbundle )
{
  int edgeindex0, edgeindex1;
  void **steplist, **steplistend;
  rfPrepSector *step;
  sideLink *sidelink;

  sideroot[0] = 0;
  sideroot[1] = 0;
  steplistend = &bundle->list[bundle->count];

  edgeindex0 = ( sideaxis[0] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN;
  edgeindex1 = ( sideaxis[1] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN;
  for( steplist = bundle->list ; steplist < steplistend ; steplist++ )
  {
    step = *steplist;
    sidelink = sideLinkBundleNext( cache, linkbundle );
    sidelink->plane = step->edge[edgeindex0];
    sidelink->type = 1;
    sidelink->prepsector = step;
    sideLinkAdd( sidelink, &sideroot[0] );
    sidelink = sideLinkBundleNext( cache, linkbundle );
    sidelink->plane = step->edge[edgeindex1];
    sidelink->type = 1;
    sidelink->prepsector = step;
    sideLinkAdd( sidelink, &sideroot[1] );
  }

  edgeindex0 = ( sideaxis[0] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX;
  edgeindex1 = ( sideaxis[1] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX;
  for( steplist = bundle->list ; steplist < steplistend ; steplist++ )
  {
    step = *steplist;
    sidelink = sideLinkBundleNext( cache, linkbundle );
    sidelink->plane = step->edge[edgeindex0];
    sidelink->type = 0;
    sidelink->prepsector = step;
    sideLinkAdd( sidelink, &sideroot[0] );
    sidelink = sideLinkBundleNext( cache, linkbundle );
    sidelink->plane = step->edge[edgeindex1];
    sidelink->type = 0;
    sidelink->prepsector = step;
    sideLinkAdd( sidelink, &sideroot[1] );
  }

  return;
}

static void sideLinkBundleFree( rfGraphCache *cache, sideLinkBundle *linkbundle )
{
  sideLinkBundle *linkbundlenext;
  for( ; linkbundle ; )
  {
    linkbundlenext = linkbundle->next;
    if( linkbundle->allocflag )
      mmVolumeRelease( &cache->memvolume, linkbundle );
    linkbundle = linkbundlenext;
  }
  return;
}


////


static void sideLinkSortChildren( rfGraphCache *cache, sideLink **leftlist, sideLink **rightlist, sideLink *sideroot, int *childlinkcount, int axis, rff plane, sideLinkBundle *linkbundle )
{
  int lcount, rcount;
  int edgemin, edgemax;
  sideLink *sidelink, *sidenext, *newlink;
  rfPrepSector *step;

  lcount = childlinkcount[0] << 1;
  rcount = childlinkcount[1] << 1;
  edgemin = RF_AXIS_TO_EDGE(axis,RF_EDGE_MIN);
  edgemax = RF_AXIS_TO_EDGE(axis,RF_EDGE_MAX);
  *leftlist = 0;
  *rightlist = 0;
  for( sidelink = sideroot ; sidelink ; sidelink = sidenext )
  {
    sidenext = sidelink->next;
    step = sidelink->prepsector;
    if( step->edge[edgemax] <= plane )
    {
      sidelink->next = *leftlist;
      *leftlist = sidelink;
      leftlist = (sideLink **)&sidelink->next;
      rcount--;
    }
    else if( step->edge[edgemin] >= plane )
    {
      sidelink->next = *rightlist;
      *rightlist = sidelink;
      rightlist = (sideLink **)&sidelink->next;
      lcount--;
    }
    else
    {
      sidelink->next = *leftlist;
      *leftlist = sidelink;
      leftlist = (sideLink **)&sidelink->next;
      newlink = sideLinkBundleNext( cache, linkbundle );
      *newlink = *sidelink;
      newlink->next = *rightlist;
      *rightlist = newlink;
      rightlist = (sideLink **)&newlink->next;
    }
  }
  childlinkcount[0] = lcount >> 1;
  childlinkcount[1] = rcount >> 1;

  return;
}


static rfPrepNode *sideLinkBuild( buildContext *build, sideLink **sideroot, int linkcount, rff *quadedge, const int *sideaxis, sideLinkBundle *linkbundle )
{
  int quadaxis, bestaxis, linkflag, childlinkcount[2], readflag;
  int countl, countr;
  rff edgemin, edgemax, distleft, distright, value, bestvalue, bestplane, edgefactor;
  rff leftquad[4], rightquad[4];
  sideLink *sidelink;
  sideLink *leftroot[2];
  sideLink *rightroot[2];
  void *child;
  rfPrepNode *prepnode;
  rfGraphCache *cache;
  prepNodeHashEntry nodehash;

  cache = build->cache;
  bestvalue = RFF_MAX;
  for( quadaxis = 1 ; quadaxis >= 0 ; quadaxis-- )
  {
    edgemin = quadedge[ ( quadaxis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN ];
    edgemax = quadedge[ ( quadaxis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX ];
    countl = 0;
    countr = linkcount;
    for( sidelink = sideroot[quadaxis] ; sidelink ; sidelink = sidelink->next )
    {
      if( sidelink->plane > edgemin )
        break;
      countl += sidelink->type;
      countr -= sidelink->type ^ 1;
    }
    edgefactor = quadedge[ ( (quadaxis^1) << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX ] - quadedge[ ( (quadaxis^1) << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN ];
    for( ; sidelink ; sidelink = sidelink->next )
    {
      distleft = sidelink->plane - edgemin;
      distright = edgemax - sidelink->plane;
      if( distright <= 0 )
        break;
      countl += sidelink->type;
      countr -= sidelink->type ^ 1;
      value = edgefactor * ( ( distleft * NODE_LINKCOUNT_COST( countl ) ) + ( distright * NODE_LINKCOUNT_COST( countr ) ) );
      if( value < bestvalue )
      {
        bestvalue = value;
        bestplane = sidelink->plane;
        bestaxis = quadaxis;
      }
    }
  }

  if( bestvalue == RFF_MAX )
  {
    /* We have links but no intersecting planes? What is going on! */
    fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
    abort();
  }

/*
printf( "  BestPlane, Axis %d, Plane %f ( %f ~ %f )\n", bestaxis, bestplane, quadedge[ ( bestaxis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN ], quadedge[ ( bestaxis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX ] );
*/

  /* Build lists of nodes on both sides of plane */
  childlinkcount[0] = linkcount;
  childlinkcount[1] = linkcount;
  sideLinkSortChildren( cache, &leftroot[0], &rightroot[0], sideroot[0], childlinkcount, sideaxis[ bestaxis ], bestplane, linkbundle );
  childlinkcount[0] = linkcount;
  childlinkcount[1] = linkcount;
  sideLinkSortChildren( cache, &leftroot[1], &rightroot[1], sideroot[1], childlinkcount, sideaxis[ bestaxis ], bestplane, linkbundle );

/*
printf( "    Linkcount %d -> %d %d\n", linkcount, childlinkcount[0], childlinkcount[1] );
*/

  /* Create graph node */
  prepnode = mmBlockAlloc( &cache->nodeblock );
  prepnode->nodeflags = RF_NODE_SET_AXIS( sideaxis[ bestaxis ] );
  prepnode->plane = bestplane;

  /* Build left branch */
  if( childlinkcount[0] == 1 )
  {
    child = leftroot[0]->prepsector;
    linkflag = RF_LINK_SECTOR;
  }
  else
  {
    memcpy( leftquad, quadedge, 4*sizeof(rff) );
    leftquad[ ( bestaxis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX ] = bestplane;
    child = sideLinkBuild( build, leftroot, childlinkcount[0], leftquad, sideaxis, linkbundle );
    linkflag = RF_LINK_NODE;
  }
  prepnode->child[0].p = child;
  prepnode->nodeflags |= linkflag << RF_NODE_LINKFLAGS_LESS_SHIFT;
#ifdef RF_NODE_DUPLINKFLAGS_LESS_SHIFT
  prepnode->nodeflags |= linkflag << RF_NODE_DUPLINKFLAGS_LESS_SHIFT;
#endif

  /* Build right branch */
  if( childlinkcount[1] == 1 )
  {
    child = rightroot[0]->prepsector;
    linkflag = RF_LINK_SECTOR;
  }
  else
  {
    memcpy( rightquad, quadedge, 4*sizeof(rff) );
    rightquad[ ( bestaxis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN ] = bestplane;
    child = sideLinkBuild( build, rightroot, childlinkcount[1], rightquad, sideaxis, linkbundle );
    linkflag = RF_LINK_NODE;
  }
  prepnode->child[1].p = child;
  prepnode->nodeflags |= linkflag << RF_NODE_LINKFLAGS_MORE_SHIFT;
#ifdef RF_NODE_DUPLINKFLAGS_LESS_SHIFT
  prepnode->nodeflags |= linkflag << RF_NODE_DUPLINKFLAGS_MORE_SHIFT;
#endif

  /* Duplicate check, look up in hash table if identical node already exists */
  nodehash.prepnode = prepnode;
  if( mmAtomicReadL( &build->nodeindex ) < build->nodehashlimit )
    mmHashLockReadOrAddEntry( build->nodehashtable, &prepNodeHash, &nodehash, &readflag );
  else
  {
    readflag = 0;
    if( mmHashLockReadEntry( build->nodehashtable, &prepNodeHash, &nodehash ) == MM_HASH_SUCCESS )
      readflag = 1;
  }
  if( readflag )
  {
    /* Duplicate node, free and connect to existing one */
    mmBlockRelease( &cache->nodeblock, prepnode );
    prepnode = nodehash.prepnode;
  }
  else
  {
    /* Set node index, set directory */
    prepnode->index = mmAtomicReadAddL( &build->nodeindex, 1 );
    mmDirectorySet( &cache->nodedir, prepnode->index, prepnode );
  }

  return prepnode;
}


#define SIDE_SECTOR_BUNDLE_SIZE (256)
#define SIDE_LINK_BUNDLE_SIZE (1024)

static void prepSectorConnect( buildContext *build, rfPrepSector *step )
{
  int edgeindex;
  rfPrepNode *linknode;
  rff quadedge[4];
  const int *sideaxis;
  void *linksectorbuffer[SIDE_SECTOR_BUNDLE_SIZE];
  prepPointerBundle sectorbundle;
  sideLink *sideroot[2];
  sideLink sidelinkbuffer[SIDE_LINK_BUNDLE_SIZE];
  sideLinkBundle linkbundle;
  rfGraphCache *cache;

  cache = build->cache;
  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    linknode = step->link[edgeindex].p;
    if( !( linknode ) )
    {
      step->link[edgeindex].p = 0;
      step->flags |= RF_LINK_SECTOR << ( RF_SECTOR_LINKFLAGS_SHIFT + edgeindex );
      /* Register sector to list for external edge */
      prepPointerBundleStore( cache, &build->extsectorbundle[ RF_OPPOSITE_EDGE( edgeindex ) ], step );
      continue;
    }
    else if( linknode->status == PREPNODE_STATUS_SECTOR )
    {
      step->link[edgeindex].p = linknode->child[0].p;
      step->flags |= RF_LINK_SECTOR << ( RF_SECTOR_LINKFLAGS_SHIFT + edgeindex );
      continue;
    }

    sideaxis = &sideAxisTable[ edgeindex & ~0x1 ];
    quadedge[0] = step->edge[ ( sideaxis[0] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN ];
    quadedge[1] = step->edge[ ( sideaxis[0] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX ];
    quadedge[2] = step->edge[ ( sideaxis[1] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN ];
    quadedge[3] = step->edge[ ( sideaxis[1] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX ];

    /* Gather all linked sectors on edge side */
    prepPointerBundleInit( cache, &sectorbundle, linksectorbuffer, SIDE_SECTOR_BUNDLE_SIZE, 1 << 30 );
    prepBuildLinkBundle( cache, linknode, edgeindex, quadedge, &sectorbundle );

    if( sectorbundle.count == 1 )
    {
      step->link[edgeindex].p = sectorbundle.list[0];
      step->flags |= RF_LINK_SECTOR << ( RF_SECTOR_LINKFLAGS_SHIFT + edgeindex );
      continue;
    }

/*
fprintf( stderr, "Sector %p, Edgeindex %d, Linkcount %d\n", step, edgeindex, (int)sectorbundle.count );
*/

    /* Sort all links, min to max, on both axis */
    linkbundle.list = sidelinkbuffer;
    linkbundle.allocflag = 0;
    linkbundle.size = SIDE_LINK_BUNDLE_SIZE;
    linkbundle.count = 0;
    linkbundle.next = 0;
    linkbundle.last = &linkbundle;
    sideLinkListSort( cache, sideroot, &sectorbundle, sideaxis, &linkbundle );


/*
printf( "New sideLinkBuild(%d)\n", edgeindex );
*/

    /* Build the node tree */
    step->link[edgeindex].p = sideLinkBuild( build, sideroot, sectorbundle.count, quadedge, sideaxis, &linkbundle );
    step->flags |= RF_LINK_NODE << ( RF_SECTOR_LINKFLAGS_SHIFT + edgeindex );
    /* Free any memory allocated if we exceeded static storage */
    prepPointerBundleFree( cache, &sectorbundle );
    sideLinkBundleFree( cache, &linkbundle );
  }

  return;
}


#if PREP_TRIREF_REORDERING

/* Avoid divisions by zero/denormals */
#define PREP_TRIREF_BASE_BIAS (1.0/16777216.0)

static rff prepTriLocalLimitSum( triLocal *tl )
{
  rff sizex, sizey, sizez;

  sizex = tl->limit[RF_EDGE_MAXX] - tl->limit[RF_EDGE_MINX];
  sizey = tl->limit[RF_EDGE_MAXY] - tl->limit[RF_EDGE_MINY];
  sizez = tl->limit[RF_EDGE_MAXZ] - tl->limit[RF_EDGE_MINZ];
  if( tl->limflags & TRIL_LIMFLAGS_OUTX )
    sizex = 0.0;
  if( tl->limflags & TRIL_LIMFLAGS_OUTY )
    sizey = 0.0;
  if( tl->limflags & TRIL_LIMFLAGS_OUTZ )
    sizez = 0.0;

  return PREP_TRIREF_BASE_BIAS + ( sizex * sizey ) + ( sizex * sizez ) + ( sizey * sizez );
}


typedef struct
{
  rfindex triindex;
  rff area;
} prepTriRef;

static inline int prepTriRefSortCmp( prepTriRef *ref0, prepTriRef *ref1 )
{
  if( ref1->area > ref0->area )
    return 1;
  return 0;
}

#define MSORT_MAIN prepTriRefSort
#define MSORT_CMP prepTriRefSortCmp
#define MSORT_TYPE prepTriRef
#include "ccmergesort.h"
#undef MSORT_MAIN
#undef MSORT_CMP
#undef MSORT_TYPE


#endif



/* Convert links to trilocals into raw indices */
static void prepSectorSetGeometry( buildContext *build, rfPrepSector *step )
{
  int index;
  int32_t sectortrimax;
  triLocal *tlocal;
  rfPrepLink *trilist;

#if PREP_TRIREF_REORDERING

  int refcount;
  rfindex triindex;
  prepTriRef reftable[PREP_TRIREF_SORT_MAX];
  prepTriRef reftmp[PREP_TRIREF_SORT_MAX];
  prepTriRef *ref, *refsort;

  index = 0;
  if( step->tricount >= 2 )
  {
    refcount = step->tricount;
    if( refcount > PREP_TRIREF_SORT_MAX )
      refcount = PREP_TRIREF_SORT_MAX;

    ref = reftable;
    trilist = PREP_SECTOR_TRILIST( step );
    for( index = 0 ; index < refcount ; index++, ref++ )
    {
      tlocal = trilist[index].p;
      triindex = tlocal->index;
      ref->triindex = triindex;
      ref->area = build->triinfo[ triindex ].area * rfsqrt( prepTriLocalLimitSum( tlocal ) * build->triinfo[ triindex ].limitsuminv );
    }

    refsort = prepTriRefSort( reftable, reftmp, refcount );

    ref = refsort;
    trilist = PREP_SECTOR_TRILIST( step );
    for( index = 0 ; index < refcount ; index++ )
      trilist[index].i = ref[index].triindex;
  }

  trilist = PREP_SECTOR_TRILIST( step );
  for( ; index < step->tricount ; index++ )
  {
    tlocal = trilist[index].p;
    trilist[index].i = tlocal->index;
  }

#else

  trilist = PREP_SECTOR_TRILIST( step );
  for( index = 0 ; index < step->tricount ; index++ )
  {
    tlocal = trilist[index].p;
    trilist[index].i = tlocal->index;
  }

#endif

  mmAtomicAddL( &build->trirefcount, step->tricount );
  mmAtomicAddL( &build->sector16size, RF_GRAPH_SIZE_ALIGN( sizeof(rfSector16) + ( step->tricount * sizeof(int16_t) ) ) );
  mmAtomicAddL( &build->sector32size, RF_GRAPH_SIZE_ALIGN( sizeof(rfSector32) + ( step->tricount * sizeof(int32_t) ) ) );
  mmAtomicAddL( &build->sector64size, RF_GRAPH_SIZE_ALIGN( sizeof(rfSector64) + ( step->tricount * sizeof(int64_t) ) ) );
  while( step->tricount > ( sectortrimax = mmAtomicRead32( &build->sectortrimax ) ) )
  {
    if( mmAtomicCmpReplace32( &build->sectortrimax, sectortrimax, step->tricount ) )
      break;
  }

  return;
}


static void prepTreeFinish( buildContext *build, rfPrepNode *prepnode )
{
  rfGraphCache *cache;
  cache = build->cache;
  if( prepnode->status == PREPNODE_STATUS_SECTOR )
  {
    prepSectorConnect( build, prepnode->child[0].p );
    prepSectorSetGeometry( build, prepnode->child[0].p );
  }
  else if( prepnode->status == PREPNODE_STATUS_NODE )
  {
    prepTreeFinish( build, prepnode->child[0].p );
    prepTreeFinish( build, prepnode->child[1].p );
  }
  else
  {
    fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
    abort();
  }

  /* We can't free that yet! Need for neighbor collection */
/*
  mmBlockRelease( &cache->nodeblock, prepnode );
*/
  return;
}


static void prepExteriorLinks( buildContext *build, void **linktable, int *linkflags )
{
  int edgeindex;
  rff quadedge[4];
  const int *sideaxis;
  prepPointerBundle *sectorbundle;
  sideLink *sideroot[2];
  sideLink sidelinkbuffer[SIDE_LINK_BUNDLE_SIZE];
  sideLinkBundle linkbundle;
  rfGraphCache *cache;

  cache = build->cache;
  *linkflags = 0;
  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    sectorbundle = &build->extsectorbundle[edgeindex];
    if( sectorbundle->count == 1 )
    {
      linktable[edgeindex] = sectorbundle->list[0];
      *linkflags |= RF_LINK_SECTOR << ( RF_SECTOR_LINKFLAGS_SHIFT + edgeindex );
      continue;
    }

    sideaxis = &sideAxisTable[ edgeindex & ~0x1 ];
    quadedge[0] = cache->edge[ ( sideaxis[0] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN ];
    quadedge[1] = cache->edge[ ( sideaxis[0] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX ];
    quadedge[2] = cache->edge[ ( sideaxis[1] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN ];
    quadedge[3] = cache->edge[ ( sideaxis[1] << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX ];

    /* Sort all links, min to max, on both axis */
    linkbundle.list = sidelinkbuffer;
    linkbundle.allocflag = 0;
    linkbundle.size = SIDE_LINK_BUNDLE_SIZE;
    linkbundle.count = 0;
    linkbundle.next = 0;
    linkbundle.last = &linkbundle;
    sideLinkListSort( cache, sideroot, sectorbundle, sideaxis, &linkbundle );

    /* Build the node tree */
    linktable[edgeindex] = sideLinkBuild( build, sideroot, sectorbundle->count, quadedge, sideaxis, &linkbundle );
    *linkflags |= RF_LINK_NODE << ( RF_SECTOR_LINKFLAGS_SHIFT + edgeindex );

    /* Free any memory allocated if we exceeded static storage */
    prepPointerBundleFree( cache, sectorbundle );
    sideLinkBundleFree( cache, &linkbundle );
  }

  return;
}


////


static void prepHaloBuild( buildContext *build )
{
  int x, y, z, index;
  rfPrepSector *step;
  rff *edge;
  rfGraphCache *cache;

  cache = build->cache;
  edge = cache->edge;

  for( z = 0 ; z < 3 ; z++ )
  {
    for( y = 0 ; y < 3 ; y++ )
    {
      for( x = 0 ; x < 3 ; x++ )
      {
        index = HALO_INDEX(x,y,z);
        build->halo[ index ] = 0;
        if( index == HALO_INDEX(1,1,1) )
          continue;
        step = mmVolumeAlloc( &cache->memvolume, sizeof(rfPrepSector) );
        step->index = mmAtomicReadAddL( &build->sectorindex, 1 );
        mmDirectorySet( &cache->sectordir, step->index, step );
        build->halo[ index ] = step;
        mmAtomicAddL( &build->sector16size, RF_GRAPH_SIZE_ALIGN( sizeof(rfSector16) ) );
        mmAtomicAddL( &build->sector32size, RF_GRAPH_SIZE_ALIGN( sizeof(rfSector32) ) );
        mmAtomicAddL( &build->sector64size, RF_GRAPH_SIZE_ALIGN( sizeof(rfSector64) ) );
      }
    }
  }

  for( z = 0 ; z < 3 ; z++ )
  {
    for( y = 0 ; y < 3 ; y++ )
    {
      for( x = 0 ; x < 3 ; x++ )
      {
        index = HALO_INDEX(x,y,z);
        step = build->halo[ index ];
        if( !( step ) )
          continue;

        step->flags = 0x3f << RF_SECTOR_LINKFLAGS_SHIFT;
        step->tricount = 0;
        switch( x )
        {
          case 0:
            step->edge[RF_EDGE_MINX] = edge[RF_EDGE_MINX];
            step->edge[RF_EDGE_MAXX] = edge[RF_EDGE_MINX];
            step->link[RF_EDGE_MINX].p = 0;
            step->link[RF_EDGE_MAXX].p = build->halo[ HALO_INDEX(x+1,y,z) ];
            break;
          case 1:
            step->edge[RF_EDGE_MINX] = edge[RF_EDGE_MINX];
            step->edge[RF_EDGE_MAXX] = edge[RF_EDGE_MAXX];
            step->link[RF_EDGE_MINX].p = 0;
            step->link[RF_EDGE_MAXX].p = 0;
            break;
          case 2:
            step->edge[RF_EDGE_MINX] = edge[RF_EDGE_MAXX];
            step->edge[RF_EDGE_MAXX] = edge[RF_EDGE_MAXX];
            step->link[RF_EDGE_MINX].p = build->halo[ HALO_INDEX(x-1,y,z) ];
            step->link[RF_EDGE_MAXX].p = 0;
            break;
        }
        switch( y )
        {
          case 0:
            step->edge[RF_EDGE_MINY] = edge[RF_EDGE_MINY];
            step->edge[RF_EDGE_MAXY] = edge[RF_EDGE_MINY];
            step->link[RF_EDGE_MINY].p = 0;
            step->link[RF_EDGE_MAXY].p = build->halo[ HALO_INDEX(x,y+1,z) ];
            break;
          case 1:
            step->edge[RF_EDGE_MINY] = edge[RF_EDGE_MINY];
            step->edge[RF_EDGE_MAXY] = edge[RF_EDGE_MAXY];
            step->link[RF_EDGE_MINY].p = 0;
            step->link[RF_EDGE_MAXY].p = 0;
            break;
          case 2:
            step->edge[RF_EDGE_MINY] = edge[RF_EDGE_MAXY];
            step->edge[RF_EDGE_MAXY] = edge[RF_EDGE_MAXY];
            step->link[RF_EDGE_MINY].p = build->halo[ HALO_INDEX(x,y-1,z) ];
            step->link[RF_EDGE_MAXY].p = 0;
            break;
        }
        switch( z )
        {
          case 0:
            step->edge[RF_EDGE_MINZ] = edge[RF_EDGE_MINZ];
            step->edge[RF_EDGE_MAXZ] = edge[RF_EDGE_MINZ];
            step->link[RF_EDGE_MINZ].p = 0;
            step->link[RF_EDGE_MAXZ].p = build->halo[ HALO_INDEX(x,y,z+1) ];
            break;
          case 1:
            step->edge[RF_EDGE_MINZ] = edge[RF_EDGE_MINZ];
            step->edge[RF_EDGE_MAXZ] = edge[RF_EDGE_MAXZ];
            step->link[RF_EDGE_MINZ].p = 0;
            step->link[RF_EDGE_MAXZ].p = 0;
            break;
          case 2:
            step->edge[RF_EDGE_MINZ] = edge[RF_EDGE_MAXZ];
            step->edge[RF_EDGE_MAXZ] = edge[RF_EDGE_MAXZ];
            step->link[RF_EDGE_MINZ].p = build->halo[ HALO_INDEX(x,y,z-1) ];
            step->link[RF_EDGE_MAXZ].p = 0;
            break;
        }
      }
    }
  }

  return;
}

static void prepHaloConnect( buildContext *build, void **extlink, int extlinkflags )
{
  int32_t linkflag;
  rfPrepSector *step;

  /* Connect the -X sector */
  step = build->halo[ HALO_INDEX( 0, 1, 1 ) ];
  step->link[RF_EDGE_MAXX].p = extlink[RF_EDGE_MAXX];
  step->flags &= ~( ( RF_LINK_SECTOR << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MAXX );
  linkflag = ( ( extlinkflags >> RF_SECTOR_LINKFLAGS_SHIFT ) >> RF_EDGE_MAXX ) & 0x1;
  step->flags |= ( linkflag << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MAXX;

  /* Connect the +X sector */
  step = build->halo[ HALO_INDEX( 2, 1, 1 ) ];
  step->link[RF_EDGE_MINX].p = extlink[RF_EDGE_MINX];
  step->flags &= ~( ( RF_LINK_SECTOR << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MINX );
  linkflag = ( ( extlinkflags >> RF_SECTOR_LINKFLAGS_SHIFT ) >> RF_EDGE_MINX ) & 0x1;
  step->flags |= ( linkflag << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MINX;

  /* Connect the -Y sector */
  step = build->halo[ HALO_INDEX( 1, 0, 1 ) ];
  step->link[RF_EDGE_MAXY].p = extlink[RF_EDGE_MAXY];
  step->flags &= ~( ( RF_LINK_SECTOR << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MAXY );
  linkflag = ( ( extlinkflags >> RF_SECTOR_LINKFLAGS_SHIFT ) >> RF_EDGE_MAXY ) & 0x1;
  step->flags |= ( linkflag << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MAXY;

  /* Connect the +Y sector */
  step = build->halo[ HALO_INDEX( 1, 2, 1 ) ];
  step->link[RF_EDGE_MINY].p = extlink[RF_EDGE_MINY];
  step->flags &= ~( ( RF_LINK_SECTOR << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MINY );
  linkflag = ( ( extlinkflags >> RF_SECTOR_LINKFLAGS_SHIFT ) >> RF_EDGE_MINY ) & 0x1;
  step->flags |= ( linkflag << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MINY;

  /* Connect the -Z sector */
  step = build->halo[ HALO_INDEX( 1, 1, 0 ) ];
  step->link[RF_EDGE_MAXZ].p = extlink[RF_EDGE_MAXZ];
  step->flags &= ~( ( RF_LINK_SECTOR << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MAXZ );
  linkflag = ( ( extlinkflags >> RF_SECTOR_LINKFLAGS_SHIFT ) >> RF_EDGE_MAXZ ) & 0x1;
  step->flags |= ( linkflag << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MAXZ;

  /* Connect the +Z sector */
  step = build->halo[ HALO_INDEX( 1, 1, 2 ) ];
  step->link[RF_EDGE_MINZ].p = extlink[RF_EDGE_MINZ];
  step->flags &= ~( ( RF_LINK_SECTOR << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MINZ );
  linkflag = ( ( extlinkflags >> RF_SECTOR_LINKFLAGS_SHIFT ) >> RF_EDGE_MINZ ) & 0x1;
  step->flags |= ( linkflag << RF_SECTOR_LINKFLAGS_SHIFT ) << RF_EDGE_MINZ;

  return;
}


////


static void prepGetTrianglePoints( rfTri *tri, rff *pt )
{
#if RF_USE_FLOAT
  double f, p[12], ptd[3];
  copy4rff2d( &p[0], tri->plane );
  copy4rff2d( &p[4], tri->edpu );
  copy4rff2d( &p[8], tri->edpv );
  rfMathPlaneIntersect3d( ptd, &p[0], &p[4], &p[8] );
  copy3d2rff( &pt[6], ptd );
  f = p[4+3];
  p[4+3] -= 1.0;
  rfMathPlaneIntersect3d( ptd, &p[0], &p[4], &p[8] );
  copy3d2rff( &pt[0], ptd );
  p[4+3] = f;
  p[8+3] -= 1.0;
  rfMathPlaneIntersect3d( ptd, &p[0], &p[4], &p[8] );
  copy3d2rff( &pt[3], ptd );
#else
  rff p[4];
  rfMathPlaneIntersect3d( &pt[6], tri->plane, tri->edpu, tri->edpv );
  p[0] = tri->edpu[0];
  p[1] = tri->edpu[1];
  p[2] = tri->edpu[2];
  p[3] = tri->edpu[3] - 1.0;
  rfMathPlaneIntersect3d( &pt[0], tri->plane, p, tri->edpv );
  p[0] = tri->edpv[0];
  p[1] = tri->edpv[1];
  p[2] = tri->edpv[2];
  p[3] = tri->edpv[3] - 1.0;
  rfMathPlaneIntersect3d( &pt[3], tri->plane, tri->edpu, p );
#endif
  return;
}


////


static void edgeExtend( rff * const restrict edge, const rff * const restrict point )
{
  if( point[0] < edge[RF_EDGE_MINX] )
    edge[RF_EDGE_MINX] = point[0];
  if( point[0] > edge[RF_EDGE_MAXX] )
    edge[RF_EDGE_MAXX] = point[0];
  if( point[1] < edge[RF_EDGE_MINY] )
    edge[RF_EDGE_MINY] = point[1];
  if( point[1] > edge[RF_EDGE_MAXY] )
    edge[RF_EDGE_MAXY] = point[1];
  if( point[2] < edge[RF_EDGE_MINZ] )
    edge[RF_EDGE_MINZ] = point[2];
  if( point[2] > edge[RF_EDGE_MAXZ] )
    edge[RF_EDGE_MAXZ] = point[2];
  return;
}



#if PREP_TRIREF_REORDERING
static rff prepTriangleArea( rff *v0, rff *v1, rff *v2 )
{
  rff area;
  rff u[RF_AXIS_COUNT], v[RF_AXIS_COUNT], cpr[RF_AXIS_COUNT];

  rfMathVectorSubStore( u, v1, v0 );
  rfMathVectorSubStore( v, v2, v0 );
  rfMathVectorCrossProduct( cpr, u, v );
  area = 0.5 * rfMathVectorMagnitude( cpr );

  return area;
}
#endif


#if PREP_TRIREF_REORDERING
static size_t readGeometryGeneric( rfGeometry *geometry, rfTri *trilist, triLocal *tlist, prepTriInfo *triinfo, uint32_t *tridataoffset, rff *edge )
#else
static size_t readGeometryGeneric( rfGeometry *geometry, rfTri *trilist, triLocal *tlist, uint32_t *tridataoffset, rff *edge )
#endif
{
  size_t triindex, graphtricount, privatedataoffset;
  size_t i0, i1, i2;
  void *v0, *v1, *v2;
  void *vertexpointer;
  void *indexpointer;
  rfTri *tri;
  triLocal *tl;
  rff vrff[9];

  tri = trilist;
  tl = tlist;
  vertexpointer = geometry->vertexpointer;
  indexpointer = geometry->indexpointer;
  graphtricount = 0;
  privatedataoffset = 0;
  for( triindex = geometry->primitivecount ; triindex ; triindex--, privatedataoffset += geometry->datastride )
  {
    if( geometry->indexpointer )
    {
      switch( geometry->indextype )
      {
        case RF_SHORT:
        case RF_USHORT:
          i0 = ((unsigned short *)indexpointer)[0];
          i1 = ((unsigned short *)indexpointer)[1];
          i2 = ((unsigned short *)indexpointer)[2];
          indexpointer = ADDRESS( indexpointer, 3*sizeof(short) );
          break;
        case RF_INT:
        case RF_UINT:
          i0 = ((unsigned int *)indexpointer)[0];
          i1 = ((unsigned int *)indexpointer)[1];
          i2 = ((unsigned int *)indexpointer)[2];
          indexpointer = ADDRESS( indexpointer, 3*sizeof(int) );
          break;
        case RF_INT8:
        case RF_UINT8:
          i0 = ((uint8_t *)indexpointer)[0];
          i1 = ((uint8_t *)indexpointer)[1];
          i2 = ((uint8_t *)indexpointer)[2];
          indexpointer = ADDRESS( indexpointer, 3*sizeof(uint8_t) );
          break;
        case RF_INT16:
        case RF_UINT16:
          i0 = ((uint16_t *)indexpointer)[0];
          i1 = ((uint16_t *)indexpointer)[1];
          i2 = ((uint16_t *)indexpointer)[2];
          indexpointer = ADDRESS( indexpointer, 3*sizeof(uint16_t) );
          break;
        case RF_INT32:
        case RF_UINT32:
          i0 = ((uint32_t *)indexpointer)[0];
          i1 = ((uint32_t *)indexpointer)[1];
          i2 = ((uint32_t *)indexpointer)[2];
          indexpointer = ADDRESS( indexpointer, 3*sizeof(uint32_t) );
          break;
        case RF_INT64:
        case RF_UINT64:
          i0 = ((uint64_t *)indexpointer)[0];
          i1 = ((uint64_t *)indexpointer)[1];
          i2 = ((uint64_t *)indexpointer)[2];
          indexpointer = ADDRESS( indexpointer, 3*sizeof(uint64_t) );
          break;
      }
      v0 = ADDRESS( vertexpointer, i0 * geometry->vertexstride );
      v1 = ADDRESS( vertexpointer, i1 * geometry->vertexstride );
      v2 = ADDRESS( vertexpointer, i2 * geometry->vertexstride );
    }
    else
    {
      v0 = vertexpointer;
      v1 = ADDRESS( v0, geometry->vertexstride );
      v2 = ADDRESS( v1, geometry->vertexstride );
      vertexpointer = ADDRESS( v2, geometry->vertexstride );
    }
    if( geometry->vertextype == RF_FLOAT )
    {
      copy3f2rff( &vrff[0], v0 );
      copy3f2rff( &vrff[3], v1 );
      copy3f2rff( &vrff[6], v2 );
    }
    else if( geometry->vertextype == RF_DOUBLE )
    {
      copy3d2rff( &vrff[0], v0 );
      copy3d2rff( &vrff[3], v1 );
      copy3d2rff( &vrff[6], v2 );
    }
    if( !( rfAddTriangle( tri, &vrff[0], &vrff[3], &vrff[6] ) ) )
      continue;
    triLocalLimits( tl, &vrff[0], &vrff[3], &vrff[6] );
    tl->index = graphtricount;
    if( tridataoffset )
      tridataoffset[graphtricount] = privatedataoffset;
#if PREP_TRIREF_REORDERING
    triinfo[ tl->index ].area = prepTriangleArea( &vrff[0], &vrff[3], &vrff[6] );
    triinfo[ tl->index ].limitsuminv = 1.0 / prepTriLocalLimitSum( tl );
#endif
    edgeExtend( edge, &vrff[0] );
    edgeExtend( edge, &vrff[3] );
    edgeExtend( edge, &vrff[6] );
    tl++;
    tri++;
    graphtricount++;
  }

  return graphtricount;
}

#if PREP_TRIREF_REORDERING
static size_t readGeometry( rfGeometry *geometry, rfTri *trilist, triLocal *tlist, prepTriInfo *triinfo, uint32_t *tridataoffset, rff *edge )
#else
static size_t readGeometry( rfGeometry *geometry, rfTri *trilist, triLocal *tlist, uint32_t *tridataoffset, rff *edge )
#endif
{
  size_t triindex, graphtricount, privatedataoffset;
  uint32_t i0, i1, i2;
  rff *v0, *v1, *v2;
  void *vertexpointer;
  void *indexpointer;
  rfTri *tri;
  triLocal *tl;

  edge[RF_EDGE_MINX] =  RFF_MAX;
  edge[RF_EDGE_MAXX] = -RFF_MAX;
  edge[RF_EDGE_MINY] =  RFF_MAX;
  edge[RF_EDGE_MAXY] = -RFF_MAX;
  edge[RF_EDGE_MINZ] =  RFF_MAX;
  edge[RF_EDGE_MAXZ] = -RFF_MAX;
  if( ( geometry->vertextype != RF_RFF ) || ( ( geometry->indexpointer ) && ( geometry->indextype != RF_INT32 ) && ( geometry->indextype != RF_UINT32 ) ) )
  {
#if PREP_TRIREF_REORDERING
    graphtricount = readGeometryGeneric( geometry, trilist, tlist, triinfo, tridataoffset, edge );
#else
    graphtricount = readGeometryGeneric( geometry, trilist, tlist, tridataoffset, edge );
#endif
  }
  else
  {
    tri = trilist;
    tl = tlist;
    indexpointer = geometry->indexpointer;
    graphtricount = 0;
    privatedataoffset = 0;
    if( geometry->indexpointer )
    {
      vertexpointer = geometry->vertexpointer;
      for( triindex = geometry->primitivecount ; triindex ; triindex--, privatedataoffset += geometry->datastride )
      {
        i0 = ((uint32_t *)indexpointer)[0];
        i1 = ((uint32_t *)indexpointer)[1];
        i2 = ((uint32_t *)indexpointer)[2];
        indexpointer = ADDRESS( indexpointer, 3*sizeof(uint32_t) );
        v0 = ADDRESS( vertexpointer, i0 * geometry->vertexstride );
        v1 = ADDRESS( vertexpointer, i1 * geometry->vertexstride );
        v2 = ADDRESS( vertexpointer, i2 * geometry->vertexstride );
        if( !( rfAddTriangle( tri, v0, v1, v2 ) ) )
          continue;
        triLocalLimits( tl, v0, v1, v2 );
        tl->index = graphtricount;
        if( tridataoffset )
          tridataoffset[graphtricount] = privatedataoffset;
#if PREP_TRIREF_REORDERING
        triinfo[ tl->index ].area = prepTriangleArea( v0, v1, v2 );
        triinfo[ tl->index ].limitsuminv = 1.0 / prepTriLocalLimitSum( tl );
#endif
        edgeExtend( edge, v0 );
        edgeExtend( edge, v1 );
        edgeExtend( edge, v2 );
        tl++;
        tri++;
        graphtricount++;
      }
    }
    else
    {
      vertexpointer = geometry->vertexpointer;
      for( triindex = geometry->primitivecount ; triindex ; triindex--, privatedataoffset += geometry->datastride )
      {
        v0 = vertexpointer;
        v1 = ADDRESS( v0, geometry->vertexstride );
        v2 = ADDRESS( v1, geometry->vertexstride );
        if( !( rfAddTriangle( tri, v0, v1, v2 ) ) )
          continue;
        triLocalLimits( tl, v0, v1, v2 );
        tl->index = graphtricount;
        if( tridataoffset )
          tridataoffset[graphtricount] = privatedataoffset;
#if PREP_TRIREF_REORDERING
        triinfo[ tl->index ].area = prepTriangleArea( v0, v1, v2 );
        triinfo[ tl->index ].limitsuminv = 1.0 / prepTriLocalLimitSum( tl );
#endif
        edgeExtend( edge, v0 );
        edgeExtend( edge, v1 );
        edgeExtend( edge, v2 );
        tl++;
        tri++;
        graphtricount++;
      }
    }
  }

  return graphtricount;
}


////


static double evalComputeNodeValue( rfPrepNode *prepnode, int edgeindex, rff *edge, rff areasum )
{
  int childindex;
  rff size[3], area, factor;
  double value;
  rff localedge[RF_EDGE_COUNT];
  const int *sideaxis;

  value = 0.0;
  for( childindex = 0 ; childindex < RF_NODE_CHILD_COUNT ; childindex++ )
  {
    memcpy( localedge, edge, 6*sizeof(rff) );
    localedge[ RF_AXIS_TO_EDGE( PREP_NODE_GETAXIS( prepnode ), childindex ) ] = prepnode->plane;

    size[0] = localedge[RF_EDGE_MAXX] - localedge[RF_EDGE_MINX];
    size[1] = localedge[RF_EDGE_MAXY] - localedge[RF_EDGE_MINY];
    size[2] = localedge[RF_EDGE_MAXZ] - localedge[RF_EDGE_MINZ];
    sideaxis = &sideAxisTable[ edgeindex & ~0x1 ];
    area = size[ sideaxis[0] ] * size[ sideaxis[1] ];

    factor = area / areasum;
    if( prepnode->nodeflags & ( RF_LINK_SECTOR << ( childindex + RF_NODE_LINKFLAGS_SHIFT ) ) )
      value += factor;
    else
      value += ( factor * EVAL_NODECOST ) + evalComputeNodeValue( prepnode->child[childindex].p, edgeindex, localedge, areasum );
  }

  return value;
}

/*
IncomingCost = AreaSum * TriangleCount;
OutgoingCost = Sum( Node * Area );
*/
static double evalComputeStepValue( rfPrepSector *step )
{
  int edgeindex;
  rff sizex, sizey, sizez, area[3], hitcost;
  double value, sidevalue;

  sizex = step->edge[RF_EDGE_MAXX] - step->edge[RF_EDGE_MINX];
  sizey = step->edge[RF_EDGE_MAXY] - step->edge[RF_EDGE_MINY];
  sizez = step->edge[RF_EDGE_MAXZ] - step->edge[RF_EDGE_MINZ];
  area[0] = sizey * sizez;
  area[1] = sizex * sizez;
  area[2] = sizex * sizey;

  hitcost = 0.0;
  value = 0.0;
  if( step->tricount )
  {
    hitcost = EVAL_TRITESTBASE + (rff)step->tricount;
    value = ( area[0] + area[1] + area[2] ) * hitcost;
  }
  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    if( step->flags & ( RF_LINK_SECTOR << ( edgeindex + RF_SECTOR_LINKFLAGS_SHIFT ) ) )
      sidevalue = 1.0;
    else
      sidevalue = EVAL_NODECOST + evalComputeNodeValue( step->link[edgeindex].p, edgeindex, step->edge, area[ RF_EDGE_TO_AXIS( edgeindex ) ] );
    sidevalue *= EVAL_SIDEFACTOR * area[ RF_EDGE_TO_AXIS( edgeindex ) ];
    value += sidevalue;
  }

  return value;
}

double rfEvaluateGraphCost( rfGraphCache *cache )
{
  rfindex i;
  rff size[3], area[3];
  rfPrepSector *step;
  double value;

  value = 0.0;
  for( i = 0 ; i < cache->sectorcount ; i++ )
  {
    step = mmDirectoryGetFast( &cache->sectordir, i );
    value += evalComputeStepValue( step );
  }
  size[0] = cache->edge[RF_EDGE_MAXX] - cache->edge[RF_EDGE_MINX];
  size[1] = cache->edge[RF_EDGE_MAXY] - cache->edge[RF_EDGE_MINY];
  size[2] = cache->edge[RF_EDGE_MAXZ] - cache->edge[RF_EDGE_MINZ];
  area[0] = size[1] * size[2];
  area[1] = size[0] * size[2];
  area[2] = size[0] * size[1];
  value /= 2.0 * ( area[0] + area[1] + area[2] );

  return (rff)value;
}


////


static void rfBuildGraphEdge( rff *graphedge, rff *geomedge )
{
  rff sizex, sizey, sizez, sizemin, extend;

  memcpy( graphedge, geomedge, RF_EDGE_COUNT*sizeof(rff) );
  edgeExtendSize( graphedge, PREP_EDGE_EXTENSION_FACTOR );

  sizex = graphedge[RF_EDGE_MAXX] - graphedge[RF_EDGE_MINX];
  sizey = graphedge[RF_EDGE_MAXY] - graphedge[RF_EDGE_MINY];
  sizez = graphedge[RF_EDGE_MAXZ] - graphedge[RF_EDGE_MINZ];
  sizemin = PREP_GRAPH_EDGE_SIZE_MIN * rffmax( rffmax( sizex, sizey ), sizez );

  if( sizex < sizemin )
  {
    extend = (1.0/2.0) * ( sizemin - sizex );
    graphedge[RF_EDGE_MINX] -= extend;
    graphedge[RF_EDGE_MAXX] += extend;
  }
  if( sizey < sizemin )
  {
    extend = (1.0/2.0) * ( sizemin - sizey );
    graphedge[RF_EDGE_MINY] -= extend;
    graphedge[RF_EDGE_MAXY] += extend;
  }
  if( sizez < sizemin )
  {
    extend = (1.0/2.0) * ( sizemin - sizez );
    graphedge[RF_EDGE_MINZ] -= extend;
    graphedge[RF_EDGE_MAXZ] += extend;
  }

  return;
}


/* Build graph cache, later converted into real graph */
void rfBuildGraphCache( rfGraphCache *cache, rfGeometry *geometry, rff *box, rfindex originwidth, rfint buildquality, rfint buildmemory )
{
  int i, roottricount, edgeindex, extlinkflags, graphtricount;
  rff buildparam, buildfactor;
  rff geomedge[RF_EDGE_COUNT];
  void *extlink[RF_EDGE_COUNT];
  rfPrepSector *step;
  rfPrepNode *rootnode;
  triLocal *tl, *tlist;
  rfPrepLink *steptri;
  prepPointerBundle *sectorbundle;
  buildContext build;
#if PREP_LINK_TRACKING
  int linkcount[6] = { 0, 0, 0, 0, 0, 0 };
#endif
  stepOp op;

  /* Set build parameters */
  build.treestopflag = 0;
  build.valuestepfactor = PREP_STEPGAIN_FACTOR;
  build.valuetricapfactor = PREP_STEPGAIN_TRICAP_FACTOR;
  if( buildquality == RF_BUILDHINT_VALUE_DEFAULT )
    buildparam = 0.5;
  else
    buildparam = (rff)( buildquality - RF_BUILDHINT_VALUE_MIN ) / (rff)( RF_BUILDHINT_VALUE_MAX - RF_BUILDHINT_VALUE_MIN );
  buildfactor = rfpow( buildparam, 2.5 );
  build.bincountfactor = PREP_BINCOUNT_BASE + ( buildfactor * PREP_BINCOUNT_RANGE );
  build.bincountmax = (int)( PREP_BINMAX_BASE + ( buildfactor * PREP_BINMAX_RANGE ) );
  if( build.bincountmax > PREP_BIN_MAXIMUM )
    build.bincountmax = PREP_BIN_MAXIMUM;
  if( buildmemory == RF_BUILDHINT_VALUE_DEFAULT )
    buildparam = 0.5;
  else
    buildparam = (rff)( buildmemory - RF_BUILDHINT_VALUE_MIN ) / (rff)( RF_BUILDHINT_VALUE_MAX - RF_BUILDHINT_VALUE_MIN );
  buildfactor = PREP_STEPCAP_BASE + ( rfpow( buildparam, 2.5 ) * PREP_STEPCAP_RANGE );
  build.stepcap = (rfindex)( buildfactor * (rff)geometry->primitivecount );
  build.sectortricap = PREP_SECTOR_TRICAP;

  /* Initialize memory managers */
  mmVolumeInit( &cache->memvolume, 4*1048576, sizeof(rfPrepSector), 1048576, 0x8 );
  mmBlockInit( &cache->nodeblock, sizeof(rfPrepNode), 16384, 16384, 0x8 );
  mmGrowInit( &build.mgrow, ( ( geometry->primitivecount >> 1 ) + 256 ) * sizeof(triLocal) );
  mmDirectoryInit( &cache->sectordir, 16, ( build.stepcap >> 16 ) + 16384 );
  mmDirectoryInit( &cache->nodedir, 16, 6 * ( ( build.stepcap >> 16 ) + 16384 ) );
  build.cache = cache;

  /* Allocate and read geometry */
  build.trilist = malloc( geometry->primitivecount * sizeof(rfTri) );
  tlist = mmGrowAlloc( &build.mgrow, geometry->primitivecount * sizeof(triLocal) );
  cache->tridataoffset = 0;
  if( geometry->datasize )
    cache->tridataoffset = mmVolumeAlloc( &cache->memvolume, geometry->primitivecount * sizeof(uint32_t) );
#if PREP_TRIREF_REORDERING
  build.triinfo = malloc( geometry->primitivecount * sizeof(prepTriInfo) );
  graphtricount = readGeometry( geometry, build.trilist, tlist, build.triinfo, cache->tridataoffset, geomedge );
#else
  graphtricount = readGeometry( geometry, build.trilist, tlist, cache->tridataoffset, geomedge );
#endif

  /* Build root sector step */
  rfBuildGraphEdge( cache->edge, ( box ? box : geomedge ) );

  step = mmVolumeAlloc( &cache->memvolume, sizeof(rfPrepSector) + ( graphtricount * sizeof(rfPrepLink) ) );
  step->tricount = graphtricount;
  step->flags = 0;
  memcpy( step->edge, cache->edge, RF_EDGE_COUNT*sizeof(rff) );
  memset( step->link, 0, RF_EDGE_COUNT*sizeof(rfPrepLink) );
#if PREP_LINK_TRACKING
  stepComputeValue( step, linkcount );
#else
  stepComputeValue( step );
#endif

  /* Store triangle references in root sector */
  steptri = PREP_SECTOR_TRILIST( step );
  tl = tlist;
  roottricount = 0;
  for( i = graphtricount ; i ; i--, tl++ )
  {
    /* If our triangles could extend beyond the box, better clip */
/*
    if( !( triClipLimits( tl, box ) ) )
      continue;
*/
    steptri->p = tl;
    steptri++;
    roottricount++;
  }
  step->tricount = roottricount;

  /* Initialize bundle buffering sectors connected to exterior */
  mmAtomicWriteL( &build.sectorindex, 0 );
  mmAtomicWriteL( &build.nodeindex, 0 );
  mmAtomicWriteL( &build.trirefcount, 0 );
  mmAtomicWriteL( &build.sector16size, 0 );
  mmAtomicWriteL( &build.sector32size, 0 );
  mmAtomicWriteL( &build.sector64size, 0 );
  mmAtomicWrite32( &build.sectortrimax, 0 );
  mmAtomicWriteL( &build.stepcount, 0 );
  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
  {
    sectorbundle = &build.extsectorbundle[edgeindex];
    sectorbundle->size = 1024;
    sectorbundle->count = 0;
    sectorbundle->list = mmVolumeAlloc( &cache->memvolume, sectorbundle->size * sizeof(rfPrepSector *) );
    sectorbundle->alloclist = sectorbundle->list;
  }

  /* Initialize root prepnode directly pointing to root sector */
  rootnode = mmBlockAlloc( &cache->nodeblock );
  rootnode->child[0].p = step;
  rootnode->status = PREPNODE_STATUS_NOTREADY;

  /* Initialize halo sectors, assign indices to halo sectors */
  prepHaloBuild( &build );
  for( edgeindex = 0 ; edgeindex < RF_EDGE_COUNT ; edgeindex++ )
    prepPointerBundleInit( cache, &build.extsectorbundle[edgeindex], 0, 1024, 1 << 30 );

  /* Build down sectors from root node, assign indices to sectors */
  mmQueueInit( &build.buildtreequeue, sizeof(stepOp), ( graphtricount >> 4 ) + 256, 8, PREP_QUEUE_BUNDLE_RANGE );
  prepTreeEvaluate( &build, rootnode );
  for( ; ; )
  {
    if( !( mmQueueGet( &build.buildtreequeue, &op ) ) )
      break;
    prepTreeBuildStep( &build, &op );
  }

  /* Resolve table of origin points */
  rfGraphOriginInit( &cache->origintable, cache->edge, originwidth );
  prepBuildOriginTable( &build, rootnode );

  /* Allocate hash table to find duplicate nodes */
  prepAllocateNodeHashTable( &build );

  /* Build sector links, assign indices to nodes, free original tree nodes */
  prepTreeFinish( &build, rootnode );

  /* Build exterior links from stored extrefs, assign indices to ext nodes */
  prepExteriorLinks( &build, extlink, &extlinkflags );

  /* Connect halo sectors */
  prepHaloConnect( &build, extlink, extlinkflags );

  /* Free memory */
  mmGrowFreeAll( &build.mgrow );
  mmQueueFree( &build.buildtreequeue );
#if PREP_TRIREF_REORDERING
  free( build.triinfo );
#endif

  /* Get cache struct ready */
  cache->trilist = build.trilist;
  cache->trianglecount = graphtricount;
  cache->nodecount = mmAtomicReadL( &build.nodeindex );
  cache->sectorcount = mmAtomicReadL( &build.sectorindex );
  cache->trirefcount = mmAtomicReadL( &build.trirefcount );
  cache->sector16size = mmAtomicReadL( &build.sector16size );
  cache->sector32size = mmAtomicReadL( &build.sector32size );
  cache->sector64size = mmAtomicReadL( &build.sector64size );
  cache->sectortrimax = mmAtomicRead32( &build.sectortrimax );

  return;
}


void rfFreeGraphCache( rfGraphCache *cache )
{
  free( cache->trilist );
  free( cache->origin );
  mmBlockFreeAll( &cache->nodeblock );
  mmVolumeFreeAll( &cache->memvolume );
  mmDirectoryFree( &cache->sectordir );
  mmDirectoryFree( &cache->nodedir );
  return;
}


////


