/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>


#define COLUMN_COUNT (30)


int main( int argc, char *argv[] )
{
  int argindex, c, i, textflag;
  size_t size;
  char *inputfilename;
  char *outputfilename;
  char *symbolname;
  FILE *inputfile;
  FILE *outputfile;
  FILE *headerfile;
  char filenamebuffer[4096];

  inputfilename = 0;
  outputfilename = 0;
  symbolname = 0;
  textflag = 0;
  for( argindex = 1 ; argindex < argc ; )
  {
    if( !( strcmp( argv[argindex], "-i" ) ) )
    {
      if( ++argindex == argc )
      {
        printf( "Error, missing argument for -i command\n" );
        return 1;
      }
      inputfilename = argv[argindex++];
    }
    else if( !( strcmp( argv[argindex], "-o" ) ) )
    {
      if( ++argindex == argc )
      {
        printf( "Error, missing argument for -o command\n" );
        return 1;
      }
      outputfilename = argv[argindex++];
    }
    else if( !( strcmp( argv[argindex], "-s" ) ) )
    {
      if( ++argindex == argc )
      {
        printf( "Error, missing argument for -s command\n" );
        return 1;
      }
      symbolname = argv[argindex++];
    }
    else if( !( strcmp( argv[argindex], "-t" ) ) )
    {
      textflag = 1;
      argindex++;
    }
    else if( !( strcmp( argv[argindex], "-b" ) ) )
    {
      textflag = 0;
      argindex++;
    }
    else
    {
      printf( "Error, unrecognized option : %s\n", argv[argindex] );
      return 1;
    }
  }

  if( !( inputfilename ) || !( outputfilename ) )
  {
    printf( "./binload -i inputfile -o outputfile [-t] [-b]\n" );
    return 1;
  }
  if( !( symbolname ) )
    symbolname = outputfilename;

  if( !( inputfile = fopen( inputfilename, "r" ) ) )
  {
    printf( "Error, can't open \"%s\" for reading\n", inputfilename );
    return 0;
  }
  snprintf( filenamebuffer, 4096, "%s.c", outputfilename );
  if( !( outputfile = fopen( filenamebuffer, "w" ) ) )
  {
    printf( "Error, can't open \"%s\" for writing\n", filenamebuffer );
    return 0;
  }
  snprintf( filenamebuffer, 4096, "%s.h", outputfilename );
  if( !( headerfile = fopen( filenamebuffer, "w" ) ) )
  {
    printf( "Error, can't open \"%s\" for writing\n", filenamebuffer );
    return 0;
  }

  fprintf( outputfile, "const char %s[] = ", symbolname );
  if( textflag )
    fprintf( outputfile, "\"\\\n" );
  else
    fprintf( outputfile, "{\n" );
  i = 0;
  for( size = 0 ; ; size++ )
  {
    c = fgetc( inputfile );
    if( c == EOF )
      break;
    if( textflag )
    {
      if( c == '\\' )
        fprintf( outputfile, "\\" );
      else if( c == '\t' )
        fprintf( outputfile, "\\t" );
      else if( c == '\n' )
        fprintf( outputfile, "\\n\\\n" );
      else if( c == '\r' )
        fprintf( outputfile, "\\r" );
      else if( c == '\"' )
        fprintf( outputfile, "\\\"" );
      else if( c == '\0' )
        fprintf( outputfile, "\\0" );
      else
        fputc( c, outputfile );
    }
    else
    {
      fprintf( outputfile, "%d,", c );
      if( ++i == COLUMN_COUNT )
      {
        fprintf( outputfile, "\n" );
        i = 0;
      }
    }
  }
  if( textflag )
    fprintf( outputfile, "\";\n\n" );
  else
    fprintf( outputfile, "0\n};\n\n" );
  fprintf( outputfile, "const long %sSize = %lld;\n\n", symbolname, (long long)size );

  fprintf( headerfile, "extern const char %s[];\n\n", symbolname );
  fprintf( headerfile, "extern const long %sSize;\n\n", symbolname, (long long)size );

  fclose( inputfile );
  fclose( outputfile );
  fclose( headerfile );
  return 0;
}



