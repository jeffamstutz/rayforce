
#include <QApplication>
#include <QMutex>
#include <QSize>
#include <QThread>
#include <QWaitCondition>



#include <stdio.h>
#include "mmthreadqt.h"



class mtThreadQT : public QThread
{
 public:
  void *(*threadmain)( void *value );
  void *value;
 protected:
  void run();
};

void mtThreadQT::run()
{
  threadmain( value );
  return;
}


////


void mtThreadCreate( mtThread *thread, void *(*threadmain)( void *value ), void *value, int flags, void *stack, size_t stacksize )
{
  mtThreadQT *qtthread = new mtThreadQT();

  qtthread->threadmain = threadmain;
  qtthread->value = value;
  qtthread->start();
  thread->qtthread = (void *)qtthread;

  return;
}

void mtThreadExit()
{
  /* No idea how to implement */
  return;
}

void mtThreadJoin( mtThread *thread )
{
  mtThreadQT *qtthread = (mtThreadQT *)thread->qtthread;
  for( ; !( qtthread->wait( ULONG_MAX ) ) ; );
  delete qtthread;
  thread->qtthread = 0;
  return;
}


////


void mtMutexInit( mtMutex *mutex )
{
  QMutex *qtmutex = new QMutex();
  mutex->qtmutex = (void *)qtmutex;
  return;
}

void mtMutexDestroy( mtMutex *mutex )
{
  QMutex *qtmutex = (QMutex *)mutex->qtmutex;
  delete qtmutex;
  mutex->qtmutex = 0;
  return;
}

void mtMutexLock( mtMutex *mutex )
{
  QMutex *qtmutex = (QMutex *)mutex->qtmutex;
  qtmutex->lock();
  return;
}

void mtMutexUnlock( mtMutex *mutex )
{
  QMutex *qtmutex = (QMutex *)mutex->qtmutex;
  qtmutex->unlock();
  return;
}

int mtMutexTryLock( mtMutex *mutex )
{
  QMutex *qtmutex = (QMutex *)mutex->qtmutex;
  return qtmutex->tryLock();
}


////


void mtSignalInit( mtSignal *signal )
{
  QWaitCondition *qtcondition = new QWaitCondition();
  signal->qtcondition = (void *)qtcondition;
  return;
}

void mtSignalDestroy( mtSignal *signal )
{
  QWaitCondition *qtcondition = (QWaitCondition *)signal->qtcondition;
  delete qtcondition;
  signal->qtcondition = 0;
  return;
}

void mtSignalWake( mtSignal *signal )
{
  QWaitCondition *qtcondition = (QWaitCondition *)signal->qtcondition;
  qtcondition->wakeOne();
  return;
}

void mtSignalBroadcast( mtSignal *signal )
{
  QWaitCondition *qtcondition = (QWaitCondition *)signal->qtcondition;
  qtcondition->wakeAll();
  return;
}

void mtSignalWait( mtSignal *signal, mtMutex *mutex )
{
  QWaitCondition *qtcondition = (QWaitCondition *)signal->qtcondition;
  for( ; !( qtcondition->wait( (QMutex *)mutex->qtmutex, ULONG_MAX ) ) ; );
  return;
}

void mtSignalWaitTimeout( mtSignal *signal, mtMutex *mutex, long milliseconds )
{
  QWaitCondition *qtcondition = (QWaitCondition *)signal->qtcondition;
  qtcondition->wait( (QMutex *)mutex->qtmutex, milliseconds );
  return;
}


////


void *foobar( void *value )
{
  int i;
  int *a;

  a = (int *)value;

  for( i = 0 ; i < 4 ; i++ )
  {
    printf( "FOO : %d\n", i );
    sleep( 1 );
  }

  return (void *)0;
}



/*
#include <pthread.h>



int main()
{
  int a;
  mtThread thread;

  a = 317;
  mtThreadCreate( &thread, foobar, &a, 0, 0, 0 );


  mtThreadJoin( &thread );


printf( "sizeof(QMutex) = %d\n", (int)sizeof(QMutex) );
printf( "%d + %d\n", (int)sizeof(pthread_mutex_t), (int)sizeof(pthread_cond_t) );


  return 1;
}

*/


