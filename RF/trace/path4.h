/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*
#ifndef TRACE_DEBUG
 #define TRACE_DEBUG (0)
#endif
*/

#if RF_ADDRBITS == 16
 #define RF_SECTOR(x) ((const rfSector16 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode16 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK16_SHIFT)
 #define RF_TRILIST RF_SECTOR16_TRILIST
 #define RF_TRILIST_TYPE int16_t
#elif RF_ADDRBITS == 32
 #define RF_SECTOR(x) ((const rfSector32 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode32 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK32_SHIFT)
 #define RF_TRILIST RF_SECTOR32_TRILIST
 #define RF_TRILIST_TYPE int32_t
#else
 #define RF_SECTOR(x) ((const rfSector64 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode64 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK64_SHIFT)
 #define RF_TRILIST RF_SECTOR64_TRILIST
 #define RF_TRILIST_TYPE int64_t
#endif

#if RF_PACKETWIDTH == 4
 #if TRACEPARAMPASSBYREFERENCE
  #ifdef TRACERAYSTORE
   #define RF_DECLPARAM TRACEPARAM *param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM &param, &raystore
  #else
   #define RF_DECLPARAM TRACEPARAM *param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM &param
  #endif
 #else
  #ifdef TRACERAYSTORE
   #define RF_DECLPARAM TRACEPARAM param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM param, &raystore
  #else
   #define RF_DECLPARAM TRACEPARAM param
   #define RF_PASSPARAM param
  #endif
 #endif
#endif


#include "pathpart.h"


#if RF_PACKETWIDTH > 4
static void RF_TRACE4NAME( const rfHandle * RF_RESTRICT handle, RF_DECLPARAM, const RF_TRACEPACKET * RF_RESTRICT packet, int packetoffset, RF_TRACERESULT * RF_RESTRICT result )
#else
static void RF_TRACE4NAME( const rfHandle * RF_RESTRICT handle, TRACEPARAM param, const RF_TRACEPACKET * RF_RESTRICT packet )
#endif
{
  int32_t rayindex;
  void *rayroot;
  rfRoot rootoffset;
#ifdef TRACERAYSTORE
  TRACERAYSTORE raystore;
#endif
#if TRACERESOLVEROOT || TRACEDISPLACEROOT || TRACEQUADROOT
  rff rayorigin[3];
#endif
#if RF_PACKETWIDTH == 4
  RF_TRACERESULT result;
 #define RF_RESULT (&result)
#else
 #define RF_RESULT (result)
#endif
#if RF_PACKETWIDTH > 4
 #define RF_PACKETOFFSET (packetoffset)
#else
 #define RF_PACKETOFFSET (0)
#endif
#if TRACEHITMODELDATA
  void *modeldata;
#endif

#if TRACEHITMODELDATA
  modeldata = RF_ADDRESS( handle->objectgraph, ((const rfGraphHeader * RF_RESTRICT)handle->objectgraph)->modeldataoffset );
#endif

#if RF_PACKETWIDTH == 4
  RF_RESULT->hitmask = 0;
 #if TRACEHITSIDE
  RF_RESULT->hitsidemask = 0;
 #endif
#endif

  for( rayindex = RF_PACKETOFFSET ; rayindex < RF_PACKETOFFSET+4 ; rayindex++ )
  {
    if( !( packet->raymask & ( 1 << rayindex ) ) )
      continue;
#if TRACESINGLEROOT
    rootoffset = packet->root[0];
#else
    rootoffset = packet->root[rayindex];
#endif

#if TRACETESTROOT
    if( !( rootoffset ) )
      continue;
#endif

#if TRACERESOLVEROOT || TRACEDISPLACEROOT || TRACEQUADROOT
    rayorigin[0] = packet->origin[rayindex+(0*RF_PACKETWIDTH)];
    rayorigin[1] = packet->origin[rayindex+(1*RF_PACKETWIDTH)];
    rayorigin[2] = packet->origin[rayindex+(2*RF_PACKETWIDTH)];
#endif

#if TRACERESOLVEROOT
    rayroot = RF_RESOLVENAME( rayorigin );
#elif TRACEDISPLACEROOT
    rayroot = RF_DISPLACENAME( RF_ADDRESS( handle->objectgraph, rootoffset ), rayorigin );
#elif TRACEQUADROOT
    if( !( rootoffset ) )
      continue;
/* TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO */
 #if 0
    if( ( rootoffset & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_PRIMITIVE )
    {
      /* TODO TODO: Intersect triangle!!! */

      continue;
    }
 #endif
/* TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO */
    rayroot = RF_ADDRESS( handle->objectgraph, rootoffset & ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK ) );
    if( ( rootoffset & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE )
      rayroot = RF_DISPLACENAME( rayroot, rayorigin );
#else
    rayroot = RF_ADDRESS( handle->objectgraph, rootoffset );
#endif

    RF_RESULT->raysrc[rayindex+(0*RF_PACKETWIDTH)] = packet->origin[rayindex+(0*RF_PACKETWIDTH)];
    RF_RESULT->raysrc[rayindex+(1*RF_PACKETWIDTH)] = packet->origin[rayindex+(1*RF_PACKETWIDTH)];
    RF_RESULT->raysrc[rayindex+(2*RF_PACKETWIDTH)] = packet->origin[rayindex+(2*RF_PACKETWIDTH)];
    RF_RESULT->vectinv[rayindex+(0*RF_PACKETWIDTH)] = 1.0 / packet->vector[rayindex+(0*RF_PACKETWIDTH)];
    RF_RESULT->vectinv[rayindex+(1*RF_PACKETWIDTH)] = 1.0 / packet->vector[rayindex+(1*RF_PACKETWIDTH)];
    RF_RESULT->vectinv[rayindex+(2*RF_PACKETWIDTH)] = 1.0 / packet->vector[rayindex+(2*RF_PACKETWIDTH)];
#ifdef TRACEDISTCLIP
    RF_RESULT->clipdist[rayindex] = packet->clipdist[rayindex];
#endif

    RF_TRACE1NAME( handle, RF_PASSPARAM, packet, rayindex, RF_RESULT, rayroot, 1 );
  }

#if TRACEHITPLANE
  for( rayindex = RF_PACKETOFFSET ; rayindex < RF_PACKETOFFSET+4 ; rayindex++ )
  {
    rfTri *tri;
    if( ( RF_RESULT->hitmask >> rayindex ) & 0x1 )
    {
      tri = RF_RESULT->hittri[rayindex];
 #if !TRACEHITPLANEPACKED
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+0] = tri->plane[0];
      RF_RESULT->hitplane[rayindex+(1*RF_PACKETWIDTH)+0] = tri->plane[1];
      RF_RESULT->hitplane[rayindex+(2*RF_PACKETWIDTH)+0] = tri->plane[2];
      RF_RESULT->hitplane[rayindex+(3*RF_PACKETWIDTH)+0] = tri->plane[3];
 #else
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+0] = tri->plane[0];
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+1] = tri->plane[1];
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+2] = tri->plane[2];
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+3] = tri->plane[3];
 #endif
    }
    else
    {
 #if !TRACEHITPLANEPACKED
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+0] = 0.0;
      RF_RESULT->hitplane[rayindex+(1*RF_PACKETWIDTH)+0] = 0.0;
      RF_RESULT->hitplane[rayindex+(2*RF_PACKETWIDTH)+0] = 0.0;
      RF_RESULT->hitplane[rayindex+(3*RF_PACKETWIDTH)+0] = 0.0;
 #else
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+0] = 0.0;
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+1] = 0.0;
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+2] = 0.0;
      RF_RESULT->hitplane[rayindex+(0*RF_PACKETWIDTH)+3] = 0.0;
 #endif
    }
  }
#endif

#if RF_PACKETWIDTH == 4

 #if TRACEHITMODELDATA
  RF_RESULT->modeldata[0] = modeldata;
  RF_RESULT->modeldata[1] = modeldata;
  RF_RESULT->modeldata[2] = modeldata;
  RF_RESULT->modeldata[3] = modeldata;
 #endif
 #if TRACEHITOBJECTDATA
  RF_RESULT->objectdata[0] = handle.objectdata;
  RF_RESULT->objectdata[1] = handle.objectdata;
  RF_RESULT->objectdata[2] = handle.objectdata;
  RF_RESULT->objectdata[3] = handle.objectdata;
 #endif
 #if TRACEHITDATA
  RF_RESULT->hittri[0] = RF_ADDRESS( RF_RESULT->hittri[0], sizeof(rfTri) );
  RF_RESULT->hittri[1] = RF_ADDRESS( RF_RESULT->hittri[1], sizeof(rfTri) );
  RF_RESULT->hittri[2] = RF_ADDRESS( RF_RESULT->hittri[2], sizeof(rfTri) );
  RF_RESULT->hittri[3] = RF_ADDRESS( RF_RESULT->hittri[3], sizeof(rfTri) );
 #endif

  TRACEHIT( RF_PASSPARAM, RF_RESULT->hitmask,
 #if TRACEHITMODELDATA
    RF_RESULT->modeldata,
 #else
    0,
 #endif
 #if TRACEHITOBJECTDATA
    RF_RESULT->objectdata,
 #else
    0,
 #endif
 #if TRACEHITVECTOR
    packet->vector,
 #else
    0,
 #endif
 #if TRACEHITPOINT
    RF_RESULT->hitpt,
 #else
    0,
 #endif
 #if TRACEHITUV
    RF_RESULT->hituv,
 #else
    0,
 #endif
 #if TRACEHITDATA
    RF_RESULT->hitdata,
 #else
    0,
 #endif
 #if TRACEHITDIST
    RF_RESULT->hitdist,
 #else
    0,
 #endif
 #if TRACEHITPLANE
    RF_RESULT->hitplane,
 #else
    0,
 #endif
 #if TRACEHITSIDE
    RF_RESULT->hitside,
 #else
    0,
 #endif
 #if TRACEHITSIDE
    RF_RESULT->root,
 #else
    0,
 #endif
 #if TRACEHITSIDE
    RF_RESULT->matrix
 #else
    0
 #endif
  );
 #ifdef TRACEEND
  TRACEEND( RF_PASSPARAM, packet->vector );
 #endif
#endif

  return;
}


/* Standard internally defined */
#undef RF_SECTOR
#undef RF_NODE
#undef RF_LINKSHIFT
#undef RF_TRILIST
#undef RF_TRILIST_TYPE

/* Special internally defined */
#undef RF_RESULT
#undef RF_PACKETOFFSET

/* Externally defined */
#if RF_PACKETWIDTH == 4
 #undef RF_CULLMODE
 #undef RF_ADDRBITS
 #undef RF_TRACE1NAME
 #undef RF_TRACE4NAME
 #undef RF_RESOLVENAME
 #undef RF_RESOLVE4NAME
 #undef RF_DISPLACENAME
 #undef RF_DISPLACE4NAME

 #undef RF_DECLPARAM
 #undef RF_PASSPARAM
 #undef RF_TRACEPACKET
 #undef RF_TRACERESULT
 #undef RF_PACKETWIDTH
#endif

