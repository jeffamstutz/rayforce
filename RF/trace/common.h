/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#ifndef _RF_TRACECOMMON_INTERNALS
 #define _RF_TRACECOMMON_INTERNALS

RF_DECL_ATTRIB static inline int rfResolveOriginIndex( rfOriginTable *origintable, const rff *origin )
{
  int x, y, z;

  if( origin[RF_AXIS_X] < origintable->graphedge[RF_EDGE_MINX] )
    x = 0;
  else if( origin[RF_AXIS_X] > origintable->graphedge[RF_EDGE_MAXX] )
    x = origintable->max;
  else
  {
    x = (int)rffloor( origintable->spacinginv[RF_AXIS_X] * ( origin[RF_AXIS_X] - origintable->edge[RF_EDGE_MINX] ) );
    if( x < 0 )
      x = 0;
    else if( x > origintable->max )
      x = origintable->max;
  }

  if( origin[RF_AXIS_Y] < origintable->graphedge[RF_EDGE_MINY] )
    y = 0;
  else if( origin[RF_AXIS_Y] > origintable->graphedge[RF_EDGE_MAXY] )
    y = origintable->max;
  else
  {
    y = (int)rffloor( origintable->spacinginv[RF_AXIS_Y] * ( origin[RF_AXIS_Y] - origintable->edge[RF_EDGE_MINY] ) );
    if( y < 0 )
      y = 0;
    else if( y > origintable->max )
      y = origintable->max;
  }

  if( origin[RF_AXIS_Z] < origintable->graphedge[RF_EDGE_MINZ] )
    z = 0;
  else if( origin[RF_AXIS_Z] > origintable->graphedge[RF_EDGE_MAXZ] )
    z = origintable->max;
  else
  {
    z = (int)rffloor( origintable->spacinginv[RF_AXIS_Z] * ( origin[RF_AXIS_Z] - origintable->edge[RF_EDGE_MINZ] ) );
    if( z < 0 )
      z = 0;
    else if( z > origintable->max )
      z = origintable->max;
  }

  return ( x + ( y * origintable->factory ) + ( z * origintable->factorz ) );
}


 #if RFTRACE__SSE__
  #define RF_SSE_EXTRACT_F0(v) _mm_cvtss_f32(v)
  #if RFTRACE__SSE3__
   #define RF_SSE_EXTRACT_F1(v) _mm_cvtss_f32( _mm_movehdup_ps( v ) )
  #else
   #define RF_SSE_EXTRACT_F1(v) _mm_cvtss_f32( _mm_shuffle_ps( v, v, 0x55 ) )
  #endif
  #define RF_SSE_EXTRACT_F2(v) _mm_cvtss_f32( _mm_movehl_ps( v, v ) )
  #define RF_SSE_EXTRACT_F3(v) _mm_cvtss_f32( _mm_shuffle_ps( v, v, 0xff ) )
 #endif


#endif

