/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#if RF_ADDRBITS == 16
 #define RF_SECTOR(x) ((const rfSector16 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode16 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK16_SHIFT)
 #define RF_TRILIST RF_SECTOR16_TRILIST
 #define RF_TRILIST_TYPE int16_t
#elif RF_ADDRBITS == 32
 #define RF_SECTOR(x) ((const rfSector32 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode32 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK32_SHIFT)
 #define RF_TRILIST RF_SECTOR32_TRILIST
 #define RF_TRILIST_TYPE int32_t
#else
 #define RF_SECTOR(x) ((const rfSector64 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode64 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK64_SHIFT)
 #define RF_TRILIST RF_SECTOR64_TRILIST
 #define RF_TRILIST_TYPE int64_t
#endif

#if TRACEPARAMPASSBYREFERENCE
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM &param, &raystore
 #else
  #define RF_PASSPARAM &param
 #endif
#else
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM param, &raystore
 #else
  #define RF_PASSPARAM param
 #endif
#endif


////


#define RF_ELEM_PREPARE_AXIS(axis) \
  edgeindex[axis] = ( axis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN; \
  vectinv[axis] = -RFF_MAX; \
  if( rffabs( vector[axis] ) > (rff)0.0 ) \
  { \
    vectinv[axis] = (rff)1.0 / vector[axis]; \
    if( vector[axis] >= (rff)0.0 ) \
      edgeindex[axis] = ( axis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX; \
  }


#ifdef TRACEMULTISAFECOUNT
 #define RF_HITTABLE_SIZE ((TRACEMULTISAFECOUNT+31)/32)
#else
 #define RF_HITTABLE_SIZE (4)
#endif


#ifndef _RF_TRACEMULTIPATH_INTERNALS
 #define _RF_TRACEMULTIPATH_INTERNALS

 #define RF_HIT_STACK_SIZE (16)

 #define RF_PATHFLAGS_RESTART (0x1)
 #define RF_PATHFLAGS_HITTABLE (0x2)

 #define RF_HITTABLE_BITSIZE (RF_HITTABLE_SIZE<<5)

#endif


/*
static void RF_TRACENAME( rfHandle *handle, TRACEPARAM param, rfRoot rootoffset, rff *origin, rff *vector
#if TRACEDISTCLIP
, rff clipdist
#endif
)
*/

static void RF_TRACENAME( const rfHandle * RF_RESTRICT handle, TRACEPARAM param, const rfRaySingle * RF_RESTRICT ray )
{
  int nredge, triindex;
  rfssize slink;
  rff vector[3];
  rff RF_ALIGN16 vectinv[4];
  int edgeindex[3];
  rff src[3], dst[3], dist, mindist;
  void *root;
  int axisindex;
  rfTri *tri;
  void *modeldata;
#if TRACEDISTCLIP
  rff clipdist;
#endif
#ifdef TRACERAYSTORE
  TRACERAYSTORE raystore;
#endif
  /* Multi-hit storage */
  int hitindex, hitloopindex;
  int pathflags;
  int hitlist;
  RF_HITSTRUCTNAME *hit, *hitfind;
  RF_HITSTRUCTNAME hitstack[RF_HIT_STACK_SIZE];
  uint32_t hittable[RF_HITTABLE_SIZE];
  /* Multi-hit storage */

  modeldata = RF_ADDRESS( handle->objectgraph, ((const rfGraphHeader * RF_RESTRICT)handle->objectgraph)->modeldataoffset );

  vector[0] = ray->vector[0];
  vector[1] = ray->vector[1];
  vector[2] = ray->vector[2];

#if TRACERESOLVEROOT
  root = RF_RESOLVENAME( handle->objectgraph, ray->origin );
#elif TRACEDISPLACEROOT
  root = RF_DISPLACENAME( RF_ADDRESS( handle->objectgraph, ray->root ), ray->origin );
#elif TRACEQUADROOT
  if( ray->root )
  {
    root = RF_ADDRESS( handle->objectgraph, ray->root & ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK ) );
    if( ( ray->root & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE )
      root = RF_DISPLACENAME( root, ray->origin );
#else
  root = RF_ADDRESS( handle->objectgraph, ray->root );
#endif

#if TRACEDISTCLIP
  clipdist = ray->clipdist;
#endif

  src[0] = ray->origin[0];
  src[1] = ray->origin[1];
  src[2] = ray->origin[2];

#if RFTRACE__SSE__
  int vectmask;
  static const float RF_ALIGN16 vect3dbase[4] = { 0.0, 0.0, 0.0, 1.0 };
  __m128 vvectinv, vvector;
  vvector = _mm_load_ps( ray->vector );
  vvectinv = _mm_div_ps( _mm_set1_ps( 1.0 ), _mm_or_ps( vvector, _mm_load_ps( vect3dbase ) ) );
  vectmask = ~_mm_movemask_ps( vvector );
  edgeindex[0] = ( RF_AXIS_X << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 0 ) & 0x1 );
  edgeindex[1] = ( RF_AXIS_Y << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 1 ) & 0x1 );
  edgeindex[2] = ( RF_AXIS_Z << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 2 ) & 0x1 );
  _mm_store_ps( vectinv, vvectinv );
#else
  RF_ELEM_PREPARE_AXIS( 0 );
  RF_ELEM_PREPARE_AXIS( 1 );
  RF_ELEM_PREPARE_AXIS( 2 );
#endif

  for( ; ; )
  {
    /* Sector traversal */
    int tricount;

    nredge = edgeindex[0];
    mindist = ( RF_SECTOR(root)->edge[ edgeindex[0] ] - src[0] ) * vectinv[0];
    dist = ( RF_SECTOR(root)->edge[ edgeindex[1] ] - src[1] ) * vectinv[1];
    if( dist < mindist )
    {
      nredge = edgeindex[1];
      mindist = dist;
    }
    dist = ( RF_SECTOR(root)->edge[ edgeindex[2] ] - src[2] ) * vectinv[2];
    if( dist < mindist )
    {
      nredge = edgeindex[2];
      mindist = dist;
    }

    dst[0] = src[0] + ( mindist * vector[0] );
    dst[1] = src[1] + ( mindist * vector[1] );
    dst[2] = src[2] + ( mindist * vector[2] );

    tricount = RF_SECTOR_GET_PRIMCOUNT( RF_SECTOR(root) );
    if( tricount )
    {
      RF_TRILIST_TYPE *trilist;

      pathflags = 0;
      for( ; ; )
      {
        hitlist = -1;
        hitindex = 0;

        trilist = RF_TRILIST( RF_SECTOR(root) );
        for( triindex = 0 ; triindex < tricount ; triindex++ )
        {
          rff dstdist, srcdist, uv[2], f, vray[3];
          rff tridist;

          if( pathflags & RF_PATHFLAGS_HITTABLE )
          {
            /* Check if we have already processed that triangle */
            if( triindex >= RF_HITTABLE_BITSIZE )
              continue;
            if( hittable[ triindex >> 5 ] & ( 1 << ( triindex & 0x1f ) ) )
              continue;
          }

          tri = (rfTri *)RF_ADDRESS( root, (rfssize)trilist[triindex] << RF_LINKSHIFT );

#if RF_CULLMODE == 0
          dstdist = rfMathPlanePoint( tri->plane, dst );
          if( dstdist <= (rff)0.0 )
            continue;
          srcdist = rfMathPlanePoint( tri->plane, src );
          if( srcdist > (rff)0.0 )
            continue;
          f = dstdist - srcdist;
          vray[0] = ( src[0] * dstdist ) - ( dst[0] * srcdist );
          vray[1] = ( src[1] * dstdist ) - ( dst[1] * srcdist );
          vray[2] = ( src[2] * dstdist ) - ( dst[2] * srcdist );
          uv[0] = rfMathVectorDotProduct( &tri->edpu[0], vray ) + ( tri->edpu[3] * f );
          if( ( uv[0] < (rff)0.0 ) || ( uv[0] > f ) )
            continue;
          uv[1] = rfMathVectorDotProduct( &tri->edpv[0], vray ) + ( tri->edpv[3] * f );
          if( ( uv[1] < (rff)0.0 ) || ( ( uv[0] + uv[1] ) > f ) )
            continue;
          f = (rff)1.0 / f;
          axisindex = nredge >> 1;
 #if TRACEHITDIST
          tridist = ( ( vray[axisindex] * f ) - ray->origin[axisindex] ) * vectinv[axisindex];
 #else
          tridist = ( ( vray[axisindex] * f ) - src[axisindex] ) * vectinv[axisindex];
 #endif
 #if TRACEDISTCLIP
          if( tridist > clipdist )
            continue;
 #endif
          hit = &hitstack[ hitindex ];
 #if TRACEHITPOINT
          hit->pt[0] = vray[0] * f;
          hit->pt[1] = vray[1] * f;
          hit->pt[2] = vray[2] * f;
 #endif
 #if TRACEHITUV
          hit->uv[0] = uv[0] * f;
          hit->uv[1] = uv[1] * f;
 #endif
          hit->side = 0;
#elif RF_CULLMODE == 1
          dstdist = rfMathPlanePoint( tri->plane, dst );
          if( dstdist > (rff)0.0 )
            continue;
          srcdist = rfMathPlanePoint( tri->plane, src );
          if( srcdist <= (rff)0.0 )
            continue;
          f = dstdist - srcdist;
          vray[0] = ( src[0] * dstdist ) - ( dst[0] * srcdist );
          vray[1] = ( src[1] * dstdist ) - ( dst[1] * srcdist );
          vray[2] = ( src[2] * dstdist ) - ( dst[2] * srcdist );
          uv[0] = rfMathVectorDotProduct( &tri->edpu[0], vray ) + ( tri->edpu[3] * f );
          if( ( uv[0] > (rff)0.0 ) || ( uv[0] < f ) )
            continue;
          uv[1] = rfMathVectorDotProduct( &tri->edpv[0], vray ) + ( tri->edpv[3] * f );
          if( ( uv[1] > (rff)0.0 ) || ( ( uv[0] + uv[1] ) < f ) )
            continue;
          f = (rff)1.0 / f;
          axisindex = nredge >> 1;
 #if TRACEHITDIST
          tridist = ( ( vray[axisindex] * f ) - ray->origin[axisindex] ) * vectinv[axisindex];
 #else
          tridist = ( ( vray[axisindex] * f ) - src[axisindex] ) * vectinv[axisindex];
 #endif
 #if TRACEDISTCLIP
          if( tridist > clipdist )
            continue;
 #endif
          hit = &hitstack[ hitindex ];
 #if TRACEHITPOINT
          hit->pt[0] = vray[0] * f;
          hit->pt[1] = vray[1] * f;
          hit->pt[2] = vray[2] * f;
 #endif
 #if TRACEHITUV
          hit->uv[0] = uv[0] * f;
          hit->uv[1] = uv[1] * f;
 #endif
          hit->side = 1;
#else
          dstdist = rfMathPlanePoint( tri->plane, dst );
          srcdist = rfMathPlanePoint( tri->plane, src );
          if( dstdist * srcdist > (rff)0.0 )
            continue;
          f = srcdist / ( srcdist - dstdist );
          vray[0] = src[0] + f * ( dst[0] - src[0] );
          vray[1] = src[1] + f * ( dst[1] - src[1] );
          vray[2] = src[2] + f * ( dst[2] - src[2] );
          uv[0] = ( rfMathVectorDotProduct( &tri->edpu[0], vray ) + tri->edpu[3] );
          if( !( uv[0] >= (rff)0.0 ) || ( uv[0] > (rff)1.0 ) )
            continue;
          uv[1] = ( rfMathVectorDotProduct( &tri->edpv[0], vray ) + tri->edpv[3] );
          if( ( uv[1] < (rff)0.0 ) || ( ( uv[0] + uv[1] ) > (rff)1.0 ) )
            continue;
          axisindex = nredge >> 1;
 #if TRACEHITDIST
          tridist = ( vray[axisindex] - ray->origin[axisindex] ) * vectinv[axisindex];
 #else
          tridist = ( vray[axisindex] - src[axisindex] ) * vectinv[axisindex];
 #endif
 #if TRACEDISTCLIP
          if( tridist > clipdist )
            continue;
 #endif
          hit = &hitstack[ hitindex ];
 #if TRACEHITPOINT
          hit->pt[0] = vray[0];
          hit->pt[1] = vray[1];
          hit->pt[2] = vray[2];
 #endif
 #if TRACEHITUV
          hit->uv[0] = uv[0];
          hit->uv[1] = uv[1];
 #endif
          hit->side = ( srcdist < (rff)0.0 );
#endif
          hit->hitdist = tridist;
          hit->triindex = triindex;

          /* Insert sort in list by insertion sort */
          int *hitprev;
          for( hitprev = &hitlist ; ; hitprev = &hitfind->next )
          {
            hitfind = &hitstack[ *hitprev ];
            if( ( *hitprev >= 0 ) && ( hit->hitdist > hitfind->hitdist ) )
              continue;
            hit->next = *hitprev;
            *hitprev = hitindex;
            break;
          }

          if( !( pathflags & RF_PATHFLAGS_RESTART ) && ( ++hitindex < RF_HIT_STACK_SIZE ) )
            hit++;
          else
          {
            /* Hit stack is full */
            /* Flush out the last entry and set the hit pointer to it */
            for( ; ; )
            {
              hitindex = *hitprev;
              hit = &hitstack[ hitindex ];
              if( hit->next == -1 )
                break;
              hitprev = &hit->next;
            }
            *hitprev = -1;
            /* Set triangle hit restart flag, to indicate we have flushed results */
            pathflags |= RF_PATHFLAGS_RESTART;
          }
        }

        if( ( pathflags & ( RF_PATHFLAGS_RESTART | RF_PATHFLAGS_HITTABLE ) ) == RF_PATHFLAGS_RESTART )
        {
          /* Initialize the table of flags of triangles intersected */
#if RF_HITTABLE_SIZE <= 6
 #if RF_HITTABLE_SIZE > 0
          hittable[0] = 0x0;
 #endif
 #if RF_HITTABLE_SIZE > 1
          hittable[1] = 0x0;
 #endif
 #if RF_HITTABLE_SIZE > 2
          hittable[2] = 0x0;
 #endif
 #if RF_HITTABLE_SIZE > 3
          hittable[3] = 0x0;
 #endif
 #if RF_HITTABLE_SIZE > 4
          hittable[4] = 0x0;
 #endif
 #if RF_HITTABLE_SIZE > 5
          hittable[5] = 0x0;
 #endif
#elif RF_HITTABLE_SIZE <= 10
          int hittableindex;
          for( hittableindex = 0 ; hittableindex < RF_HITTABLE_SIZE ; hittableindex++ )
            hittable[hittableindex] = 0x0;
#else
          int hittableindex, hittablecount;
          hittablecount = ( tricount + 31 ) >> 5;
          if( hittablecount > RF_HITTABLE_SIZE )
            hittablecount = RF_HITTABLE_SIZE;
          for( hittableindex = 0 ; hittableindex < hittablecount ; hittableindex++ )
            hittable[hittableindex] = 0x0;
#endif
          pathflags |= RF_PATHFLAGS_HITTABLE;
        }

        /* Process all sorted hits in loop */
        trilist = RF_TRILIST( RF_SECTOR(root) );
        for( hitloopindex = hitlist ; hitloopindex != -1 ; hitloopindex = hit->next )
        {
          hit = &hitstack[ hitloopindex ];
          tri = (rfTri *)RF_ADDRESS( root, (rfssize)trilist[ hit->triindex ] << RF_LINKSHIFT );
 #if TRACEHITPOINT
  #if TRACEHITUV
          if( !( TRACEHIT( RF_PASSPARAM, modeldata, handle->objectdata, vector, hit->pt, hit->uv, RF_ADDRESS( tri, sizeof(rfTri) ), hit->hitdist, tri->plane, hit->side, RF_ADDRESSDIFF( root, handle->objectgraph ), 0 ) ) )
            goto done;
  #else
          if( !( TRACEHIT( RF_PASSPARAM, modeldata, handle->objectdata, vector, hit->pt, 0, RF_ADDRESS( tri, sizeof(rfTri) ), hit->hitdist, tri->plane, hit->side, RF_ADDRESSDIFF( root, handle->objectgraph ), 0 ) ) )
            goto done;
  #endif
 #else
  #if TRACEHITUV
          if( !( TRACEHIT( RF_PASSPARAM, modeldata, handle->objectdata, vector, 0, hit->uv, RF_ADDRESS( tri, sizeof(rfTri) ), hit->hitdist, tri->plane, hit->side, RF_ADDRESSDIFF( root, handle->objectgraph ), 0 ) ) )
            goto done;
  #else
          if( !( TRACEHIT( RF_PASSPARAM, modeldata, handle->objectdata, vector, 0, 0, RF_ADDRESS( tri, sizeof(rfTri) ), hit->hitdist, tri->plane, hit->side, RF_ADDRESSDIFF( root, handle->objectgraph ), 0 ) ) )
            goto done;
  #endif
 #endif
          if( pathflags & RF_PATHFLAGS_RESTART )
          {
            /* Set bitflag for triangle */
            if( hit->triindex < RF_HITTABLE_BITSIZE )
              hittable[ hit->triindex >> 5 ] |= 1 << ( hit->triindex & 0x1f );
          }
        }

        if( !( pathflags & RF_PATHFLAGS_RESTART ) )
          break;
        pathflags &= ~RF_PATHFLAGS_RESTART;
      }
    }
    src[0] = dst[0];
    src[1] = dst[1];
    src[2] = dst[2];

#if TRACEDISTCLIP
    if( mindist >= clipdist )
    {
 #ifdef TRACECLIP
      TRACECLIP( RF_PASSPARAM, vector, RF_ADDRESSDIFF( root, handle->objectgraph ) );
 #endif
      break;
    }
#endif

    /* Traverse through the sector's edge */
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) ) )
    {
      /* Neighbor is node */
#if TRACEDISTCLIP
      clipdist -= mindist;
#endif
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );
    }
    else
    {
      /* Neighbor is sector */
      slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
      continue;
    }

    /* Node traversal */
    for( ; ; )
    {
#if 0
      int linkflags, nodeside;
      linkflags = RF_NODE(root)->flags;
      nodeside = RF_NODE_MORE;
      if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
        nodeside = RF_NODE_LESS;
      root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
      if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
        break;
#else
      int linkflags;
      linkflags = RF_NODE(root)->flags;
      if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
#endif

    }
  }

#if TRACEQUADROOT
  }
#endif

  done:

#ifdef TRACEEND
  TRACEEND( RF_PASSPARAM, vector );
#endif

  return;
}


#undef RF_SECTOR
#undef RF_NODE
#undef RF_LINKSHIFT
#undef RF_TRILIST
#undef RF_TRILIST_TYPE
#undef RF_PASSPARAM
#undef RF_ELEM_PREPARE_AXIS
#undef RF_HITTABLE_SIZE


#undef RF_CULLMODE
#undef RF_ADDRBITS
#undef RF_TRACENAME
#undef RF_RESOLVENAME
#undef RF_DISPLACENAME

