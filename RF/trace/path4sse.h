/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*
#ifndef TRACE_DEBUG
 #define TRACE_DEBUG (0)
#endif
*/

#if RF_ADDRBITS == 16
 #define RF_SECTOR(x) ((const rfSector16 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode16 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK16_SHIFT)
 #define RF_TRILIST RF_SECTOR16_TRILIST
 #define RF_TRILIST_TYPE int16_t
#elif RF_ADDRBITS == 32
 #define RF_SECTOR(x) ((const rfSector32 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode32 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK32_SHIFT)
 #define RF_TRILIST RF_SECTOR32_TRILIST
 #define RF_TRILIST_TYPE int32_t
#else
 #define RF_SECTOR(x) ((const rfSector64 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode64 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK64_SHIFT)
 #define RF_TRILIST RF_SECTOR64_TRILIST
 #define RF_TRILIST_TYPE int64_t
#endif

#if RF_PACKETWIDTH == 4
 #if TRACEPARAMPASSBYREFERENCE
  #ifdef TRACERAYSTORE
   #define RF_DECLPARAM TRACEPARAM *param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM &param, &raystore
  #else
   #define RF_DECLPARAM TRACEPARAM *param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM &param
  #endif
 #else
  #ifdef TRACERAYSTORE
   #define RF_DECLPARAM TRACEPARAM param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM param, &raystore
  #else
   #define RF_DECLPARAM TRACEPARAM param
   #define RF_PASSPARAM param
  #endif
 #endif
#endif


#if RFTRACE__SSE4_1__
 #include "pathpartsse.h"
#else
 #include "pathpart.h"
#endif


#if RF_PACKETWIDTH > 4
static void RF_TRACE4NAME( const rfHandle * RF_RESTRICT handle, RF_DECLPARAM, const RF_TRACEPACKET * RF_RESTRICT packet, int packetoffset, RF_TRACERESULT * RF_RESTRICT result )
#else
static void RF_TRACE4NAME( const rfHandle * RF_RESTRICT handle, TRACEPARAM param, const RF_TRACEPACKET * RF_RESTRICT packet )
#endif
{
  int32_t nredge;
  int32_t donemask;
  rfssize slink;
  int32_t raymask, raymaskinv, vecflag;
  __m128 vsrc[3], vdst[3];
  __m128 vunit;
  __m128 vector[3];
  __m128 vectinv[3];
  __m128 dstdist, srcdist, vsum;
  __m128 vray[3], utd, vtd;
  __m128 pl0, pl1, pl2, pl3;
#if RF_CPUSLOW_SHR
  static const int32_t maskcount[16] = { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };
#endif
  int32_t edgeindex[4];
  int32_t edgevmask[3];
  void *root;
#if TRACEDISTCLIP || TRACEHITDIST
  int32_t axisindex;
#endif
#ifdef TRACERAYSTORE
  TRACERAYSTORE raystore;
#endif
#if TRACEDISTCLIP
  int32_t clipmask;
  __m128 vclipdist;
#endif
#if !TRACERESOLVEROOT
  rfRoot rootoffset;
#endif
#if TRACEHITROOT
  rfRoot hitrootoffset;
#endif
#if TRACEHITDIST
  __m128 vhitdist;
#endif
#if TRACEHITSIDE
  __m128 vhitside;
  int32_t hitsidemask;
#endif
 #define F 0x0
 #define T 0xffffffff
  static const uint32_t ortable[16*4] RF_ALIGN16 =
  {
    F,F,F,F,
    T,F,F,F,
    F,T,F,F,
    T,T,F,F,
    F,F,T,F,
    T,F,T,F,
    F,T,T,F,
    T,T,T,F,
    F,F,F,T,
    T,F,F,T,
    F,T,F,T,
    T,T,F,T,
    F,F,T,T,
    T,F,T,T,
    F,T,T,T,
    T,T,T,T,
  };
 #undef F
 #undef T
#if RF_PACKETWIDTH == 4
  RF_TRACERESULT result;
 #define RF_RESULT (&result)
#else
 #define RF_RESULT (result)
#endif
#if RF_PACKETWIDTH > 4
 #define RF_PACKETOFFSET (packetoffset)
#else
 #define RF_PACKETOFFSET (0)
#endif

  rff rayorigin[3];

#if TRACEQUADROOT
  int displaceflag;
#endif
  void *incoherentroot[4];
#if TRACEHITMODELDATA
  void *modeldata;
#endif

#if TRACEHITMODELDATA
  modeldata = RF_ADDRESS( handle->objectgraph, ((const rfGraphHeader * RF_RESTRICT)handle->objectgraph)->modeldataoffset );
#endif

  raymask = ( packet->raymask >> RF_PACKETOFFSET ) & 0xf;
  donemask = 0x0;

  vunit = _mm_set1_ps( 1.0 );

  vsrc[0] = _mm_load_ps( &packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)] );
  vsrc[1] = _mm_load_ps( &packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)] );
  vsrc[2] = _mm_load_ps( &packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)] );

  vector[0] = _mm_load_ps( &packet->vector[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)] );
  vector[1] = _mm_load_ps( &packet->vector[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)] );
  vector[2] = _mm_load_ps( &packet->vector[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)] );

#if RF_PACKETWIDTH == 4
  RF_RESULT->hitmask = 0;
 #if TRACEHITMODELDATA
 #endif
 #if TRACEHITOBJECTDATA
 #endif
 #if TRACEHITVECTOR
 #endif
 #if TRACEHITPOINT
  _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vunit );
  _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vunit );
  _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], vunit );
 #endif
 #if TRACEHITUV
  _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vunit );
  _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vunit );
 #endif
 #if TRACEHITDATA
 #endif
 #if TRACEHITDIST
  _mm_store_ps( &RF_RESULT->hitdist[RF_PACKETOFFSET], vunit );
 #endif
 #if TRACEHITPLANE
 #endif
 #if TRACEHITSIDE
  RF_RESULT->hitsidemask = 0;
 #endif
 #if TRACEHITROOT
 #endif
 #if TRACEHITMATRIX
 #endif
#endif

#if TRACEDISTCLIP
  vclipdist = _mm_load_ps( &packet->clipdist[RF_PACKETOFFSET] );
#endif

  raymaskinv = raymask ^ 0xF;
  vecflag = 0;
  vectinv[0] = _mm_div_ps( vunit, vector[0] );
  edgevmask[0] = raymaskinv | ( _mm_movemask_ps( vector[0] ) ^ 0xF );
  if( (uint32_t)(edgevmask[0]-1) < 0xE )
    vecflag = 1;
  vectinv[1] = _mm_div_ps( vunit, vector[1] );
  edgevmask[1] = raymaskinv | ( _mm_movemask_ps( vector[1] ) ^ 0xF );
  if( (uint32_t)(edgevmask[1]-1) < 0xE )
    vecflag = 1;
  vectinv[2] = _mm_div_ps( vunit, vector[2] );
  edgevmask[2] = raymaskinv | ( _mm_movemask_ps( vector[2] ) ^ 0xF );
  if( (uint32_t)(edgevmask[2]-1) < 0xE )
    vecflag = 1;

  /* Storage for trace1 */
  _mm_store_ps( &RF_RESULT->vectinv[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vectinv[0] );
  _mm_store_ps( &RF_RESULT->vectinv[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vectinv[1] );
  _mm_store_ps( &RF_RESULT->vectinv[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], vectinv[2] );

#if TRACESINGLEROOT
  if( vecflag )
  {
    rootoffset = packet->root[0];
 #if TRACEQUADROOT
    displaceflag = ( ( rootoffset & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE );
    rootoffset &= ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK );
 #endif
 #if TRACETESTROOT || TRACEQUADROOT
    if( !( rootoffset ) )
      raymask = 0x0;
 #endif
    root = RF_ADDRESS( handle->objectgraph, rootoffset );
    incoherentroot[0] = root;
    incoherentroot[1] = root;
    incoherentroot[2] = root;
    incoherentroot[3] = root;
 #if TRACEDISPLACEROOT
    goto incoherentdisplace;
 #elif TRACEQUADROOT
    if( displaceflag )
      goto incoherentdisplace;
    else
      goto incoherent;
 #else
    goto incoherent;
 #endif
  }
#else
 #if TRACERESOLVEROOT
  if( vecflag )
 #else
  if( ( vecflag ) || ( ( packet->root[RF_PACKETOFFSET+0] ^ packet->root[RF_PACKETOFFSET+1] ) | ( packet->root[RF_PACKETOFFSET+2] ^ packet->root[RF_PACKETOFFSET+3] ) | ( packet->root[RF_PACKETOFFSET+0] ^ packet->root[RF_PACKETOFFSET+2] ) ) )
 #endif
  {
    rfRoot rootoffset0, rootoffset1, rootoffset2, rootoffset3;
    rootoffset0 = packet->root[RF_PACKETOFFSET+0];
    rootoffset1 = packet->root[RF_PACKETOFFSET+1];
    rootoffset2 = packet->root[RF_PACKETOFFSET+2];
    rootoffset3 = packet->root[RF_PACKETOFFSET+3];
 #if TRACEQUADROOT
    displaceflag = ( ( rootoffset0 & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE ) | ( ( rootoffset1 & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE ) | ( ( rootoffset2 & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE ) | ( ( rootoffset3 & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE );
    rootoffset0 &= ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK );
    rootoffset1 &= ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK );
    rootoffset2 &= ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK );
    rootoffset3 &= ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK );
 #endif
 #if TRACETESTROOT || TRACEQUADROOT
    if( !( rootoffset0 ) )
      raymask &= ~0x1;
    if( !( rootoffset1 ) )
      raymask &= ~0x2;
    if( !( rootoffset2 ) )
      raymask &= ~0x4;
    if( !( rootoffset3 ) )
      raymask &= ~0x8;
 #endif
    incoherentroot[0] = RF_ADDRESS( handle->objectgraph, rootoffset0 );
    incoherentroot[1] = RF_ADDRESS( handle->objectgraph, rootoffset1 );
    incoherentroot[2] = RF_ADDRESS( handle->objectgraph, rootoffset2 );
    incoherentroot[3] = RF_ADDRESS( handle->objectgraph, rootoffset3 );
 #if TRACEDISPLACEROOT
    goto incoherentdisplace;
 #elif TRACEQUADROOT
    if( displaceflag )
      goto incoherentdisplace;
    else
      goto incoherent;
 #else
    goto incoherent;
 #endif
  }
#endif

#if TRACESINGLEROOT
  rootoffset = packet->root[0];
#else
  rootoffset = packet->root[RF_PACKETOFFSET+0];
#endif

#if TRACETESTROOT
  if( !( rootoffset ) )
    root = handle->objectgraph;
  else
  {
#endif

#if TRACERESOLVEROOT
  root = RF_RESOLVE4NAME( handle->objectgraph, &packet->origin[RF_PACKETOFFSET], RF_PACKETWIDTH );
  if( !( root ) )
  {
    rayorigin[0] = packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)+0];
    rayorigin[1] = packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)+0];
    rayorigin[2] = packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)+0];
    incoherentroot[0] = RF_RESOLVENAME( rayorigin );
    rayorigin[0] = packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)+1];
    rayorigin[1] = packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)+1];
    rayorigin[2] = packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)+1];
    incoherentroot[1] = RF_RESOLVENAME( rayorigin );
    rayorigin[0] = packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)+2];
    rayorigin[1] = packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)+2];
    rayorigin[2] = packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)+2];
    incoherentroot[2] = RF_RESOLVENAME( rayorigin );
    rayorigin[0] = packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)+3];
    rayorigin[1] = packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)+3];
    rayorigin[2] = packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)+3];
    incoherentroot[3] = RF_RESOLVENAME( rayorigin );
    goto incoherent;
  }
#elif TRACEDISPLACEROOT
  root = RF_ADDRESS( handle->objectgraph, rootoffset );
  root = RF_DISPLACE4NAME( root, &packet->origin[RF_PACKETOFFSET], RF_PACKETWIDTH );
  if( !( root ) )
  {
    root = RF_ADDRESS( handle->objectgraph, rootoffset );
    incoherentroot[0] = root;
    incoherentroot[1] = root;
    incoherentroot[2] = root;
    incoherentroot[3] = root;
    goto incoherentdisplace;
  }
#elif TRACEQUADROOT
  if( !( rootoffset ) )
    root = handle->objectgraph;
  else
  {
    if( ( rootoffset & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_PRIMITIVE )
    {
      RF_TRILIST_TYPE *trilist;
      rfTri *tri;
      root = RF_ADDRESS( handle->objectgraph, rootoffset & ~( RF_QUADTRACE_TYPEMASK | ( (size_t)RF_QUADTRACE_PRIMINDEX_MAX )<< RF_QUADTRACE_PRIMINDEX_SHIFT ) );
      trilist = RF_TRILIST( RF_SECTOR(root) );
      tri = (rfTri *)RF_ADDRESS( root, (rfssize)trilist[ rootoffset >> RF_QUADTRACE_PRIMINDEX_SHIFT ] << RF_LINKSHIFT );
 #if TRACEHITPOINT || TRACEHITUV || TRACEHITDIST
      __m128 vscalesize;
      vscalesize = _mm_set1_ps( ((const rfGraphHeader *)handle->objectgraph)->mediansizef );
      vdst[0] = _mm_add_ps( vsrc[0], _mm_mul_ps( vscalesize, vector[0] ) );
      vdst[1] = _mm_add_ps( vsrc[1], _mm_mul_ps( vscalesize, vector[1] ) );
      vdst[2] = _mm_add_ps( vsrc[2], _mm_mul_ps( vscalesize, vector[2] ) );
      pl0 = _mm_set1_ps( tri->plane[0] );
      pl1 = _mm_set1_ps( tri->plane[1] );
      pl2 = _mm_set1_ps( tri->plane[2] );
      pl3 = _mm_set1_ps( tri->plane[3] );
      dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vdst[0] ), _mm_mul_ps( pl1, vdst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vdst[2] ), pl3 ) );
      srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
      vsum = _mm_div_ps( srcdist, _mm_sub_ps( srcdist, dstdist ) );
 #if TRACEHITPOINT || TRACEHITUV
      vray[0] = _mm_add_ps( vsrc[0], _mm_mul_ps( vsum, _mm_sub_ps( vdst[0], vsrc[0] ) ) );
      vray[1] = _mm_add_ps( vsrc[1], _mm_mul_ps( vsum, _mm_sub_ps( vdst[1], vsrc[1] ) ) );
      vray[2] = _mm_add_ps( vsrc[2], _mm_mul_ps( vsum, _mm_sub_ps( vdst[2], vsrc[2] ) ) );
 #endif
 #if TRACEHITUV
      utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_set1_ps( tri->edpu[3] ) );
      vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_set1_ps( tri->edpv[3] ) );
 #endif
  #if TRACEHITDIST
      vhitdist = _mm_mul_ps( vsum, vscalesize );
  #endif
  #if TRACEHITSIDE && RF_CULLMODE == 2
      hitsidemask = _mm_movemask_ps( _mm_cmplt_ps( srcdist, _mm_set1_ps( 0.0 ) ) );
  #endif
 #elif TRACEHITDIST
      dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vdst[0] ), _mm_mul_ps( pl1, vdst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vdst[2] ), pl3 ) );
      srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
      vhitdist = _mm_mul_ps( vsum, vscalesize );
  #if TRACEHITSIDE && RF_CULLMODE == 2
      hitsidemask = _mm_movemask_ps( _mm_cmplt_ps( srcdist, _mm_set1_ps( 0.0 ) ) );
  #endif
 #elif TRACEHITSIDE && RF_CULLMODE == 2
      srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
      hitsidemask = _mm_movemask_ps( _mm_cmplt_ps( srcdist, _mm_set1_ps( 0.0 ) ) );
 #endif
 #if TRACEHITSIDE
  #if RF_CULLMODE == 0
      hitsidemask = 0x0;
  #elif RF_CULLMODE == 1
      hitsidemask = 0xf;
  #endif
 #endif
 #if TRACEHITMODELDATA
 #endif
 #if TRACEHITOBJECTDATA
 #endif
 #if TRACEHITVECTOR
 #endif
 #if TRACEHITPOINT
      _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vray[0] );
      _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vray[1] );
      _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], vray[2] );
 #endif
 #if TRACEHITUV
      _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], utd );
      _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vtd );
 #endif
 #if TRACEHITDATA || TRACEHITPLANE
      RF_RESULT->hittri[RF_PACKETOFFSET+0] = tri;
      RF_RESULT->hittri[RF_PACKETOFFSET+1] = tri;
      RF_RESULT->hittri[RF_PACKETOFFSET+2] = tri;
      RF_RESULT->hittri[RF_PACKETOFFSET+3] = tri;
 #endif
 #if TRACEHITDIST
      _mm_store_ps( &RF_RESULT->hitdist[RF_PACKETOFFSET], vhitdist );
 #endif
 #if TRACEHITPLANE
 #endif
 #if TRACEHITSIDE
  #if RF_PACKETWIDTH == 4
      RF_RESULT->hitsidemask = hitsidemask;
  #else
      RF_RESULT->hitsidemask &= ~( hitmask << RF_PACKETOFFSET );
      RF_RESULT->hitsidemask |= hitsidemask << RF_PACKETOFFSET;
  #endif
 #endif
 #if TRACEHITROOT
      hitrootoffset = (rfRoot)RF_ADDRESSDIFF( root, handle->objectgraph );
      RF_RESULT->hitroot[RF_PACKETOFFSET+0] = hitrootoffset;
      RF_RESULT->hitroot[RF_PACKETOFFSET+1] = hitrootoffset;
      RF_RESULT->hitroot[RF_PACKETOFFSET+2] = hitrootoffset;
      RF_RESULT->hitroot[RF_PACKETOFFSET+3] = hitrootoffset;
 #endif
 #if TRACEHITMATRIX
 #endif
      donemask = raymask;
    }
    else
    {
      void *rootbase;
      root = RF_ADDRESS( handle->objectgraph, rootoffset & ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK ) );
      rootbase = root;
      if( ( rootoffset & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE )
      {
        root = RF_DISPLACE4NAME( root, &packet->origin[RF_PACKETOFFSET], RF_PACKETWIDTH );
/*
        root = RF_RESOLVE4NAME( handle->objectgraph, &packet->origin[RF_PACKETOFFSET], RF_PACKETWIDTH );
*/
        if( !( root ) )
        {
          incoherentroot[0] = rootbase;
          incoherentroot[1] = rootbase;
          incoherentroot[2] = rootbase;
          incoherentroot[3] = rootbase;
          goto incoherentdisplace;
        }
      }
      else
        root = RF_ADDRESS( handle->objectgraph, rootoffset & ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK ) );
#else
  root = RF_ADDRESS( handle->objectgraph, rootoffset );
#endif

  edgeindex[RF_AXIS_X] = ( RF_AXIS_X << RF_EDGE_AXIS_SHIFT ) | ( ( edgevmask[RF_AXIS_X] >> 0 ) & 1 );
  edgeindex[RF_AXIS_Y] = ( RF_AXIS_Y << RF_EDGE_AXIS_SHIFT ) | ( ( edgevmask[RF_AXIS_Y] >> 0 ) & 1 );
  edgeindex[RF_AXIS_Z] = ( RF_AXIS_Z << RF_EDGE_AXIS_SHIFT ) | ( ( edgevmask[RF_AXIS_Z] >> 0 ) & 1 );

  for( ; ; )
  {
    /* Sector traversal */
    int32_t vmask0, vmask1, vmask2, submask;
    __m128 vxdist, vydist, vmindist;

/*
  // elem->edgeindex[3] = elem->edgeindex[2];

    int32_t edgebits, edgeref, mask;
    __m128 vindexmask;
    const static int32_t edgeymask[4] RF_ALIGN16 = { 0x80, 0x80, 0x80, 0x80 };
    const static int32_t brmask[0x10] = { 0x0000, 0x000f, 0x00f0, 0x00ff, 0x0f00, 0x0f0f, 0x0ff0, 0x0fff, 0xf000, 0xf00f, 0xf0f0, 0xf0ff, 0xff00, 0xff0f, 0xfff0, 0xffff };
    const static int32_t edgemaskfmaskshfit[4] = { 0x0000, 0x1111, 0x0000, 0xffff };
    const static int8_t maskshfit[0x10] = { 16, 0, 4, 0, 8, 0, 4, 0, 12, 0, 4, 0, 8, 0, 4, 0 };

    vxdist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[0] ] ), vsrc[0] ), vectinv[0] );
    vydist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[1] ] ), vsrc[1] ), vectinv[1] );
    vindexmask = _mm_and_ps( _mm_cmplt_ps( vydist, vxdist ), _mm_load_ps( (void *)edgeymask ) );
    vydist = _mm_min_ps( vydist, vxdist );
    vmindist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[2] ] ), vsrc[2] ), vectinv[2] );
    edgebits = _mm_movemask_epi8( (__m128i)_mm_or_ps( vindexmask, _mm_cmplt_ps( vmindist, vydist ) ) );
    vmindist = _mm_min_ps( vmindist, vydist );
    mask = brmask[ raymask ];
    edgebits &= mask;

    submask = 0x0;
    edgeref = ( edgebits >> maskshfit[ raymask ] ) & 3;
    nredge = edgeindex[edgeref];
    if( edgebits != ( edgemask[edgeref] & mask ) )
    {
      submask = raymask;
      vmask0 = _mm_movemask_ps( _mm_cmpeq_ps( vmindist, vxdist ) ) & raymask;
      if( maskcount[ vmask0 ] >= RF_SSE_BUNDLE_MIN )
      {
        submask -= vmask0;
        nredge = edgeindex[0];
      }
      else if( maskcount[ vmask1 = _mm_movemask_ps( _mm_cmpeq_ps( vmindist, vydist ) ) & ( raymask & ( vmask0 ^ 0xF ) ) ] >= RF_SSE_BUNDLE_MIN )
      {
        submask -= vmask1;
        nredge = edgeindex[1];
      }
      else if( maskcount[ vmask2 = ( ( vmask0 | vmask1 ) ^ raymask ) ] >= RF_SSE_BUNDLE_MIN )
      {
        submask -= vmask2;
        nredge = edgeindex[2];
      }
    }

*/

    vxdist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[0] ] ), vsrc[0] ), vectinv[0] );
    vydist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[1] ] ), vsrc[1] ), vectinv[1] );
    vmindist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[2] ] ), vsrc[2] ), vectinv[2] );
    vmindist = _mm_min_ps( vmindist, _mm_min_ps( vxdist, vydist ) );

    submask = 0x0;
    vmask0 = _mm_movemask_ps( _mm_cmpeq_ps( vmindist, vxdist ) ) & raymask;
    nredge = edgeindex[2];
    if( vmask0 == raymask )
      nredge = edgeindex[0];
    else if( ( vmask1 = ( _mm_movemask_ps( _mm_cmpeq_ps( vmindist, vydist ) ) & raymask ) ) == raymask )
      nredge = edgeindex[1];
    else if( vmask0 | vmask1 )
    {
      submask = raymask;
#if RF_CPUSLOW_SHR
      if( maskcount[vmask0] >= 2 )
#else
      if( 0xfec8 & ( 1 << vmask0 ) )
#endif
      {
        submask -= vmask0;
        nredge = edgeindex[0];
      }
#if RF_CPUSLOW_SHR
      else if( maskcount[vmask1] >= 2 )
#else
      else if( 0xfec8 & ( 1 << vmask1 ) )
#endif
      {
        submask -= vmask1;
        nredge = edgeindex[1];
      }
#if RF_CPUSLOW_SHR
      else if( maskcount[ vmask2 = ( ( vmask0 | vmask1 ) ^ raymask ) ] >= 2 )
#else
      else if( 0xfec8 & ( 1 << ( vmask2 = ( ( vmask0 | vmask1 ) ^ raymask ) ) ) )
#endif
        submask -= vmask2;
    }

    if( !( RF_SECTOR_GET_PRIMCOUNT( RF_SECTOR(root) ) ) )
    {
#if TRACEDISTCLIP
      clipmask = _mm_movemask_ps( _mm_cmplt_ps( vclipdist, vmindist ) ) & raymask;
 #if TRACEHITROOT
      if( clipmask )
      {
        if( clipmask & 0x1 )
          RF_RESULT->hitroot[RF_PACKETOFFSET+0] = root;
        if( clipmask & 0x2 )
          RF_RESULT->hitroot[RF_PACKETOFFSET+1] = root;
        if( clipmask & 0x4 )
          RF_RESULT->hitroot[RF_PACKETOFFSET+2] = root;
        if( clipmask & 0x8 )
          RF_RESULT->hitroot[RF_PACKETOFFSET+3] = root;
        raymask -= clipmask;
        submask &= ~clipmask;
      }
 #else
      raymask -= clipmask;
      submask &= ~clipmask;
 #endif
#endif

      if( submask )
      {
        void *rayroot;
        int32_t raynredge;

        vsrc[0] = _mm_add_ps( vsrc[0], _mm_mul_ps( vmindist, vector[0] ) );
        vsrc[1] = _mm_add_ps( vsrc[1], _mm_mul_ps( vmindist, vector[1] ) );
        vsrc[2] = _mm_add_ps( vsrc[2], _mm_mul_ps( vmindist, vector[2] ) );

        _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vsrc[0] );
        _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vsrc[1] );
        _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], vsrc[2] );
#if TRACEDISTCLIP
        _mm_store_ps( &RF_RESULT->clipdist[RF_PACKETOFFSET], vclipdist );
#endif

        if( submask & 0x1 )
        {
          raynredge = edgeindex[2];
          if( vmask0 & 0x1 )
            raynredge = edgeindex[0];
          else if( vmask1 & 0x1 )
            raynredge = edgeindex[1];
          rayroot = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ raynredge ] << RF_LINKSHIFT );
          if( rayroot )
            RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+0, RF_RESULT, rayroot, ( RF_SECTOR(root)->flags >> ( RF_SECTOR_LINKFLAGS_SHIFT + raynredge ) ) & 1 );
        }
        if( submask & 0x2 )
        {
          raynredge = edgeindex[2];
          if( vmask0 & 0x2 )
            raynredge = edgeindex[0];
          else if( vmask1 & 0x2 )
            raynredge = edgeindex[1];
          rayroot = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ raynredge ] << RF_LINKSHIFT );
          if( rayroot )
            RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+1, RF_RESULT, rayroot, ( RF_SECTOR(root)->flags >> ( RF_SECTOR_LINKFLAGS_SHIFT + raynredge ) ) & 1 );
        }
        if( submask & 0x4 )
        {
          raynredge = edgeindex[2];
          if( vmask0 & 0x4 )
            raynredge = edgeindex[0];
          else if( vmask1 & 0x4 )
            raynredge = edgeindex[1];
          rayroot = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ raynredge ] << RF_LINKSHIFT );
          if( rayroot )
            RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+2, RF_RESULT, rayroot, ( RF_SECTOR(root)->flags >> ( RF_SECTOR_LINKFLAGS_SHIFT + raynredge ) ) & 1 );
        }
        if( submask & 0x8 )
        {
          raynredge = edgeindex[2];
          if( vmask0 & 0x8 )
            raynredge = edgeindex[0];
          else if( vmask1 & 0x8 )
            raynredge = edgeindex[1];
          rayroot = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ raynredge ] << RF_LINKSHIFT );
          if( rayroot )
            RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+3, RF_RESULT, rayroot, ( RF_SECTOR(root)->flags >> ( RF_SECTOR_LINKFLAGS_SHIFT + raynredge ) ) & 1 );
        }
        raymask -= submask;
        if( !( raymask ) )
          goto done;

        if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
        {
          slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
          if( !( slink ) )
            goto done;
          root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
          continue;
        }
      }
      else
      {
        if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
        {
          slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
          if( !( slink ) )
            goto done;
          root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
          continue;
        }

#if TRACEDISTCLIP
        vclipdist = _mm_sub_ps( vclipdist, vmindist );
#endif
        vsrc[0] = _mm_add_ps( vsrc[0], _mm_mul_ps( vmindist, vector[0] ) );
        vsrc[1] = _mm_add_ps( vsrc[1], _mm_mul_ps( vmindist, vector[1] ) );
        vsrc[2] = _mm_add_ps( vsrc[2], _mm_mul_ps( vmindist, vector[2] ) );
      }
    }
    else
    {
      int32_t a;
      __m128 vtridst[3];
      RF_TRILIST_TYPE *trilist;

      vtridst[0] = vdst[0] = _mm_add_ps( _mm_mul_ps( vmindist, vector[0] ), vsrc[0] );
      vtridst[1] = vdst[1] = _mm_add_ps( _mm_mul_ps( vmindist, vector[1] ), vsrc[1] );
      vtridst[2] = vdst[2] = _mm_add_ps( _mm_mul_ps( vmindist, vector[2] ), vsrc[2] );

      trilist = RF_TRILIST( RF_SECTOR(root) );
      for( a = RF_SECTOR(root)->primcount ; a ; a-- )
      {
        int32_t tflags, hitmask;
        rfTri *tri;
#if RF_CULLMODE == 2
        int32_t signmask;
#endif

        tri = (rfTri *)RF_ADDRESS( root, (rfssize)(*trilist++) << RF_LINKSHIFT );
        pl0 = _mm_set1_ps( tri->plane[0] );
        pl1 = _mm_set1_ps( tri->plane[1] );
        pl2 = _mm_set1_ps( tri->plane[2] );
        pl3 = _mm_set1_ps( tri->plane[3] );

#if RF_CULLMODE == 0
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        tflags = _mm_movemask_ps( dstdist ) & raymask;
        if( tflags == raymask )
          continue;
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        tflags |= ( _mm_movemask_ps( srcdist ) ^ 0xF ) & raymask;
        if( tflags == raymask )
          continue;
        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );
        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags |= _mm_movemask_ps( utd ) & raymask;
        if( tflags == raymask )
          continue;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        tflags |= _mm_movemask_ps( vtd ) & raymask;
        if( tflags == raymask )
          continue;
        tflags |= _mm_movemask_ps( _mm_cmple_ps( vsum, _mm_add_ps( utd, vtd ) ) ) & raymask;
        if( tflags == raymask )
          continue;
#elif RF_CULLMODE == 1
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        tflags = ( _mm_movemask_ps( dstdist ) ^ 0xF ) & raymask;
        if( tflags == raymask )
          continue;
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        tflags |= ( _mm_movemask_ps( srcdist ) ) & raymask;
        if( tflags == raymask )
          continue;
        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );
        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags |= ( _mm_movemask_ps( utd ) ^ 0xF ) & raymask;
        if( tflags == raymask )
          continue;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        tflags |= ( _mm_movemask_ps( vtd ) ^ 0xF ) & raymask;
        if( tflags == raymask )
          continue;
        tflags |= ( _mm_movemask_ps( _mm_cmple_ps( _mm_add_ps( utd, vtd ), vsum ) ) ) & raymask;
        if( tflags == raymask )
          continue;
#else
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        tflags = ( _mm_movemask_ps( _mm_mul_ps( srcdist, dstdist ) ) ^ 0xF ) & raymask;
        if( tflags == raymask )
          continue;
        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );
        signmask = _mm_movemask_ps( vsum );
        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags |= ( _mm_movemask_ps( utd ) ^ signmask ) & raymask;
        if( tflags == raymask )
          continue;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        tflags |= ( _mm_movemask_ps( vtd ) ^ signmask ) & raymask;
        if( tflags == raymask )
          continue;
        tflags |= ( _mm_movemask_ps( _mm_cmple_ps( vsum, _mm_add_ps( utd, vtd ) ) ) ^ signmask ) & raymask;
        if( tflags == raymask )
          continue;
#endif

        vsum = _mm_div_ps( vunit, vsum );
#if TRACEDISTCLIP
        /* TODO: Optimize me */
        axisindex = nredge >> 1;
        tflags |= _mm_movemask_ps( _mm_cmple_ps( vclipdist, _mm_mul_ps( _mm_sub_ps( _mm_mul_ps( vray[axisindex], vsum ), vsrc[axisindex] ), vectinv[axisindex] ) ) );
        if( tflags == raymask )
          continue;
#endif
#if TRACEHITUV
        utd = _mm_mul_ps( utd, vsum );
        vtd = _mm_mul_ps( vtd, vsum );
#endif
        hitmask = tflags ^ raymask;

        donemask |= hitmask;
        if( !( tflags ) )
        {
          vtridst[0] = _mm_mul_ps( vray[0], vsum );
          vtridst[1] = _mm_mul_ps( vray[1], vsum );
          vtridst[2] = _mm_mul_ps( vray[2], vsum );
        }
        else
        {
          __m128 vblendmask;
          vblendmask = _mm_load_ps( (void *)&ortable[ hitmask << 2 ] );
#if RFTRACE__SSE4_1__ && !RF_CPUSLOW_BLENDVPS
          vtridst[0] = _mm_blendv_ps( vtridst[0], _mm_mul_ps( vray[0], vsum ), vblendmask );
          vtridst[1] = _mm_blendv_ps( vtridst[1], _mm_mul_ps( vray[1], vsum ), vblendmask );
          vtridst[2] = _mm_blendv_ps( vtridst[2], _mm_mul_ps( vray[2], vsum ), vblendmask );
#else
          vtridst[0] = _mm_or_ps( _mm_and_ps( _mm_mul_ps( vray[0], vsum ), vblendmask ), _mm_andnot_ps( vblendmask, vtridst[0] ) );
          vtridst[1] = _mm_or_ps( _mm_and_ps( _mm_mul_ps( vray[1], vsum ), vblendmask ), _mm_andnot_ps( vblendmask, vtridst[1] ) );
          vtridst[2] = _mm_or_ps( _mm_and_ps( _mm_mul_ps( vray[2], vsum ), vblendmask ), _mm_andnot_ps( vblendmask, vtridst[2] ) );
#endif
        }

#if TRACEHITDIST
        /* TODO: Optimize me */
        axisindex = nredge >> 1;
        vhitdist = _mm_mul_ps( _mm_sub_ps( vtridst[axisindex], _mm_load_ps( &packet->origin[RF_PACKETOFFSET+(axisindex*RF_PACKETWIDTH)] ) ), vectinv[axisindex] );
#endif
#if TRACEHITSIDE
 #if RF_CULLMODE == 0
        hitsidemask = 0x0;
 #elif RF_CULLMODE == 1
        hitsidemask = hitmask;
 #else
        hitsidemask = _mm_movemask_ps( _mm_cmplt_ps( srcdist, _mm_set1_ps( 0.0 ) ) ) & hitmask;
 #endif
#endif
        if( hitmask == 0xF )
        {
#if TRACEHITMODELDATA
#endif
#if TRACEHITOBJECTDATA
#endif
#if TRACEHITVECTOR
#endif
#if TRACEHITPOINT
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vtridst[0] );
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vtridst[1] );
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], vtridst[2] );
#endif
#if TRACEHITUV
          _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], utd );
          _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vtd );
#endif
#if TRACEHITDATA || TRACEHITPLANE
          RF_RESULT->hittri[RF_PACKETOFFSET+0] = tri;
          RF_RESULT->hittri[RF_PACKETOFFSET+1] = tri;
          RF_RESULT->hittri[RF_PACKETOFFSET+2] = tri;
          RF_RESULT->hittri[RF_PACKETOFFSET+3] = tri;
#endif
#if TRACEHITDIST
          _mm_store_ps( &RF_RESULT->hitdist[RF_PACKETOFFSET], vhitdist );
#endif
#if TRACEHITPLANE
#endif
#if TRACEHITSIDE
 #if RF_PACKETWIDTH == 4
          RF_RESULT->hitsidemask = hitsidemask;
 #else
          RF_RESULT->hitsidemask &= ~( hitmask << RF_PACKETOFFSET );
          RF_RESULT->hitsidemask |= hitsidemask << RF_PACKETOFFSET;
 #endif
#endif
#if TRACEHITROOT
          hitrootoffset = (rfRoot)RF_ADDRESSDIFF( root, handle->objectgraph );
          RF_RESULT->hitroot[RF_PACKETOFFSET+0] = hitrootoffset;
          RF_RESULT->hitroot[RF_PACKETOFFSET+1] = hitrootoffset;
          RF_RESULT->hitroot[RF_PACKETOFFSET+2] = hitrootoffset;
          RF_RESULT->hitroot[RF_PACKETOFFSET+3] = hitrootoffset;
#endif
#if TRACEHITMATRIX
#endif
        }
        else
        {
#if TRACEHITPOINT || TRACEHITUV || TRACEHITDIST
          __m128 vblendmask;
#endif
#if TRACEHITROOT
          hitrootoffset = (rfRoot)RF_ADDRESSDIFF( root, handle->objectgraph );
#endif
          if( hitmask & 1 )
          {
#if TRACEHITDATA || TRACEHITPLANE
            RF_RESULT->hittri[RF_PACKETOFFSET+0] = tri;
#endif
#if TRACEHITROOT
            RF_RESULT->hitroot[RF_PACKETOFFSET+0] = hitrootoffset;
#endif
          }
          if( hitmask & 2 )
          {
#if TRACEHITDATA || TRACEHITPLANE
            RF_RESULT->hittri[RF_PACKETOFFSET+1] = tri;
#endif
#if TRACEHITROOT
            RF_RESULT->hitroot[RF_PACKETOFFSET+1] = hitrootoffset;
#endif
          }
          if( hitmask & 4 )
          {
#if TRACEHITDATA || TRACEHITPLANE
            RF_RESULT->hittri[RF_PACKETOFFSET+2] = tri;
#endif
#if TRACEHITROOT
            RF_RESULT->hitroot[RF_PACKETOFFSET+2] = hitrootoffset;
#endif
          }
          if( hitmask & 8 )
          {
#if TRACEHITDATA || TRACEHITPLANE
            RF_RESULT->hittri[RF_PACKETOFFSET+3] = tri;
#endif
#if TRACEHITROOT
            RF_RESULT->hitroot[RF_PACKETOFFSET+3] = hitrootoffset;
#endif
          }

#if TRACEHITPOINT || TRACEHITUV || TRACEHITDIST
          vblendmask = _mm_load_ps( (void *)&ortable[ hitmask << 2 ] );
#endif
#if TRACEHITMODELDATA
#endif
#if TRACEHITOBJECTDATA
#endif
#if TRACEHITVECTOR
#endif
#if TRACEHITPOINT
 #if RFTRACE__SSE4_1__ && !RF_CPUSLOW_BLENDVPS
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], _mm_blendv_ps( _mm_load_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)] ), vtridst[0], vblendmask ) );
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], _mm_blendv_ps( _mm_load_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)] ), vtridst[1], vblendmask ) );
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], _mm_blendv_ps( _mm_load_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)] ), vtridst[2], vblendmask ) );
 #else
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], _mm_or_ps( _mm_and_ps( vtridst[0], vblendmask ), _mm_andnot_ps( vblendmask, _mm_load_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)] ) ) ) );
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], _mm_or_ps( _mm_and_ps( vtridst[1], vblendmask ), _mm_andnot_ps( vblendmask, _mm_load_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)] ) ) ) );
          _mm_store_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], _mm_or_ps( _mm_and_ps( vtridst[2], vblendmask ), _mm_andnot_ps( vblendmask, _mm_load_ps( &RF_RESULT->hitpt[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)] ) ) ) );
 #endif
#endif
#if TRACEHITUV
 #if RFTRACE__SSE4_1__ && !RF_CPUSLOW_BLENDVPS
          _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], _mm_blendv_ps( _mm_load_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)] ), utd, vblendmask ) );
          _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], _mm_blendv_ps( _mm_load_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)] ), vtd, vblendmask ) );
 #else
          _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], _mm_or_ps( _mm_and_ps( utd, vblendmask ), _mm_andnot_ps( vblendmask, _mm_load_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)] ) ) ) );
          _mm_store_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], _mm_or_ps( _mm_and_ps( vtd, vblendmask ), _mm_andnot_ps( vblendmask, _mm_load_ps( &RF_RESULT->hituv[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)] ) ) ) );
 #endif
#endif
#if TRACEHITDATA
#endif
#if TRACEHITDIST
 #if RFTRACE__SSE4_1__ && !RF_CPUSLOW_BLENDVPS
          _mm_store_ps( &RF_RESULT->hitdist[RF_PACKETOFFSET], _mm_blendv_ps( _mm_load_ps( &RF_RESULT->hitdist[RF_PACKETOFFSET] ), vhitdist, vblendmask ) );
 #else
          _mm_store_ps( &RF_RESULT->hitdist[RF_PACKETOFFSET], _mm_or_ps( _mm_and_ps( vhitdist, vblendmask ), _mm_andnot_ps( vblendmask, _mm_load_ps( &RF_RESULT->hitdist[RF_PACKETOFFSET] ) ) ) );
 #endif
#endif
#if TRACEHITPLANE
#endif
#if TRACEHITSIDE
          RF_RESULT->hitsidemask &= ~( hitmask << RF_PACKETOFFSET );
          RF_RESULT->hitsidemask |= hitsidemask << RF_PACKETOFFSET;
#endif
#if TRACEHITROOT
#endif
#if TRACEHITMATRIX
#endif
        }
      }

      submask &= ~donemask;
#if TRACEDISTCLIP
      clipmask = _mm_movemask_ps( _mm_cmplt_ps( vclipdist, vmindist ) ) & raymask;
 #if TRACEHITROOT
      if( clipmask )
      {
        if( clipmask & 0x1 )
          RF_RESULT->hitroot[RF_PACKETOFFSET+0] = root;
        if( clipmask & 0x2 )
          RF_RESULT->hitroot[RF_PACKETOFFSET+1] = root;
        if( clipmask & 0x4 )
          RF_RESULT->hitroot[RF_PACKETOFFSET+2] = root;
        if( clipmask & 0x8 )
          RF_RESULT->hitroot[RF_PACKETOFFSET+3] = root;
        raymask -= clipmask;
        submask &= ~clipmask;
      }
 #else
      raymask -= clipmask;
      submask &= ~clipmask;
 #endif
#endif

      if( submask )
      {
        void *rayroot;
        int32_t raynredge;

        _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vdst[0] );
        _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vdst[1] );
        _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], vdst[2] );
#if TRACEDISTCLIP
        _mm_store_ps( &RF_RESULT->clipdist[RF_PACKETOFFSET], vclipdist );
#endif

        if( submask & 0x1 )
        {
          raynredge = edgeindex[2];
          if( vmask0 & 0x1 )
            raynredge = edgeindex[0];
          else if( vmask1 & 0x1 )
            raynredge = edgeindex[1];
          rayroot = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ raynredge ] << RF_LINKSHIFT );
          if( rayroot )
            RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+0, RF_RESULT, rayroot, ( RF_SECTOR(root)->flags >> ( RF_SECTOR_LINKFLAGS_SHIFT + raynredge ) ) & 1 );
        }
        if( submask & 0x2 )
        {
          raynredge = edgeindex[2];
          if( vmask0 & 0x2 )
            raynredge = edgeindex[0];
          else if( vmask1 & 0x2 )
            raynredge = edgeindex[1];
          rayroot = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ raynredge ] << RF_LINKSHIFT );
          if( rayroot )
            RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+1, RF_RESULT, rayroot, ( RF_SECTOR(root)->flags >> ( RF_SECTOR_LINKFLAGS_SHIFT + raynredge ) ) & 1 );
        }
        if( submask & 0x4 )
        {
          raynredge = edgeindex[2];
          if( vmask0 & 0x4 )
            raynredge = edgeindex[0];
          else if( vmask1 & 0x4 )
            raynredge = edgeindex[1];
          rayroot = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ raynredge ] << RF_LINKSHIFT );
          if( rayroot )
            RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+2, RF_RESULT, rayroot, ( RF_SECTOR(root)->flags >> ( RF_SECTOR_LINKFLAGS_SHIFT + raynredge ) ) & 1 );
        }
        if( submask & 0x8 )
        {
          raynredge = edgeindex[2];
          if( vmask0 & 0x8 )
            raynredge = edgeindex[0];
          else if( vmask1 & 0x8 )
            raynredge = edgeindex[1];
          rayroot = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ raynredge ] << RF_LINKSHIFT );
          if( rayroot )
            RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+3, RF_RESULT, rayroot, ( RF_SECTOR(root)->flags >> ( RF_SECTOR_LINKFLAGS_SHIFT + raynredge ) ) & 1 );
        }
      }

      raymask &= ~( submask | donemask );
      if( !( raymask ) )
        goto done;
      if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
      {
        slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
        if( !( slink ) )
          goto done;
        root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
        continue;
      }

 #if TRACEDISTCLIP
      vclipdist = _mm_sub_ps( vclipdist, vmindist );
 #endif
      vsrc[0] = vdst[0];
      vsrc[1] = vdst[1];
      vsrc[2] = vdst[2];
    }

    root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );

    _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vsrc[0] );
    _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vsrc[1] );
    _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], vsrc[2] );

    /* Node traversal */
    for( ; ; )
    {
      int32_t linkflags, nbits;
      __m128 nodesrc;

      linkflags = RF_NODE(root)->flags;

      /* TODO: What about branches? Merges? Something? */
#if 1
      nodesrc = _mm_load_ps( &RF_RESULT->raysrc[ RF_PACKETOFFSET + ( RF_NODE_GET_AXIS(linkflags) * RF_PACKETWIDTH ) ] );
#elif 0
      nodesrc = vsrc[ RF_NODE_GET_AXIS(linkflags) ];
#else
      nodesrc = vsrc[2];
      if( !( RF_NODE_GET_AXIS(linkflags) ) )
        nodesrc = vsrc[0];
      else if( RF_NODE_GET_AXIS(linkflags) == 1 )
        nodesrc = vsrc[1];
#endif

      nbits = _mm_movemask_ps( _mm_cmplt_ps( nodesrc, _mm_set1_ps( RF_NODE(root)->plane ) ) ) & raymask;
      if( !( nbits ) )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
      else if( nbits == raymask )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
      {
        int32_t nbitsl, snmask, rayflag, raypath, nbitscount;
        void *rayroot;
        nbitsl = nbits ^ raymask;
        snmask = nbits & raymask;
#if RF_CPUSLOW_SHR
        nbitscount = maskcount[ nbits ];
        if( nbitscount > maskcount[ nbitsl ] )
#else
        nbitscount = ( ( 0xfee9e994 >> (nbits+nbits) ) & 0x3 );
        if( nbitscount > ( ( 0xfee9e994 >> (nbitsl+nbitsl) ) & 0x3 ) )
#endif
        {
          snmask ^= raymask;
          if( nbitscount < 2 )
            snmask = raymask;
        }

#if TRACEDISTCLIP
        _mm_store_ps( &RF_RESULT->clipdist[RF_PACKETOFFSET], vclipdist );
#endif

        if( snmask & 0x1 )
        {
          rayflag = ( nbitsl >> 0 ) & 0x1;
          rayroot = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[ rayflag ] << RF_LINKSHIFT );
          raypath = ( linkflags >> ( RF_NODE_LINKFLAGS_SHIFT + rayflag ) ) & 0x1;
          RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+0, RF_RESULT, rayroot, raypath );
        }
        if( snmask & 0x2 )
        {
          rayflag = ( nbitsl >> 1 ) & 0x1;
          rayroot = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[ rayflag ] << RF_LINKSHIFT );
          raypath = ( linkflags >> ( RF_NODE_LINKFLAGS_SHIFT + rayflag ) ) & 0x1;
          RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+1, RF_RESULT, rayroot, raypath );
        }
        if( snmask & 0x4 )
        {
          rayflag = ( nbitsl >> 2 ) & 0x1;
          rayroot = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[ rayflag ] << RF_LINKSHIFT );
          raypath = ( linkflags >> ( RF_NODE_LINKFLAGS_SHIFT + rayflag ) ) & 0x1;
          RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+2, RF_RESULT, rayroot, raypath );
        }
        if( snmask & 0x8 )
        {
          rayflag = ( nbitsl >> 3 ) & 0x1;
          rayroot = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[ rayflag ] << RF_LINKSHIFT );
          raypath = ( linkflags >> ( RF_NODE_LINKFLAGS_SHIFT + rayflag ) ) & 0x1;
          RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+3, RF_RESULT, rayroot, raypath );
        }
        raymask -= snmask;
        if( !( raymask ) )
          goto done;
      }
    }
  }

#if TRACEQUADROOT
    }
  }
#endif

#if TRACETESTROOT
  }
#endif

  goto done;

////

#if TRACEDISPLACEROOT || ( !TRACERESOLVEROOT && !TRACESINGLEROOT ) || TRACEQUADROOT
  incoherentdisplace:

  rayorigin[0] = packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)+0];
  rayorigin[1] = packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)+0];
  rayorigin[2] = packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)+0];
  incoherentroot[0] = RF_DISPLACENAME( incoherentroot[0], rayorigin );
  rayorigin[0] = packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)+1];
  rayorigin[1] = packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)+1];
  rayorigin[2] = packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)+1];
  incoherentroot[1] = RF_DISPLACENAME( incoherentroot[1], rayorigin );
  rayorigin[0] = packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)+2];
  rayorigin[1] = packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)+2];
  rayorigin[2] = packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)+2];
  incoherentroot[2] = RF_DISPLACENAME( incoherentroot[2], rayorigin );
  rayorigin[0] = packet->origin[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)+3];
  rayorigin[1] = packet->origin[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)+3];
  rayorigin[2] = packet->origin[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)+3];
  incoherentroot[3] = RF_DISPLACENAME( incoherentroot[3], rayorigin );
#endif

  incoherent:

  _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], vsrc[0] );
  _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], vsrc[1] );
  _mm_store_ps( &RF_RESULT->raysrc[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], vsrc[2] );
#if TRACEDISTCLIP
  _mm_store_ps( &RF_RESULT->clipdist[RF_PACKETOFFSET], vclipdist );
#endif

  if( raymask & 1 )
    RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+0, RF_RESULT, incoherentroot[0], 1 );
  if( raymask & 2 )
    RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+1, RF_RESULT, incoherentroot[1], 1 );
  if( raymask & 4 )
    RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+2, RF_RESULT, incoherentroot[2], 1 );
  if( raymask & 8 )
    RF_TRACE1NAME( handle, RF_PASSPARAM, packet, RF_PACKETOFFSET+3, RF_RESULT, incoherentroot[3], 1 );

////

  done:

#if TRACEHITPLANE
  {
    rfTri *tri0, *tri1, *tri2, *tri3;
    donemask |= ( RF_RESULT->hitmask >> RF_PACKETOFFSET ) & 0xf;
    tri0 = RF_RESULT->hittri[RF_PACKETOFFSET+0];
    tri1 = RF_RESULT->hittri[RF_PACKETOFFSET+1];
    tri2 = RF_RESULT->hittri[RF_PACKETOFFSET+2];
    tri3 = RF_RESULT->hittri[RF_PACKETOFFSET+3];
    if( donemask & 0x1 )
      pl0 = _mm_load_ps( tri0->plane );
    else
      pl0 = _mm_set1_ps( 0.0 );
    if( donemask & 0x2 )
      pl1 = _mm_load_ps( tri1->plane );
    else
      pl1 = _mm_set1_ps( 0.0 );
    if( donemask & 0x4 )
      pl2 = _mm_load_ps( tri2->plane );
    else
      pl2 = _mm_set1_ps( 0.0 );
    if( donemask & 0x8 )
      pl3 = _mm_load_ps( tri3->plane );
    else
      pl3 = _mm_set1_ps( 0.0 );
 #if !TRACEHITPLANEPACKED
    __m128 t0, t1, t2, t3;
    t0 = _mm_unpacklo_ps( pl0, pl1 );
    t1 = _mm_unpacklo_ps( pl2, pl3 );
    t2 = _mm_unpackhi_ps( pl0, pl1 );
    t3 = _mm_unpackhi_ps( pl2, pl3 );
    _mm_store_ps( &RF_RESULT->hitplane[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], _mm_movelh_ps( t0, t1 ) );
    _mm_store_ps( &RF_RESULT->hitplane[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], _mm_movehl_ps( t1, t0 ) );
    _mm_store_ps( &RF_RESULT->hitplane[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], _mm_movelh_ps( t2, t3 ) );
    _mm_store_ps( &RF_RESULT->hitplane[RF_PACKETOFFSET+(3*RF_PACKETWIDTH)], _mm_movehl_ps( t3, t2 ) );
 #else
    _mm_store_ps( &RF_RESULT->hitplane[RF_PACKETOFFSET+(0*RF_PACKETWIDTH)], pl0 );
    _mm_store_ps( &RF_RESULT->hitplane[RF_PACKETOFFSET+(1*RF_PACKETWIDTH)], pl1 );
    _mm_store_ps( &RF_RESULT->hitplane[RF_PACKETOFFSET+(2*RF_PACKETWIDTH)], pl2 );
    _mm_store_ps( &RF_RESULT->hitplane[RF_PACKETOFFSET+(3*RF_PACKETWIDTH)], pl3 );
 #endif
  }
#endif

  RF_RESULT->hitmask |= donemask << RF_PACKETOFFSET;

#if RF_PACKETWIDTH == 4

 #if TRACEHITMODELDATA
  RF_RESULT->modeldata[0] = modeldata;
  RF_RESULT->modeldata[1] = modeldata;
  RF_RESULT->modeldata[2] = modeldata;
  RF_RESULT->modeldata[3] = modeldata;
 #endif
 #if TRACEHITOBJECTDATA
  RF_RESULT->objectdata[0] = handle.objectdata;
  RF_RESULT->objectdata[1] = handle.objectdata;
  RF_RESULT->objectdata[2] = handle.objectdata;
  RF_RESULT->objectdata[3] = handle.objectdata;
 #endif
 #if TRACEHITDATA
  RF_RESULT->hittri[0] = RF_ADDRESS( RF_RESULT->hittri[0], sizeof(rfTri) );
  RF_RESULT->hittri[1] = RF_ADDRESS( RF_RESULT->hittri[1], sizeof(rfTri) );
  RF_RESULT->hittri[2] = RF_ADDRESS( RF_RESULT->hittri[2], sizeof(rfTri) );
  RF_RESULT->hittri[3] = RF_ADDRESS( RF_RESULT->hittri[3], sizeof(rfTri) );
 #endif

  TRACEHIT( RF_PASSPARAM, RF_RESULT->hitmask,
 #if TRACEHITMODELDATA
    RF_RESULT->modeldata,
 #else
    0,
 #endif
 #if TRACEHITOBJECTDATA
    RF_RESULT->objectdata,
 #else
    0,
 #endif
 #if TRACEHITVECTOR
    packet->vector,
 #else
    0,
 #endif
 #if TRACEHITPOINT
    RF_RESULT->hitpt,
 #else
    0,
 #endif
 #if TRACEHITUV
    RF_RESULT->hituv,
 #else
    0,
 #endif
 #if TRACEHITDATA
    RF_RESULT->hittri,
 #else
    0,
 #endif
 #if TRACEHITDIST
    RF_RESULT->hitdist,
 #else
    0,
 #endif
 #if TRACEHITPLANE
    RF_RESULT->hitplane,
 #else
    0,
 #endif
 #if TRACEHITSIDE
    RF_RESULT->hitside,
 #else
    0,
 #endif
 #if TRACEHITSIDE
    RF_RESULT->root,
 #else
    0,
 #endif
 #if TRACEHITSIDE
    RF_RESULT->matrix
 #else
    0
 #endif
  );
 #ifdef TRACEEND
  TRACEEND( RF_PASSPARAM, packet->vector );
 #endif
#endif

  return;
}


/* Standard internally defined */
#undef RF_SECTOR
#undef RF_NODE
#undef RF_LINKSHIFT
#undef RF_TRILIST
#undef RF_TRILIST_TYPE

/* Special internally defined */
#undef RF_RESULT
#undef RF_PACKETOFFSET

/* Externally defined */
#if RF_PACKETWIDTH == 4
 #undef RF_CULLMODE
 #undef RF_ADDRBITS
 #undef RF_TRACE1NAME
 #undef RF_TRACE4NAME
 #undef RF_RESOLVENAME
 #undef RF_RESOLVE4NAME
 #undef RF_DISPLACENAME
 #undef RF_DISPLACE4NAME

 #undef RF_DECLPARAM
 #undef RF_PASSPARAM
 #undef RF_TRACEPACKET
 #undef RF_TRACERESULT
 #undef RF_PACKETWIDTH
#endif

