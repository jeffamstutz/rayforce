/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*
#ifndef TRACE_DEBUG
 #define TRACE_DEBUG (0)
#endif
*/


////


#ifndef RFTRACE_SECTOR_TRAVERSAL_VARIANT
 #define RFTRACE_SECTOR_TRAVERSAL_VARIANT (1)
#endif

#ifndef RF_NODE_SSE_TRAVERSAL
 #define RF_NODE_SSE_TRAVERSAL (0)
#endif

#if !RFTRACE__SSE3__
 #error ERROR: A minimum of SSE3 is required to compile this path
#endif


static void RF_TRACE1NAME( const rfHandle * RF_RESTRICT handle, RF_DECLPARAM, const RF_TRACEPACKET * RF_RESTRICT packet, int packetoffset, RF_TRACERESULT * RF_RESTRICT result, void *root, int rootflag )
{
  int nredge;
  rfssize slink;
#if TRACEHITSIDE
  int hitside;
#endif
  rff RF_ALIGN16 vectinv[4];
  int RF_ALIGN16 edgeindex[4];
  rff RF_ALIGN16 src[4];
#if TRACEHITUV
  rff hituv[2];
#endif
#if TRACEDISTCLIP
  rff clipdist;
#endif
#if TRACEDISTCLIP || TRACEHITDIST
  rff hitdist;
  __m128 vhitdist;
#endif
  rfTri *trihit;
#if TRACEDISTCLIP || TRACEHITDIST
  int axisindex;
#endif
#ifdef TRACERAYSTORE
  TRACERAYSTORE raystore;
#endif
#if TRACEHITMODELDATA
  void *modeldata;
#endif
  __m128 vsrc;
  __m128 vdst;
  __m128 vvector;
#if TRACEDISTCLIP || TRACEHITDIST || RFTRACE_SECTOR_TRAVERSAL_VARIANT == 0
  __m128 vvectinv;
#endif
  __m128 vmindist;

#if TRACEHITMODELDATA
  modeldata = RF_ADDRESS( handle->objectgraph, ((const rfGraphHeader * RF_RESTRICT)handle->objectgraph)->modeldataoffset );
#endif

  vvector = _mm_set_ps( 0.0, packet->vector[packetoffset+(2*RF_PACKETWIDTH)], packet->vector[packetoffset+(1*RF_PACKETWIDTH)], packet->vector[packetoffset+(0*RF_PACKETWIDTH)] );
  vsrc = _mm_set_ps( 1.0, result->raysrc[packetoffset+(2*RF_PACKETWIDTH)], result->raysrc[packetoffset+(1*RF_PACKETWIDTH)], result->raysrc[packetoffset+(0*RF_PACKETWIDTH)] );

  if( !( rootflag ) )
  {
    _mm_store_ps( src, vsrc );
    for( ; ; )
    {
      int linkflags;
      linkflags = RF_NODE(root)->flags;
      if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
    }
  }

#if TRACEDISTCLIP
  clipdist = result->clipdist[packetoffset];
#endif

  edgeindex[0] = ( RF_AXIS_X << RF_EDGE_AXIS_SHIFT ) | ( packet->vector[packetoffset+(0*RF_PACKETWIDTH)] >= 0.0 );
  edgeindex[1] = ( RF_AXIS_Y << RF_EDGE_AXIS_SHIFT ) | ( packet->vector[packetoffset+(1*RF_PACKETWIDTH)] >= 0.0 );
  edgeindex[2] = ( RF_AXIS_Z << RF_EDGE_AXIS_SHIFT ) | ( packet->vector[packetoffset+(2*RF_PACKETWIDTH)] >= 0.0 );
  vectinv[0] = result->vectinv[packetoffset+(0*RF_PACKETWIDTH)];
  vectinv[1] = result->vectinv[packetoffset+(1*RF_PACKETWIDTH)];
  vectinv[2] = result->vectinv[packetoffset+(2*RF_PACKETWIDTH)];
  vectinv[3] = 1.0;
#if TRACEDISTCLIP || TRACEHITDIST || RFTRACE_SECTOR_TRAVERSAL_VARIANT == 0
  vvectinv = _mm_load_ps( vectinv );
#endif

#if TRACEHITSIDE
  hitside = 0;
#endif
#if TRACEHITSIDE
  hitside = 0;
#endif
  for( ; ; )
  {
    /* Sector traversal */
    int tricount;

#if TRACE_DEBUG
printf( "  Sector %p, edge %f,%f ; %f,%f ; %f,%f\n", root, RF_SECTOR(root)->edge[0], RF_SECTOR(root)->edge[1], RF_SECTOR(root)->edge[2], RF_SECTOR(root)->edge[3], RF_SECTOR(root)->edge[4], RF_SECTOR(root)->edge[5] );
#endif

    __m128 vdisty, vdistz;
#if RFTRACE_SECTOR_TRAVERSAL_VARIANT == 0
    __m128 vedge;
    vedge = _mm_movelh_ps( _mm_unpacklo_ps( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[0] ] ), _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[1] ] ) ), _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[2] ] ) );
    vmindist = _mm_mul_ps( _mm_sub_ps( vedge, vsrc ), vvectinv );
    vdisty = _mm_movehdup_ps( vmindist );
    vdistz = _mm_movehl_ps( vdistz, vmindist );
    nredge = edgeindex[0];
    if( _mm_comilt_ss( vdisty, vmindist ) )
    {
      nredge = edgeindex[1];
      vmindist = vdisty;
    }
    if( _mm_comilt_ss( vdistz, vmindist ) )
    {
      nredge = edgeindex[2];
      vmindist = vdistz;
    }
#elif RFTRACE_SECTOR_TRAVERSAL_VARIANT == 1
    vmindist = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[0] ] ), vsrc ), _mm_set_ss( vectinv[0] ) );
    vdisty = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[1] ] ), _mm_movehdup_ps( vsrc ) ), _mm_set_ss( vectinv[1] ) );
    nredge = edgeindex[0];
    if( _mm_comilt_ss( vdisty, vmindist ) )
    {
      nredge = edgeindex[1];
      vmindist = vdisty;
    }
    vdistz = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[2] ] ), _mm_movehl_ps( vsrc, vsrc ) ), _mm_set_ss( vectinv[2] ) );
    if( _mm_comilt_ss( vdistz, vmindist ) )
    {
      nredge = edgeindex[2];
      vmindist = vdistz;
    }
#elif RFTRACE_SECTOR_TRAVERSAL_VARIANT == 2
    vmindist = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[0] ] ), vsrc ), _mm_set_ss( vectinv[0] ) );
    vdisty = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[1] ] ), _mm_movehdup_ps( vsrc ) ), _mm_set_ss( vectinv[1] ) );
    nredge = edgeindex[0];
    if( _mm_comilt_ss( vdisty, vmindist ) )
      nredge = edgeindex[1];
    vmindist = _mm_min_ss( vmindist, vdisty );
    vdistz = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[2] ] ), _mm_movehl_ps( vsrc, vsrc ) ), _mm_set_ss( vectinv[2] ) );
    if( _mm_comilt_ss( vdistz, vmindist ) )
      nredge = edgeindex[2];
    vmindist = _mm_min_ss( vmindist, vdistz );
#endif

    tricount = RF_SECTOR_GET_PRIMCOUNT( RF_SECTOR(root) );
    if( tricount )
    {
      RF_TRILIST_TYPE *trilist;
      __m128 vzero, vone;

      vdst = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vmindist, vmindist, 0x0 ), vvector ) );
      vzero = _mm_xor_ps( vzero, vzero );
      vone = _mm_set_ss( 1.0 );

      trihit = 0;
      trilist = RF_TRILIST( RF_SECTOR(root) );
      do
      {
        __m128 vtriplane, vdstdist, vsrcdist, vf, vvray, vuv0, vuv1;
        rfTri *tri;

        tri = (rfTri *)RF_ADDRESS( root, (rfssize)(*trilist++) << RF_LINKSHIFT );

        vtriplane = _mm_load_ps( tri->plane );
#if RF_CULLMODE == 0
 #if RFTRACE__SSE4_1__
        vdstdist = _mm_dp_ps( vtriplane, vdst, 0xff );
        if( _mm_comile_ss( vdstdist, vzero ) )
          continue;
        vsrcdist = _mm_dp_ps( vtriplane, vsrc, 0xff );
        if( _mm_comigt_ss( vsrcdist, vzero ) )
          continue;
        vf = _mm_sub_ss( vdstdist, vsrcdist );
        vvray = _mm_sub_ps( _mm_mul_ps( vdstdist, vsrc ), _mm_mul_ps( vsrcdist, vdst ) );
        vuv0 = _mm_dp_ps( _mm_load_ps( tri->edpu ), vvray, 0xff );
        if( _mm_comilt_ss( vuv0, vzero ) || _mm_comigt_ss( vuv0, vf ) )
          continue;
        vuv1 = _mm_dp_ps( _mm_load_ps( tri->edpv ), vvray, 0xff );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vf ) )
          continue;
 #else
        vdstdist = _mm_mul_ps( vtriplane, vdst );
        vdstdist = _mm_hadd_ps( vdstdist, vdstdist );
        vdstdist = _mm_add_ss( vdstdist, _mm_movehdup_ps( vdstdist ) );
        if( _mm_comile_ss( vdstdist, vzero ) )
          continue;
        vsrcdist = _mm_mul_ps( vtriplane, vsrc );
        vsrcdist = _mm_hadd_ps( vsrcdist, vsrcdist );
        vsrcdist = _mm_add_ss( vsrcdist, _mm_movehdup_ps( vsrcdist ) );
        if( _mm_comigt_ss( vsrcdist, vzero ) )
          continue;
        vf = _mm_sub_ss( vdstdist, vsrcdist );
        vdstdist = _mm_shuffle_ps( vdstdist, vdstdist, 0x0 );
        vsrcdist = _mm_shuffle_ps( vsrcdist, vsrcdist, 0x0 );
        vvray = _mm_sub_ps( _mm_mul_ps( vdstdist, vsrc ), _mm_mul_ps( vsrcdist, vdst ) );
        vuv0 = _mm_mul_ps( _mm_load_ps( tri->edpu ), vvray );
        vuv1 = _mm_mul_ps( _mm_load_ps( tri->edpv ), vvray );
        vuv0 = _mm_hadd_ps( vuv0, vuv1 );
        vuv0 = _mm_hadd_ps( vuv0, vuv0 );
        if( _mm_comilt_ss( vuv0, vzero ) )
          continue;
        vuv1 = _mm_movehdup_ps( vuv0 );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vf ) )
          continue;
 #endif
        vf = _mm_div_ss( vone, vf );
        vdst = _mm_mul_ps( vvray, _mm_shuffle_ps( vf, vf, 0x0 ) );
 #if TRACEHITUV
        hituv[0] = _mm_cvtss_f32( _mm_mul_ss( vuv0, vf ) );
        hituv[1] = _mm_cvtss_f32( _mm_mul_ss( vuv1, vf ) );
 #endif
 #if TRACEHITSIDE
        hitside = 0;
 #endif
#elif RF_CULLMODE == 1
 #if RFTRACE__SSE4_1__
        vdstdist = _mm_dp_ps( vtriplane, vdst, 0xff );
        if( _mm_comigt_ss( vdstdist, vzero ) )
          continue;
        vsrcdist = _mm_dp_ps( vtriplane, vsrc, 0xff );
        if( _mm_comile_ss( vsrcdist, vzero ) )
          continue;
        vf = _mm_sub_ss( vdstdist, vsrcdist );
        vvray = _mm_sub_ps( _mm_mul_ps( vdstdist, vsrc ), _mm_mul_ps( vsrcdist, vdst ) );
        vuv0 = _mm_dp_ps( _mm_load_ps( tri->edpu ), vvray, 0xff );
        if( _mm_comigt_ss( vuv0, vzero ) || _mm_comilt_ss( vuv0, vf ) )
          continue;
        vuv1 = _mm_dp_ps( _mm_load_ps( tri->edpv ), vvray, 0xff );
        if( _mm_comigt_ss( vuv1, vzero ) || _mm_comilt_ss( _mm_add_ss( vuv0, vuv1 ), vf ) )
          continue;
 #else
        vdstdist = _mm_mul_ps( vtriplane, vdst );
        vdstdist = _mm_hadd_ps( vdstdist, vdstdist );
        vdstdist = _mm_add_ss( vdstdist, _mm_movehdup_ps( vdstdist ) );
        if( _mm_comigt_ss( vdstdist, vzero ) )
          continue;
        vsrcdist = _mm_mul_ps( vtriplane, vsrc );
        vsrcdist = _mm_hadd_ps( vsrcdist, vsrcdist );
        vsrcdist = _mm_add_ss( vsrcdist, _mm_movehdup_ps( vsrcdist ) );
        if( _mm_comile_ss( vsrcdist, vzero ) )
          continue;
        vf = _mm_sub_ss( vdstdist, vsrcdist );
        vdstdist = _mm_shuffle_ps( vdstdist, vdstdist, 0x0 );
        vsrcdist = _mm_shuffle_ps( vsrcdist, vsrcdist, 0x0 );
        vvray = _mm_sub_ps( _mm_mul_ps( vdstdist, vsrc ), _mm_mul_ps( vsrcdist, vdst ) );
        vuv0 = _mm_mul_ps( _mm_load_ps( tri->edpu ), vvray );
        vuv1 = _mm_mul_ps( _mm_load_ps( tri->edpv ), vvray );
        vuv0 = _mm_hadd_ps( vuv0, vuv1 );
        vuv0 = _mm_hadd_ps( vuv0, vuv0 );
        if( _mm_comigt_ss( vuv0, vzero ) )
          continue;
        vuv1 = _mm_movehdup_ps( vuv0 );
        if( _mm_comigt_ss( vuv1, vzero ) || _mm_comilt_ss( _mm_add_ss( vuv0, vuv1 ), vf ) )
          continue;
 #endif
        vf = _mm_div_ss( vone, vf );
        vdst = _mm_mul_ps( vvray, _mm_shuffle_ps( vf, vf, 0x0 ) );
 #if TRACEHITUV
        hituv[0] = _mm_cvtss_f32( _mm_mul_ss( vuv0, vf ) );
        hituv[1] = _mm_cvtss_f32( _mm_mul_ss( vuv1, vf ) );
 #endif
 #if TRACEHITSIDE
        hitside = 1;
 #endif
#else
 #if RFTRACE__SSE4_1__
        vdstdist = _mm_dp_ps( vtriplane, vdst, 0xff );
        vsrcdist = _mm_dp_ps( vtriplane, vsrc, 0xff );
        if( _mm_comigt_ss( _mm_mul_ss( vdstdist, vsrcdist ), vzero ) )
          continue;
        vf = _mm_div_ss( vsrcdist, _mm_sub_ss( vsrcdist, vdstdist ) );
        vvray = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vf, vf, 0x0 ), _mm_sub_ps( vdst, vsrc ) ) );
        vf = _mm_div_ss( vsrcdist, _mm_sub_ss( vsrcdist, vdstdist ) );
        vvray = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vf, vf, 0x0 ), _mm_sub_ps( vdst, vsrc ) ) );
        vuv0 = _mm_dp_ps( _mm_load_ps( tri->edpu ), vvray, 0xff );
        if( !( _mm_comige_ss( vuv0, vzero ) ) || _mm_comigt_ss( vuv0, vone ) )
          continue;
        vuv1 = _mm_dp_ps( _mm_load_ps( tri->edpv ), vvray, 0xff );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vone ) )
          continue;
 #else
        vdstdist = _mm_mul_ps( vtriplane, vdst );
        vsrcdist = _mm_mul_ps( vtriplane, vsrc );
        vsrcdist = _mm_hadd_ps( vsrcdist, vdstdist );
        vsrcdist = _mm_hadd_ps( vsrcdist, vsrcdist );
        vdstdist = _mm_movehdup_ps( vsrcdist );
        if( _mm_comigt_ss( _mm_mul_ss( vdstdist, vsrcdist ), vzero ) )
          continue;
        vf = _mm_div_ss( vsrcdist, _mm_sub_ss( vsrcdist, vdstdist ) );
        vvray = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vf, vf, 0x0 ), _mm_sub_ps( vdst, vsrc ) ) );
  #if 1
        vuv0 = _mm_mul_ps( _mm_load_ps( tri->edpu ), vvray );
        vuv1 = _mm_mul_ps( _mm_load_ps( tri->edpv ), vvray );
        vuv0 = _mm_hadd_ps( vuv0, vuv1 );
        vuv0 = _mm_hadd_ps( vuv0, vuv0 );
        if( !( _mm_comige_ss( vuv0, vzero ) ) )
          continue;
        vuv1 = _mm_movehdup_ps( vuv0 );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vone ) )
          continue;
  #else
        vuv0 = _mm_mul_ps( _mm_load_ps( tri->edpu ), vvray );
        vuv0 = _mm_hadd_ps( vuv0, vuv0 );
        vuv0 = _mm_add_ss( vuv0, _mm_movehdup_ps( vuv0 ) );
        if( !( _mm_comige_ss( vuv0, vzero ) ) || _mm_comigt_ss( vuv0, vone ) )
          continue;
        vuv1 = _mm_mul_ps( _mm_load_ps( tri->edpv ), vvray );
        vuv1 = _mm_hadd_ps( vuv1, vuv1 );
        vuv1 = _mm_add_ss( vuv1, _mm_movehdup_ps( vuv1 ) );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vone ) )
          continue;
  #endif
 #endif
        vdst = vvray;
 #if TRACEHITUV
        hituv[0] = _mm_cvtss_f32( vuv0 );
        hituv[1] = _mm_cvtss_f32( vuv1 );
 #endif
 #if TRACEHITSIDE
        hitside = _mm_comilt_ss( vsrcdist, vzero );
 #endif
#endif
        trihit = tri;
      } while( --tricount );

      if( trihit )
        break;
    }

#if TRACEDISTCLIP
    if( _mm_cvtss_f32( vmindist ) > clipdist )
      goto traceclip;
#endif

    /* Traverse through the sector's edge */
    if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
    {
      /* Neighbor is sector */
      slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
      if( !( slink ) )
      {
 #if TRACEHITROOT
        result->hitroot[packetoffset] = 0;
 #endif
        return;
      }
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
      continue;
    }

    /* Neighbor is node */
#if TRACEDISTCLIP
    clipdist -= _mm_cvtss_f32( vmindist );
#endif
    vsrc = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vmindist, vmindist, 0x0 ), vvector ) );
#if !RF_NODE_SSE_TRAVERSAL
    _mm_store_ps( src, vsrc );
#endif
    root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );

    /* Node traversal */
    for( ; ; )
    {
#if TRACE_DEBUG
printf( "  Node %p, axis %d, plane %f\n", root, RF_NODE_GET_AXIS( RF_NODE(root)->flags ), RF_NODE(root)->plane );
#endif
      int linkflags;
      linkflags = RF_NODE(root)->flags;
#if RF_NODE_SSE_TRAVERSAL
      linkflags >>= RF_NODE_AXISMASK_SHIFT;
      if( _mm_movemask_ps( _mm_sub_ps( _mm_set1_ps( RF_NODE(root)->plane ), vsrc ) ) & linkflags )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( RF_LINK_SECTOR << (RF_NODE_DUPLINKFLAGS_MORE_SHIFT-RF_NODE_AXISMASK_SHIFT) ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( RF_LINK_SECTOR << (RF_NODE_DUPLINKFLAGS_LESS_SHIFT-RF_NODE_AXISMASK_SHIFT) ) )
          break;
      }
#else
      if( _mm_comilt_ss( _mm_load_ss( &src[ RF_NODE_GET_AXIS(linkflags) ] ), _mm_load_ss( &RF_NODE(root)->plane ) ) )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
#endif
    }
  }

#if TRACEDISTCLIP || TRACEHITDIST
  vhitdist = _mm_mul_ps( _mm_sub_ps( vdst, _mm_set_ps( 0.0, packet->origin[packetoffset+(2*RF_PACKETWIDTH)], packet->origin[packetoffset+(1*RF_PACKETWIDTH)], packet->origin[packetoffset+(0*RF_PACKETWIDTH)] ) ), vvectinv );
  if( nredge >= 4 )
    vhitdist = _mm_movehl_ps( vhitdist, vhitdist );
  else if( nredge >= 2 )
    vhitdist = _mm_movehdup_ps( vhitdist );
 #if TRACEDISTCLIP
  if( _mm_cvtss_f32( vhitdist ) >= clipdist )
    goto traceclip;
 #endif
#endif

  /* Result write */
  result->hitmask |= 1 << packetoffset;
#if TRACEHITMODELDATA
#endif
#if TRACEHITOBJECTDATA
#endif
#if TRACEHITVECTOR
#endif
#if TRACEHITPOINT
  result->hitpt[packetoffset+(0*RF_PACKETWIDTH)] = dst[0];
  result->hitpt[packetoffset+(1*RF_PACKETWIDTH)] = dst[1];
  result->hitpt[packetoffset+(2*RF_PACKETWIDTH)] = dst[2];
#endif
#if TRACEHITUV
  result->hituv[packetoffset+(0*RF_PACKETWIDTH)] = hituv[0];
  result->hituv[packetoffset+(1*RF_PACKETWIDTH)] = hituv[1];
#endif
#if TRACEHITDATA || TRACEHITPLANE
  result->hittri[packetoffset] = trihit;
#endif
#if TRACEHITDIST
  result->hitdist[packetoffset] = hitdist;
#endif
#if TRACEHITPLANE
#endif
#if TRACEHITSIDE
  result->hitsidemask |= hitside << packetoffset;
#endif
#if TRACEHITROOT
  result->hitroot[packetoffset] = (rfRoot)RF_ADDRESSDIFF( root, handle->objectgraph );
#endif
#if TRACEHITMATRIX
  result->hitmatrix[packetoffset] = 0;
#endif

  return;
}


