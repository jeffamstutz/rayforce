/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#if RF_PACKETWIDTH == 8
 #if TRACEPARAMPASSBYREFERENCE
  #ifdef TRACERAYSTORE
   #define RF_DECLPARAM TRACEPARAM *param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM &param, &raystore
  #else
   #define RF_DECLPARAM TRACEPARAM *param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM &param
  #endif
 #else
  #ifdef TRACERAYSTORE
   #define RF_DECLPARAM TRACEPARAM param, TRACERAYSTORE *raystore
   #define RF_PASSPARAM param, &raystore
  #else
   #define RF_DECLPARAM TRACEPARAM param
   #define RF_PASSPARAM param
  #endif
 #endif
#endif


#if RFTRACE__SSE__
 #include "path4sse.h"
#else
 #include "path4.h"
#endif


#if RF_PACKETWIDTH > 8
static void RF_TRACE8NAME( const rfHandle * RF_RESTRICT handle, RF_DECLPARAM, const RF_TRACEPACKET * RF_RESTRICT packet, int packetoffset, RF_TRACERESULT * RF_RESTRICT result )
#else
static void RF_TRACE8NAME( const rfHandle * RF_RESTRICT handle, TRACEPARAM param, const RF_TRACEPACKET * RF_RESTRICT packet )
#endif
{
#if RF_PACKETWIDTH == 8
  RF_TRACERESULT result;
 #define RF_RESULT (&result)
#else
 #define RF_RESULT (result)
#endif
  RF_TRACE4NAME( handle, RF_PASSPARAM, packet, 0, RF_RESULT );
  RF_TRACE4NAME( handle, RF_PASSPARAM, packet, 4, RF_RESULT );
  return;
}

/* Special internally defined */
#undef RF_RESULT
#undef RF_PACKETOFFSET

/* Externally defined */
#if RF_PACKETWIDTH == 8
 #undef RF_CULLMODE
 #undef RF_ADDRBITS
 #undef RF_TRACE1NAME
 #undef RF_TRACE4NAME
 #undef RF_TRACE8NAME
 #undef RF_RESOLVENAME
 #undef RF_RESOLVE4NAME
 #undef RF_RESOLVE8NAME
 #undef RF_DISPLACENAME
 #undef RF_DISPLACE4NAME
 #undef RF_DISPLACE8NAME

 #undef RF_DECLPARAM
 #undef RF_PASSPARAM
 #undef RF_TRACEPACKET
 #undef RF_TRACERESULT
 #undef RF_PACKETWIDTH
#endif

