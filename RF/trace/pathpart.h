/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*
#ifndef TRACE_DEBUG
 #define TRACE_DEBUG (0)
#endif
*/


////


static void RF_TRACE1NAME( const rfHandle * RF_RESTRICT handle, RF_DECLPARAM, const RF_TRACEPACKET * RF_RESTRICT packet, int packetoffset, RF_TRACERESULT * RF_RESTRICT result, void *root, int rootflag )
{
  int nredge;
  rfssize slink;
  rff vector[4];
#if TRACEHITSIDE
  int hitside;
#endif
  rff vectinv[4];
  int edgeindex[3];
  rff src[4], dst[4], dist, mindist;
#if TRACEHITUV
  rff hituv[2];
#endif
#if TRACEDISTCLIP
  rff clipdist;
#endif
#if TRACEDISTCLIP || TRACEHITDIST
  rff hitdist;
#endif
  rfTri *trihit;
#if TRACEDISTCLIP || TRACEHITDIST
  int axisindex;
#endif
#ifdef TRACERAYSTORE
  TRACERAYSTORE raystore;
#endif
#if TRACEHITMODELDATA
  void *modeldata;
#endif

#if TRACEHITMODELDATA
  modeldata = RF_ADDRESS( handle->objectgraph, ((const rfGraphHeader * RF_RESTRICT)handle->objectgraph)->modeldataoffset );
#endif

  vector[0] = packet->vector[packetoffset+(0*RF_PACKETWIDTH)];
  vector[1] = packet->vector[packetoffset+(1*RF_PACKETWIDTH)];
  vector[2] = packet->vector[packetoffset+(2*RF_PACKETWIDTH)];
  vector[3] = 0.0;

  src[0] = result->raysrc[packetoffset+(0*RF_PACKETWIDTH)];
  src[1] = result->raysrc[packetoffset+(1*RF_PACKETWIDTH)];
  src[2] = result->raysrc[packetoffset+(2*RF_PACKETWIDTH)];
  src[3] = 1.0;

  if( !( rootflag ) )
  {
    for( ; ; )
    {
      int linkflags;
      linkflags = RF_NODE(root)->flags;
      if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
    }
  }

#if TRACEDISTCLIP
  clipdist = result->clipdist[packetoffset];
#endif

  edgeindex[0] = ( RF_AXIS_X << RF_EDGE_AXIS_SHIFT ) | ( vector[0] >= 0.0 );
  edgeindex[1] = ( RF_AXIS_Y << RF_EDGE_AXIS_SHIFT ) | ( vector[1] >= 0.0 );
  edgeindex[2] = ( RF_AXIS_Z << RF_EDGE_AXIS_SHIFT ) | ( vector[2] >= 0.0 );
  vectinv[0] = result->vectinv[packetoffset+(0*RF_PACKETWIDTH)];
  vectinv[1] = result->vectinv[packetoffset+(1*RF_PACKETWIDTH)];
  vectinv[2] = result->vectinv[packetoffset+(2*RF_PACKETWIDTH)];
  vectinv[3] = 1.0;

#if TRACEHITSIDE
  hitside = 0;
#endif
  for( ; ; )
  {
    /* Sector traversal */
    int tricount;

#if TRACE_DEBUG
printf( "  Sector %p, edge %f,%f ; %f,%f ; %f,%f\n", root, RF_SECTOR(root)->edge[0], RF_SECTOR(root)->edge[1], RF_SECTOR(root)->edge[2], RF_SECTOR(root)->edge[3], RF_SECTOR(root)->edge[4], RF_SECTOR(root)->edge[5] );
#endif

    nredge = edgeindex[0];
    mindist = ( RF_SECTOR(root)->edge[ edgeindex[0] ] - src[0] ) * vectinv[0];
    dist = ( RF_SECTOR(root)->edge[ edgeindex[1] ] - src[1] ) * vectinv[1];
    if( dist < mindist )
    {
      nredge = edgeindex[1];
      mindist = dist;
    }
    dist = ( RF_SECTOR(root)->edge[ edgeindex[2] ] - src[2] ) * vectinv[2];
    if( dist < mindist )
    {
      nredge = edgeindex[2];
      mindist = dist;
    }

    tricount = RF_SECTOR_GET_PRIMCOUNT( RF_SECTOR(root) );
    if( tricount )
    {
      RF_TRILIST_TYPE *trilist;

      dst[0] = src[0] + ( mindist * vector[0] );
      dst[1] = src[1] + ( mindist * vector[1] );
      dst[2] = src[2] + ( mindist * vector[2] );

      trihit = 0;
      trilist = RF_TRILIST( RF_SECTOR(root) );
      do
      {
        rff dstdist, srcdist, uv[2], f, vray[3];
        rfTri *tri;

        tri = (rfTri *)RF_ADDRESS( root, (rfssize)(*trilist++) << RF_LINKSHIFT );

#if RF_CULLMODE == 0
        dstdist = rfMathPlanePoint( tri->plane, dst );
        if( dstdist <= (rff)0.0 )
          continue;
        srcdist = rfMathPlanePoint( tri->plane, src );
        if( srcdist > (rff)0.0 )
          continue;
        f = dstdist - srcdist;
        vray[0] = ( src[0] * dstdist ) - ( dst[0] * srcdist );
        vray[1] = ( src[1] * dstdist ) - ( dst[1] * srcdist );
        vray[2] = ( src[2] * dstdist ) - ( dst[2] * srcdist );
        uv[0] = rfMathVectorDotProduct( &tri->edpu[0], vray ) + ( tri->edpu[3] * f );
        if( ( uv[0] < (rff)0.0 ) || ( uv[0] > f ) )
          continue;
        uv[1] = rfMathVectorDotProduct( &tri->edpv[0], vray ) + ( tri->edpv[3] * f );
        if( ( uv[1] < (rff)0.0 ) || ( ( uv[0] + uv[1] ) > f ) )
          continue;
        f = (rff)1.0 / f;
        dst[0] = vray[0] * f;
        dst[1] = vray[1] * f;
        dst[2] = vray[2] * f;
 #if TRACEHITUV
        hituv[0] = uv[0] * f;
        hituv[1] = uv[1] * f;
 #endif
 #if TRACEHITSIDE
        hitside = 0;
 #endif
#elif RF_CULLMODE == 1
        dstdist = rfMathPlanePoint( tri->plane, dst );
        if( dstdist > (rff)0.0 )
          continue;
        srcdist = rfMathPlanePoint( tri->plane, src );
        if( srcdist <= (rff)0.0 )
          continue;
        f = dstdist - srcdist;
        vray[0] = ( src[0] * dstdist ) - ( dst[0] * srcdist );
        vray[1] = ( src[1] * dstdist ) - ( dst[1] * srcdist );
        vray[2] = ( src[2] * dstdist ) - ( dst[2] * srcdist );
        uv[0] = rfMathVectorDotProduct( &tri->edpu[0], vray ) + ( tri->edpu[3] * f );
        if( ( uv[0] > (rff)0.0 ) || ( uv[0] < f ) )
          continue;
        uv[1] = rfMathVectorDotProduct( &tri->edpv[0], vray ) + ( tri->edpv[3] * f );
        if( ( uv[1] > (rff)0.0 ) || ( ( uv[0] + uv[1] ) < f ) )
          continue;
        f = (rff)1.0 / f;
        dst[0] = vray[0] * f;
        dst[1] = vray[1] * f;
        dst[2] = vray[2] * f;
 #if TRACEHITUV
        hituv[0] = uv[0] * f;
        hituv[1] = uv[1] * f;
 #endif
 #if TRACEHITSIDE
        hitside = 1;
 #endif
#else
        dstdist = rfMathPlanePoint( tri->plane, dst );
        srcdist = rfMathPlanePoint( tri->plane, src );
        if( dstdist * srcdist > (rff)0.0 )
          continue;
        f = srcdist / ( srcdist - dstdist );
        vray[0] = src[0] + f * ( dst[0] - src[0] );
        vray[1] = src[1] + f * ( dst[1] - src[1] );
        vray[2] = src[2] + f * ( dst[2] - src[2] );
        uv[0] = ( rfMathVectorDotProduct( &tri->edpu[0], vray ) + tri->edpu[3] );
        if( !( uv[0] >= (rff)0.0 ) || ( uv[0] > (rff)1.0 ) )
          continue;
        uv[1] = ( rfMathVectorDotProduct( &tri->edpv[0], vray ) + tri->edpv[3] );
        if( ( uv[1] < (rff)0.0 ) || ( ( uv[0] + uv[1] ) > (rff)1.0 ) )
          continue;
        dst[0] = vray[0];
        dst[1] = vray[1];
        dst[2] = vray[2];
 #if TRACEHITUV
        hituv[0] = uv[0];
        hituv[1] = uv[1];
 #endif
 #if TRACEHITSIDE
        hitside = ( srcdist < (rff)0.0 );
 #endif
#endif

        trihit = tri;
      } while( --tricount );

      if( trihit )
        break;
    }

#if TRACEDISTCLIP
    if( mindist >= clipdist )
    {
 #if TRACEHITROOT
      result->hitroot[packetoffset] = RF_ADDRESSDIFF( root, handle->objectgraph );
 #endif
      return;
    }
#endif

    /* Traverse through the sector's edge */
    if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
    {
      /* Neighbor is sector */
      slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
      if( !( slink ) )
      {
 #if TRACEHITROOT
        result->hitroot[packetoffset] = 0;
 #endif
        return;
      }
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
      continue;
    }

    /* Neighbor is node */
#if TRACEDISTCLIP
    clipdist -= mindist;
#endif
    src[0] += mindist * vector[0];
    src[1] += mindist * vector[1];
    src[2] += mindist * vector[2];
    root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );

    /* Node traversal */
#if 0
    if( nredge < 2 )
    {
      for( ; ; )
      {
        int linkflags;
        rff nodesrc;
        linkflags = RF_NODE(root)->flags;
        nodesrc = ( RF_NODE_TEST_AXIS_Y(linkflags) ? src[1] : src[2] );
        if( nodesrc < RF_NODE(root)->plane )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
      }
    }
    else if( nredge < 4 )
    {
      for( ; ; )
      {
        int linkflags;
        rff nodesrc;
        linkflags = RF_NODE(root)->flags;
        nodesrc = ( RF_NODE_TEST_AXIS_X(linkflags) ? src[0] : src[2] );
        if( nodesrc < RF_NODE(root)->plane )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
      }
    }
    else
    {
      for( ; ; )
      {
        int linkflags;
        rff nodesrc;
        linkflags = RF_NODE(root)->flags;
        nodesrc = ( RF_NODE_TEST_AXIS_X(linkflags) ? src[0] : src[1] );
        if( nodesrc < RF_NODE(root)->plane )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
      }
    }
#else
    for( ; ; )
    {
#if TRACE_DEBUG
printf( "  Node %p\n", root );
printf( "  Node %p, axis %d, plane %f\n", root, RF_NODE_GET_AXIS( RF_NODE(root)->flags ), RF_NODE(root)->plane );
#endif
      int linkflags;
      linkflags = RF_NODE(root)->flags;
      if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
    }
#endif

  }

#if TRACEDISTCLIP || TRACEHITDIST
  axisindex = nredge >> 1;
  hitdist = ( dst[axisindex] - packet->origin[packetoffset+(axisindex*RF_PACKETWIDTH)] ) * vectinv[axisindex];
 #if TRACEDISTCLIP
  if( hitdist > clipdist )
  {
 #if TRACEHITROOT
    result->hitroot[packetoffset] = 0;
 #endif
    return;
  }
 #endif
#endif

  /* Result write */
  result->hitmask |= 1 << packetoffset;
#if TRACEHITMODELDATA
#endif
#if TRACEHITOBJECTDATA
#endif
#if TRACEHITVECTOR
#endif
#if TRACEHITPOINT
  result->hitpt[packetoffset+(0*RF_PACKETWIDTH)] = dst[0];
  result->hitpt[packetoffset+(1*RF_PACKETWIDTH)] = dst[1];
  result->hitpt[packetoffset+(2*RF_PACKETWIDTH)] = dst[2];
#endif
#if TRACEHITUV
  result->hituv[packetoffset+(0*RF_PACKETWIDTH)] = utd;
  result->hituv[packetoffset+(1*RF_PACKETWIDTH)] = vtd;
#endif
#if TRACEHITDATA || TRACEHITPLANE
  result->hittri[packetoffset] = trihit;
#endif
#if TRACEHITDIST
  result->hitdist[packetoffset] = hitdist;
#endif
#if TRACEHITPLANE
#endif
#if TRACEHITSIDE
  result->hitsidemask |= hitside << packetoffset;
#endif
#if TRACEHITROOT
  result->hitroot[packetoffset] = (rfRoot)RF_ADDRESSDIFF( root, handle->objectgraph );
#endif
#if TRACEHITMATRIX
  result->hitmatrix[packetoffset] = 0;
#endif

  return;
}


