/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*
#ifndef TRACE_DEBUG
 #define TRACE_DEBUG (0)
#endif
*/

 #define TRACE_DEBUG (0)


#if RF_ADDRBITS == 16
 #define RF_SECTOR(x) ((const rfSector16 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode16 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK16_SHIFT)
 #define RF_TRILIST RF_SECTOR16_TRILIST
 #define RF_TRILIST_TYPE int16_t
#elif RF_ADDRBITS == 32
 #define RF_SECTOR(x) ((const rfSector32 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode32 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK32_SHIFT)
 #define RF_TRILIST RF_SECTOR32_TRILIST
 #define RF_TRILIST_TYPE int32_t
#else
 #define RF_SECTOR(x) ((const rfSector64 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode64 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK64_SHIFT)
 #define RF_TRILIST RF_SECTOR64_TRILIST
 #define RF_TRILIST_TYPE int64_t
#endif

#if TRACEPARAMPASSBYREFERENCE
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM &param, &raystore
 #else
  #define RF_PASSPARAM &param
 #endif
#else
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM param, &raystore
 #else
  #define RF_PASSPARAM param
 #endif
#endif


////


#define RF_STACK_DEPTH (64)


#define RF_TEST_OBJECT_INTERSECTION (1)


#define RF_ELEM_PREPARE_AXIS(axis) \
  edgeindex[axis] = ( axis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN; \
  vectinv[axis] = -RFF_MAX; \
  if( rffabs( vector[axis] ) > (rff)0.0 ) \
  { \
    vectinv[axis] = (rff)1.0 / vector[axis]; \
    if( vector[axis] >= (rff)0.0 ) \
      edgeindex[axis] = ( axis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX; \
  }


static void RF_TRACENAME( rfHandle *handle, TRACEPARAM param, rfRoot rootdef, rff *origin, rff *rayvector
#if TRACEDISTCLIP
, rff clipdistbase
#endif
)
{
  int nredge;
  rfssize slink;
#if TRACEHITSIDE
  int hitside;
#endif
  rff vectinv[3];
  int edgeindex[3];
  rff src[3], dst[3], dist, mindist, hitdist;
#if TRACEHITUV
  rff hituv[2];
#endif
  void *root;
  rfTri *trihit;
  void *modeldata;
  void *objectdata;
  void *objectmatrix;
  int axisindex;
#ifdef TRACERAYSTORE
  TRACERAYSTORE raystore;
#endif
  rff clipdist, sumdist;
  rfRegion *region;
  int objlistindex;
  int32_t objectindex;
  rff vector[3];
#ifdef TRACEVOID
  int voidflag;
#endif
  uint32_t stack[RF_STACK_DEPTH];
  int stackoffset;
#if RF_CULLOBJTEST
  int hitsideskip, trihitside;
#endif

  /* Clipping distance, initialize to scene diagonal size */
#if TRACEDISTCLIP
  clipdist = fminf( clipdistbase, RFF_MAX );
#else
  clipdist = RFF_MAX;
#endif

  /* Acquire root region */
  region = RF_ADDRESS( handle->scenegraph, ((rfSceneHeader *)handle->scenegraph)->rootoffset );
  objlistindex = 0;

#ifdef TRACEVOID
  voidflag = 1;
#endif
  stackoffset = 0;
  for( ; ; )
  {
    /* Compute dst[] for scene traversal */
    vectinv[0] = -RFF_MAX;
    if( rffabs( rayvector[0] ) > (rff)0.0 )
      vectinv[0] = (rff)1.0 / rayvector[0];
    vectinv[1] = -RFF_MAX;
    if( rffabs( rayvector[1] ) > (rff)0.0 )
      vectinv[1] = (rff)1.0 / rayvector[1];
    vectinv[2] = -RFF_MAX;
    if( rffabs( rayvector[2] ) > (rff)0.0 )
      vectinv[2] = (rff)1.0 / rayvector[2];

    objectindex = -2;
    do
    {
      if( region->objectcount )
      {
        /* Leaf region */
        int32_t *objlist;
        objlist = RF_REGIONLEAF_OBJECTLIST(region);
#if RF_TEST_OBJECT_INTERSECTION
        for( ; objlistindex < region->objectcount ; objlistindex++ )
        {
          rfSceneObjectEntry *objentry;
          rff t1, t2, t3, t4, t5, t6, tmin, tmax;

          objectindex = objlist[objlistindex];
          objentry = &RF_SCENEHEADER_OBJECTTABLE(handle->scenegraph)[objectindex];

          t1 = ( objentry->edge[RF_EDGE_MINX] - origin[0] ) * vectinv[0];
          t2 = ( objentry->edge[RF_EDGE_MAXX] - origin[0] ) * vectinv[0];
          t3 = ( objentry->edge[RF_EDGE_MINY] - origin[1] ) * vectinv[1];
          t4 = ( objentry->edge[RF_EDGE_MAXY] - origin[1] ) * vectinv[1];
          t5 = ( objentry->edge[RF_EDGE_MINZ] - origin[2] ) * vectinv[2];
          t6 = ( objentry->edge[RF_EDGE_MAXZ] - origin[2] ) * vectinv[2];
          tmin = fmaxf( fmaxf( fminf( t1, t2 ), fminf( t3, t4 ) ), fmaxf( fminf( t5, t6 ), 0.0 ) );
          tmax = fminf( fminf( fmaxf( t1, t2 ), fmaxf( t3, t4 ) ), fminf( fmaxf( t5, t6 ), clipdist ) );
          if( tmax >= tmin )
            break;
        }
        if( objlistindex < region->objectcount )
          objlistindex++;
        else
        {
          objectindex = -1;
          if( stackoffset > 0 )
          {
            objectindex = -2;
            stackoffset -= sizeof(uint32_t);
            region = RF_ADDRESS( handle->scenegraph, *(uint32_t *)RF_ADDRESS( stack, stackoffset ) );
            objlistindex = 0;
          }
        }
#else
        if( objlistindex < region->objectcount )
          objectindex = objlist[objlistindex++];
        else
        {
          objectindex = -1;
          if( stackoffset > 0 )
          {
            objectindex = -2;
            stackoffset -= sizeof(uint32_t);
            region = RF_ADDRESS( handle->scenegraph, *(uint32_t *)RF_ADDRESS( stack, stackoffset ) );
            objlistindex = 0;
          }
        }
#endif
      }
      else
      {
        /* Regular region */
        int childflags;
        rff t1, t2, t3, t4, t5, t6, tminleft, tminright, tmax;

        childflags = 0x0;

        t1 = ( region->leftedge[RF_EDGE_MINX] - origin[0] ) * vectinv[0];
        t2 = ( region->leftedge[RF_EDGE_MAXX] - origin[0] ) * vectinv[0];
        t3 = ( region->leftedge[RF_EDGE_MINY] - origin[1] ) * vectinv[1];
        t4 = ( region->leftedge[RF_EDGE_MAXY] - origin[1] ) * vectinv[1];
        t5 = ( region->leftedge[RF_EDGE_MINZ] - origin[2] ) * vectinv[2];
        t6 = ( region->leftedge[RF_EDGE_MAXZ] - origin[2] ) * vectinv[2];
        tminleft = fmaxf( fmaxf( fminf( t1, t2 ), fminf( t3, t4 ) ), fmaxf( fminf( t5, t6 ), 0.0 ) );
        tmax = fminf( fminf( fmaxf( t1, t2 ), fmaxf( t3, t4 ) ), fminf( fmaxf( t5, t6 ), clipdist ) );
        if( tminleft <= tmax )
          childflags |= 0x1;

        t1 = ( region->rightedge[RF_EDGE_MINX] - origin[0] ) * vectinv[0];
        t2 = ( region->rightedge[RF_EDGE_MAXX] - origin[0] ) * vectinv[0];
        t3 = ( region->rightedge[RF_EDGE_MINY] - origin[1] ) * vectinv[1];
        t4 = ( region->rightedge[RF_EDGE_MAXY] - origin[1] ) * vectinv[1];
        t5 = ( region->rightedge[RF_EDGE_MINZ] - origin[2] ) * vectinv[2];
        t6 = ( region->rightedge[RF_EDGE_MAXZ] - origin[2] ) * vectinv[2];
        tminright = fmaxf( fmaxf( fminf( t1, t2 ), fminf( t3, t4 ) ), fmaxf( fminf( t5, t6 ), 0.0 ) );
        tmax = fminf( fminf( fmaxf( t1, t2 ), fmaxf( t3, t4 ) ), fminf( fmaxf( t5, t6 ), clipdist ) );
        if( tminright <= tmax )
          childflags |= 0x2;

        if( childflags == 0x0 )
        {
          objectindex = -1;
          if( stackoffset > 0 )
          {
            objectindex = -2;
            stackoffset -= sizeof(uint32_t);
            region = RF_ADDRESS( handle->scenegraph, *(uint32_t *)RF_ADDRESS( stack, stackoffset ) );
          }
        }
        else if( childflags == 0x1 )
          region = RF_ADDRESS( region, region->childleft );
        else if( childflags == 0x2 )
          region = RF_ADDRESS( region, region->childright );
        else
        {
          /* We intersect both children */
          if( tminright > tminleft )
          {
            *(uint32_t *)RF_ADDRESS( stack, stackoffset ) = RF_ADDRESSDIFF( region, handle->scenegraph ) + region->childright;
            stackoffset += sizeof(uint32_t);
            region = RF_ADDRESS( region, region->childleft );
          }
          else
          {
            *(uint32_t *)RF_ADDRESS( stack, stackoffset ) = RF_ADDRESSDIFF( region, handle->scenegraph ) + region->childleft;
            stackoffset += sizeof(uint32_t);
            region = RF_ADDRESS( region, region->childright );
          }
        }
      }
    } while( objectindex < -1 );

    /* Test intersection between ray and object */
    if( objectindex >= 0 )
    {
      rfSceneObjectEntry *objentry;

      objentry = &RF_SCENEHEADER_OBJECTTABLE(handle->scenegraph)[objectindex];

      /* Transform origin and vector to object space, store in src[] and vector[] */
      rfMathMatrixMulPoint( src, origin, objentry->matrixinv );
      rfMathMatrixMulVector( vector, rayvector, objentry->matrixinv );

      modeldata = RF_ADDRESS( objentry->objectgraph, ((const rfGraphHeader * RF_RESTRICT)objentry->objectgraph)->modeldataoffset );
      objectdata = objentry->objectdata;
      objectmatrix = objentry->matrix;
#if RF_CULLOBJTEST
      hitsideskip = objentry->hitsideskip;
#endif

#if TRACETESTROOT
      root = 0;
      if( rootdef )
      {
#endif

#if TRACERESOLVEROOT
      root = RF_RESOLVENAME( objentry->objectgraph, src );
#elif TRACEDISPLACEROOT
      root = RF_RESOLVENAME( objentry->objectgraph, src );
#elif TRACEQUADROOT
      root = 0;
      if( rootdef )
      {
 #if ( RF_CONFIG_ROOT_FORMAT == 0 ) || ( RF_CONFIG_ROOT_FORMAT == 1 )
      root = RF_RESOLVENAME( objentry->objectgraph, src );
 #else
      if( rootdef->extra == objectindex )
        root = RF_ADDRESS( objentry->objectgraph, rootdef->graphoffset );
      else
        root = RF_RESOLVENAME( objentry->objectgraph, src );
 #endif
#else
 #if ( RF_CONFIG_ROOT_FORMAT == 0 ) || ( RF_CONFIG_ROOT_FORMAT == 1 )
      root = RF_RESOLVENAME( objentry->objectgraph, src );
 #else
      if( rootdef->extra == objectindex )
        root = RF_ADDRESS( objentry->objectgraph, rootdef->graphoffset );
      else
        root = RF_RESOLVENAME( objentry->objectgraph, src );
 #endif
#endif

#if TRACE_DEBUG
printf( "Trace Object %d : %f %f %f : %f %f %f\n", objectindex, src[0], src[1], src[2], vector[0], vector[1], vector[2] );
#endif

      RF_ELEM_PREPARE_AXIS( 0 );
      RF_ELEM_PREPARE_AXIS( 1 );
      RF_ELEM_PREPARE_AXIS( 2 );

#if TRACEHITSIDE
      hitside = 0;
#endif
      trihit = 0;
      sumdist = 0.0;
      for( ; ; )
      {
        /* Sector traversal */
        int tricount;

#if TRACE_DEBUG
printf( "  Sector %p, edge %f,%f ; %f,%f ; %f,%f\n", root, RF_SECTOR(root)->edge[0], RF_SECTOR(root)->edge[1], RF_SECTOR(root)->edge[2], RF_SECTOR(root)->edge[3], RF_SECTOR(root)->edge[4], RF_SECTOR(root)->edge[5] );
#endif

        nredge = edgeindex[0];
        mindist = ( RF_SECTOR(root)->edge[ edgeindex[0] ] - src[0] ) * vectinv[0];
        dist = ( RF_SECTOR(root)->edge[ edgeindex[1] ] - src[1] ) * vectinv[1];
        if( dist < mindist )
        {
          nredge = edgeindex[1];
          mindist = dist;
        }
        dist = ( RF_SECTOR(root)->edge[ edgeindex[2] ] - src[2] ) * vectinv[2];
        if( dist < mindist )
        {
          nredge = edgeindex[2];
          mindist = dist;
        }

        tricount = RF_SECTOR_GET_PRIMCOUNT( RF_SECTOR(root) );
        if( tricount )
        {
          RF_TRILIST_TYPE *trilist;

          dst[0] = src[0] + ( mindist * vector[0] );
          dst[1] = src[1] + ( mindist * vector[1] );
          dst[2] = src[2] + ( mindist * vector[2] );

          trilist = RF_TRILIST( RF_SECTOR(root) );
          do
          {
            rff dstdist, srcdist, uv[2], f, vray[3];
            rfTri *tri;

            tri = (rfTri *)RF_ADDRESS( root, ((rfssize)*trilist++) << RF_LINKSHIFT );

#if RF_CULLMODE == 0
            dstdist = rfMathPlanePoint( tri->plane, dst );
            if( dstdist <= (rff)0.0 )
              continue;
            srcdist = rfMathPlanePoint( tri->plane, src );
            if( srcdist > (rff)0.0 )
              continue;
            f = dstdist - srcdist;
            vray[0] = ( src[0] * dstdist ) - ( dst[0] * srcdist );
            vray[1] = ( src[1] * dstdist ) - ( dst[1] * srcdist );
            vray[2] = ( src[2] * dstdist ) - ( dst[2] * srcdist );
            uv[0] = rfMathVectorDotProduct( &tri->edpu[0], vray ) + ( tri->edpu[3] * f );
            if( ( uv[0] < (rff)0.0 ) || ( uv[0] > f ) )
              continue;
            uv[1] = rfMathVectorDotProduct( &tri->edpv[0], vray ) + ( tri->edpv[3] * f );
            if( ( uv[1] < (rff)0.0 ) || ( ( uv[0] + uv[1] ) > f ) )
              continue;
            f = (rff)1.0 / f;
            dst[0] = vray[0] * f;
            dst[1] = vray[1] * f;
            dst[2] = vray[2] * f;
 #if TRACEHITUV
            hituv[0] = uv[0] * f;
            hituv[1] = uv[1] * f;
 #endif
 #if TRACEHITSIDE
            hitside = 0;
 #endif
#elif RF_CULLMODE == 1
            dstdist = rfMathPlanePoint( tri->plane, dst );
            if( dstdist > (rff)0.0 )
              continue;
            srcdist = rfMathPlanePoint( tri->plane, src );
            if( srcdist <= (rff)0.0 )
              continue;
            f = dstdist - srcdist;
            vray[0] = ( src[0] * dstdist ) - ( dst[0] * srcdist );
            vray[1] = ( src[1] * dstdist ) - ( dst[1] * srcdist );
            vray[2] = ( src[2] * dstdist ) - ( dst[2] * srcdist );
            uv[0] = rfMathVectorDotProduct( &tri->edpu[0], vray ) + ( tri->edpu[3] * f );
            if( ( uv[0] > (rff)0.0 ) || ( uv[0] < f ) )
              continue;
            uv[1] = rfMathVectorDotProduct( &tri->edpv[0], vray ) + ( tri->edpv[3] * f );
            if( ( uv[1] > (rff)0.0 ) || ( ( uv[0] + uv[1] ) < f ) )
              continue;
            f = (rff)1.0 / f;
            dst[0] = vray[0] * f;
            dst[1] = vray[1] * f;
            dst[2] = vray[2] * f;
 #if TRACEHITUV
            hituv[0] = uv[0] * f;
            hituv[1] = uv[1] * f;
 #endif
 #if TRACEHITSIDE
            hitside = 1;
 #endif
#else
            dstdist = rfMathPlanePoint( tri->plane, dst );
            srcdist = rfMathPlanePoint( tri->plane, src );
            if( dstdist * srcdist > (rff)0.0 )
              continue;
 #if RF_CULLOBJTEST
            trihitside = ( srcdist < (rff)0.0 );
            if( trihitside == hitsideskip )
              continue;
 #endif
            f = srcdist / ( srcdist - dstdist );
            vray[0] = src[0] + f * ( dst[0] - src[0] );
            vray[1] = src[1] + f * ( dst[1] - src[1] );
            vray[2] = src[2] + f * ( dst[2] - src[2] );
            uv[0] = ( rfMathVectorDotProduct( &tri->edpu[0], vray ) + tri->edpu[3] );
            if( !( uv[0] >= (rff)0.0 ) || ( uv[0] > (rff)1.0 ) )
              continue;
            uv[1] = ( rfMathVectorDotProduct( &tri->edpv[0], vray ) + tri->edpv[3] );
            if( ( uv[1] < (rff)0.0 ) || ( ( uv[0] + uv[1] ) > (rff)1.0 ) )
              continue;
            dst[0] = vray[0];
            dst[1] = vray[1];
            dst[2] = vray[2];
 #if TRACEHITUV
            hituv[0] = uv[0];
            hituv[1] = uv[1];
 #endif
 #if TRACEHITSIDE
  #if RF_CULLOBJTEST
            hitside = trihitside;
  #else
            hitside = ( srcdist < (rff)0.0 );
  #endif
 #endif
#endif
            trihit = tri;
          } while( --tricount );

          if( trihit )
          {
            axisindex = nredge >> 1;
            hitdist = sumdist + ( ( dst[axisindex] - src[axisindex] ) * vectinv[axisindex] );
            if( hitdist >= clipdist )
              break;
            TRACEHIT( RF_PASSPARAM, modeldata, RF_OBJECT_DATA( objectdata ), vector, dst,
#if TRACEHITUV
              hituv,
#else
              0,
#endif
              RF_ADDRESS( trihit, sizeof(rfTri) ),
#if TRACEHITDIST
              hitdist,
#else
              0.0,
#endif
              trihit->plane,
#if TRACEHITSIDE
              hitside,
#else
              0,
#endif
              RF_ADDRESSDIFF( root, handle->objectgraph ), objectmatrix );
            clipdist = hitdist;
#ifdef TRACEVOID
            voidflag = 0;
#endif
            break;
          }
        }

        sumdist += mindist;
        if( sumdist >= clipdist )
        {
#if TRACE_DEBUG
printf( "    Clip %f > %f\n", sumdist, clipdist );
#endif
          break;
        }
        src[0] += mindist * vector[0];
        src[1] += mindist * vector[1];
        src[2] += mindist * vector[2];

        /* Traverse through the sector's edge */
        if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
        {

#if TRACE_DEBUG
printf( "    Link Sector Offset %d ( %d )\n", (int)RF_SECTOR(root)->link[ nredge ], (int)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );
#endif

          /* Neighbor is sector */
          slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
          if( !( slink ) )
          {
#if TRACE_DEBUG
printf( "    End Of Graph\n" );
#endif
            break;
          }
          root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
          continue;
        }


#if TRACE_DEBUG
printf( "    Link Node Offset %d ( %d )\n", (int)RF_SECTOR(root)->link[ nredge ], (int)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );
#endif


        /* Neighbor is node */
        root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );

        /* Node traversal */
        for( ; ; )
        {
#if TRACE_DEBUG
printf( "  Node %p, axis %d, plane %f\n", root, RF_NODE_GET_AXIS( RF_NODE(root)->flags ), RF_NODE(root)->plane );
#endif

#if 0
          int linkflags, nodeside;
          linkflags = RF_NODE(root)->flags;
          nodeside = RF_NODE_MORE;
          if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
            nodeside = RF_NODE_LESS;
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
            break;
#else
          int linkflags;
          linkflags = RF_NODE(root)->flags;
          if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          {
            root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
            if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
              break;
          }
          else
          {
            root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
            if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
              break;
          }
#endif

        }
      }

#if TRACEQUADROOT
      }
#endif

#if TRACETESTROOT
      }
#endif

    }
    else
      break;
  }

#ifdef TRACEVOID
  if( voidflag )
    TRACEVOID( RF_PASSPARAM, vector, 0 );
#endif

#ifdef TRACEEND
  TRACEEND( RF_PASSPARAM, rayvector );
#endif

  return;
}


#undef RF_SECTOR
#undef RF_NODE
#undef RF_LINKSHIFT
#undef RF_TRILIST
#undef RF_TRILIST_TYPE
#undef RF_PASSPARAM
#undef RF_ELEM_PREPARE_AXIS


#undef RF_CULLMODE
#undef RF_ADDRBITS
#undef RF_TRACENAME
#undef RF_RESOLVENAME
#undef RF_DISPLACENAME

