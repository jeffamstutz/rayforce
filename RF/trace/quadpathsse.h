/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*
#ifndef TRACE_DEBUG
 #define TRACE_DEBUG (0)
#endif
*/

#if RF_ADDRBITS == 16
 #define RF_SECTOR(x) ((const rfSector16 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode16 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK16_SHIFT)
 #define RF_TRILIST RF_SECTOR16_TRILIST
 #define RF_TRILIST_TYPE int16_t
#elif RF_ADDRBITS == 32
 #define RF_SECTOR(x) ((const rfSector32 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode32 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK32_SHIFT)
 #define RF_TRILIST RF_SECTOR32_TRILIST
 #define RF_TRILIST_TYPE int32_t
#else
 #define RF_SECTOR(x) ((const rfSector64 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode64 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK64_SHIFT)
 #define RF_TRILIST RF_SECTOR64_TRILIST
 #define RF_TRILIST_TYPE int64_t
#endif

#if TRACEPARAMPASSBYREFERENCE
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM &param, &raystore
 #else
  #define RF_PASSPARAM &param
 #endif
#else
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM param, &raystore
 #else
  #define RF_PASSPARAM param
 #endif
#endif


static void RF_TRACENAME( const rfHandle * RF_RESTRICT handle, TRACEPARAM param, const rfRayQuad * RF_RESTRICT quad )
{
  int32_t nredge;
  int32_t vecflag;
  rfssize slink;
  __m128 vsrc[3], vdst[3];
  __m128 vunit;
  __m128 vector[3];
  __m128 vectinv[3];
  int32_t edgeindex[4];
  int32_t edgevmask[4];
  void *root;
  void *quadroot;
#if !TRACERESOLVEROOT
  rfRoot rootoffset;
#endif
#if TRACERESOLVEROOT || TRACEDISPLACEROOT
  void *rootparts[4];
#endif
#if TRACEDISTCLIP
  __m128 vclipdist;
#endif

 #define F 0x0
 #define T ~0x0
  static const uint32_t ortable[16*4] RF_ALIGN16 =
  {
    F,F,F,F,
    T,F,F,F,
    F,T,F,F,
    T,T,F,F,
    F,F,T,F,
    T,F,T,F,
    F,T,T,F,
    T,T,T,F,
    F,F,F,T,
    T,F,F,T,
    F,T,F,T,
    T,T,F,T,
    F,F,T,T,
    T,F,T,T,
    F,T,T,T,
    T,T,T,T,
  };
 #undef F
 #undef T

#if !TRACERESOLVEROOT
 #if !TRACESINGLEROOT
  rootoffset = ( quad->root[0] ^ quad->root[1] ) | ( quad->root[2] ^ quad->root[3] ) | ( quad->root[0] ^ quad->root[2] );
  if( rootoffset )
    goto incoherent;
 #endif
  rootoffset = quad->root[0];
#endif

#if TRACETESTROOT
  if( !( rootoffset ) )
    root = handle->objectgraph;
  else
  {
#endif

#if TRACERESOLVEROOT
  root = RF_RESOLVE4NAME( handle->objectgraph, ray->origin, 4 );
  if( !( root ) )
    goto incoherent;
#elif TRACEDISPLACEROOT
  root = RF_ADDRESS( handle->objectgraph, rootoffset );
  root = RF_DISPLACE4NAME( root, ray->origin, 4 );
  if( !( root ) )
    goto incoherent;
#elif TRACEQUADROOT
  if( !( rootoffset ) )
    root = handle->objectgraph;
  else
  {
    if( ( rootoffset & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_PRIMITIVE )
      root = RF_ADDRESS( handle->objectgraph, rootoffset );
    else
    {
      if( ( rootoffset & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE )
      {
        root = RF_DISPLACE4NAME( root, ray->origin, 4 );
        if( !( root ) )
          goto incoherent;
      }
      else
        root = RF_ADDRESS( handle->objectgraph, rootoffset & ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK ) );
#else
  root = RF_ADDRESS( handle->objectgraph, rootoffset );
#endif

  vunit = _mm_set1_ps( 1.0 );

  vector[0] = _mm_load_ps( &quad->vector[0] );
  vector[1] = _mm_load_ps( &quad->vector[4] );
  vector[2] = _mm_load_ps( &quad->vector[8] );

  vecflag = 0;
  vectinv[0] = _mm_div_ps( vunit, vector[0] );
  edgevmask[0] = _mm_movemask_ps( vectinv[0] ) ^ 0xF;
  if( (uint32_t)(edgevmask[0]-1) < 0xE )
    vecflag = 1;
  vectinv[1] = _mm_div_ps( vunit, vector[1] );
  edgevmask[1] = _mm_movemask_ps( vectinv[1] ) ^ 0xF;
  if( (uint32_t)(edgevmask[1]-1) < 0xE )
    vecflag = 1;
  vectinv[2] = _mm_div_ps( vunit, vector[2] );
  edgevmask[2] = _mm_movemask_ps( vectinv[2] ) ^ 0xF;
  if( (uint32_t)(edgevmask[2]-1) < 0xE )
    vecflag = 1;
  if( vecflag )
  {
    quadroot = root;
    goto end;
  }

  edgeindex[RF_AXIS_X] = ( RF_AXIS_X << RF_EDGE_AXIS_SHIFT ) | ( ( edgevmask[RF_AXIS_X] >> 0 ) & 1 );
  edgeindex[RF_AXIS_Y] = ( RF_AXIS_Y << RF_EDGE_AXIS_SHIFT ) | ( ( edgevmask[RF_AXIS_Y] >> 0 ) & 1 );
  edgeindex[RF_AXIS_Z] = ( RF_AXIS_Z << RF_EDGE_AXIS_SHIFT ) | ( ( edgevmask[RF_AXIS_Z] >> 0 ) & 1 );

  vsrc[RF_AXIS_X] = _mm_load_ps( &quad->origin[0] );
  vsrc[RF_AXIS_Y] = _mm_load_ps( &quad->origin[4] );
  vsrc[RF_AXIS_Z] = _mm_load_ps( &quad->origin[8] );

#if TRACEDISTCLIP
  vclipdist = _mm_load_ps( quad->clipdist );
#endif

  for( ; ; )
  {
    /* Sector traversal */
    int32_t vmask0, vmask1;
    int32_t breakflag;
    __m128 vxdist, vydist,  vmindist;

    quadroot = root;

/*
  // elem->edgeindex[3] = elem->edgeindex[2];

    __m128 vzdist;
    int32_t edgebits;
    const static int32_t testmask[4] = { 0x0000, 0x1111, 0x0000, 0xffff };
    const static int32_t ymask[4] RF_ALIGN16 = { 0x80, 0x80, 0x80, 0x80 };

    vydist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[1] ] ), vsrc[1] ), vectinv[1] );
    vzdist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[2] ] ), vsrc[2] ), vectinv[2] );
    vmindist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[0] ] ), vsrc[0] ), vectinv[0] );
    vmindist = _mm_min_ps( vmindist, _mm_min_ps( vzdist, vydist ) );

    edgebits = _mm_movemask_epi8( (__m128i)_mm_or_ps( _mm_and_ps( _mm_cmpeq_ps( vydist, vmindist ), _mm_load_ps( (void *)ymask ) ), _mm_cmpeq_ps( vzdist, vmindist ) ) );
    nredge = edgeindex[ edgebits & 0x3 ];

    breakflag = 0x0;
    if( edgebits != testmask[ edgebits & 0x3 ] )
      breakflag = 0xF;
*/

    vxdist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[0] ] ), vsrc[0] ), vectinv[0] );
    vydist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[1] ] ), vsrc[1] ), vectinv[1] );
    vmindist = _mm_mul_ps( _mm_sub_ps( _mm_set1_ps( RF_SECTOR(root)->edge[ edgeindex[2] ] ), vsrc[2] ), vectinv[2] );
    vmindist = _mm_min_ps( vmindist, _mm_min_ps( vxdist, vydist ) );

    breakflag = 0;
    vmask0 = _mm_movemask_ps( _mm_cmpeq_ps( vmindist, vxdist ) );
    nredge = edgeindex[2];
    if( vmask0 == 0xF )
      nredge = edgeindex[0];
    else if( ( vmask1 = _mm_movemask_ps( _mm_cmpeq_ps( vmindist, vydist ) ) ) == 0xF )
      nredge = edgeindex[1];
    else if( vmask0 | vmask1 )
      breakflag = 1;

    if( !( RF_SECTOR_GET_PRIMCOUNT( RF_SECTOR(root) ) ) )
    {
#if TRACEDISTCLIP
      if( !( _mm_movemask_ps( _mm_cmplt_ps( vmindist, vclipdist ) ) ) )
      {
        quadroot = handle->objectgraph;
        goto end;
      }
#endif

      if( breakflag )
        goto end;

      if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
      {
        /* Neighbor is sector */
        slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
        if( !( slink ) )
        {
          quadroot = handle->objectgraph;
          goto end;
        }
        root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
        continue;
      }

      vdst[0] = _mm_add_ps( _mm_mul_ps( vmindist, vector[0] ), vsrc[0] );
      vdst[1] = _mm_add_ps( _mm_mul_ps( vmindist, vector[1] ), vsrc[1] );
      vdst[2] = _mm_add_ps( _mm_mul_ps( vmindist, vector[2] ), vsrc[2] );
    }
    else
    {
      int triindex, trihitindex;
      int32_t targetmask;
      __m128 vtridst[3];
      RF_TRILIST_TYPE *trilist;
      rfTri *trihit;

      /* TODO: foo */
      trihit = 0;
      targetmask = 0x0;
      trihitindex = 0x0;

      vdst[0] = _mm_add_ps( _mm_mul_ps( vmindist, vector[0] ), vsrc[0] );
      vdst[1] = _mm_add_ps( _mm_mul_ps( vmindist, vector[1] ), vsrc[1] );
      vdst[2] = _mm_add_ps( _mm_mul_ps( vmindist, vector[2] ), vsrc[2] );

      vtridst[0] = vdst[0];
      vtridst[1] = vdst[1];
      vtridst[2] = vdst[2];

      trilist = RF_TRILIST( RF_SECTOR(root) );
      for( triindex = 0 ; triindex < RF_SECTOR(root)->primcount ; triindex++ )
      {
        int32_t tflags, hitmask;
        rfTri *tri;
        __m128 dstdist, srcdist, vray[3];
        __m128 utd, vtd;
        __m128 pl0, pl1, pl2, pl3;
        __m128 f;
        __m128 vsum;
#if RF_CULLMODE == 2
        int32_t signmask;
#endif
#if TRACEDISTCLIP
        int32_t axisindex;
#endif

        tri = (rfTri *)RF_ADDRESS( root, (rfssize)(*trilist++) << RF_LINKSHIFT );
        pl0 = _mm_set1_ps( tri->plane[0] );
        pl1 = _mm_set1_ps( tri->plane[1] );
        pl2 = _mm_set1_ps( tri->plane[2] );
        pl3 = _mm_set1_ps( tri->plane[3] );

#if RF_CULLMODE == 0
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        tflags = _mm_movemask_ps( dstdist );
        if( tflags == 0xF )
          continue;
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        tflags |= _mm_movemask_ps( srcdist ) ^ 0xF;
        if( tflags == 0xF )
          continue;
        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );
        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags |= _mm_movemask_ps( utd );
        if( tflags == 0xF )
          continue;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        tflags |= _mm_movemask_ps( vtd );
        if( tflags == 0xF )
          continue;
        tflags |= _mm_movemask_ps( _mm_cmple_ps( vsum, _mm_add_ps( utd, vtd ) ) );
        if( tflags == 0xF )
          continue;
#elif RF_CULLMODE == 1
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        tflags = _mm_movemask_ps( dstdist ) ^ 0xF;
        if( tflags == 0xF )
          continue;
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        tflags |= _mm_movemask_ps( srcdist );
        if( tflags == 0xF )
          continue;
        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );
        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags |= _mm_movemask_ps( utd ) ^ 0xF;
        if( tflags == 0xF )
          continue;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        tflags |= _mm_movemask_ps( vtd ) ^ 0xF;
        if( tflags == 0xF )
          continue;
        tflags |= _mm_movemask_ps( _mm_cmple_ps( _mm_add_ps( utd, vtd ), vsum ) );
        if( tflags == 0xF )
          continue;
#else
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        tflags = _mm_movemask_ps( _mm_mul_ps( srcdist, dstdist ) ) ^ 0xF;
        if( tflags == 0xF )
          continue;
        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );
        signmask = _mm_movemask_ps( vsum );
        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags |= _mm_movemask_ps( utd ) ^ signmask;
        if( tflags == 0xF )
          continue;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        tflags |= _mm_movemask_ps( vtd ) ^ signmask;
        if( tflags == 0xF )
          continue;
        tflags |= _mm_movemask_ps( _mm_cmple_ps( vsum, _mm_add_ps( utd, vtd ) ) ) ^ signmask;
        if( tflags == 0xF )
          continue;
#endif

        f = _mm_div_ps( vunit, vsum );
#if TRACEDISTCLIP
        axisindex = nredge >> 1;
        tflags |= _mm_movemask_ps( _mm_cmple_ps( vclipdist, _mm_mul_ps( _mm_sub_ps( _mm_mul_ps( vray[axisindex], f ), vsrc[axisindex] ), vectinv[axisindex] ) ) );
        if( tflags == 0xF )
          continue;
#endif
        utd = _mm_mul_ps( utd, f );
        vtd = _mm_mul_ps( vtd, f );
        hitmask = tflags ^ 0xF;

        if( !( tflags ) )
        {
          vtridst[0] = _mm_mul_ps( vray[0], f );
          vtridst[1] = _mm_mul_ps( vray[1], f );
          vtridst[2] = _mm_mul_ps( vray[2], f );
        }
        else
        {
          __m128 vor;
          vor = _mm_load_ps( (void *)&ortable[ hitmask << 2 ] );
          /* TODO: SSE 4.1 blendvps!!! */
          vtridst[0] = _mm_or_ps( _mm_and_ps( _mm_mul_ps( vray[0], f ), vor ), _mm_andnot_ps( vor, vtridst[0] ) );
          vtridst[1] = _mm_or_ps( _mm_and_ps( _mm_mul_ps( vray[1], f ), vor ), _mm_andnot_ps( vor, vtridst[1] ) );
          vtridst[2] = _mm_or_ps( _mm_and_ps( _mm_mul_ps( vray[2], f ), vor ), _mm_andnot_ps( vor, vtridst[2] ) );
        }
        trihit = tri;
        trihitindex = triindex;
        targetmask = hitmask;
      }

      /* We hit a triangle but it's not a complete hit, return sector */
      /* TODO: only test targetmask */
      if( ( trihit ) && ( targetmask != 0xF ) )
        goto end;

      /* Does any other triangle of the sector intersect our volume? */
      trilist = RF_TRILIST( RF_SECTOR(root) );
      for( triindex = 0 ; triindex < RF_SECTOR(root)->primcount ; triindex++ )
      {
        int32_t tflags, testmask, tcount;
        rfTri *tri;
        __m128 dstdist, srcdist, vray[3];
        __m128 utd, vtd;
        __m128 pl0, pl1, pl2, pl3;
        __m128 vsum;
#if RF_CULLMODE == 2
        int32_t signmask;
#endif

        tri = (rfTri *)RF_ADDRESS( root, (rfssize)(*trilist++) << RF_LINKSHIFT );
        if( tri == trihit )
          continue;
        pl0 = _mm_set1_ps( tri->plane[0] );
        pl1 = _mm_set1_ps( tri->plane[1] );
        pl2 = _mm_set1_ps( tri->plane[2] );
        pl3 = _mm_set1_ps( tri->plane[3] );

#if RF_CULLMODE == 0
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        if( _mm_movemask_ps( dstdist ) == 0xF )
          continue;
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        if( !( _mm_movemask_ps( srcdist ) ) )
          continue;

        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );

        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags = _mm_movemask_ps( utd );
        tcount = 0;
        if( (uint32_t)(tflags-1) < 0xE )
          tcount = 1;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        testmask = _mm_movemask_ps( vtd );
        tflags |= testmask;
        if( (uint32_t)(testmask-1) < 0xE )
          tcount++;
        testmask = _mm_movemask_ps( _mm_cmple_ps( vsum, _mm_add_ps( utd, vtd ) ) );
        tflags |= testmask;
        if( (uint32_t)(testmask-1) < 0xE )
          tcount++;

        if( ( tflags != 0xF ) || ( tcount >= 2 ) )
          goto end;
#elif RF_CULLMODE == 1
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        if( !( _mm_movemask_ps( dstdist ) ) )
          continue;
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        if( _mm_movemask_ps( srcdist ) == 0xF )
          continue;

        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );

        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags = _mm_movemask_ps( utd ) ^ 0xF;
        tcount = 0;
        if( (uint32_t)(tflags-1) < 0xE )
          tcount = 1;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        testmask = _mm_movemask_ps( vtd ) ^ 0xF;
        tflags |= testmask;
        if( (uint32_t)(testmask-1) < 0xE )
          tcount++;
        testmask = _mm_movemask_ps( _mm_cmple_ps( _mm_add_ps( utd, vtd ), vsum ) );
        tflags |= testmask;
        if( (uint32_t)(testmask-1) < 0xE )
          tcount++;

        if( ( tflags != 0xF ) || ( tcount >= 2 ) )
          goto end;
#else
        dstdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vtridst[0] ), _mm_mul_ps( pl1, vtridst[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vtridst[2] ), pl3 ) );
        srcdist = _mm_add_ps( _mm_add_ps( _mm_mul_ps( pl0, vsrc[0] ), _mm_mul_ps( pl1, vsrc[1] ) ), _mm_add_ps( _mm_mul_ps( pl2, vsrc[2] ), pl3 ) );
        if( !( _mm_movemask_ps( _mm_mul_ps( srcdist, dstdist ) ) ) )
          continue;

        vsum = _mm_sub_ps( dstdist, srcdist );
        vray[0] = _mm_sub_ps( _mm_mul_ps( vsrc[0], dstdist ), _mm_mul_ps( vtridst[0], srcdist ) );
        vray[1] = _mm_sub_ps( _mm_mul_ps( vsrc[1], dstdist ), _mm_mul_ps( vtridst[1], srcdist ) );
        vray[2] = _mm_sub_ps( _mm_mul_ps( vsrc[2], dstdist ), _mm_mul_ps( vtridst[2], srcdist ) );
        signmask = _mm_movemask_ps( vsum );

        utd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpu[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpu[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpu[3] ), vsum ) );
        tflags = _mm_movemask_ps( utd ) ^ signmask;
        tcount = 0;
        if( (uint32_t)(tflags-1) < 0xE )
          tcount = 1;
        vtd = _mm_add_ps( _mm_add_ps( _mm_add_ps( _mm_mul_ps( _mm_set1_ps( tri->edpv[0] ), vray[0] ), _mm_mul_ps( _mm_set1_ps( tri->edpv[1] ), vray[1] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[2] ), vray[2] ) ), _mm_mul_ps( _mm_set1_ps( tri->edpv[3] ), vsum ) );
        testmask = _mm_movemask_ps( vtd ) ^ signmask;
        tflags |= testmask;
        if( (uint32_t)(testmask-1) < 0xE )
          tcount++;
        testmask = _mm_movemask_ps( _mm_cmple_ps( vsum, _mm_add_ps( utd, vtd ) ) ) ^ signmask;
        tflags |= testmask;
        if( (uint32_t)(testmask-1) < 0xE )
          tcount++;

        if( ( tflags != 0xF ) || ( tcount >= 2 ) )
          goto end;
#endif
      }

      /* The whole volume hits the triangle */
      /* TODO: only test targetmask */
      if( ( trihit ) && ( targetmask == 0xF ) )
      {
        if( ( trihitindex <= RF_QUADTRACE_PRIMINDEX_MAX ) && ( (size_t)((const rfGraphHeader *)handle->objectgraph)->graphdatasize < (((size_t)1)<<RF_QUADTRACE_PRIMINDEX_SHIFT) ) )
          quadroot = RF_ADDRESS( root, ( (size_t)trihitindex << RF_QUADTRACE_PRIMINDEX_SHIFT ) | RF_QUADTRACE_TYPE_PRIMITIVE );
#ifdef TRACEQUADHIT
        TRACEQUADHIT( RF_PASSPARAM,
#if TRACEHITDATA
          RF_ADDRESS( trihit, sizeof(rfTri) )
#else
          0,
#endif
#if TRACEHITPLANE
          trihit->plane,
#else
          0,
#endif
          (rfRoot)RF_ADDRESSDIFF( quadroot, handle->objectgraph ) );
        return;
#endif
        goto end;
      }

      if( breakflag )
        goto end;

      if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
      {
        /* Neighbor is sector */
        slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
        if( !( slink ) )
        {
          quadroot = handle->objectgraph;
          goto end;
        }
        root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
        continue;
      }
    }

    /* Neighbor is node */
    root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );
    vsrc[0] = vdst[0];
    vsrc[1] = vdst[1];
    vsrc[2] = vdst[2];

    /* Node traversal */
    for( ; ; )
    {
      int32_t linkflags, nbits;
      __m128 nodesrc;

      linkflags = RF_NODE(root)->flags;

      /* TODO: What about branches? Merges? Something? */
      nodesrc = vsrc[ RF_NODE_GET_AXIS(linkflags) ];

      nbits = _mm_movemask_ps( _mm_cmplt_ps( nodesrc, _mm_set1_ps( RF_NODE(root)->plane ) ) );
      if( !( nbits ) )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
      else if( nbits == 0xF )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
        goto end;
    }
  }

#if TRACEQUADROOT
    }
  }
#endif

#if TRACETESTROOT
  }
#endif

  end:
  TRACEQUAD( RF_PASSPARAM, (rfRoot)RF_ADDRESSDIFF( quadroot, handle->objectgraph ) );

  return;

#if TRACERESOLVEROOT || TRACEDISPLACEROOT || TRACEQUADROOT || ( !TRACERESOLVEROOT && !TRACESINGLEROOT )
  incoherent:
  {
    rff center[RF_AXIS_COUNT];
    center[RF_AXIS_X] = 0.25 * ( quad->origin[(0*RF_AXIS_COUNT)+RF_AXIS_X] + quad->origin[(1*RF_AXIS_COUNT)+RF_AXIS_X] + quad->origin[(2*RF_AXIS_COUNT)+RF_AXIS_X] + quad->origin[(3*RF_AXIS_COUNT)+RF_AXIS_X] );
    center[RF_AXIS_Y] = 0.25 * ( quad->origin[(0*RF_AXIS_COUNT)+RF_AXIS_Y] + quad->origin[(1*RF_AXIS_COUNT)+RF_AXIS_Y] + quad->origin[(2*RF_AXIS_COUNT)+RF_AXIS_Y] + quad->origin[(3*RF_AXIS_COUNT)+RF_AXIS_Y] );
    center[RF_AXIS_Z] = 0.25 * ( quad->origin[(0*RF_AXIS_COUNT)+RF_AXIS_Z] + quad->origin[(1*RF_AXIS_COUNT)+RF_AXIS_Z] + quad->origin[(2*RF_AXIS_COUNT)+RF_AXIS_Z] + quad->origin[(3*RF_AXIS_COUNT)+RF_AXIS_Z] );
    quadroot = RF_ADDRESS( RF_RESOLVENAME( handle->objectgraph, center ), RF_QUADTRACE_TYPE_SECTORDISPLACE );
    goto end;
  }
#endif
}


#undef RF_SECTOR
#undef RF_NODE
#undef RF_LINKSHIFT
#undef RF_TRILIST
#undef RF_TRILIST_TYPE
#undef RF_PASSPARAM
#undef RF_ELEM_PREPARE_AXIS

#undef RF_CULLMODE
#undef RF_ADDRBITS
#undef RF_TRACENAME
#undef RF_RESOLVENAME
#undef RF_DISPLACENAME

