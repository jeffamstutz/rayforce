/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*
#ifndef TRACE_DEBUG
 #define TRACE_DEBUG (0)
#endif
*/

#if RF_ADDRBITS == 16
 #define RF_SECTOR(x) ((const rfSector16 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode16 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK16_SHIFT)
 #define RF_TRILIST RF_SECTOR16_TRILIST
 #define RF_TRILIST_TYPE int16_t
#elif RF_ADDRBITS == 32
 #define RF_SECTOR(x) ((const rfSector32 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode32 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK32_SHIFT)
 #define RF_TRILIST RF_SECTOR32_TRILIST
 #define RF_TRILIST_TYPE int32_t
#else
 #define RF_SECTOR(x) ((const rfSector64 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode64 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK64_SHIFT)
 #define RF_TRILIST RF_SECTOR64_TRILIST
 #define RF_TRILIST_TYPE int64_t
#endif

#if TRACEPARAMPASSBYREFERENCE
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM &param, &raystore
 #else
  #define RF_PASSPARAM &param
 #endif
#else
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM param, &raystore
 #else
  #define RF_PASSPARAM param
 #endif
#endif


////


#define RF_ELEM_PREPARE_AXIS(axis) \
  edgeindex[axis] = ( axis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN; \
  vectinv[axis] = -RFF_MAX; \
  if( rffabs( vector[axis] ) > (rff)0.0 ) \
  { \
    vectinv[axis] = (rff)1.0 / vector[axis]; \
    if( vector[axis] >= (rff)0.0 ) \
      edgeindex[axis] = ( axis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX; \
  }


#ifndef RFTRACE_SECTOR_TRAVERSAL_VARIANT
 #define RFTRACE_SECTOR_TRAVERSAL_VARIANT (1)
#endif

#ifndef RF_NODE_SSE_TRAVERSAL
 #define RF_NODE_SSE_TRAVERSAL (0)
#endif

#if !RFTRACE__SSE3__
 #error ERROR: A minimum of SSE3 is required to compile this path
#endif


static void RF_TRACENAME( const rfHandle * RF_RESTRICT handle, TRACEPARAM param, const rfRaySingle * RF_RESTRICT ray )
{
  int nredge;
  rfssize slink;
  rff RF_ALIGN16 vector[4];
#if TRACEHITSIDE
  int hitside;
#endif
  rff RF_ALIGN16 vectinv[4];
  int RF_ALIGN16 edgeindex[4];
  rff RF_ALIGN16 src[4];
  rff RF_ALIGN16 dst[4];
#if TRACEHITUV
  rff hituv[2];
#endif
#if TRACEDISTCLIP
  rff clipdist;
#endif
#if TRACEDISTCLIP || TRACEHITDIST
  rff hitdist;
  __m128 vhitdist;
#endif
  void *root;
  rfTri *trihit;
  void *modeldata;
#if TRACEDISTCLIP || TRACEHITDIST
  int axisindex;
#endif
#ifdef TRACERAYSTORE
  TRACERAYSTORE raystore;
#endif
  int vectmask;
  static const float RF_ALIGN16 vect3dbase[4] = { 0.0, 0.0, 0.0, 1.0 };
  __m128 vsrc;
  __m128 vdst;
  __m128 vvector;
  __m128 vvectinv;
  __m128 vmindist;

  modeldata = RF_ADDRESS( handle->objectgraph, ((const rfGraphHeader * RF_RESTRICT)handle->objectgraph)->modeldataoffset );

  vector[0] = ray->vector[0];
  vector[1] = ray->vector[1];
  vector[2] = ray->vector[2];

#if TRACETESTROOT
  root = 0;
  if( ray->root )
  {
#endif

#if TRACERESOLVEROOT
  root = RF_RESOLVENAME( handle->objectgraph, ray->origin );
#elif TRACEDISPLACEROOT
  root = RF_DISPLACENAME( RF_ADDRESS( handle->objectgraph, ray->root ), ray->origin );
#elif TRACEQUADROOT
  root = 0;
  if( !( ray->root ) )
    goto tracevoid;
  else
  {
    trihit = 0;
    if( ( ray->root & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_PRIMITIVE )
    {
      RF_TRILIST_TYPE *trilist;
      rfTri *tri;
      root = RF_ADDRESS( handle->objectgraph, ray->root & ~( RF_QUADTRACE_TYPEMASK | ( (size_t)RF_QUADTRACE_PRIMINDEX_MAX )<< RF_QUADTRACE_PRIMINDEX_SHIFT ) );
      trilist = RF_TRILIST( RF_SECTOR(root) );
      tri = (rfTri *)RF_ADDRESS( root, (rfssize)trilist[ ray->root >> RF_QUADTRACE_PRIMINDEX_SHIFT ] << RF_LINKSHIFT );
 #if TRACEHITPOINT || TRACEHITUV
      rff srcdist, dstdist, f, vray[3];
      rff scalesize;
      /* This might not seem the simplest way, but it avoids CUDA division inaccuracy problems */
      scalesize = ((const rfGraphHeader *)handle.objectgraph)->mediansizef;
      dst[0] = src[0] + ( scalesize * vector[0] );
      dst[1] = src[1] + ( scalesize * vector[1] );
      dst[2] = src[2] + ( scalesize * vector[2] );
      dstdist = ( tri->plane[0] * dst[0] ) + ( tri->plane[1] * dst[1] ) + ( tri->plane[2] * dst[2] ) + ( tri->plane[3] );
      srcdist = ( tri->plane[0] * src[0] ) + ( tri->plane[1] * src[1] ) + ( tri->plane[2] * src[2] ) + ( tri->plane[3] );
      f = srcdist / ( srcdist - dstdist );
      vray[0] = src[0] + ( f * ( dst[0] - src[0] ) );
      vray[1] = src[1] + ( f * ( dst[1] - src[1] ) );
      vray[2] = src[2] + ( f * ( dst[2] - src[2] ) );
  #if TRACEHITPOINT
      dst[0] = vray[0];
      dst[1] = vray[1];
      dst[2] = vray[2];
  #endif
  #if TRACEHITUV
      hituv[0] = ( rfMathVectorDotProduct( &tri->edpu[0], vray ) + tri->edpu[3] );
      hituv[1] = ( rfMathVectorDotProduct( &tri->edpv[0], vray ) + tri->edpv[3] );
  #endif
  #if TRACEHITDIST
      hitdist = f * scalesize;
  #endif
  #if TRACEHITSIDE && RF_CULLMODE == 2
      hitside = ( srcdist < (rff)0.0 );
  #endif
 #elif TRACEHITDIST
      rff srcdist;
      srcdist = rfMathPlanePoint( tri->plane, src );
      hitdist = srcdist / ( srcdist - ( ( tri->plane[0] * ( src[0] + vector[0] ) ) + ( tri->plane[1] * ( src[1] + vector[1] ) ) + ( tri->plane[2] * ( src[2] + vector[2] ) ) + ( tri->plane[3] ) ) );
  #if TRACEHITSIDE && RF_CULLMODE == 2
      hitside = ( srcdist < (rff)0.0 );
  #endif
 #elif TRACEHITSIDE && RF_CULLMODE == 2
      rff srcdist;
      srcdist = rfMathPlanePoint( tri->plane, src );
      hitside = ( srcdist < (rff)0.0 );
 #endif
 #if TRACEHITSIDE && RF_CULLMODE == 0
      hitside = 0;
 #elif TRACEHITSIDE && RF_CULLMODE == 1
      hitside = 1;
 #endif
      if( 1 )
        trihit = tri;
 #ifdef TRACEVALIDATETRIANGLE
      else if( !( TRACEVALIDATETRIANGLE( RF_PASSPARAM, modeldata, handle.objectdata, vector, RF_ADDRESS( trihit, sizeof(rfTri) ), trihit->plane ) ) )
        trihit = 0;
 #endif
 #ifdef TRACEVALIDATESIDE
      else if( !( TRACEVALIDATESIDE( RF_PASSPARAM, modeldata, handle.objectdata, vector, RF_ADDRESS( trihit, sizeof(rfTri) ), trihit->plane, hitside ) ) )
        trihit = 0;
 #endif
 #ifdef TRACEVALIDATEPOINT
      else if( !( TRACEVALIDATEPOINT( RF_PASSPARAM, modeldata, handle.objectdata, vector, RF_ADDRESS( trihit, sizeof(rfTri) ), trihit->plane ), hitside, vray, hitdist ) ) )
        trihit = 0;
 #endif
 #ifdef TRACEVALIDATE
      else if( !( TRACEVALIDATE( RF_PASSPARAM, modeldata, handle.objectdata, vector, RF_ADDRESS( trihit, sizeof(rfTri) ), trihit->plane ), hitside, vray, hitdist, uv ) ) )
        trihit = 0;
 #endif
    }
    if( !( trihit ) )
    {
      root = RF_ADDRESS( handle->objectgraph, ray->root & ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK ) );
      if( ( ray->root & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE )
        root = RF_DISPLACENAME( root, ray->origin );
#else
  root = RF_ADDRESS( handle->objectgraph, ray->root );
#endif

  vector[3] = 0.0;
#if TRACEDISTCLIP
  clipdist = ray->clipdist;
#endif

  vsrc = _mm_load_ps( ray->origin );
  vvector = _mm_load_ps( ray->vector );

  vvectinv = _mm_div_ps( _mm_set1_ps( 1.0 ), _mm_or_ps( vvector, _mm_load_ps( vect3dbase ) ) );
  vectmask = ~_mm_movemask_ps( vvector );
  edgeindex[0] = ( RF_AXIS_X << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 0 ) & 0x1 );
  edgeindex[1] = ( RF_AXIS_Y << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 1 ) & 0x1 );
  edgeindex[2] = ( RF_AXIS_Z << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 2 ) & 0x1 );
#if RFTRACE_SECTOR_TRAVERSAL_VARIANT != 0
  _mm_store_ps( vectinv, vvectinv );
#endif

#if TRACEHITSIDE
  hitside = 0;
#endif
  for( ; ; )
  {
    /* Sector traversal */
    int tricount;

#if TRACE_DEBUG
printf( "  Sector %p, edge %f,%f ; %f,%f ; %f,%f\n", root, RF_SECTOR(root)->edge[0], RF_SECTOR(root)->edge[1], RF_SECTOR(root)->edge[2], RF_SECTOR(root)->edge[3], RF_SECTOR(root)->edge[4], RF_SECTOR(root)->edge[5] );
#endif

    __m128 vdisty, vdistz;
#if RFTRACE_SECTOR_TRAVERSAL_VARIANT == 0
    __m128 vedge;
    vedge = _mm_movelh_ps( _mm_unpacklo_ps( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[0] ] ), _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[1] ] ) ), _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[2] ] ) );
    vmindist = _mm_mul_ps( _mm_sub_ps( vedge, vsrc ), vvectinv );
    vdisty = _mm_movehdup_ps( vmindist );
    vdistz = _mm_movehl_ps( vdistz, vmindist );
    nredge = edgeindex[0];
    if( _mm_comilt_ss( vdisty, vmindist ) )
    {
      nredge = edgeindex[1];
      vmindist = vdisty;
    }
    if( _mm_comilt_ss( vdistz, vmindist ) )
    {
      nredge = edgeindex[2];
      vmindist = vdistz;
    }
#elif RFTRACE_SECTOR_TRAVERSAL_VARIANT == 1
    vmindist = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[0] ] ), vsrc ), _mm_set_ss( vectinv[0] ) );
    vdisty = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[1] ] ), _mm_movehdup_ps( vsrc ) ), _mm_set_ss( vectinv[1] ) );
    nredge = edgeindex[0];
    if( _mm_comilt_ss( vdisty, vmindist ) )
    {
      nredge = edgeindex[1];
      vmindist = vdisty;
    }
    vdistz = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[2] ] ), _mm_movehl_ps( vsrc, vsrc ) ), _mm_set_ss( vectinv[2] ) );
    if( _mm_comilt_ss( vdistz, vmindist ) )
    {
      nredge = edgeindex[2];
      vmindist = vdistz;
    }
#elif RFTRACE_SECTOR_TRAVERSAL_VARIANT == 2
    vmindist = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[0] ] ), vsrc ), _mm_set_ss( vectinv[0] ) );
    vdisty = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[1] ] ), _mm_movehdup_ps( vsrc ) ), _mm_set_ss( vectinv[1] ) );
    nredge = edgeindex[0];
    if( _mm_comilt_ss( vdisty, vmindist ) )
      nredge = edgeindex[1];
    vmindist = _mm_min_ss( vmindist, vdisty );
    vdistz = _mm_mul_ss( _mm_sub_ss( _mm_load_ss( &RF_SECTOR(root)->edge[ edgeindex[2] ] ), _mm_movehl_ps( vsrc, vsrc ) ), _mm_set_ss( vectinv[2] ) );
    if( _mm_comilt_ss( vdistz, vmindist ) )
      nredge = edgeindex[2];
    vmindist = _mm_min_ss( vmindist, vdistz );
#endif

    tricount = RF_SECTOR_GET_PRIMCOUNT( RF_SECTOR(root) );
    if( tricount )
    {
      RF_TRILIST_TYPE *trilist;
      __m128 vzero, vone;

      vdst = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vmindist, vmindist, 0x0 ), vvector ) );
      vzero = _mm_xor_ps( vzero, vzero );
      vone = _mm_set_ss( 1.0 );

      trihit = 0;
      trilist = RF_TRILIST( RF_SECTOR(root) );
      do
      {
        __m128 vtriplane, vdstdist, vsrcdist, vf, vvray, vuv0, vuv1;
        rfTri *tri;

        tri = (rfTri *)RF_ADDRESS( root, (rfssize)(*trilist++) << RF_LINKSHIFT );

        vtriplane = _mm_load_ps( tri->plane );
#if RF_CULLMODE == 0
 #if RFTRACE__SSE4_1__
        vdstdist = _mm_dp_ps( vtriplane, vdst, 0xff );
        if( _mm_comile_ss( vdstdist, vzero ) )
          continue;
        vsrcdist = _mm_dp_ps( vtriplane, vsrc, 0xff );
        if( _mm_comigt_ss( vsrcdist, vzero ) )
          continue;
        vf = _mm_sub_ss( vdstdist, vsrcdist );
        vvray = _mm_sub_ps( _mm_mul_ps( vdstdist, vsrc ), _mm_mul_ps( vsrcdist, vdst ) );
        vuv0 = _mm_dp_ps( _mm_load_ps( tri->edpu ), vvray, 0xff );
        if( _mm_comilt_ss( vuv0, vzero ) || _mm_comigt_ss( vuv0, vf ) )
          continue;
        vuv1 = _mm_dp_ps( _mm_load_ps( tri->edpv ), vvray, 0xff );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vf ) )
          continue;
 #else
        vdstdist = _mm_mul_ps( vtriplane, vdst );
        vdstdist = _mm_hadd_ps( vdstdist, vdstdist );
        vdstdist = _mm_add_ss( vdstdist, _mm_movehdup_ps( vdstdist ) );
        if( _mm_comile_ss( vdstdist, vzero ) )
          continue;
        vsrcdist = _mm_mul_ps( vtriplane, vsrc );
        vsrcdist = _mm_hadd_ps( vsrcdist, vsrcdist );
        vsrcdist = _mm_add_ss( vsrcdist, _mm_movehdup_ps( vsrcdist ) );
        if( _mm_comigt_ss( vsrcdist, vzero ) )
          continue;
        vf = _mm_sub_ss( vdstdist, vsrcdist );
        vdstdist = _mm_shuffle_ps( vdstdist, vdstdist, 0x0 );
        vsrcdist = _mm_shuffle_ps( vsrcdist, vsrcdist, 0x0 );
        vvray = _mm_sub_ps( _mm_mul_ps( vdstdist, vsrc ), _mm_mul_ps( vsrcdist, vdst ) );
        vuv0 = _mm_mul_ps( _mm_load_ps( tri->edpu ), vvray );
        vuv1 = _mm_mul_ps( _mm_load_ps( tri->edpv ), vvray );
        vuv0 = _mm_hadd_ps( vuv0, vuv1 );
        vuv0 = _mm_hadd_ps( vuv0, vuv0 );
        if( _mm_comilt_ss( vuv0, vzero ) )
          continue;
        vuv1 = _mm_movehdup_ps( vuv0 );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vf ) )
          continue;
 #endif
        vf = _mm_div_ss( vone, vf );
        vdst = _mm_mul_ps( vvray, _mm_shuffle_ps( vf, vf, 0x0 ) );
 #if TRACEHITUV
        hituv[0] = _mm_cvtss_f32( _mm_mul_ss( vuv0, vf ) );
        hituv[1] = _mm_cvtss_f32( _mm_mul_ss( vuv1, vf ) );
 #endif
 #if TRACEHITSIDE
        hitside = 0;
 #endif
#elif RF_CULLMODE == 1
 #if RFTRACE__SSE4_1__
        vdstdist = _mm_dp_ps( vtriplane, vdst, 0xff );
        if( _mm_comigt_ss( vdstdist, vzero ) )
          continue;
        vsrcdist = _mm_dp_ps( vtriplane, vsrc, 0xff );
        if( _mm_comile_ss( vsrcdist, vzero ) )
          continue;
        vf = _mm_sub_ss( vdstdist, vsrcdist );
        vvray = _mm_sub_ps( _mm_mul_ps( vdstdist, vsrc ), _mm_mul_ps( vsrcdist, vdst ) );
        vuv0 = _mm_dp_ps( _mm_load_ps( tri->edpu ), vvray, 0xff );
        if( _mm_comigt_ss( vuv0, vzero ) || _mm_comilt_ss( vuv0, vf ) )
          continue;
        vuv1 = _mm_dp_ps( _mm_load_ps( tri->edpv ), vvray, 0xff );
        if( _mm_comigt_ss( vuv1, vzero ) || _mm_comilt_ss( _mm_add_ss( vuv0, vuv1 ), vf ) )
          continue;
 #else
        vdstdist = _mm_mul_ps( vtriplane, vdst );
        vdstdist = _mm_hadd_ps( vdstdist, vdstdist );
        vdstdist = _mm_add_ss( vdstdist, _mm_movehdup_ps( vdstdist ) );
        if( _mm_comigt_ss( vdstdist, vzero ) )
          continue;
        vsrcdist = _mm_mul_ps( vtriplane, vsrc );
        vsrcdist = _mm_hadd_ps( vsrcdist, vsrcdist );
        vsrcdist = _mm_add_ss( vsrcdist, _mm_movehdup_ps( vsrcdist ) );
        if( _mm_comile_ss( vsrcdist, vzero ) )
          continue;
        vf = _mm_sub_ss( vdstdist, vsrcdist );
        vdstdist = _mm_shuffle_ps( vdstdist, vdstdist, 0x0 );
        vsrcdist = _mm_shuffle_ps( vsrcdist, vsrcdist, 0x0 );
        vvray = _mm_sub_ps( _mm_mul_ps( vdstdist, vsrc ), _mm_mul_ps( vsrcdist, vdst ) );
        vuv0 = _mm_mul_ps( _mm_load_ps( tri->edpu ), vvray );
        vuv1 = _mm_mul_ps( _mm_load_ps( tri->edpv ), vvray );
        vuv0 = _mm_hadd_ps( vuv0, vuv1 );
        vuv0 = _mm_hadd_ps( vuv0, vuv0 );
        if( _mm_comigt_ss( vuv0, vzero ) )
          continue;
        vuv1 = _mm_movehdup_ps( vuv0 );
        if( _mm_comigt_ss( vuv1, vzero ) || _mm_comilt_ss( _mm_add_ss( vuv0, vuv1 ), vf ) )
          continue;
 #endif
        vf = _mm_div_ss( vone, vf );
        vdst = _mm_mul_ps( vvray, _mm_shuffle_ps( vf, vf, 0x0 ) );
 #if TRACEHITUV
        hituv[0] = _mm_cvtss_f32( _mm_mul_ss( vuv0, vf ) );
        hituv[1] = _mm_cvtss_f32( _mm_mul_ss( vuv1, vf ) );
 #endif
 #if TRACEHITSIDE
        hitside = 1;
 #endif
#else
 #if RFTRACE__SSE4_1__
        vdstdist = _mm_dp_ps( vtriplane, vdst, 0xff );
        vsrcdist = _mm_dp_ps( vtriplane, vsrc, 0xff );
        if( _mm_comigt_ss( _mm_mul_ss( vdstdist, vsrcdist ), vzero ) )
          continue;
        vf = _mm_div_ss( vsrcdist, _mm_sub_ss( vsrcdist, vdstdist ) );
        vvray = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vf, vf, 0x0 ), _mm_sub_ps( vdst, vsrc ) ) );
        vf = _mm_div_ss( vsrcdist, _mm_sub_ss( vsrcdist, vdstdist ) );
        vvray = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vf, vf, 0x0 ), _mm_sub_ps( vdst, vsrc ) ) );
        vuv0 = _mm_dp_ps( _mm_load_ps( tri->edpu ), vvray, 0xff );
        if( !( _mm_comige_ss( vuv0, vzero ) ) || _mm_comigt_ss( vuv0, vone ) )
          continue;
        vuv1 = _mm_dp_ps( _mm_load_ps( tri->edpv ), vvray, 0xff );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vone ) )
          continue;
 #else
        vdstdist = _mm_mul_ps( vtriplane, vdst );
        vsrcdist = _mm_mul_ps( vtriplane, vsrc );
        vsrcdist = _mm_hadd_ps( vsrcdist, vdstdist );
        vsrcdist = _mm_hadd_ps( vsrcdist, vsrcdist );
        vdstdist = _mm_movehdup_ps( vsrcdist );
        if( _mm_comigt_ss( _mm_mul_ss( vdstdist, vsrcdist ), vzero ) )
          continue;
        vf = _mm_div_ss( vsrcdist, _mm_sub_ss( vsrcdist, vdstdist ) );
        vvray = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vf, vf, 0x0 ), _mm_sub_ps( vdst, vsrc ) ) );
  #if 1
        vuv0 = _mm_mul_ps( _mm_load_ps( tri->edpu ), vvray );
        vuv1 = _mm_mul_ps( _mm_load_ps( tri->edpv ), vvray );
        vuv0 = _mm_hadd_ps( vuv0, vuv1 );
        vuv0 = _mm_hadd_ps( vuv0, vuv0 );
        if( !( _mm_comige_ss( vuv0, vzero ) ) )
          continue;
        vuv1 = _mm_movehdup_ps( vuv0 );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vone ) )
          continue;
  #else
        vuv0 = _mm_mul_ps( _mm_load_ps( tri->edpu ), vvray );
        vuv0 = _mm_hadd_ps( vuv0, vuv0 );
        vuv0 = _mm_add_ss( vuv0, _mm_movehdup_ps( vuv0 ) );
        if( !( _mm_comige_ss( vuv0, vzero ) ) || _mm_comigt_ss( vuv0, vone ) )
          continue;
        vuv1 = _mm_mul_ps( _mm_load_ps( tri->edpv ), vvray );
        vuv1 = _mm_hadd_ps( vuv1, vuv1 );
        vuv1 = _mm_add_ss( vuv1, _mm_movehdup_ps( vuv1 ) );
        if( _mm_comilt_ss( vuv1, vzero ) || _mm_comigt_ss( _mm_add_ss( vuv0, vuv1 ), vone ) )
          continue;
  #endif
 #endif
        vdst = vvray;
 #if TRACEHITUV
        hituv[0] = _mm_cvtss_f32( vuv0 );
        hituv[1] = _mm_cvtss_f32( vuv1 );
 #endif
 #if TRACEHITSIDE
        hitside = _mm_comilt_ss( vsrcdist, vzero );
 #endif
#endif
        trihit = tri;
      } while( --tricount );

      if( trihit )
        break;
    }

#if TRACEDISTCLIP
    if( _mm_cvtss_f32( vmindist ) > clipdist )
      goto traceclip;
#endif

    /* Traverse through the sector's edge */
    if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
    {
      /* Neighbor is sector */
      slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
      if( !( slink ) )
        goto tracevoid;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
      continue;
    }

    /* Neighbor is node */
#if TRACEDISTCLIP
    clipdist -= _mm_cvtss_f32( vmindist );
#endif
    vsrc = _mm_add_ps( vsrc, _mm_mul_ps( _mm_shuffle_ps( vmindist, vmindist, 0x0 ), vvector ) );
#if !RF_NODE_SSE_TRAVERSAL
    _mm_store_ps( src, vsrc );
#endif
    root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );

    /* Node traversal */
    for( ; ; )
    {
#if TRACE_DEBUG
printf( "  Node %p, axis %d, plane %f\n", root, RF_NODE_GET_AXIS( RF_NODE(root)->flags ), RF_NODE(root)->plane );
#endif
      int linkflags;
      linkflags = RF_NODE(root)->flags;
#if RF_NODE_SSE_TRAVERSAL
      linkflags >>= RF_NODE_AXISMASK_SHIFT;
      if( _mm_movemask_ps( _mm_sub_ps( _mm_set1_ps( RF_NODE(root)->plane ), vsrc ) ) & linkflags )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( RF_LINK_SECTOR << (RF_NODE_DUPLINKFLAGS_MORE_SHIFT-RF_NODE_AXISMASK_SHIFT) ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( RF_LINK_SECTOR << (RF_NODE_DUPLINKFLAGS_LESS_SHIFT-RF_NODE_AXISMASK_SHIFT) ) )
          break;
      }
#else
      if( _mm_comilt_ss( _mm_load_ss( &src[ RF_NODE_GET_AXIS(linkflags) ] ), _mm_load_ss( &RF_NODE(root)->plane ) ) )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
#endif
    }
  }

#if TRACEQUADROOT
    }
  }
#endif

#if TRACEDISTCLIP || TRACEHITDIST
  vhitdist = _mm_mul_ps( _mm_sub_ps( vdst, _mm_load_ps( ray->origin ) ), vvectinv );
  if( nredge >= 4 )
    vhitdist = _mm_movehl_ps( vhitdist, vhitdist );
  else if( nredge >= 2 )
    vhitdist = _mm_movehdup_ps( vhitdist );
 #if TRACEDISTCLIP
  if( _mm_cvtss_f32( vhitdist ) >= clipdist )
    goto traceclip;
 #endif
#endif

  _mm_store_ps( dst, vdst );
  TRACEHIT( RF_PASSPARAM, modeldata, handle->objectdata, vector, dst,
#if TRACEHITUV
    hituv,
#else
    0,
#endif
    RF_ADDRESS( trihit, sizeof(rfTri) ),
#if TRACEHITDIST
    _mm_cvtss_f32( vhitdist ),
#else
    0.0,
#endif
    trihit->plane,
#if TRACEHITSIDE
    hitside,
#else
    0,
#endif
    RF_ADDRESSDIFF( root, handle->objectgraph ), 0 );

#if TRACETESTROOT
  }
#endif

  end:

#ifdef TRACEEND
  TRACEEND( RF_PASSPARAM, vector );
#endif

  return;

////

  tracevoid:
#ifdef TRACEVOID
  TRACEVOID( RF_PASSPARAM, vector, 0 );
#endif
  goto end;

////

#if TRACEDISTCLIP
  traceclip:
 #ifdef TRACEVOID
  TRACEVOID( RF_PASSPARAM, vector, RF_ADDRESSDIFF( root, handle->objectgraph ) );
 #endif
  goto end;
#endif
}


#undef RF_SECTOR
#undef RF_NODE
#undef RF_LINKSHIFT
#undef RF_TRILIST
#undef RF_TRILIST_TYPE
#undef RF_PASSPARAM
#undef RF_ELEM_PREPARE_AXIS


#undef RF_CULLMODE
#undef RF_ADDRBITS
#undef RF_TRACENAME
#undef RF_RESOLVENAME
#undef RF_DISPLACENAME

