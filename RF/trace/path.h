/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*
#ifndef TRACE_DEBUG
 #define TRACE_DEBUG (0)
#endif
*/

#if RF_ADDRBITS == 16
 #define RF_SECTOR(x) ((const rfSector16 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode16 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK16_SHIFT)
 #define RF_TRILIST RF_SECTOR16_TRILIST
 #define RF_TRILIST_TYPE int16_t
#elif RF_ADDRBITS == 32
 #define RF_SECTOR(x) ((const rfSector32 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode32 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK32_SHIFT)
 #define RF_TRILIST RF_SECTOR32_TRILIST
 #define RF_TRILIST_TYPE int32_t
#else
 #define RF_SECTOR(x) ((const rfSector64 * RF_RESTRICT)x)
 #define RF_NODE(x) ((const rfNode64 * RF_RESTRICT)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK64_SHIFT)
 #define RF_TRILIST RF_SECTOR64_TRILIST
 #define RF_TRILIST_TYPE int64_t
#endif

#if TRACEPARAMPASSBYREFERENCE
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM &param, &raystore
 #else
  #define RF_PASSPARAM &param
 #endif
#else
 #ifdef TRACERAYSTORE
  #define RF_PASSPARAM param, &raystore
 #else
  #define RF_PASSPARAM param
 #endif
#endif


////


#define RF_ELEM_PREPARE_AXIS(axis) \
  edgeindex[axis] = ( axis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MIN; \
  vectinv[axis] = -RFF_MAX; \
  if( rffabs( vector[axis] ) > (rff)0.0 ) \
  { \
    vectinv[axis] = (rff)1.0 / vector[axis]; \
    if( vector[axis] >= (rff)0.0 ) \
      edgeindex[axis] = ( axis << RF_EDGE_AXIS_SHIFT ) | RF_EDGE_MAX; \
  }


static void RF_TRACENAME( const rfHandle * RF_RESTRICT handle, TRACEPARAM param, const rfRaySingle * RF_RESTRICT ray )
{
  int nredge;
  rfssize slink;
  rff vector[3];
#if TRACEHITSIDE
  int hitside;
#endif
  rff RF_ALIGN16 vectinv[4];
  int edgeindex[3];
  rff src[3], dst[3], dist, mindist;
#if TRACEHITUV
  rff hituv[2];
#endif
#if TRACEDISTCLIP
  rff clipdist;
#endif
#if TRACEDISTCLIP || TRACEHITDIST
  rff hitdist;
#endif
  void *root;
  rfTri *trihit;
  void *modeldata;
#if TRACEDISTCLIP || TRACEHITDIST
  int axisindex;
#endif
#ifdef TRACERAYSTORE
  TRACERAYSTORE raystore;
#endif

  modeldata = RF_ADDRESS( handle->objectgraph, ((const rfGraphHeader * RF_RESTRICT)handle->objectgraph)->modeldataoffset );

  vector[0] = ray->vector[0];
  vector[1] = ray->vector[1];
  vector[2] = ray->vector[2];

#if TRACETESTROOT
  root = 0;
  if( ray->root )
  {
#endif

#if TRACERESOLVEROOT
  root = RF_RESOLVENAME( handle->objectgraph, ray->origin );
#elif TRACEDISPLACEROOT
  root = RF_DISPLACENAME( RF_ADDRESS( handle->objectgraph, ray->root ), ray->origin );
#elif TRACEQUADROOT
  root = 0;
  if( !( ray->root ) )
    goto tracevoid;
  else
  {
    trihit = 0;
    if( ( ray->root & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_PRIMITIVE )
    {
      RF_TRILIST_TYPE *trilist;
      rfTri *tri;
      root = RF_ADDRESS( handle->objectgraph, ray->root & ~( RF_QUADTRACE_TYPEMASK | ( (size_t)RF_QUADTRACE_PRIMINDEX_MAX )<< RF_QUADTRACE_PRIMINDEX_SHIFT ) );
      trilist = RF_TRILIST( RF_SECTOR(root) );
      tri = (rfTri *)RF_ADDRESS( root, (rfssize)trilist[ ray->root >> RF_QUADTRACE_PRIMINDEX_SHIFT ] << RF_LINKSHIFT );
 #if TRACEHITPOINT || TRACEHITUV
      rff srcdist, dstdist, f, vray[3];
      rff scalesize;
      /* This might not seem the simplest way, but it avoids CUDA division inaccuracy problems */
      scalesize = ((const rfGraphHeader *)handle.objectgraph)->mediansizef;
      dst[0] = src[0] + ( scalesize * vector[0] );
      dst[1] = src[1] + ( scalesize * vector[1] );
      dst[2] = src[2] + ( scalesize * vector[2] );
      dstdist = ( tri->plane[0] * dst[0] ) + ( tri->plane[1] * dst[1] ) + ( tri->plane[2] * dst[2] ) + ( tri->plane[3] );
      srcdist = ( tri->plane[0] * src[0] ) + ( tri->plane[1] * src[1] ) + ( tri->plane[2] * src[2] ) + ( tri->plane[3] );
      f = srcdist / ( srcdist - dstdist );
      vray[0] = src[0] + ( f * ( dst[0] - src[0] ) );
      vray[1] = src[1] + ( f * ( dst[1] - src[1] ) );
      vray[2] = src[2] + ( f * ( dst[2] - src[2] ) );
  #if TRACEHITPOINT
      dst[0] = vray[0];
      dst[1] = vray[1];
      dst[2] = vray[2];
  #endif
  #if TRACEHITUV
      hituv[0] = ( rfMathVectorDotProduct( &tri->edpu[0], vray ) + tri->edpu[3] );
      hituv[1] = ( rfMathVectorDotProduct( &tri->edpv[0], vray ) + tri->edpv[3] );
  #endif
  #if TRACEHITDIST
      hitdist = f * scalesize;
  #endif
  #if TRACEHITSIDE && RF_CULLMODE == 2
      hitside = ( srcdist < (rff)0.0 );
  #endif
 #elif TRACEHITDIST
      rff srcdist;
      srcdist = rfMathPlanePoint( tri->plane, src );
      hitdist = srcdist / ( srcdist - ( ( tri->plane[0] * ( src[0] + vector[0] ) ) + ( tri->plane[1] * ( src[1] + vector[1] ) ) + ( tri->plane[2] * ( src[2] + vector[2] ) ) + ( tri->plane[3] ) ) );
  #if TRACEHITSIDE && RF_CULLMODE == 2
      hitside = ( srcdist < (rff)0.0 );
  #endif
 #elif TRACEHITSIDE && RF_CULLMODE == 2
      rff srcdist;
      srcdist = rfMathPlanePoint( tri->plane, src );
      hitside = ( srcdist < (rff)0.0 );
 #endif
 #if TRACEHITSIDE && RF_CULLMODE == 0
      hitside = 0;
 #elif TRACEHITSIDE && RF_CULLMODE == 1
      hitside = 1;
 #endif
      if( 1 )
        trihit = tri;
 #ifdef TRACEVALIDATETRIANGLE
      else if( !( TRACEVALIDATETRIANGLE( RF_PASSPARAM, modeldata, handle.objectdata, vector, RF_ADDRESS( trihit, sizeof(rfTri) ), trihit->plane ) ) )
        trihit = 0;
 #endif
 #ifdef TRACEVALIDATESIDE
      else if( !( TRACEVALIDATESIDE( RF_PASSPARAM, modeldata, handle.objectdata, vector, RF_ADDRESS( trihit, sizeof(rfTri) ), trihit->plane, hitside ) ) )
        trihit = 0;
 #endif
 #ifdef TRACEVALIDATEPOINT
      else if( !( TRACEVALIDATEPOINT( RF_PASSPARAM, modeldata, handle.objectdata, vector, RF_ADDRESS( trihit, sizeof(rfTri) ), trihit->plane ), hitside, vray, hitdist ) ) )
        trihit = 0;
 #endif
 #ifdef TRACEVALIDATE
      else if( !( TRACEVALIDATE( RF_PASSPARAM, modeldata, handle.objectdata, vector, RF_ADDRESS( trihit, sizeof(rfTri) ), trihit->plane ), hitside, vray, hitdist, uv ) ) )
        trihit = 0;
 #endif
    }
    if( !( trihit ) )
    {
      root = RF_ADDRESS( handle->objectgraph, ray->root & ~( RF_QUADTRACE_TYPEMASK | RF_QUADTRACE_PRIMINDEXMASK ) );
      if( ( ray->root & RF_QUADTRACE_TYPEMASK ) == RF_QUADTRACE_TYPE_SECTORDISPLACE )
        root = RF_DISPLACENAME( root, ray->origin );
#else
  root = RF_ADDRESS( handle->objectgraph, ray->root );
#endif

#if TRACEDISTCLIP
  clipdist = ray->clipdist;
#endif

  src[0] = ray->origin[0];
  src[1] = ray->origin[1];
  src[2] = ray->origin[2];

#if RFTRACE__SSE__
  int vectmask;
  static const float RF_ALIGN16 vect3dbase[4] = { 0.0, 0.0, 0.0, 1.0 };
  __m128 vvectinv, vvector;
  vvector = _mm_load_ps( ray->vector );
  vvectinv = _mm_div_ps( _mm_set1_ps( 1.0 ), _mm_or_ps( vvector, _mm_load_ps( vect3dbase ) ) );
  vectmask = ~_mm_movemask_ps( vvector );
  edgeindex[0] = ( RF_AXIS_X << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 0 ) & 0x1 );
  edgeindex[1] = ( RF_AXIS_Y << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 1 ) & 0x1 );
  edgeindex[2] = ( RF_AXIS_Z << RF_EDGE_AXIS_SHIFT ) | ( ( vectmask >> 2 ) & 0x1 );
  _mm_store_ps( vectinv, vvectinv );
#else
  RF_ELEM_PREPARE_AXIS( 0 );
  RF_ELEM_PREPARE_AXIS( 1 );
  RF_ELEM_PREPARE_AXIS( 2 );
#endif

#if TRACEHITSIDE
  hitside = 0;
#endif
  for( ; ; )
  {
    /* Sector traversal */
    int tricount;

#if TRACE_DEBUG
printf( "  Sector %p, edge %f,%f ; %f,%f ; %f,%f\n", root, RF_SECTOR(root)->edge[0], RF_SECTOR(root)->edge[1], RF_SECTOR(root)->edge[2], RF_SECTOR(root)->edge[3], RF_SECTOR(root)->edge[4], RF_SECTOR(root)->edge[5] );
#endif

    nredge = edgeindex[0];
    mindist = ( RF_SECTOR(root)->edge[ edgeindex[0] ] - src[0] ) * vectinv[0];
    dist = ( RF_SECTOR(root)->edge[ edgeindex[1] ] - src[1] ) * vectinv[1];
    if( dist < mindist )
    {
      nredge = edgeindex[1];
      mindist = dist;
    }
    dist = ( RF_SECTOR(root)->edge[ edgeindex[2] ] - src[2] ) * vectinv[2];
    if( dist < mindist )
    {
      nredge = edgeindex[2];
      mindist = dist;
    }

    tricount = RF_SECTOR_GET_PRIMCOUNT( RF_SECTOR(root) );
    if( tricount )
    {
      RF_TRILIST_TYPE *trilist;

      dst[0] = src[0] + ( mindist * vector[0] );
      dst[1] = src[1] + ( mindist * vector[1] );
      dst[2] = src[2] + ( mindist * vector[2] );

      trihit = 0;
      trilist = RF_TRILIST( RF_SECTOR(root) );
      do
      {
        rff dstdist, srcdist, uv[2], f, vray[3];
        rfTri *tri;

        tri = (rfTri *)RF_ADDRESS( root, (rfssize)(*trilist++) << RF_LINKSHIFT );

#if RF_CULLMODE == 0
        dstdist = rfMathPlanePoint( tri->plane, dst );
        if( dstdist <= (rff)0.0 )
          continue;
        srcdist = rfMathPlanePoint( tri->plane, src );
        if( srcdist > (rff)0.0 )
          continue;
        f = dstdist - srcdist;
        vray[0] = ( src[0] * dstdist ) - ( dst[0] * srcdist );
        vray[1] = ( src[1] * dstdist ) - ( dst[1] * srcdist );
        vray[2] = ( src[2] * dstdist ) - ( dst[2] * srcdist );
        uv[0] = rfMathVectorDotProduct( &tri->edpu[0], vray ) + ( tri->edpu[3] * f );
        if( ( uv[0] < (rff)0.0 ) || ( uv[0] > f ) )
          continue;
        uv[1] = rfMathVectorDotProduct( &tri->edpv[0], vray ) + ( tri->edpv[3] * f );
        if( ( uv[1] < (rff)0.0 ) || ( ( uv[0] + uv[1] ) > f ) )
          continue;
        f = (rff)1.0 / f;
        dst[0] = vray[0] * f;
        dst[1] = vray[1] * f;
        dst[2] = vray[2] * f;
 #if TRACEHITUV
        hituv[0] = uv[0] * f;
        hituv[1] = uv[1] * f;
 #endif
 #if TRACEHITSIDE
        hitside = 0;
 #endif
#elif RF_CULLMODE == 1
        dstdist = rfMathPlanePoint( tri->plane, dst );
        if( dstdist > (rff)0.0 )
          continue;
        srcdist = rfMathPlanePoint( tri->plane, src );
        if( srcdist <= (rff)0.0 )
          continue;
        f = dstdist - srcdist;
        vray[0] = ( src[0] * dstdist ) - ( dst[0] * srcdist );
        vray[1] = ( src[1] * dstdist ) - ( dst[1] * srcdist );
        vray[2] = ( src[2] * dstdist ) - ( dst[2] * srcdist );
        uv[0] = rfMathVectorDotProduct( &tri->edpu[0], vray ) + ( tri->edpu[3] * f );
        if( ( uv[0] > (rff)0.0 ) || ( uv[0] < f ) )
          continue;
        uv[1] = rfMathVectorDotProduct( &tri->edpv[0], vray ) + ( tri->edpv[3] * f );
        if( ( uv[1] > (rff)0.0 ) || ( ( uv[0] + uv[1] ) < f ) )
          continue;
        f = (rff)1.0 / f;
        dst[0] = vray[0] * f;
        dst[1] = vray[1] * f;
        dst[2] = vray[2] * f;
 #if TRACEHITUV
        hituv[0] = uv[0] * f;
        hituv[1] = uv[1] * f;
 #endif
 #if TRACEHITSIDE
        hitside = 1;
 #endif
#else
        dstdist = rfMathPlanePoint( tri->plane, dst );
        srcdist = rfMathPlanePoint( tri->plane, src );
        if( dstdist * srcdist > (rff)0.0 )
          continue;
        f = srcdist / ( srcdist - dstdist );
        vray[0] = src[0] + f * ( dst[0] - src[0] );
        vray[1] = src[1] + f * ( dst[1] - src[1] );
        vray[2] = src[2] + f * ( dst[2] - src[2] );
        uv[0] = ( rfMathVectorDotProduct( &tri->edpu[0], vray ) + tri->edpu[3] );
        if( !( uv[0] >= (rff)0.0 ) || ( uv[0] > (rff)1.0 ) )
          continue;
        uv[1] = ( rfMathVectorDotProduct( &tri->edpv[0], vray ) + tri->edpv[3] );
        if( ( uv[1] < (rff)0.0 ) || ( ( uv[0] + uv[1] ) > (rff)1.0 ) )
          continue;
        dst[0] = vray[0];
        dst[1] = vray[1];
        dst[2] = vray[2];
 #if TRACEHITUV
        hituv[0] = uv[0];
        hituv[1] = uv[1];
 #endif
 #if TRACEHITSIDE
        hitside = ( srcdist < (rff)0.0 );
 #endif
#endif

        trihit = tri;
      } while( --tricount );

      if( trihit )
        break;
    }

#if TRACEDISTCLIP
    if( mindist >= clipdist )
      goto traceclip;
#endif

    /* Traverse through the sector's edge */
    if( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << nredge ) )
    {
      /* Neighbor is sector */
      slink = (rfssize)RF_SECTOR(root)->link[ nredge ];
      if( !( slink ) )
        goto tracevoid;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
      continue;
    }

    /* Neighbor is node */
#if TRACEDISTCLIP
    clipdist -= mindist;
#endif
    src[0] += mindist * vector[0];
    src[1] += mindist * vector[1];
    src[2] += mindist * vector[2];
    root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ nredge ] << RF_LINKSHIFT );

    /* Node traversal */
    for( ; ; )
    {
#if TRACE_DEBUG
printf( "  Node %p, axis %d, plane %f\n", root, RF_NODE_GET_AXIS( RF_NODE(root)->flags ), RF_NODE(root)->plane );
#endif

#if 0
      int linkflags, nodeside;
      linkflags = RF_NODE(root)->flags;
      nodeside = RF_NODE_MORE;
      if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
        nodeside = RF_NODE_LESS;
      root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
      if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
        break;
#else
      int linkflags;
      linkflags = RF_NODE(root)->flags;
      if( src[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
          break;
      }
      else
      {
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
          break;
      }
#endif

    }
  }

#if TRACEQUADROOT
    }
  }
#endif

#if TRACEDISTCLIP || TRACEHITDIST
  axisindex = nredge >> 1;
  hitdist = ( dst[axisindex] - ray->origin[axisindex] ) * vectinv[axisindex];
 #if TRACEDISTCLIP
  if( hitdist > clipdist )
    goto traceclip;
 #endif
#endif

  TRACEHIT( RF_PASSPARAM, modeldata, handle->objectdata, vector, dst,
#if TRACEHITUV
    hituv,
#else
    0,
#endif
    RF_ADDRESS( trihit, sizeof(rfTri) ),
#if TRACEHITDIST
    hitdist,
#else
    0.0,
#endif
    trihit->plane,
#if TRACEHITSIDE
    hitside,
#else
    0,
#endif
    RF_ADDRESSDIFF( root, handle->objectgraph ), 0 );

#if TRACETESTROOT
  }
#endif

  end:
#ifdef TRACEEND
  TRACEEND( RF_PASSPARAM, vector );
#endif
  return;

////

  tracevoid:
#ifdef TRACEVOID
  TRACEVOID( RF_PASSPARAM, vector, 0 );
#endif
  goto end;

////

#if TRACEDISTCLIP
  traceclip:
 #ifdef TRACEVOID
  TRACEVOID( RF_PASSPARAM, vector, RF_ADDRESSDIFF( root, handle->objectgraph ) );
 #endif
  goto end;
#endif
}


#undef RF_SECTOR
#undef RF_NODE
#undef RF_LINKSHIFT
#undef RF_TRILIST
#undef RF_TRILIST_TYPE
#undef RF_PASSPARAM
#undef RF_ELEM_PREPARE_AXIS


#undef RF_CULLMODE
#undef RF_ADDRBITS
#undef RF_TRACENAME
#undef RF_RESOLVENAME
#undef RF_DISPLACENAME

