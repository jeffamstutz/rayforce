/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#if RF_ADDRBITS == 16
 #define RF_SECTOR(x) ((const rfSector16 *)x)
 #define RF_NODE(x) ((const rfNode16 *)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK16_SHIFT)
#elif RF_ADDRBITS == 32
 #define RF_SECTOR(x) ((const rfSector32 *)x)
 #define RF_NODE(x) ((const rfNode32 *)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK32_SHIFT)
#else
 #define RF_SECTOR(x) ((const rfSector64 *)x)
 #define RF_NODE(x) ((const rfNode64 *)x)
 #define RF_LINKSHIFT (RF_GRAPH_LINK64_SHIFT)
#endif


RF_DECL_ATTRIB static inline void *RF_RESOLVENAME( void *graphmemory, const rff *origin )
{
  rfssize slink;
  int linkflags, nodeside;
  int index;
  rfOriginTable *origintable;
  void *root;

  origintable = (rfOriginTable *)RF_ADDRESS( graphmemory, RF_GRAPHHEADER_SIZE );
  index = rfResolveOriginIndex( origintable, origin );
  root = RF_ADDRESS( graphmemory, RF_ORIGINTABLE_LIST(origintable)[index] );

/*
printf( "Index %d, edge %f,%f ; %f,%f ; %f,%f\n", (int)index, RF_SECTOR(root)->edge[0], RF_SECTOR(root)->edge[1], RF_SECTOR(root)->edge[2], RF_SECTOR(root)->edge[3], RF_SECTOR(root)->edge[4], RF_SECTOR(root)->edge[5] );
*/

  /* We only need to walk "up" on XYZ */

  for( ; ; )
  {
    if( origin[RF_AXIS_X] < RF_SECTOR(root)->edge[ RF_EDGE_MAXX ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXX ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXX ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXX ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }

  for( ; ; )
  {
    if( origin[RF_AXIS_Y] < RF_SECTOR(root)->edge[ RF_EDGE_MAXY ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXY ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXY ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXY ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }

  for( ; ; )
  {
    if( origin[RF_AXIS_Z] < RF_SECTOR(root)->edge[ RF_EDGE_MAXZ ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXZ ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXZ ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXZ ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }

  return root;
}



RF_DECL_ATTRIB static inline void *RF_DISPLACENAME( void *root, const rff *origin )
{
  int linkflags, nodeside;
  rfssize slink;

  /* X */
  for( ; ; )
  {
    if( origin[RF_AXIS_X] > RF_SECTOR(root)->edge[ RF_EDGE_MINX ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MINX ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINX ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINX ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  for( ; ; )
  {
    if( origin[RF_AXIS_X] < RF_SECTOR(root)->edge[ RF_EDGE_MAXX ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXX ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXX ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXX ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  /* X */

  /* Y */
  for( ; ; )
  {
    if( origin[RF_AXIS_Y] > RF_SECTOR(root)->edge[ RF_EDGE_MINY ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MINY ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINY ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINY ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  for( ; ; )
  {
    if( origin[RF_AXIS_Y] < RF_SECTOR(root)->edge[ RF_EDGE_MAXY ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXY ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXY ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXY ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  /* Y */

  /* Z */
  for( ; ; )
  {
    if( origin[RF_AXIS_Z] > RF_SECTOR(root)->edge[ RF_EDGE_MINZ ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MINZ ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINZ ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINZ ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  for( ; ; )
  {
    if( origin[RF_AXIS_Z] < RF_SECTOR(root)->edge[ RF_EDGE_MAXZ ] )
      break;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXZ ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXZ ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nodeside = RF_NODE_MORE;
        if( origin[ RF_NODE_GET_AXIS(linkflags) ] < RF_NODE(root)->plane )
          nodeside = RF_NODE_LESS;
        root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[nodeside] << RF_LINKSHIFT );
        if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << nodeside ) )
          break;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXZ ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  /* Z */

  return root;
}


////


#if RFTRACE__SSE__

 #ifdef RF_RESOLVE4NAME

RF_DECL_ATTRIB static inline void *RF_RESOLVE4NAME( void *graphmemory, const rff *origin, int axiswidth )
{
  int32_t edgemask, linkflags, nbits;
  rfssize slink;
  int index;
  rfOriginTable *origintable;
  void *root;
  __m128 vsrc[RF_AXIS_COUNT];
  const rff *srcaxis;
  rff src[RF_AXIS_COUNT];

  origintable = (rfOriginTable *)RF_ADDRESS( graphmemory, RF_GRAPHHEADER_SIZE );
  srcaxis = &origin[ RF_AXIS_X * axiswidth ];
  src[RF_AXIS_X] = rffmin( rffmin( srcaxis[0], srcaxis[1] ), rffmin( srcaxis[2], srcaxis[3] ) );
  srcaxis = &origin[ RF_AXIS_Y * axiswidth ];
  src[RF_AXIS_Y] = rffmin( rffmin( srcaxis[0], srcaxis[1] ), rffmin( srcaxis[2], srcaxis[3] ) );
  srcaxis = &origin[ RF_AXIS_Z * axiswidth ];
  src[RF_AXIS_Z] = rffmin( rffmin( srcaxis[0], srcaxis[1] ), rffmin( srcaxis[2], srcaxis[3] ) );

  index = rfResolveOriginIndex( origintable, src );
  root = RF_ADDRESS( graphmemory, RF_ORIGINTABLE_LIST(origintable)[index] );

  vsrc[RF_AXIS_X] = _mm_load_ps( &origin[ RF_AXIS_X * axiswidth ] );
  vsrc[RF_AXIS_Y] = _mm_load_ps( &origin[ RF_AXIS_Y * axiswidth ] );
  vsrc[RF_AXIS_Z] = _mm_load_ps( &origin[ RF_AXIS_Z * axiswidth ] );

/*
printf( "Index %d, edge %f,%f ; %f,%f ; %f,%f\n", (int)index, RF_SECTOR(root)->edge[0], RF_SECTOR(root)->edge[1], RF_SECTOR(root)->edge[2], RF_SECTOR(root)->edge[3], RF_SECTOR(root)->edge[4], RF_SECTOR(root)->edge[5] );
*/

  /* We only need to walk "up" on XYZ */

  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmplt_ps( vsrc[RF_AXIS_X], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MAXX ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXX ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXX ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXX ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }

  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmplt_ps( vsrc[RF_AXIS_Y], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MAXY ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXY ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXY ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXY ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }

  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmplt_ps( vsrc[RF_AXIS_Z], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MAXZ ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXZ ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXZ ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXZ ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }

  return root;
}

 #endif

 #ifdef RF_DISPLACE4NAME

RF_DECL_ATTRIB static inline void *RF_DISPLACE4NAME( void *root, const rff *origin, int axiswidth )
{
  int32_t edgemask, linkflags, nbits;
  rfssize slink;
  __m128 vsrc[RF_AXIS_COUNT];

  vsrc[RF_AXIS_X] = _mm_load_ps( &origin[ RF_AXIS_X * axiswidth ] );
  vsrc[RF_AXIS_Y] = _mm_load_ps( &origin[ RF_AXIS_Y * axiswidth ] );
  vsrc[RF_AXIS_Z] = _mm_load_ps( &origin[ RF_AXIS_Z * axiswidth ] );

  /* X */
  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmpgt_ps( vsrc[RF_AXIS_X], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MINX ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MINX ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINX ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINX ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmplt_ps( vsrc[RF_AXIS_X], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MAXX ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXX ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXX ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXX ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  /* X */

  /* Y */
  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmpgt_ps( vsrc[RF_AXIS_Y], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MINY ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MINY ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINY ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINY ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmplt_ps( vsrc[RF_AXIS_Y], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MAXY ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXY ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXY ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXY ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  /* Y */

  /* Z */
  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmpgt_ps( vsrc[RF_AXIS_Z], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MINZ ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MINZ ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINZ ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MINZ ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  for( ; ; )
  {
    edgemask = _mm_movemask_ps( _mm_cmplt_ps( vsrc[RF_AXIS_Z], _mm_set1_ps( RF_SECTOR(root)->edge[ RF_EDGE_MAXZ ] ) ) );
    if( edgemask == 0xf )
      break;
    else if( edgemask )
      return 0;
    if( !( RF_SECTOR(root)->flags & ( (RF_LINK_SECTOR<<RF_SECTOR_LINKFLAGS_SHIFT) << RF_EDGE_MAXZ ) ) )
    {
      root = RF_ADDRESS( root, (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXZ ] << RF_LINKSHIFT );
      for( ; ; )
      {
        linkflags = RF_NODE(root)->flags;
        nbits = _mm_movemask_ps( _mm_cmplt_ps( vsrc[ RF_NODE_GET_AXIS(linkflags) ], _mm_set1_ps( RF_NODE(root)->plane ) ) );
        if( !( nbits ) )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_MORE] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_MORE ) )
            break;
        }
        else if( nbits == 0xF )
        {
          root = RF_ADDRESS( root, (rfssize)RF_NODE(root)->link[RF_NODE_LESS] << RF_LINKSHIFT );
          if( linkflags & ( (RF_LINK_SECTOR<<RF_NODE_LINKFLAGS_SHIFT) << RF_NODE_LESS ) )
            break;
        }
        else
          return 0;
      }
    }
    else
    {
      slink = (rfssize)RF_SECTOR(root)->link[ RF_EDGE_MAXZ ];
      if( !( slink ) )
        break;
      root = RF_ADDRESS( root, slink << RF_LINKSHIFT );
    }
  }
  /* Z */

  return root;
}

 #endif

 #undef RF_RESOLVE4NAME
 #undef RF_DISPLACE4NAME

#endif



#undef RF_SECTOR
#undef RF_NODE
#undef RF_LINKSHIFT
#undef RF_RESOLVENAME
#undef RF_DISPLACENAME


#undef RF_ADDRBITS

