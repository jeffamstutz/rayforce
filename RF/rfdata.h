/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#ifndef RFDATA_H
#define RFDATA_H


#include "rfgraph.h"

#include "cpuinfo.h"


typedef struct
{
  uint32_t flags;
  int addressmode;
  rff edge[RF_EDGE_COUNT];

  rfint target;
  int numanode;
  int deviceindex;

  void *memory;
  size_t memsize;
  size_t datasize;
  void *origintable;

  size_t graphcost;
  size_t sectorcount;
  size_t nodecount;
  size_t trianglecount;
  size_t trirefcount;
  size_t trirefmaximum;

} rfGraph;

#define RF_GRAPH_FLAGS_PACKED (0x1)
#define RF_GRAPH_FLAGS_INTERLEAVED (0x2)

enum
{
  RF_GRAPH_ADDRESSMODE_16BITS,
  RF_GRAPH_ADDRESSMODE_32BITS,
  RF_GRAPH_ADDRESSMODE_64BITS
};


typedef struct
{
  size_t primitivecount;
  void *datapointer;
  size_t datasize;
  size_t datastride;
  void *vertexpointer;
  rfint vertextype;
  size_t vertexstride;
  void *indexpointer;
  rfint indextype;
  void *modeldata;
  size_t modeldatasize;
  int estimatecostflag;
} rfGeometry;


/**/


struct _rfContext
{
  int errorcode;
  void (*errorhandler)( void *errorp, rfint code, char *string );
  void *errorpointer;

  cpuInfo cpu;

  void *numadata;
  int numanodecount;

  void *cudadata;
  int cudadevicecount;

  void *scenelist;
};


struct _rfScene
{
  rfContext *context;
  int flags;

  rfint target;
  rfint targetindex;
  void *targetpointer;
  rfint datatype;

  void *modellist;
  void *objectlist;
  rfint activeobjectcount;

  rfObject *singleobject;
  rff edge[RF_EDGE_COUNT];



  /* TODO: Storage for multiobjects scenes */
  size_t scenegraphmemsize;



  rfHandle scenehandle;

  mmListNode contextnode;
};

#define RF_SCENE_FLAGS_MODIFIED (0x1)
#define RF_SCENE_FLAGS_READY (0x2)
#define RF_SCENE_FLAGS_MULTIOBJECTS (0x4)


struct _rfModel
{
  rfContext *context;
  rfScene *scene;
  int flags;
  rfint modeltype;
  mmAtomic32 usecount;

  /* Geometry */
  rfGeometry geom;

  /* Graph build parameters */
  int buildquality; /* From RF_BUILDHINT_VALUE_MIN to RF_BUILDHINT_VALUE_MAX */
  int buildmemory; /* From RF_BUILDHINT_VALUE_MIN to RF_BUILDHINT_VALUE_MAX */
  int buildlayout; /* RF_BUILDHINT_LAYOUTMODE_{FULL,NONE} */

  /* Graph */
  rfGraph graph;

  mmListNode scenenode;
};

#define RF_MODEL_FLAGS_BUILT (0x1)
#define RF_MODEL_FLAGS_DELETED (0x2)


struct _rfObject
{
  rfContext *context;
  rfScene *scene;
  int flags;

  rfModel *model;
  void *value;
  int cullmode;
  rff matrix[16];
  rff matrixinv[16];

  int sceneobjectindex;

  /* Stored in system, NUMA or CUDA memory as appropriate for scene */
  void *objectdata;
  size_t objectdatasize;

  mmListNode scenenode;
};

#define RF_OBJECT_FLAGS_IDENTITYMATRIX (0x1)
#define RF_OBJECT_FLAGS_ENABLED (0x2)


/**/


void rfRegisterError( rfContext *context, rfint code, char *string );


/**/


#endif /* RFDATA_H */


