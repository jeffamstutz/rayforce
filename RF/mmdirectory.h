/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

typedef struct
{
  mmAtomicP *table;
  size_t pagecount;
  size_t pagesize;
  size_t pagemask;
  size_t pageshift;
} mmDirectory;

int mmDirectoryInit( mmDirectory *dir, size_t pageshift, size_t pagecount );
void mmDirectoryFree( mmDirectory *dir );

void mmDirectorySetSize( mmDirectory *dir, size_t size );

void mmDirectorySet( mmDirectory *dir, size_t index, void *entry );
void *mmDirectoryGet( mmDirectory *dir, size_t index );
void mmDirectorySetFast( mmDirectory *dir, size_t index, void *entry );
void *mmDirectoryGetFast( mmDirectory *dir, size_t index );


