/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <pthread.h>
#include <sched.h>
#include <limits.h>


#ifdef __cplusplus
extern "C" {
#endif


static inline void mtYield()
{
  return;
}


typedef struct mtThread mtThread;

struct mtThread
{
  void *qtthread;
};

#define MT_THREAD_FLAGS_JOINABLE (0x1)
#define MT_THREAD_FLAGS_SETSTACK (0x2)

void mtThreadCreate( mtThread *thread, void *(*threadmain)( void *value ), void *value, int flags, void *stack, size_t stacksize );
void mtThreadExit();
void mtThreadJoin( mtThread *thread );


typedef struct mtMutex mtMutex;

struct mtMutex
{
  void *qtmutex;
};

void mtMutexInit( mtMutex *mutex );
void mtMutexDestroy( mtMutex *mutex );
void mtMutexLock( mtMutex *mutex );
void mtMutexUnlock( mtMutex *mutex );
int mtMutexTryLock( mtMutex *mutex );


typedef struct mtSignal mtSignal;

struct mtSignal
{
  void *qtcondition;
};

void mtSignalInit( mtSignal *signal );
void mtSignalDestroy( mtSignal *signal );
void mtSignalWake( mtSignal *signal );
void mtSignalBroadcast( mtSignal *signal );
void mtSignalWait( mtSignal *signal, mtMutex *mutex );
void mtSignalWaitTimeout( mtSignal *signal, mtMutex *mutex, long milliseconds );


#ifdef MM_ATOMIC_SUPPORT

typedef struct mtSpin mtSpin;

struct mtSpin
{
  mmAtomic32 atomicspin;
};

static inline void mtSpinInit( mtSpin *spin )
{
  mmAtomicWrite32( &spin->atomicspin, 0x0 );
  return;
}

static inline void mtSpinDestroy( mtSpin *spin )
{
  return;
}

static inline void mtSpinLock( mtSpin *spin )
{
  mmAtomicSpin32( &spin->atomicspin, 0x0, 0x1 );
  return;
}

static inline void mtSpinUnlock( mtSpin *spin )
{
  mmAtomicWrite32( &spin->atomicspin, 0x0 );
  return;
}

static inline int mtSpinTryLock( mtSpin *spin )
{
  return mmAtomicCmpReplace32( &spin->atomicspin, 0x0, 0x1 );
}

#else

typedef struct mtMutex mtSpin;

 #define mtSpinInit(a) mtMutexInit(a)
 #define mtSpinDestroy(a) mtMutexDestroy(a)
 #define mtSpinLock(a) mtMutexLock(a)
 #define mtSpinUnlock(a) mtMutexUnlock(a)
 #define mtSpinTryLock(a) mtMutexTryLock(a)

#endif


#ifdef __cplusplus
}
#endif


