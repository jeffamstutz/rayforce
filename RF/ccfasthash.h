/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */


/*
#define CC_FASTHASH_ENTRY_TYPE mmDebugSymbolHash

#define CC_FASTHASH_ENTRYKEY(x) MMDEBUG(SymbolHashKey)(x)
#define CC_FASTHASH_ENTRYCMP(x,y) MMDEBUG(SymbolHashCmp)(x,y)
#define CC_FASTHASH_ENTRYVALID(x) MMDEBUG(SymbolHashValid)(x)
#define CC_FASTHASH_ENTRYCLEAR(x) MMDEBUG(SymbolHashClear)(x)

#define CC_FASTHASH_INIT MMDEBUG(SymbolHashInit)
#define CC_FASTHASH_GET MMDEBUG(SymbolHashGet)
#define CC_FASTHASH_SET MMDEBUG(SymbolHashSet)
#define CC_FASTHASH_DELETE MMDEBUG(SymbolHashDelete)
*/

static void CC_FASTHASH_INIT( CC_FASTHASH_ENTRY_TYPE *entrytable, uint32_t hashmask )
{
  int entryindex;
  CC_FASTHASH_ENTRY_TYPE *entry;

  for( entryindex = 0 ; entryindex <= hashmask ; entryindex++ )
  {
    entry = &entrytable[entryindex];
    CC_FASTHASH_ENTRYCLEAR( entry );
  }

  return;
}

static CC_FASTHASH_ENTRY_TYPE *CC_FASTHASH_GET( CC_FASTHASH_ENTRY_TYPE *entrytable, CC_FASTHASH_ENTRY_TYPE *entryref, uint32_t hashmask )
{
  uint32_t hashkey;
  CC_FASTHASH_ENTRY_TYPE *entry;

  hashkey = CC_FASTHASH_ENTRYKEY( entryref ) & hashmask;
  for( ; ; hashkey = ( hashkey + 1 ) & hashmask )
  {
    entry = &entrytable[hashkey];
    if( !( CC_FASTHASH_ENTRYVALID( entry ) ) )
      break;
    if( CC_FASTHASH_ENTRYCMP( entry, entryref ) )
      return entry;
  }

  return 0;
}

static void CC_FASTHASH_SET( CC_FASTHASH_ENTRY_TYPE *entrytable, CC_FASTHASH_ENTRY_TYPE *entryref, uint32_t hashmask )
{
  uint32_t hashkey;
  CC_FASTHASH_ENTRY_TYPE *entry;

  hashkey = CC_FASTHASH_ENTRYKEY( entryref ) & hashmask;
  for( ; ; hashkey = ( hashkey + 1 ) & hashmask )
  {
    entry = &entrytable[hashkey];
    if( !( CC_FASTHASH_ENTRYVALID( entry ) ) )
    {
      *entry = *entryref;
      break;
    }
    if( CC_FASTHASH_ENTRYCMP( entry, entryref ) )
    {
      *entry = *entryref;
      break;
    }
  }

  return;
}

static void CC_FASTHASH_DELETE( CC_FASTHASH_ENTRY_TYPE *entrytable, CC_FASTHASH_ENTRY_TYPE *entryref, uint32_t hashmask )
{
  uint32_t hashkey, delbase, targetkey, sourcekey, targetindex, sourceindex;
  CC_FASTHASH_ENTRY_TYPE *entry, *entrysource, *entrytarget;

  hashkey = CC_FASTHASH_ENTRYKEY( entryref ) & hashmask;
  for( ; ; hashkey = ( hashkey + 1 ) & hashmask )
  {
    entry = &entrytable[hashkey];
    if( !( CC_FASTHASH_ENTRYVALID( entry ) ) )
    {
      fprintf( stderr, "SHOULD NOT HAPPEN %s:%d\n", __FILE__, __LINE__ );
      return;
    }
    if( CC_FASTHASH_ENTRYCMP( entry, entryref ) )
      break;
  }
  for( delbase = hashkey ; ; )
  {
    delbase = ( delbase - 1 ) & hashmask;
    if( !( CC_FASTHASH_ENTRYVALID( &entrytable[delbase] ) ) )
      break;
  }
  delbase = ( delbase + 1 ) & hashmask;
  for( ; ; )
  {
    targetkey = hashkey;
    entrytarget = 0;
    for( sourceindex = hashkey ; ; )
    {
      sourceindex = ( sourceindex + 1 ) & hashmask;
      entrysource = &entrytable[sourceindex];
      if( !( CC_FASTHASH_ENTRYVALID( entrysource ) ) )
        break;
      sourcekey = CC_FASTHASH_ENTRYKEY( entrysource ) & hashmask;
      /* Check if moving the entry backwards is allowed without breaking chain */
      if( targetkey >= delbase )
      {
        if( ( sourcekey < delbase ) || ( sourcekey > targetkey ) )
          continue;
      }
      else if( ( sourcekey > targetkey ) && ( sourcekey < delbase ) )
        continue;
      entrytarget = entrysource;
      targetkey = sourcekey;
      targetindex = sourceindex;
    }
    entry = &entrytable[hashkey];
    if( !( entrytarget ) )
    {
      CC_FASTHASH_ENTRYCLEAR( entry );
      break;
    }
    *entry = *entrytarget;
    hashkey = targetindex;
  }

  return;
}


#undef CC_FASTHASH_ENTRY_TYPE
#undef CC_FASTHASH_ENTRYKEY
#undef CC_FASTHASH_ENTRYCMP
#undef CC_FASTHASH_ENTRYVALID
#undef CC_FASTHASH_ENTRYCLEAR

#undef CC_FASTHASH_INIT
#undef CC_FASTHASH_GET
#undef CC_FASTHASH_SET
#undef CC_FASTHASH_DELETE
