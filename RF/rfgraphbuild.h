/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */


////


void rfGraphOriginInit( rfOriginTable *origintable, rff *edge, rfsize originwidth );

typedef struct
{
  mmVolumeHead memvolume;
  mmBlockHead nodeblock;
  mmDirectory sectordir;
  mmDirectory nodedir;

  rff edge[RF_EDGE_COUNT];

  rfOriginTable origintable;
  rforigin *origin;

  rfTri *trilist;
  rfindex trianglecount;
  rfindex nodecount;
  rfindex sectorcount;
  rfindex trirefcount;
  rfsize sector16size;
  rfsize sector32size;
  rfsize sector64size;

  int sectortrimax;

  /* Layout information */
  int layoutmode;
  int layoutaddressmode;
  rfindex *sectoroffset;
  rfindex *nodeoffset;
  rfindex *triangleoffset;
  size_t layoutmemory;
  /* Layout information */

  /* Triangle data offsets */
  uint32_t *tridataoffset;

} rfGraphCache;

int rfBuildTriangle( rfTri * const restrict tri, rff * restrict v0, rff * restrict v1, rff * restrict v2, const rff biasf );

void rfBuildGraphCache( rfGraphCache *cache, rfGeometry *geometry, rff *box, rfindex originwidth, rfint buildquality, rfint buildmemory );
void rfFreeGraphCache( rfGraphCache *cache );
double rfEvaluateGraphCost( rfGraphCache *cache );

void rfInitGraphLayout( rfGraphCache *cache );
void rfBuildGraphLayout( rfGraphCache *cache, rfGeometry *geometry, rfint buildlayout );
void rfFreeGraphLayout( rfGraphCache *cache );

int rfBuildGraphMemory( rfContext *context, rfGraph *graph, rfGraphCache *cache, rfGeometry *geometry, rfint target, rfint targetindex, void *targetpointer );
void rfFreeGraphMemory( rfContext *context, rfGraph *graph );

int rfBuildGraph( rfContext *context, rfGraph *graph, rfGeometry *geometry, rff *box, rfindex originwidth, rfint target, rfint targetindex, void *targetpointer, rfint buildquality, rfint buildmemory, rfint buildlayout );
int rfSetGraph( rfContext *context, rfGraph *graph, void *graphmem, size_t graphmemsize, rfint target, rfint targetindex, void *targetpointer );
int rfCopyGraph( rfContext *context, rfGraph *graph, rfGraph *graphref, rfint target, rfint targetindex, void *targetpointer );

enum
{
  RF_GRAPHCACHE_LAYOUTMODE_NONE,
  RF_GRAPHCACHE_LAYOUTMODE_FULL
};


////


typedef union
{
  void *p;
  rfindex i;
} rfPrepLink;

typedef struct
{
  rfindex index;
  int32_t flags;
  int32_t tricount;
  rff edge[RF_EDGE_COUNT];
  rff value;
  rfPrepLink link[RF_EDGE_COUNT];
} rfPrepSector;

#define PREP_SECTOR_TRILIST(x) (rfPrepLink *)(ADDRESS(x,sizeof(rfPrepSector)))

typedef struct
{
  rfindex index;
  uint32_t status;
  rff plane;
  int32_t nodeflags;
  rfPrepLink child[RF_NODE_CHILD_COUNT];
} rfPrepNode;

#define PREP_NODE_GETAXIS(n) ((n)->nodeflags&0x3)

enum
{
  PREPNODE_STATUS_NOTREADY,
  PREPNODE_STATUS_NODE,
  PREPNODE_STATUS_SECTOR
};


////


#define RF_CACHE_IDENTIFIER "RayforceGraph"

#if RF_CONFIG_PACK_MEMORY
 #define RF_CACHE_VERSION (0x0004)
#else
 #define RF_CACHE_VERSION (0x0003)
#endif


#define RF_GRAPH_ALIGNMENT (1<<4)

#define RF_GRAPH_SIZE_ALIGN(x) (((x)+(RF_GRAPH_ALIGNMENT-1))&~(RF_GRAPH_ALIGNMENT-1))


#define RF_GRAPH_OFFSET_SHIFT (4)

#define RF_GRAPH_OFFSET_COUNT(x) (((x)+(RF_GRAPH_ALIGNMENT-1))>>RF_GRAPH_OFFSET_SHIFT)


////




