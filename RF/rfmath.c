/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <time.h>
#include <stdarg.h>
#include <errno.h>
#include <assert.h>

#include "cpuconfig.h"

#include "rfmath.h"


void rfMathMatrixIdentityf( float *m )
{
  memset( m, 0, sizeof(float)*16 );
  m[0*4+0] = m[1*4+1] = m[2*4+2] = m[3*4+3] = 1.0;
  return;
}

void rfMathMatrixIdentityd( double *m )
{
  memset( m, 0, sizeof(double)*16 );
  m[0*4+0] = m[1*4+1] = m[2*4+2] = m[3*4+3] = 1.0;
  return;
}



void rfMathMatrixInvertf( float *mdst, float *m )
{
  float sx, sy, sz;
  sx = 1.0 / ( m[0*4+0]*m[0*4+0] + m[0*4+1]*m[0*4+1] + m[0*4+2]*m[0*4+2] );
  sy = 1.0 / ( m[1*4+0]*m[1*4+0] + m[1*4+1]*m[1*4+1] + m[1*4+2]*m[1*4+2] );
  sz = 1.0 / ( m[2*4+0]*m[2*4+0] + m[2*4+1]*m[2*4+1] + m[2*4+2]*m[2*4+2] );
  mdst[0*4+0] = m[0*4+0] * sx;
  mdst[1*4+0] = m[0*4+1] * sx;
  mdst[2*4+0] = m[0*4+2] * sx;
  mdst[3*4+0] = -( m[3*4+0]*m[0*4+0] + m[3*4+1]*m[0*4+1] + m[3*4+2]*m[0*4+2] ) * sx;
  mdst[0*4+1] = m[1*4+0] * sy;
  mdst[1*4+1] = m[1*4+1] * sy;
  mdst[2*4+1] = m[1*4+2] * sy;
  mdst[3*4+1] = -( m[3*4+0]*m[1*4+0] + m[3*4+1]*m[1*4+1] + m[3*4+2]*m[1*4+2] ) * sy;
  mdst[0*4+2] = m[2*4+0] * sz;
  mdst[1*4+2] = m[2*4+1] * sz;
  mdst[2*4+2] = m[2*4+2] * sz;
  mdst[3*4+2] = -( m[3*4+0]*m[2*4+0] + m[3*4+1]*m[2*4+1] + m[3*4+2]*m[2*4+2] ) * sz;
  mdst[0*4+3] = 0.0;
  mdst[1*4+3] = 0.0;
  mdst[2*4+3] = 0.0;
  mdst[3*4+3] = 1.0;
  return;
}

void rfMathMatrixInvertd( double *mdst, double *m )
{
  double sx, sy, sz;
  sx = 1.0 / ( m[0*4+0]*m[0*4+0] + m[0*4+1]*m[0*4+1] + m[0*4+2]*m[0*4+2] );
  sy = 1.0 / ( m[1*4+0]*m[1*4+0] + m[1*4+1]*m[1*4+1] + m[1*4+2]*m[1*4+2] );
  sz = 1.0 / ( m[2*4+0]*m[2*4+0] + m[2*4+1]*m[2*4+1] + m[2*4+2]*m[2*4+2] );
  mdst[0*4+0] = m[0*4+0] * sx;
  mdst[1*4+0] = m[0*4+1] * sx;
  mdst[2*4+0] = m[0*4+2] * sx;
  mdst[3*4+0] = -( m[3*4+0]*m[0*4+0] + m[3*4+1]*m[0*4+1] + m[3*4+2]*m[0*4+2] ) * sx;
  mdst[0*4+1] = m[1*4+0] * sy;
  mdst[1*4+1] = m[1*4+1] * sy;
  mdst[2*4+1] = m[1*4+2] * sy;
  mdst[3*4+1] = -( m[3*4+0]*m[1*4+0] + m[3*4+1]*m[1*4+1] + m[3*4+2]*m[1*4+2] ) * sy;
  mdst[0*4+2] = m[2*4+0] * sz;
  mdst[1*4+2] = m[2*4+1] * sz;
  mdst[2*4+2] = m[2*4+2] * sz;
  mdst[3*4+2] = -( m[3*4+0]*m[2*4+0] + m[3*4+1]*m[2*4+1] + m[3*4+2]*m[2*4+2] ) * sz;
  mdst[0*4+3] = 0.0;
  mdst[1*4+3] = 0.0;
  mdst[2*4+3] = 0.0;
  mdst[3*4+3] = 1.0;
  return;
}



void rfMathPlaneIntersect3f( float *pt, float *p0, float *p1, float *p2 )
{
  float d;
  float n0[3], n1[3], n2[3];
  rfMathVectorCrossProduct( n0, &p1[0], &p2[0] );
  rfMathVectorCrossProduct( n1, &p2[0], &p0[0] );
  rfMathVectorCrossProduct( n2, &p0[0], &p1[0] );
  d = -1.0 / rfMathVectorDotProduct( p0, n0 );
  rfMathVectorMulScalar( n0, p0[3] );
  rfMathVectorMulScalar( n1, p1[3] );
  rfMathVectorMulScalar( n2, p2[3] );
  pt[0] = ( n0[0] + n1[0] + n2[0] ) * d;
  pt[1] = ( n0[1] + n1[1] + n2[1] ) * d;
  pt[2] = ( n0[2] + n1[2] + n2[2] ) * d;
  return;
}

void rfMathPlaneIntersect3d( double *pt, double *p0, double *p1, double *p2 )
{
  double d;
  double n0[3], n1[3], n2[3];
  rfMathVectorCrossProduct( n0, &p1[0], &p2[0] );
  rfMathVectorCrossProduct( n1, &p2[0], &p0[0] );
  rfMathVectorCrossProduct( n2, &p0[0], &p1[0] );
  d = -1.0 / rfMathVectorDotProduct( p0, n0 );
  rfMathVectorMulScalar( n0, p0[3] );
  rfMathVectorMulScalar( n1, p1[3] );
  rfMathVectorMulScalar( n2, p2[3] );
  pt[0] = ( n0[0] + n1[0] + n2[0] ) * d;
  pt[1] = ( n0[1] + n1[1] + n2[1] ) * d;
  pt[2] = ( n0[2] + n1[2] + n2[2] ) * d;
  return;
}
