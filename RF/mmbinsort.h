/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */


void *mmBinSortInit( size_t itemlistoffset, int rootbucketcount, int groupbucketcount, double rootmin, double rootmax, int groupthreshold, double (*itemvaluecallback)( void *item ), int maxdepth, int numanodeindex );
void mmBinSortFree( void *binsort );


void mmBinSortAdd( void *binsort, void *item, double itemvalue );
void mmBinSortRemove( void *binsort, void *item, double itemvalue );
void mmBinSortUpdate( void *binsort, void *item, double olditemvalue, double newitemvalue );

void *mmBinSortGetRootBucket( void *binsort, int bucketindex, int *itemcount );
void *mmBinSortGetGroupBucket( void *binsortgroup, int bucketindex, int *itemcount );


void *mmBinSortGetFirstItem( void *binsort, double failmax );


