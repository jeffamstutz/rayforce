/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>
#ifdef __unix__
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <fcntl.h>
 #include <sys/mman.h>
#endif

#include "cpuconfig.h"

#include "cpuinfo.h"

#include "cc.h"
#include "mm.h"
#include "mmdirectory.h"

#include "rfmath.h"

#include "rf.h"
#include "rfdefs.h"
#include "rfdata.h"
#include "rfinternal.h"
#include "rfgraph.h"
#include "rfgraphbuild.h"
#include "rfscene.h"

#include "rfnuma.h"


////


/**
 * @file
 *
 * Rayforce core interface.
 */

/** @defgroup libRF
 *
 * The libRF interface exposes the Rayforce standard API functions.
 *
 * @{
 */


static void defaultErrorHandler( void *userpointer, rfint code, char *string )
{
  char *codestring;
  switch( code )
  {
    case RF_ERROR_INVALID_VALUE:
      codestring = "RF_ERROR_INVALID_VALUE";
      break;
    case RF_ERROR_INVALID_TARGET:
      codestring = "RF_ERROR_INVALID_TARGET";
      break;
    case RF_ERROR_INVALID_OPERATION:
      codestring = "RF_ERROR_INVALID_OPERATION";
      break;
    case RF_ERROR_NOT_SUPPORTED:
      codestring = "RF_ERROR_NOT_SUPPORTED";
      break;
    case RF_ERROR_MEMORY_DENIED:
      codestring = "RF_ERROR_MEMORY_DENIED";
      break;
    case RF_ERROR_NUMA_BAD_OPERATION:
      codestring = "RF_ERROR_NUMA_BAD_OPERATION";
      break;
    case RF_ERROR_NUMA_MEMORY_DENIED:
      codestring = "RF_ERROR_NUMA_MEMORY_DENIED";
      break;
    case RF_ERROR_NUMA_UNKNOWN:
      codestring = "RF_ERROR_NUMA_UNKNOWN";
      break;
    case RF_ERROR_CUDA_BAD_OPERATION:
      codestring = "RF_ERROR_CUDA_BAD_OPERATION";
      break;
    case RF_ERROR_CUDA_MEMORY_DENIED:
      codestring = "RF_ERROR_CUDA_MEMORY_DENIED";
      break;
    case RF_ERROR_CUDA_UNKNOWN:
      codestring = "RF_ERROR_CUDA_UNKNOWN";
      break;
    default:
      codestring = 0;
  }
  fprintf( stderr, "WARNING: %s, %s\n", codestring, string );
  return;
}

void rfRegisterError( rfContext *context, rfint code, char *string )
{
  if( context->errorhandler )
    context->errorhandler( context->errorpointer, code, string );
  context->errorcode = code;
  return;
}


////


/* Context management */

/**
 * Create a context
 *
 * @return Returns a context handle
 *
 * <b>rfCreateContext()</b> creates a new context and returns its handle. If <b>rfCreateContext()</b> fails to create the context, a zero pointer is returned.
 *
 * Almost all commands operate within a context ; scenes, models and objects are all specific to a context and can not be shared between multiple contexts. If resources are to be shared, use of multiple scenes within a same context is recommended.
 */
rfContext *rfCreateContext()
{
  rfContext *context;

  mmInit();

  context = malloc( sizeof(rfContext) );

  context->errorcode = RF_ERROR_NONE;
  context->errorhandler = defaultErrorHandler;
  context->errorpointer = 0;

  cpuGetInfo( &context->cpu );

  context->numadata = 0;
  context->cudadata = 0;
  context->scenelist = 0;

#if RF_CONFIG_NUMA_SUPPORT
  context->numadata = rfNumaInit( context );
#endif

#if RF_CONFIG_CUDA_SUPPORT
  context->cudadata = rfCudaInit( context );
#endif

  return context;
}

void rfDestroyContext( rfContext *context )
{
  for( ; context->scenelist ; )
    rfDestroyScene( context->scenelist );

#if RF_CONFIG_NUMA_SUPPORT
  context->numadata = 0;
#endif

#if RF_CONFIG_CUDA_SUPPORT
  context->cudadata = 0;
#endif

  return;
}


/**
 * Query capability support
 *
 * @param capability Specifies a symbolic constant, <b>RF_CAPABILITY_NUMA</b>, <b>RF_CAPABILITY_CUDA</b>, <b>RF_CAPABILITY_OPENCL</b> are accepted.
 * @return Returns the boolean value for queried capability.
 *
 * <b>rfQueryCapability()</b> queries support for a specified capability from the Rayforce engine. A non-zero return value indicates that the capability is supported. If the specified capability does not match any symbolic constant known to the implementation, no error will be produced and a value of zero is returned.
 */
int rfQueryCapability( rfContext *context, rfint capability )
{
  int retvalue;

  retvalue = 0;
  switch( capability )
  {
    case RF_CAPABILITY_NUMA:
      retvalue = ( context->numadata ? 1 : 0 );
      break;
    case RF_CAPABILITY_CUDA:
      retvalue = ( context->cudadata ? 1 : 0 );
      break;
    case RF_CAPABILITY_CPU_CMOV:
      retvalue = context->cpu.capcmov;
      break;
    case RF_CAPABILITY_CPU_CLFLUSH:
      retvalue = context->cpu.capclflush;
      break;
    case RF_CAPABILITY_CPU_TSC:
      retvalue = context->cpu.captsc;
      break;
    case RF_CAPABILITY_CPU_MMX:
      retvalue = context->cpu.capmmx;
      break;
    case RF_CAPABILITY_CPU_MMXEXT:
      retvalue = context->cpu.capmmxext;
      break;
    case RF_CAPABILITY_CPU_3DNOW:
      retvalue = context->cpu.cap3dnow;
      break;
    case RF_CAPABILITY_CPU_3DNOWEXT:
      retvalue = context->cpu.cap3dnowext;
      break;
    case RF_CAPABILITY_CPU_SSE:
      retvalue = context->cpu.capsse;
      break;
    case RF_CAPABILITY_CPU_SSE2:
      retvalue = context->cpu.capsse2;
      break;
    case RF_CAPABILITY_CPU_SSE3:
      retvalue = context->cpu.capsse3;
      break;
    case RF_CAPABILITY_CPU_SSSE3:
      retvalue = context->cpu.capssse3;
      break;
    case RF_CAPABILITY_CPU_SSE4P1:
      retvalue = context->cpu.capsse4p1;
      break;
    case RF_CAPABILITY_CPU_SSE4P2:
      retvalue = context->cpu.capsse4p2;
      break;
    case RF_CAPABILITY_CPU_SSE4A:
      retvalue = context->cpu.capsse4a;
      break;
    case RF_CAPABILITY_CPU_AVX:
      retvalue = context->cpu.capavx;
      break;
    case RF_CAPABILITY_CPU_AVX2:
      retvalue = context->cpu.capavx2;
      break;
    case RF_CAPABILITY_CPU_XOP:
      retvalue = context->cpu.capxop;
      break;
    case RF_CAPABILITY_CPU_FMA3:
      retvalue = context->cpu.capfma3;
      break;
    case RF_CAPABILITY_CPU_FMA4:
      retvalue = context->cpu.capfma4;
      break;
    case RF_CAPABILITY_CPU_MISALIGNSSE:
      retvalue = context->cpu.capmisalignsse;
      break;
    case RF_CAPABILITY_CPU_AES:
      retvalue = context->cpu.capaes;
      break;
    case RF_CAPABILITY_CPU_PCLMUL:
      retvalue = context->cpu.cappclmul;
      break;
    case RF_CAPABILITY_CPU_RDRND:
      retvalue = context->cpu.caprdrnd;
      break;
    case RF_CAPABILITY_CPU_CMPXCHG16B:
      retvalue = context->cpu.capcmpxchg16b;
      break;
    case RF_CAPABILITY_CPU_POPCNT:
      retvalue = context->cpu.cappopcnt;
      break;
    case RF_CAPABILITY_CPU_LZCNT:
      retvalue = context->cpu.caplzcnt;
      break;
    case RF_CAPABILITY_CPU_MOVBE:
      retvalue = context->cpu.capmovbe;
      break;
    case RF_CAPABILITY_CPU_RDTSCP:
      retvalue = context->cpu.caprdtscp;
      break;
    case RF_CAPABILITY_CPU_CONSTANTTSC:
      retvalue = context->cpu.capconstanttsc;
      break;
    case RF_CAPABILITY_CPU_F16C:
      retvalue = context->cpu.capf16c;
      break;
    case RF_CAPABILITY_CPU_BMI:
      retvalue = context->cpu.capbmi;
      break;
    case RF_CAPABILITY_CPU_BMI2:
      retvalue = context->cpu.capbmi2;
      break;
    case RF_CAPABILITY_CPU_TBM:
      retvalue = context->cpu.captbm;
      break;
    case RF_CAPABILITY_CPU_ADX:
      retvalue = context->cpu.capadx;
      break;
    case RF_CAPABILITY_CPU_AVX512F:
      retvalue = context->cpu.capavx512f;
      break;
    case RF_CAPABILITY_CPU_AVX512PF:
      retvalue = context->cpu.capavx512pf;
      break;
    case RF_CAPABILITY_CPU_AVX512ER:
      retvalue = context->cpu.capavx512er;
      break;
    case RF_CAPABILITY_CPU_AVX512CD:
      retvalue = context->cpu.capavx512cd;
      break;
    case RF_CAPABILITY_CPU_SHA:
      retvalue = context->cpu.capsha;
      break;
    case RF_CAPABILITY_CPU_CACHELINE_L1CODE:
      retvalue = context->cpu.cachelineL1code;
      break;
    case RF_CAPABILITY_CPU_CACHESIZE_L1CODE:
      retvalue = context->cpu.cachesizeL1code;
      break;
    case RF_CAPABILITY_CPU_CACHEASSOCIATIVITY_L1CODE:
      retvalue = context->cpu.cacheassociativityL1code;
      break;
    case RF_CAPABILITY_CPU_CACHESHARED_L1CODE:
      retvalue = context->cpu.cachesharedL1code;
      break;
    case RF_CAPABILITY_CPU_CACHELINE_L1DATA:
      retvalue = context->cpu.cachelineL1data;
      break;
    case RF_CAPABILITY_CPU_CACHESIZE_L1DATA:
      retvalue = context->cpu.cachesizeL1data;
      break;
    case RF_CAPABILITY_CPU_CACHEASSOCIATIVITY_L1DATA:
      retvalue = context->cpu.cacheassociativityL1data;
      break;
    case RF_CAPABILITY_CPU_CACHESHARED_L1DATA:
      retvalue = context->cpu.cachesharedL1data;
      break;
    case RF_CAPABILITY_CPU_CACHEUNIFIED_L1:
      retvalue = context->cpu.cacheunifiedL1;
      break;
    case RF_CAPABILITY_CPU_CACHEFLUSHSIZE:
      retvalue = context->cpu.clflushsize;
      break;
    case RF_CAPABILITY_CPU_CACHELINE_L2:
      retvalue = context->cpu.cachelineL2;
      break;
    case RF_CAPABILITY_CPU_CACHESIZE_L2:
      retvalue = context->cpu.cachesizeL2;
      break;
    case RF_CAPABILITY_CPU_CACHEASSOCIATIVITY_L2:
      retvalue = context->cpu.cacheassociativityL2;
      break;
    case RF_CAPABILITY_CPU_CACHESHARED_L2:
      retvalue = context->cpu.cachesharedL2;
      break;
    case RF_CAPABILITY_CPU_CACHELINE_L3:
      retvalue = context->cpu.cachelineL3;
      break;
    case RF_CAPABILITY_CPU_CACHESIZE_L3:
      retvalue = context->cpu.cachesizeL3;
      break;
    case RF_CAPABILITY_CPU_CACHEASSOCIATIVITY_L3:
      retvalue = context->cpu.cacheassociativityL3;
      break;
    case RF_CAPABILITY_CPU_CACHESHARED_L3:
      retvalue = context->cpu.cachesharedL3;
      break;
  }

  return retvalue;
}


/**
 * Query error information
 *
 * @param context Specifies the context.
 * @return Returns an error code for the context.
 *
 * The <b>rfGetError()</b> command returns error status information for the specified context. Each detectable error is assigned a numeric code and symbolic name. When an error occurs, the context's error code is replaced by the new error. The error code is reset to <b>RF_ERROR_NONE</b> when <b>rfGetError()</b> is called. Each context is entirely independant and has its own error code.
 *
 * See the command <a>rfErrorHandler</a> to install an error callback handler able to register a verbose error message when errors occur.
 *
 * The errors codes are defined as follow :
 *
 * <dl>
 * <dt><b>Rf_ERROR_NONE</b><dd>
 * No error has been recorded.  The value of this symbolic constant is guaranteed to be zero.
 * <dt><b>RF_ERROR_INVALID_VALUE</b><dd>
 * An invalid value has been passed to a command. The command had no effect.
 * <dt><b>RF_ERROR_INVALID_TARGET</b><dd>
 * A scene's specified target was invalid for a command, or the scene's target could not accomodate the command. The command had no effect.
 * <dt><b>RF_ERROR_INVALID_OPERATION</b><dd>
 * A command has described an operation that was invalid. The command had no effect.
 * <dt><b>RF_ERROR_NOT_SUPPORTED</b><dd>
 * A command has attempted an operation that was known to the implementation but that it doesn't support at the time. This error will also be produced for operations requiring absent hardware functionalities. The command had no effect.
 * <dt><b>RF_ERROR_MEMORY_DENIED</b><dd>
 * An operation could not be completed due to a failed system memory allocation. For some operations, a failed memory allocation may cause immediate program termination. The command had no effect.
 * <dt><b>RF_ERROR_NUMA_BAD_OPERATION</b><dd>
 * A command described an invalid NUMA operation. The command had no effect.
 * <dt><b>RF_ERROR_NUMA_MEMORY_DENIED</b><dd>
 * An operation could not be completed due to a failed NUMA memory allocation. The command had no effect.
 * <dt><b>RF_ERROR_NUMA_UNKNOWN</b><dd>
 * An undefined NUMA error has occured.
 * <dt><b>RF_ERROR_CUDA_BAD_OPERATION</b><dd>
 * A command described an invalid CUDA operation. The command had no effect.
 * <dt><b>RF_ERROR_CUDA_MEMORY_DENIED</b><dd>
 * An operation could not be completed due to a failed CUDA memory allocation. The command had no effect.
 * <dt><b>RF_ERROR_CUDA_UNKNOWN</b><dd>
 * An undefined CUDA error has occured.
 * </dl>
 */
rfint rfGetError( rfContext *context )
{
  rfint errorcode;
  errorcode = context->errorcode;
  context->errorcode = 0;
  return errorcode;
}


/**
 * Install an error handler
 *
 * @param context Specifies the context.
 * @param handler Specifies the error handler.
 * @param errorp Specifies an opaque pointer passed to the error handler.
 *
 *
 *
 */
void rfErrorHandler( rfContext *context, void (*handler)( void *errorp, rfint code, char *string ), void *errorp )
{
  context->errorhandler = handler;
  context->errorpointer = errorp;
  return;
}


/* Returns affinity to pipeline, returns < 0 if pipeline can not be used */
static int getPipelineAffinity( rfContext *context, rfPipeline *pipeline )
{
  int affinity;
  cpuInfo cpu;

  /* We can't roll that in a loop due to the use of bitfields in cpuInfo ... */
  affinity = 0;
  cpuGetInfo( &cpu );
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_SSE )
  {
    affinity += 4;
    if( !( context->cpu.capsse ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_SSE2 )
  {
    affinity += 4;
    if( !( context->cpu.capsse2 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_SSE3 )
  {
    affinity += 4;
    if( !( context->cpu.capsse3 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_SSSE3 )
  {
    affinity += 2;
    if( !( context->cpu.capssse3 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_SSE4P1 )
  {
    affinity += 4;
    if( !( context->cpu.capsse4p1 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_SSE4P2 )
  {
    affinity += 1;
    if( !( context->cpu.capsse4p2 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_SSE4A )
  {
    affinity += 3;
    if( !( context->cpu.capsse4a ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_AVX )
  {
    affinity += 4;
    if( !( context->cpu.capavx ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_AVX2 )
  {
    affinity += 4;
    if( !( context->cpu.capavx2 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_XOP )
  {
    affinity += 4;
    if( !( context->cpu.capxop ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_FMA3 )
  {
    affinity += 3;
    if( !( context->cpu.capfma3 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_FMA4 )
  {
    affinity += 3;
    if( !( context->cpu.capfma4 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_RDRND )
  {
    affinity += 2;
    if( !( context->cpu.caprdrnd ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_POPCNT )
  {
    affinity += 1;
    if( !( context->cpu.cappopcnt ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_LZCNT )
  {
    affinity += 1;
    if( !( context->cpu.caplzcnt ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_F16C )
  {
    affinity += 1;
    if( !( context->cpu.capf16c ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_BMI )
  {
    affinity += 1;
    if( !( context->cpu.capbmi ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_BMI2 )
  {
    affinity += 1;
    if( !( context->cpu.capbmi2 ) )
      goto invalid;
  }
  if( pipeline->reqflags & RF_PIPELINE_REQFLAG_TBM )
  {
    affinity += 1;
    if( !( context->cpu.captbm ) )
      goto invalid;
  }

  return affinity;

  invalid:
  return -1;
}


/* Find best pipeline */
rfPipeline *rfSelectPipeline( rfContext *context, rfPipeline **pipelinelist, int pipelinecount )
{
  int pipelineindex, bestvalue, value;
  rfPipeline *bestpipeline;

  bestvalue = -1;
  bestpipeline = 0;
  for( pipelineindex = 0 ; pipelineindex < pipelinecount ; pipelineindex++ )
  {
    value = getPipelineAffinity( context, pipelinelist[pipelineindex] );
    if( value > bestvalue )
    {
      bestvalue = value;
      bestpipeline = pipelinelist[pipelineindex];
    }
  }

  return bestpipeline;
}


/* Scene management */
rfScene *rfCreateScene( rfContext *context, rfint target, rfint targetindex, void *targetpointer )
{
  rfScene *scene;

  switch( target )
  {
    case RF_TARGET_SYSTEM:
      break;
#if RF_CONFIG_NUMA_SUPPORT
    case RF_TARGET_NUMA:
      if( !( context->numadata ) )
      {
        rfRegisterError( context, RF_ERROR_NOT_SUPPORTED, "rfCreateScene( target )" );
        return 0;
      }
      if( (rfuint)targetindex >= mmcontext.nodecount )
      {
        rfRegisterError( context, RF_ERROR_NOT_SUPPORTED, "rfCreateScene( targetindex )" );
        return 0;
      }
      break;
#endif
#if RF_CONFIG_CUDA_SUPPORT
    case RF_TARGET_CUDA:
      if( !( context->cudadata ) )
      {
        rfRegisterError( context, RF_ERROR_NOT_SUPPORTED, "rfCreateScene( target )" );
        return 0;
      }
      if( !( rfCudaAcquireDevice( context, targetindex ) ) )
      {
        rfRegisterError( context, RF_ERROR_NOT_SUPPORTED, "rfCreateScene( targetindex )" );
        return 0;
      }
      break;
#endif
#if RF_CONFIG_OPENCL_SUPPORT
    case RF_TARGET_OPENCL:
      rfRegisterError( context, RF_ERROR_NOT_SUPPORTED, "rfCreateScene( target )" );
      return 0;
#endif
    default:
      rfRegisterError( context, RF_ERROR_INVALID_TARGET, "rfCreateScene( target )" );
      return 0;
  }

  scene = malloc( sizeof(rfScene) );
  scene->context = context;
  scene->flags = RF_SCENE_FLAGS_MODIFIED;
  scene->singleobject = 0;
  scene->target = target;
  scene->targetindex = targetindex;
  scene->targetpointer = targetpointer;
  scene->datatype = RF_FLOAT;
  scene->modellist = 0;
  scene->objectlist = 0;
  scene->activeobjectcount = 0;
  scene->scenegraphmemsize = 0;
  mmListAdd( &context->scenelist, scene, offsetof(rfScene,contextnode) );

  return scene;
}

void rfDestroyScene( rfScene *scene )
{
  rfModel *model, *nextmodel;
  for( ; scene->objectlist ; )
    rfDestroyObject( scene->objectlist );
  for( model = scene->modellist ; model ; model = nextmodel )
  {
    nextmodel = model->scenenode.next;
    rfFreeGraphMemory( model->context, &model->graph );
    mmListRemove( model, offsetof(rfModel,scenenode) );
    free( model );
  }
  switch( scene->target )
  {
    case RF_TARGET_SYSTEM:
    case RF_TARGET_NUMA:
      break;
#if RF_CONFIG_CUDA_SUPPORT
    case RF_TARGET_CUDA:
      rfCudaReleaseDevice( scene->context, scene->targetindex );
      break;
#endif
    default:
      break;
  }
  mmListRemove( scene, offsetof(rfScene,contextnode) );
  free( scene );
  return;
}


void rfSceneEnv( rfScene *scene, rfint param, rfint value, void *data )
{
  switch( param )
  {
    case RF_SCENE_DATATYPE:
      if( value == RF_FLOAT )
        scene->datatype = value;
      else if( value == RF_DOUBLE )
        rfRegisterError( scene->context, RF_ERROR_NOT_SUPPORTED, "rfSceneEnv( value )" );
      else
        rfRegisterError( scene->context, RF_ERROR_INVALID_VALUE, "rfSceneEnv( value )" );
      break;
    default:
      rfRegisterError( scene->context, RF_ERROR_INVALID_VALUE, "rfSceneEnv( param )" );
      break;
  }
  return;
}

int rfReadyScene( rfScene *scene )
{
  if( scene->flags & RF_SCENE_FLAGS_MODIFIED )
  {
    if( !( rfBuildScene( scene->context, scene ) ) )
    {
      rfRegisterError( scene->context, RF_ERROR_INVALID_OPERATION, "rfReadyScene( scene incomplete )" );
      return 0;
    }
  }
  if( !( scene->flags & RF_SCENE_FLAGS_READY ) )
  {
    rfRegisterError( scene->context, RF_ERROR_INVALID_OPERATION, "rfReadyScene( scene incomplete )" );
    return 0;
  }
  return 1;
}

rfHandle *rfAcquireScene( rfScene *scene )
{
  if( scene->flags & RF_SCENE_FLAGS_MODIFIED )
  {
    if( !( rfBuildScene( scene->context, scene ) ) )
    {
      rfRegisterError( scene->context, RF_ERROR_INVALID_OPERATION, "rfAcquireScene( scene incomplete )" );
      return 0;
    }
  }
  if( !( scene->flags & RF_SCENE_FLAGS_READY ) )
  {
    rfRegisterError( scene->context, RF_ERROR_INVALID_OPERATION, "rfAcquireScene( scene incomplete )" );
    return 0;
  }
  return &scene->scenehandle;
}

void *rfTracePath( rfScene *scene, rfHandle *handle, rfPipeline *pipeline )
{
  int pathindex;
  void *path;

  /* We aren't building 16 and 64 bits pipelines anymore, for now anyway */
  if( handle->addressmode != RF_GRAPH_ADDRESSMODE_32BITS )
  {
    rfRegisterError( scene->context, RF_ERROR_NOT_SUPPORTED, "rfTracePath( scene, pipeline )" );
    return 0;
  }

  /* Graph culling mode */
  if( handle->cullmode == RF_CULLMODE_BACK )
    pathindex = 0;
  else if( handle->cullmode == RF_CULLMODE_FRONT )
    pathindex = 1;
  else if( handle->cullmode == RF_CULLMODE_NONE )
    pathindex = 2;
  else
  {
    rfRegisterError( scene->context, RF_ERROR_NOT_SUPPORTED, "rfTracePath( scene, pipeline )" );
    return 0;
  }

  /* Single object versus scene tracing */
  if( handle->handletype == RF_HANDLETYPE_OBJECT )
    path = pipeline->object[pathindex];
  else if( handle->handletype == RF_HANDLETYPE_SCENE )
    path = pipeline->scene[pathindex];
  else
  {
    rfRegisterError( scene->context, RF_ERROR_NOT_SUPPORTED, "rfTracePath( scene, pipeline )" );
    return 0;
  }

  if( !( path ) )
  {
    rfRegisterError( scene->context, RF_ERROR_NOT_SUPPORTED, "rfTracePath( scene, pipeline )" );
    return 0;
  }

  return path;
}

void rfReleaseScene( rfScene *scene, rfHandle *handle )
{
  /* TODO */

  return;
}

void rfGetBoundingBoxf( rfScene *scene, float *box )
{
  if( scene->flags & RF_SCENE_FLAGS_MODIFIED )
  {
    if( !( rfBuildScene( scene->context, scene ) ) )
    {
      rfRegisterError( scene->context, RF_ERROR_INVALID_OPERATION, "rfGetBoundingBoxf( scene incomplete )" );
      memset( box, 0, 6*sizeof(float) );
      return;
    }
  }
  box[0] = (float)scene->edge[RF_EDGE_MINX];
  box[1] = (float)scene->edge[RF_EDGE_MAXX];
  box[2] = (float)scene->edge[RF_EDGE_MINY];
  box[3] = (float)scene->edge[RF_EDGE_MAXY];
  box[4] = (float)scene->edge[RF_EDGE_MINZ];
  box[5] = (float)scene->edge[RF_EDGE_MAXZ];
  return;
}

void rfGetBoundingBoxd( rfScene *scene, double *box )
{
  if( scene->flags & RF_SCENE_FLAGS_MODIFIED )
  {
    if( !( rfBuildScene( scene->context, scene ) ) )
    {
      rfRegisterError( scene->context, RF_ERROR_INVALID_OPERATION, "rfGetBoundingBoxd( scene incomplete )" );
      memset( box, 0, 6*sizeof(double) );
      return;
    }
  }
  box[0] = (double)scene->edge[RF_EDGE_MINX];
  box[1] = (double)scene->edge[RF_EDGE_MAXX];
  box[2] = (double)scene->edge[RF_EDGE_MINY];
  box[3] = (double)scene->edge[RF_EDGE_MAXY];
  box[4] = (double)scene->edge[RF_EDGE_MINZ];
  box[5] = (double)scene->edge[RF_EDGE_MAXZ];
  return;
}

/* Model management */
rfModel *rfCreateModel( rfScene *scene, rfint modeltype )
{
  rfModel *model;

  switch( modeltype )
  {
    case RF_MODEL_TYPE_TRIANGLES:
      break;
    default:
      rfRegisterError( scene->context, RF_ERROR_INVALID_VALUE, "rfCreateModel( modeltype )" );
      return 0;
  }

  model = malloc( sizeof(rfModel) );
  model->context = scene->context;
  model->scene = scene;
  model->flags = 0;
  model->modeltype = modeltype;
  mmAtomicWrite32( &model->usecount, 0 );
  model->geom.datapointer = 0;
  model->geom.datasize = 0;
  model->geom.datastride = 0;
  model->geom.vertexpointer = 0;
  model->geom.vertextype = 0;
  model->geom.vertexstride = 0;
  model->geom.indexpointer = 0;
  model->geom.indextype = 0;
  model->geom.modeldata = 0;
  model->geom.modeldatasize = 0;
  model->geom.estimatecostflag = 0;
  model->buildquality = RF_BUILDHINT_VALUE_DEFAULT;
  model->buildmemory = RF_BUILDHINT_VALUE_DEFAULT;
  model->buildlayout = RF_BUILDHINT_LAYOUTMODE_DEFAULT;
  mmListAdd( &scene->modellist, model, offsetof(rfModel,scenenode) );

  return model;
}

int rfCopyModel( rfModel *model, rfModel *modelref )
{
  int retval;
  rfScene *scene;

  scene = model->scene;
  if( mmAtomicRead32( &model->usecount ) )
    scene->flags |= RF_SCENE_FLAGS_MODIFIED;


  if( model->flags & RF_MODEL_FLAGS_BUILT )
  {
    rfFreeGraphMemory( model->context, &model->graph );
    /* TODO: Anything else to do here? */
  }


  model->flags = modelref->flags;
  model->modeltype = modelref->modeltype;
  model->geom = modelref->geom;
  retval = rfCopyGraph( model->context, &model->graph, &modelref->graph, scene->target, scene->targetindex, scene->targetpointer );

  return retval;
}

void rfDestroyModel( rfModel *model )
{
  model->flags |= RF_MODEL_FLAGS_DELETED;
  if( !( mmAtomicRead32( &model->usecount ) ) )
  {
    rfFreeGraphMemory( model->context, &model->graph );
    mmListRemove( model, offsetof(rfModel,scenenode) );
    free( model );
  }
  return;
}

void rfModelEnv( rfModel *model, rfint param, rfint value, void *data )
{
  switch( param )
  {
    case RF_BUILDHINT_QUALITY:
      if( (unsigned)value > RF_BUILDHINT_VALUE_MAX )
        value = RF_BUILDHINT_VALUE_DEFAULT;
      model->buildquality = value;
      break;
    case RF_BUILDHINT_MEMORY:
      if( (unsigned)value > RF_BUILDHINT_VALUE_MAX )
        value = RF_BUILDHINT_VALUE_DEFAULT;
      model->buildmemory = value;
      break;
    case RF_BUILDHINT_LAYOUT:
      if ( (unsigned)value < RF_BUILDHINT_LAYOUTMODE_NONE &&
           (unsigned)value > RF_BUILDHINT_LAYOUTMODE_FULL)
        value = RF_BUILDHINT_LAYOUTMODE_FULL;
      model->buildlayout = value;
      break;
    case RF_GRAPH_VALUE_FLAG:
      model->geom.estimatecostflag = ( value != 0 );
      break;
    default:
      rfRegisterError( model->context, RF_ERROR_INVALID_VALUE, "rfModelEnv( param )" );
      break;
  }
  return;
}

void rfPrimitiveData( rfModel *model, void *pointer, size_t size, size_t stride )
{
  model->geom.datapointer = pointer;
  model->geom.datasize = size;
  model->geom.datastride = stride;
  return;
}

void rfVertexPointer( rfModel *model, void *pointer, rfint type, size_t stride )
{
  switch( type )
  {
    case RF_FLOAT:
      break;
    case RF_DOUBLE:
      break;
    default:
      rfRegisterError( model->context, RF_ERROR_INVALID_VALUE, "rfVertexPointer( type )" );
      return;
  }
  model->geom.vertexpointer = pointer;
  model->geom.vertextype = type;
  model->geom.vertexstride = stride;
  return;
}


void rfIndexPointer( rfModel *model, void *pointer, rfint type )
{
  switch( type )
  {
    case RF_SHORT:
    case RF_USHORT:
      break;
    case RF_INT:
    case RF_UINT:
      break;
    case RF_INT8:
    case RF_UINT8:
      break;
    case RF_INT16:
    case RF_UINT16:
      break;
    case RF_INT32:
    case RF_UINT32:
      break;
    case RF_INT64:
    case RF_UINT64:
      break;
    case 0:
      if( !( pointer ) )
        break;
    default:
      rfRegisterError( model->context, RF_ERROR_INVALID_VALUE, "rfIndexPointer( type )" );
      return;
  }
  model->geom.indexpointer = pointer;
  model->geom.indextype = type;
  return;
}

void rfModelData( rfModel *model, void *data, size_t datasize )
{
  model->geom.modeldata = data;
  model->geom.modeldatasize = datasize;
  return;
}

int rfReadyModel( rfModel *model, size_t primitivecount, rfint flags )
{
  long originwidth;
  rfScene *scene;

  scene = model->scene;
  if( mmAtomicRead32( &model->usecount ) )
    scene->flags |= RF_SCENE_FLAGS_MODIFIED;

  if( model->flags & RF_MODEL_FLAGS_BUILT )
  {
    /* TODO: Clear model */

    abort();

    model->flags &= ~RF_MODEL_FLAGS_BUILT;
  }

  model->geom.primitivecount = primitivecount;

  /* TODO: Decide origin width */
  originwidth = 32;
  if( !( rfBuildGraph( model->context, &model->graph, &model->geom, 0, originwidth, scene->target, scene->targetindex, scene->targetpointer, model->buildquality, model->buildmemory, model->buildlayout ) ) )
  {
    rfRegisterError( model->context, RF_ERROR_INVALID_OPERATION, "rfReadyModel( target, geom )" );
    return 0;
  }

  model->flags |= RF_MODEL_FLAGS_BUILT;
  return 1;
}


int rfFinishModel( rfModel *model )
{
  /* TODO: If model building is asynchroneous, wait for it to be completed, and unlocking geometry memory */
  return 1;
}


void rfClearModel( rfModel *model )
{

  /* TODO: What do we do if model already in use? */


  if( model->flags & RF_MODEL_FLAGS_BUILT )
  {
    /* TODO: Clear model */

    abort();

    model->flags &= ~RF_MODEL_FLAGS_BUILT;
  }
  return;
}


int rfGetModelCache( rfModel *model, void *cache, size_t *cachesize )
{
  rfScene *scene;

  if( !( cache ) )
  {
    *cachesize = model->graph.datasize;
    return 1;
  }
  if( !( model->flags & RF_MODEL_FLAGS_BUILT ) )
  {
    rfRegisterError( model->context, RF_ERROR_INVALID_OPERATION, "rfGetModelCache( Model not built )" );
    return 0;
  }
  if( *cachesize < model->graph.datasize )
  {
    rfRegisterError( model->context, RF_ERROR_INVALID_OPERATION, "rfGetModelCache( cachesize )" );
    return 0;
  }

  scene = model->scene;
  switch( scene->target )
  {
    case RF_TARGET_SYSTEM:
      memcpy( cache, model->graph.memory, model->graph.datasize );
      break;
#if RF_CONFIG_NUMA_SUPPORT
    case RF_TARGET_NUMA:
      memcpy( cache, model->graph.memory, model->graph.datasize );
      break;
#endif
#if RF_CONFIG_CUDA_SUPPORT
    case RF_TARGET_CUDA:
      if( !( rfCudaGetGraph( model->context, scene->targetindex, &model->graph, cache ) ) )
        return 0;
      break;
#endif
    default:
      fprintf( stderr, "ERROR %s:%d\n", __FILE__, __LINE__ );
      abort();
  }

  return 1;
}

int rfModelCache( rfModel *model, void *cache, size_t cachesize )
{
  rfScene *scene;

  if( model->flags & RF_MODEL_FLAGS_BUILT )
  {

    /* TODO: Clear model */

    abort();


    model->flags &= ~RF_MODEL_FLAGS_BUILT;
  }

  scene = model->scene;
  if( !( rfSetGraph( model->context, &model->graph, cache, cachesize, scene->target, scene->targetindex, scene->targetpointer ) ) )
  {
    rfRegisterError( model->context, RF_ERROR_INVALID_OPERATION, "rfSetModelCache( bad cache )" );
    return 0;
  }

  model->flags |= RF_MODEL_FLAGS_BUILT;
  return 1;
}

int rfModelCacheFile( rfModel *model, char *filename )
{
  int retval;
  void *cache;
  size_t cachesize;
#ifdef __unix__
  int fd;
  struct stat filestat;
#else
  FILE *file;
#endif

#ifdef __unix__
  if( ( fd = open( filename, O_RDONLY ) ) == -1 )
    return 0;
  if( fstat( fd, &filestat ) )
  {
    close( fd );
    return 0;
  }
  cachesize = filestat.st_size;
  if( ( cache = mmap( 0, cachesize, PROT_READ, MAP_SHARED, fd, 0 ) ) == MAP_FAILED )
  {
    close( fd );
    return 0;
  }
#else
  if( !( file = fopen( filename, "r" ) ) )
    return 0;
  fseek( file, 0, SEEK_END );
  cachesize = ftell( file );
  fseek( file, 0, SEEK_SET );
  if( !( cache = malloc( cachesize ) ) )
  {
    fclose( file );
    return 0;
  }
  cachesize = fread( cache, 1, cachesize, file );
#endif

  retval = rfModelCache( model, cache, cachesize );

#ifdef __unix__
  munmap( cache, cachesize );
  close( fd );
#else
  free( cache );
  fclose( file );
#endif

  return retval;
}

size_t rfQueryModel( rfModel *model, int property )
{
  size_t value;

  switch( property )
  {
    case RF_PROPERTY_GRAPH_MEMORY:
      value = model->graph.datasize;
      break;
    case RF_PROPERTY_GRAPH_COST:
      value = model->graph.graphcost;
      break;
    case RF_PROPERTY_SECTOR_COUNT:
      value = model->graph.sectorcount;
      break;
    case RF_PROPERTY_NODE_COUNT:
      value = model->graph.nodecount;
      break;
    case RF_PROPERTY_TRIANGLE_COUNT:
      value = model->graph.trianglecount;
      break;
    case RF_PROPERTY_TRIREF_COUNT:
      value = model->graph.trirefcount;
      break;
    case RF_PROPERTY_TRIREF_MAXIMUM:
      value = model->graph.trirefmaximum;
      break;
    default:
      rfRegisterError( model->context, RF_ERROR_INVALID_VALUE, "rfQueryModel( property )" );
      value = 0;
      break;
  }

  return value;
}


/* Object management */
rfObject *rfCreateObject( rfScene *scene )
{
  rfObject *object;

  object = malloc( sizeof(rfObject) );
  object->context = scene->context;
  object->scene = scene;
  object->flags = RF_OBJECT_FLAGS_IDENTITYMATRIX;
  object->model = 0;
  object->value = 0;
  object->cullmode = RF_CULLMODE_NONE;
  rfMathMatrixIdentity( object->matrix );
  rfMathMatrixIdentity( object->matrixinv );
  object->sceneobjectindex = -1;
  object->objectdata = 0;
  object->objectdatasize = 0;
  mmListAdd( &scene->objectlist, object, offsetof(rfObject,scenenode) );

  return object;
}

void rfDestroyObject( rfObject *object )
{
  rfModel *model;
  model = object->model;
  if( ( model ) && ( model->flags & RF_MODEL_FLAGS_DELETED ) )
  {
    if( !( mmAtomicAddRead32( &model->usecount, -1 ) ) )
    {
      mmListRemove( model, offsetof(rfModel,scenenode) );
      free( model );
    }
  }
  mmListRemove( object, offsetof(rfObject,scenenode) );
  free( object );
  return;
}

void rfAttachModel( rfObject *object, rfModel *model )
{
  rfScene *scene;
  if( object->scene != model->scene )
  {
    rfRegisterError( object->context, RF_ERROR_INVALID_OPERATION, "rfAttachModel( object->scene != model->scene )" );
    return;
  }
  object->model = model;
  if( !( model ) )
  {
    if( object->flags & RF_OBJECT_FLAGS_ENABLED )
    {
      rfRegisterError( object->context, RF_ERROR_INVALID_OPERATION, "rfAttachModel( NULL model on enabled object )" );
      return;
    }
    model = object->model;
    if( model )
      mmAtomicSub32( &model->usecount, 1 );
  }
  else
  {
    if( !( model->flags & RF_MODEL_FLAGS_BUILT ) )
    {
      rfRegisterError( object->context, RF_ERROR_INVALID_OPERATION, "rfAttachModel( model not ready )" );
      return;
    }
    object->model = model;
    mmAtomicAdd32( &model->usecount, 1 );
    scene = object->scene;
    if( object->flags & RF_OBJECT_FLAGS_ENABLED )
      scene->flags |= RF_SCENE_FLAGS_MODIFIED;
  }
  return;
}

int rfObjectData( rfObject *object, void *data, size_t datasize )
{
  return rfBuildObjectData( object->context, object, data, datasize );
}

void rfCullFace( rfObject *object, rfint mode )
{
  switch( mode )
  {
    case RF_CULLMODE_NONE:
      break;
    case RF_CULLMODE_FRONT:
      break;
    case RF_CULLMODE_BACK:
      break;
    default:
      rfRegisterError( object->context, RF_ERROR_INVALID_VALUE, "rfCullFace( mode )" );
      return;
  }
  object->cullmode = mode;
  rfBuildObjectData( object->context, object, 0, 0 );
  return;
}

void rfLoadMatrixf( rfObject *object, float *m )
{
  int i;
  rfScene *scene;
  if( !( m ) )
  {
    object->flags |= RF_OBJECT_FLAGS_IDENTITYMATRIX;
    rfMathMatrixIdentity( object->matrix );
  }
  else
  {
    object->flags &= ~RF_OBJECT_FLAGS_IDENTITYMATRIX;
    for( i = 0 ; i < 16 ; i++ )
      object->matrix[i] = m[i];
  }
  /* TODO: Always invert matrix in double precision */
#if RF_USE_FLOAT
  rfMathMatrixInvertf( object->matrixinv, object->matrix );
#else
  rfMathMatrixInvertd( object->matrixinv, object->matrix );
#endif
  scene = object->scene;
  if( object->flags & RF_OBJECT_FLAGS_ENABLED )
    scene->flags |= RF_SCENE_FLAGS_MODIFIED;
  return;
}

void rfLoadMatrixd( rfObject *object, double *m )
{
  int i;
  rfScene *scene;
  if( !( m ) )
  {
    object->flags |= RF_OBJECT_FLAGS_IDENTITYMATRIX;
    rfMathMatrixIdentity( object->matrix );
  }
  else
  {
    object->flags &= ~RF_OBJECT_FLAGS_IDENTITYMATRIX;
    for( i = 0 ; i < 16 ; i++ )
      object->matrix[i] = m[i];
  }
  /* TODO: Always invert matrix in double precision */
#if RF_USE_FLOAT
  rfMathMatrixInvertf( object->matrixinv, object->matrix );
#else
  rfMathMatrixInvertd( object->matrixinv, object->matrix );
#endif
  scene = object->scene;
  if( object->flags & RF_OBJECT_FLAGS_ENABLED )
    scene->flags |= RF_SCENE_FLAGS_MODIFIED;
  return;
}


int rfEnableObject( rfObject *object )
{
  rfScene *scene;
  rfModel *model;
  if( object->flags & RF_OBJECT_FLAGS_ENABLED )
    return 1;
  model = object->model;
  if( !( model ) )
    return 0;
  if( !( model->flags & RF_MODEL_FLAGS_BUILT ) )
    return 0;
  scene = object->scene;
  scene->activeobjectcount++;
  object->flags |= RF_OBJECT_FLAGS_ENABLED;
  scene->flags |= RF_SCENE_FLAGS_MODIFIED;
  return 1;
}

void rfDisableObject( rfObject *object )
{
  rfScene *scene;
  if( !( object->flags & RF_OBJECT_FLAGS_ENABLED ) )
    return;
  scene = object->scene;
  scene->activeobjectcount--;
  object->flags &= ~RF_OBJECT_FLAGS_ENABLED;
  scene->flags |= RF_SCENE_FLAGS_MODIFIED;
  return;
}




/** @} */

