/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "cpuconfig.h"

#include "cc.h"
#include "mm.h"
#include "mmdirectory.h"

#include "rf.h"
#include "rfdefs.h"
#include "rfdata.h"
#include "rfinternal.h"
#include "rfgraph.h"

void rfGraphOriginInit( rfOriginTable *origintable, rff *edge, rfsize originwidth )
{
  rff spacing, extsize, minoffset;
  rff size[RF_AXIS_COUNT];

  memcpy( origintable->graphedge, edge, RF_EDGE_COUNT*sizeof(rff) );
  size[RF_AXIS_X] = edge[RF_EDGE_MAXX] - edge[RF_EDGE_MINX];
  size[RF_AXIS_Y] = edge[RF_EDGE_MAXY] - edge[RF_EDGE_MINY];
  size[RF_AXIS_Z] = edge[RF_EDGE_MAXZ] - edge[RF_EDGE_MINZ];
  if( originwidth < 3 )
    originwidth = 3;

  spacing = 1.0 / (rff)( originwidth - 2 );
  extsize = spacing * (rff)( originwidth -1 );
  minoffset = -0.5 * spacing;
  origintable->spacing[RF_AXIS_X] = spacing * size[RF_AXIS_X];
  origintable->spacing[RF_AXIS_Y] = spacing * size[RF_AXIS_Y];
  origintable->spacing[RF_AXIS_Z] = spacing * size[RF_AXIS_Z];
  origintable->spacinginv[RF_AXIS_X] = 1.0 / origintable->spacing[RF_AXIS_X];
  origintable->spacinginv[RF_AXIS_Y] = 1.0 / origintable->spacing[RF_AXIS_Y];
  origintable->spacinginv[RF_AXIS_Z] = 1.0 / origintable->spacing[RF_AXIS_Z];
  origintable->edge[RF_EDGE_MINX] = edge[RF_EDGE_MINX] + ( minoffset * size[RF_AXIS_X] );
  origintable->edge[RF_EDGE_MINY] = edge[RF_EDGE_MINY] + ( minoffset * size[RF_AXIS_Y] );
  origintable->edge[RF_EDGE_MINZ] = edge[RF_EDGE_MINZ] + ( minoffset * size[RF_AXIS_Z] );
  origintable->edge[RF_EDGE_MAXX] = origintable->edge[RF_EDGE_MINX] + ( extsize * size[RF_AXIS_X] );
  origintable->edge[RF_EDGE_MAXY] = origintable->edge[RF_EDGE_MINY] + ( extsize * size[RF_AXIS_Y] );
  origintable->edge[RF_EDGE_MAXZ] = origintable->edge[RF_EDGE_MINZ] + ( extsize * size[RF_AXIS_Z] );
  origintable->width = originwidth;
  origintable->max = originwidth-1;
  origintable->totalcount = originwidth * originwidth * originwidth;
  origintable->factory = originwidth;
  origintable->factorz = originwidth * originwidth;

/*
OriginWidth 3, spacing 1.0
-0.5 ~ 0.5 ~ 1.5

OriginWidth 4, spacing 0.5
-0.25 ~ 0.25 ~ 0.75 ~ 1.25

OriginWidth 5, spacing 0.33
-0.166 ~ 0.166 ~ 0.50 ~ 0.833 ~ 1.166

OriginWidth 6, spacing 0.25
-0.125 ~ 0.125 ~ 0.375 ~ 0.625 ~ 0.875 ~ 1.125

OriginWidth 7, spacing 0.20
-0.100 ~ 0.100 ~ 0.300 ~ 0.500 ~ 0.700 ~ 0.900 ~ 1.100
*/

  return;
}


////


#define RF_DECL_ATTRIB
#define RFNAME(n) _rf##n


#include "trace/common.h"


#define RF_ADDRBITS (16)
#define RF_RESOLVENAME RFNAME(Resolve16)
#define RF_DISPLACENAME RFNAME(Displacec16)
#include "trace/resolve.h"

#define RF_ADDRBITS (32)
#define RF_RESOLVENAME RFNAME(Resolve32)
#define RF_DISPLACENAME RFNAME(Displacec32)
#include "trace/resolve.h"

#define RF_ADDRBITS (64)
#define RF_RESOLVENAME RFNAME(Resolve64)
#define RF_DISPLACENAME RFNAME(Displacec64)
#include "trace/resolve.h"


#undef RF_DECL_ATTRIB


////


static rfRoot rfResolveOrigin( rfScene *scene, rfHandle *handle, rff *origin )
{
  rfRoot root;
  rfGraphHeader *header;


  /* TODO TODO */
  if( !( handle->objectgraph ) )
    return 0;
  /* TODO TODO */


  root = 0;
  switch( scene->target )
  {
    case RF_TARGET_SYSTEM:
    case RF_TARGET_NUMA:
      header = (rfGraphHeader *)handle->objectgraph;
      switch( header->sectoraddrbits )
      {
        case 16:
          root = ADDRESSDIFF( RFNAME(Resolve16)( handle->objectgraph, origin ), handle->objectgraph );
          break;
        case 32:
          root = ADDRESSDIFF( RFNAME(Resolve32)( handle->objectgraph, origin ), handle->objectgraph );
          break;
        case 64:
          root = ADDRESSDIFF( RFNAME(Resolve64)( handle->objectgraph, origin ), handle->objectgraph );
          break;
        default:
          rfRegisterError( scene->context, RF_ERROR_NOT_SUPPORTED, "rfCreateScene( target )" );
          break;
      }
      break;
#if RF_CONFIG_CUDA_SUPPORT
    case RF_TARGET_CUDA:
      root = rfCudaResolveOrigin( scene, handle, origin );
      break;
#endif
#if RF_CONFIG_OPENCL_SUPPORT
    case RF_TARGET_OPENCL:
      rfRegisterError( scene->context, RF_ERROR_NOT_SUPPORTED, "rfCreateScene( target )" );
      return 0;
#endif
    default:
      rfRegisterError( scene->context, RF_ERROR_INVALID_TARGET, "rfCreateScene( target )" );
      return 0;
  }

  return root;
}


////


rfRoot rfResolveOriginf( rfScene *scene, rfHandle *handle, float *origin )
{
#if RF_USE_FLOAT
  return rfResolveOrigin( scene, handle, origin );
#else
  rff org[RF_AXIS_COUNT];
  copy3f2rff( org, origin );
  return rfResolveOrigin( scene, handle, org );
#endif
}

rfRoot rfResolveOrigind( rfScene *scene, rfHandle *handle, double *origin )
{
#if RF_USE_DOUBLE
  return rfResolveOrigin( scene, handle, origin );
#else
  rff org[RF_AXIS_COUNT];
  copy3d2rff( org, origin );
  return rfResolveOrigin( scene, handle, org );
#endif
}

