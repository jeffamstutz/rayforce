/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */


unsigned int ccSpaceCurveMap2D( int mapbits, unsigned int *point );
void ccSpaceCurveUnmap2D( int mapbits, unsigned int curveindex, unsigned int *point );
uint64_t ccSpaceCurveMap2D64( int mapbits, uint64_t *point );
void ccSpaceCurveUnmap2D64( int mapbits, uint64_t curveindex, uint64_t *point );


unsigned int ccSpaceCurveMap3D( int mapbits, unsigned int *point );
void ccSpaceCurveUnmap3D( int mapbits, unsigned int curveindex, unsigned int *point );
uint64_t ccSpaceCurveMap3D64( int mapbits, uint64_t *point );
void ccSpaceCurveUnmap3D64( int mapbits, uint64_t curveindex, uint64_t *point );

unsigned int ccSpaceCurveMap3D10Bits( unsigned int *point );
void ccSpaceCurveUnmap3D10Bits( unsigned int curveindex, unsigned int *point );
uint64_t ccSpaceCurveMap3D21Bits64( uint64_t *point );
void ccSpaceCurveUnmap3D21Bits64( uint64_t curveindex, uint64_t *point );


unsigned int ccSpaceCurveMap4D( int mapbits, unsigned int *point );
void ccSpaceCurveUnmap4D( int mapbits, unsigned int curveindex, unsigned int *point );
uint64_t ccSpaceCurveMap4D64( int mapbits, uint64_t *point );
void ccSpaceCurveUnmap4D64( int mapbits, uint64_t curveindex, uint64_t *point );


unsigned int ccSpaceCurveMap5D( int mapbits, unsigned int *point );
void ccSpaceCurveUnmap5D( int mapbits, unsigned int curveindex, unsigned int *point );
uint64_t ccSpaceCurveMap5D64( int mapbits, uint64_t *point );
void ccSpaceCurveUnmap5D64( int mapbits, uint64_t curveindex, uint64_t *point );

