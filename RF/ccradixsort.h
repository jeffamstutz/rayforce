/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

/*

Templates C style!

#include this whole file with the following definitions set:

#define RSORT_MAIN MyInlinedRadixSort
#define RSORT_RADIX myradixfunction
#define RSORT_TYPE foobar
#define RSORT_RADIXBITS (12)
#define RSORT_BIGGEST_FIRST (1)

*/


#ifndef RSORT_COPY
 #define RSORT_COPY(d,s) (*(d)=*(s))
 #define CC_RSORT_COPY
#endif


#ifdef RSORT_CONTEXT
 #define RSORT_CONTEXT_PARAM , RSORT_CONTEXT context
 #define RSORT_CONTEXT_PASS context,
 #define RSORT_CONTEXT_PASSLAST , context
#else
 #define RSORT_CONTEXT_PARAM
 #define RSORT_CONTEXT_PASS
 #define RSORT_CONTEXT_PASSLAST
#endif


#define CC_RSORT_RADIXBINCOUNT (1<<RSORT_RADIXBITS)


static RSORT_TYPE *RSORT_MAIN( RSORT_TYPE *src, RSORT_TYPE *tmp, int count, int sortbitcount RSORT_CONTEXT_PARAM )
{
  int index, bitindex, nextbitindex, radix, sum, maxsum;
  int sumtable[CC_RSORT_RADIXBINCOUNT];
  int prefix[CC_RSORT_RADIXBINCOUNT];
  RSORT_TYPE *dst;
  RSORT_TYPE *itemsrc;
  RSORT_TYPE *itemdst;
  RSORT_TYPE *swap;

  for( index = 0 ; index < CC_RSORT_RADIXBINCOUNT ; index++ )
    sumtable[index] = 0;
  itemsrc = src;
  for( index = 0 ; index < count ; index++ )
  {
    radix = RSORT_RADIX( RSORT_CONTEXT_PASS itemsrc, 0 );
    sumtable[radix]++;
    itemsrc++;
  }

  dst = tmp;
  for( bitindex = 0 ; bitindex < sortbitcount ; bitindex += RSORT_RADIXBITS )
  {
#if RSORT_BIGGEST_FIRST
    sum = 0;
    maxsum = 0;
    for( index = CC_RSORT_RADIXBINCOUNT-1 ; index >= 0 ; index-- )
    {
      prefix[index] = sum;
      sum += sumtable[index];
      if( sumtable[index] > maxsum )
        maxsum = sumtable[index];
    }
#else
    sum = 0;
    maxsum = 0;
    for( index = 0 ; index < CC_RSORT_RADIXBINCOUNT ; index++ )
    {
      prefix[index] = sum;
      sum += sumtable[index];
      if( sumtable[index] > maxsum )
        maxsum = sumtable[index];
    }
#endif

    if( maxsum < count )
    {
      for( index = 0 ; index < CC_RSORT_RADIXBINCOUNT ; index++ )
        sumtable[index] = 0;
      nextbitindex = bitindex + RSORT_RADIXBITS;
      if( nextbitindex < sortbitcount )
      {
        itemsrc = src;
        for( index = 0 ; index < count ; index++ )
        {
          radix = RSORT_RADIX( RSORT_CONTEXT_PASS itemsrc, bitindex );
          itemdst = &dst[ prefix[radix]++ ];
          radix = RSORT_RADIX( RSORT_CONTEXT_PASS itemsrc, nextbitindex );
          sumtable[radix]++;
          RSORT_COPY( itemdst, itemsrc );
          itemsrc++;
        }
      }
      else
      {
        itemsrc = src;
        for( index = 0 ; index < count ; index++ )
        {
          radix = RSORT_RADIX( RSORT_CONTEXT_PASS itemsrc, bitindex );
          itemdst = &dst[ prefix[radix]++ ];
          RSORT_COPY( itemdst, itemsrc );
          itemsrc++;
        }
      }
      swap = src;
      src = dst;
      dst = swap;
    }
  }

  return src;
}


#ifdef CC_RSORT_COPY
 #undef RSORT_COPY
 #undef CC_RSORT_COPY
#endif

#undef RSORT_CONTEXT_PARAM
#undef RSORT_CONTEXT_PASS
#undef RSORT_CONTEXT_PASSLAST

