/* *****************************************************************************
 *
 * Copyright (c) 2007-2013 Alexis Naveros.
 * Portions developed under contract to the SURVICE Engineering Company.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 *
 * *****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <float.h>
#include <math.h>

#include "cpuconfig.h"

#include "mm.h"
#include "mmdirectory.h"

#include "rf.h"
#include "rfdefs.h"
#include "rfdata.h"

#include "rfnuma.h"




void *rfNumaInit()
{
  if( !( mmcontext.numaflag ) )
    return 0;
  else
    return &mmcontext;
}


rfint rfNumaCpuCount( rfContext *context )
{
  return mmcontext.cpucount;
}

rfint rfNumaNodeCount( rfContext *context )
{
  if( !( mmcontext.numaflag ) )
    return 0;
  return mmcontext.nodecount;
}

size_t rfNumaSystemMemory( rfContext *context )
{
  if( mmcontext.sysmemory < 0 )
    return 0;
  if( ( sizeof(size_t) < sizeof(uint64_t) ) && ( mmcontext.sysmemory >= ((uint64_t)1)<<(8*sizeof(size_t)) ) )
    return ~0;
  return mmcontext.sysmemory;
}

int rfNumaQueryNode( rfContext *context, rfint numanode, rfint *nodecpucount, size_t *memory )
{
  if( !( mmcontext.numaflag ) )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "No NUMA Support" );
    return 0;
  }
  if( (unsigned)numanode >= mmcontext.nodecount )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "Bad NUMA Node" );
    return 0;
  }
  if( nodecpucount )
    *nodecpucount = mmcontext.nodecpucount[numanode];
  if( memory )
    *memory = mmcontext.nodesize[numanode];
  return 1;
}


int rfNumaGetNodeForCpu( rfContext *context, rfint cpuindex, rfint *numanode )
{
  if( !( mmcontext.numaflag ) )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "No NUMA Support" );
    return 0;
  }
  if( numanode )
    *numanode = mmcontext.cpunode[cpuindex];
  return 1;
}


int rfNumaBindNodeThread( rfContext *context, rfint numanode, rfint index )
{
  int cpuindex;

  if( !( mmcontext.numaflag ) )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "No NUMA Support" );
    return 0;
  }
  if( (unsigned)numanode >= mmcontext.nodecount )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "Bad NUMA Node" );
    return 0;
  }

  if( index < 0 )
    mmThreadBindToNode( numanode );
  else
  {
    for( cpuindex = 0 ; cpuindex < mmcontext.cpucount ; cpuindex++ )
    {
      if( mmcontext.cpunode[cpuindex] != numanode )
        continue;
      if( --index < 0 )
      {
        mmThreadBindToCpu( cpuindex );
        break;
      }
    }
    if( index >= 0 )
    {
      rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "NUMA Node Bad CPU Index" );
      return 0;
    }
  }

  return 1;
}


int rfNumaBindCpuThread( rfContext *context, rfint cpuindex )
{
  if( (unsigned)cpuindex >= mmcontext.cpucount )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "Bad CPU Index" );
    return 0;
  }
  mmThreadBindToCpu( cpuindex );
  return 1;
}


void *rfNumaAlloc( rfContext *context, rfint numanode, size_t size )
{
  if( !( mmcontext.numaflag ) )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "No NUMA Support" );
    return 0;
  }
  if( (unsigned)numanode >= mmcontext.nodecount )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "Bad NUMA Node" );
    return 0;
  }
  return mmNodeAlloc( numanode, size );
}


void rfNumaFree( rfContext *context, rfint numanode, void *mptr, size_t size )
{
  if( !( mmcontext.numaflag ) )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "No NUMA Support" );
    return;
  }
  if( (unsigned)numanode >= mmcontext.nodecount )
  {
    rfRegisterError( context, RF_ERROR_NUMA_BAD_OPERATION, "Bad NUMA Node" );
    return;
  }
  mmNodeFree( numanode, mptr, size );
  return;
}



